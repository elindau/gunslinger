package se.rhel.gunslinger;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "gunslinger";
		cfg.useGL20 = true;
		cfg.width = 800;
		cfg.height = 480;
		cfg.vSyncEnabled = true;
		cfg.forceExit = true;
		cfg.resizable = true;
		new LwjglApplication(new Gunslinger(), cfg);
	}
}
