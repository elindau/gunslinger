package se.rhel.gunslinger.network;

import java.io.IOException;

import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.ClientWorld;
import se.rhel.gunslinger.model.Levels;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.network.Network.ActorTween;
import se.rhel.gunslinger.network.Network.BossFiringCannon;
import se.rhel.gunslinger.network.Network.BossStateType;
import se.rhel.gunslinger.network.Network.EnemyAttack;
import se.rhel.gunslinger.network.Network.EnemyCredentials;
import se.rhel.gunslinger.network.Network.EnemyDead;
import se.rhel.gunslinger.network.Network.EnemyHPUpdate;
import se.rhel.gunslinger.network.Network.EnemySpawn;
import se.rhel.gunslinger.network.Network.EnemyUpdateState;
import se.rhel.gunslinger.network.Network.GameObjectSpawn;
import se.rhel.gunslinger.network.Network.GameReady;
import se.rhel.gunslinger.network.Network.JoinReply;
import se.rhel.gunslinger.network.Network.JoinReplyFlag;
import se.rhel.gunslinger.network.Network.JoinRequest;
import se.rhel.gunslinger.network.Network.PlayerCanSwitchWeapon;
import se.rhel.gunslinger.network.Network.PlayerHPUpdate;
import se.rhel.gunslinger.network.Network.PlayerJoinLeave;
import se.rhel.gunslinger.network.Network.PlayerPickup;
import se.rhel.gunslinger.network.Network.PlayerShoots;
import se.rhel.gunslinger.network.Network.PlayerState;
import se.rhel.gunslinger.network.Network.PlayerThrow;
import se.rhel.gunslinger.network.Network.ScreenChange;
import se.rhel.gunslinger.network.Network.StageCleared;
import se.rhel.gunslinger.network.Network.StageReset;
import se.rhel.gunslinger.network.Network.StageUpdate;
import se.rhel.gunslinger.observers.ClientCollisionListener;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class KryoClient {
	
	public Client 			mClient;
	private ClientWorld 	mWorldModel;
	public int 				mId;
	public String 			remoteIP;
	public String 			mName;
	
	private JoinReplyFlag 	mFlag = JoinReplyFlag.WAITING;
	private boolean 		mGameReady = false;
	private int 			mSlots = 0;
	
	private ActorTween 		mActorTween;
	private Levels			mStartLevel;
	
	public KryoClient(String name) {
		mWorldModel = new ClientWorld(this);
		mName = name;
		
		mClient = new Client();
		mClient.start();

		Network.register(mClient);
		
		mClient.addListener(new Listener() {
			public void connected(Connection connection) {
				handleConnect(connection);
			}
			
			public void received(Connection connection, Object object) {
				handleMessage(connection.getID(), object);
			}
			
			public void disconnected(Connection connection) {
				handleDisconnect(connection);
			}
			
		});
	}
	
	protected void handleDisconnect(Connection connection) {
		if (mGameReady) {
			mWorldModel.onDisconnect();
		} else {
			mClient.sendTCP(new PlayerJoinLeave(connection.getID(), mName, false));
		}
	}

	protected void handleConnect(Connection connection) {
		mId = connection.getID();
		remoteIP = connection.getRemoteAddressTCP().toString();
		JoinRequest request = new JoinRequest(mName);
		mClient.sendTCP(request);
		mClient.updateReturnTripTime();
	}

	protected void handleMessage(int id, Object message) {
		if (message instanceof PlayerJoinLeave) 			{ mWorldModel.getNetworkUpdate().playerJoinLeave((PlayerJoinLeave)message); } 
		else if (message instanceof PlayerState) 			{ mWorldModel.getNetworkUpdate().addUpdatePackage((PlayerState)message); }
		else if (message instanceof PlayerShoots) 			{ mWorldModel.getNetworkUpdate().addUpdatePackage((PlayerShoots)message); }
		else if (message instanceof PlayerPickup) 			{ mWorldModel.getNetworkUpdate().addUpdatePackage((PlayerPickup)message); }
		else if (message instanceof BossFiringCannon)		{ mWorldModel.getNetworkUpdate().addUpdatePackage((BossFiringCannon)message); }
		else if (message instanceof EnemyUpdateState) 		{ mWorldModel.getNetworkUpdate().addUpdatePackage((EnemyUpdateState)message); }
		else if (message instanceof EnemyAttack) 			{ mWorldModel.getNetworkUpdate().addUpdatePackage((EnemyAttack)message); }
		else if (message instanceof GameObjectSpawn) 		{ mWorldModel.getNetworkUpdate().addUpdatePackage((GameObjectSpawn)message); }
		else if (message instanceof EnemyHPUpdate)			{ mWorldModel.getNetworkUpdate().addUpdatePackage((EnemyHPUpdate)message); }
		else if (message instanceof PlayerHPUpdate)			{ mWorldModel.getNetworkUpdate().addUpdatePackage((PlayerHPUpdate)message); }
		else if (message instanceof PlayerCanSwitchWeapon) 	{ mWorldModel.getNetworkUpdate().addUpdatePackage((PlayerCanSwitchWeapon)message); }
		else if (message instanceof PlayerThrow)			{ mWorldModel.getNetworkUpdate().addUpdatePackage((PlayerThrow)message); }
		else if (message instanceof EnemyDead) 				{ mWorldModel.getNetworkUpdate().addUpdatePackage((EnemyDead)message); }
		else if (message instanceof StageReset)				{ mWorldModel.getNetworkUpdate().addUpdatePackage((StageReset)message); }
		else if (message instanceof BossStateType)			{ mWorldModel.getNetworkUpdate().addUpdatePackage((BossStateType)message); }
		else if (message instanceof StageUpdate) 			{ stageUpdate(message); }
		else if (message instanceof Levels)					{ levelSet(message); }
		else if (message instanceof ActorTween)				{ actorTweened(message); }
		else if (message instanceof StageCleared) 			{ stageClearedAtServer(message); }
		else if (message instanceof ScreenChange)			{ changeScreen(message); }
		else if (message instanceof EnemySpawn) 			{ enemySpawn(message); }
		else if (message instanceof JoinReply) 				{ joinReply(message); }
		else if (message instanceof GameReady) 				{ gameReady(message); }
	}

	private void stageClearedAtServer(Object message) {
		mWorldModel.levelManager().setStageCleared(true);
	}
	
	private void actorTweened(Object message) {
		mActorTween = (ActorTween) message;
	}
	
	public ActorTween getTween() {
		return mActorTween;
	}
	
	private void changeScreen(Object message) {
		// Do something
		mWorldModel.setScreen(((ScreenChange)message).toScreen);
	}
	
	public void reset() {
		mStartLevel = null;
		mWorldModel.reset();
	}
	
	private void levelSet(Object message) {
		mWorldModel.clear((Levels) message);
		mStartLevel = (Levels) message;
	}
	
	public Levels startLevel() {
		return mStartLevel;
	}

	private void stageUpdate(Object message) {
		mWorldModel.stageUpdate((StageUpdate)message);
	}

	private void enemySpawn(Object message) {
		for (EnemyCredentials cred : ((EnemySpawn)message).mEnemies) {
			mWorldModel.getNetworkUpdate().addUpdatePackage(cred);
		}
	}

	private void gameReady(Object message) {
		mGameReady = true;
	}

	private void joinReply(Object message) {
		JoinReply reply = (JoinReply)message;
		
		mSlots = reply.mServerSlots;
		mFlag = reply.mFlag;
		
		if (reply.mFlag == JoinReplyFlag.ACCEPTED) {
			mWorldModel.onConnect(mName);
		}
	}

	public void connect(String host) throws IOException {
		mClient.connect(5000, host, Network.port, Network.portUdp);
	}
	
	public void connectLocal() throws IOException {
		connect("localhost");
	}
	
	public void ping() {
		if (mClient.isConnected()) {
			mClient.updateReturnTripTime();
		}
	}

	public ClientWorld getWorldModel() {
		return mWorldModel;
	}
	
	public void shutdown() {
		mClient.stop();
		mClient.close();
	}

	public void sendMessageUDP(Object message) {
		if(mClient.isConnected()) {
			mClient.sendUDP(message);
		}
	}
	
	public JoinReplyFlag getFlag() {
		return mFlag;
	}
	
	public boolean isClientConnected() {
		return mFlag == JoinReplyFlag.ACCEPTED;
	}
	
	public boolean isGameReady() {
		return mGameReady;
	}

	public int getServerSlots() {
		return mSlots;
	}


}
