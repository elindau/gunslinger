package se.rhel.gunslinger.network;

import java.io.IOException;
import java.util.ArrayList;

import se.rhel.gunslinger.model.ServerWorld;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.network.Network.GameObjectSpawn;
import se.rhel.gunslinger.network.Network.GameReady;
import se.rhel.gunslinger.network.Network.JoinReply;
import se.rhel.gunslinger.network.Network.JoinReplyFlag;
import se.rhel.gunslinger.network.Network.JoinRequest;
import se.rhel.gunslinger.network.Network.PlayerJoinLeave;
import se.rhel.gunslinger.network.Network.PlayerPickup;
import se.rhel.gunslinger.network.Network.PlayerShoots;
import se.rhel.gunslinger.network.Network.PlayerState;
import se.rhel.gunslinger.network.Network.PlayerThrow;
import se.rhel.gunslinger.network.Network.PlayerWeaponSwitch;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;

public class KryoServer  {
	
	private Server mServer;
	private ServerWorld mWorld;
	private int mSlots;
	private boolean mGameReady = false;
	private ArrayList<GunslingerConnection> mConnections = new ArrayList<GunslingerConnection>();

	public KryoServer(int slots) throws IOException {
		Log.set(Log.LEVEL_INFO);
		mSlots = slots;
		
		mServer = new Server() {
			protected Connection newConnection() {
				return new GunslingerConnection();
			}
		};

		mWorld = new ServerWorld(this);
		Network.register(mServer);
		
		mServer.addListener(new Listener() {

			public void received(Connection c, Object message) {
				GunslingerConnection connection = (GunslingerConnection) c;
				
				if (message instanceof JoinRequest) 			{ JoinRequest(connection, message);}
				else if (message instanceof PlayerState) 		{ PlayerState(connection, message);} 
				else if (message instanceof PlayerShoots) 		{ PlayerShoots(connection, message);} 
				else if (message instanceof PlayerThrow) 		{ PlayerThrow(connection, message);} 
				else if (message instanceof PlayerWeaponSwitch) { PlayerSwitchWeapon(connection, message);}
				else if (message instanceof PlayerPickup)		{ PlayerProbablyBoughtGunInShopWhileNotBeingHost(connection, message);}
			}

			public void disconnected (Connection c) {
				GunslingerConnection connection = (GunslingerConnection)c;
				if (connection.name != null) {
					PlayerJoinLeave reply  = new PlayerJoinLeave(connection.getID(), connection.name, false);
					mServer.sendToAllExceptTCP(connection.getID(), reply);
					mWorld.removePlayer(reply);
					mConnections.remove(connection);
				}
			}
		});
		
		mServer.bind(Network.port,Network.portUdp);
		mServer.start();
	}
	
	private void PlayerProbablyBoughtGunInShopWhileNotBeingHost(GunslingerConnection connection, Object message) {
		PlayerPickup pp = (PlayerPickup)message;
		// Howmany f�r agera kostnad just i det h�r fallet
		mWorld.playerBoughtGun(pp.wepType, pp.howMany, pp.mPlayerID);
	}
	
	protected void PlayerSwitchWeapon(GunslingerConnection connection, Object message) {
		mWorld.PlayerSwappedWeapon((PlayerWeaponSwitch) message);
	}

	private void PlayerThrow(GunslingerConnection connection, Object message) {
		mWorld.playerThrow((PlayerThrow)message);
		mServer.sendToAllExceptTCP(connection.getID(), message);
	}

	public void sendMessage(Object message) {
		mServer.sendToAllTCP(message);
	}
	
	public void update(float delta) {
		mWorld.update(delta);
	}
	
	static class GunslingerConnection extends Connection {
		public String name;
	}
	
	public boolean isGameReady() {
		return mGameReady;
	}
	
	public int getSlots() {
		return mSlots;
	}
	
	public void shutdown() {
		System.out.println("SERVER Z� CLOSED!");
		mServer.stop();
		mServer.close();
	}
	
	private void GameReady() {
		mGameReady = true;
		mServer.sendToAllTCP(new GameReady());
	}
	
	private void JoinRequest(GunslingerConnection connection, Object message) {
		JoinRequest req = ((JoinRequest)message);
		
		//Finns det plats p� servern
		if (mConnections.size() < mSlots) {
			if (connection.name != null) return;
			if (req.mName == null) return;
			if (req.mName.trim().length() == 0) return;
			connection.name = req.mName;
			
			//L�gg till uppkopplingen till servern
			mConnections.add(connection);
			
			//Ber�tta f�r spelaren att han accepterats
			connection.sendTCP(new JoinReply(JoinReplyFlag.ACCEPTED, mSlots));
			
			//Ber�tta f�r de andra p� servern att en ny har anl�nt
			PlayerJoinLeave newPlayer = new PlayerJoinLeave(connection.getID(), connection.name, true);
			mServer.sendToAllExceptTCP(connection.getID(), newPlayer);
			mWorld.addPlayer(newPlayer);
			
			//Ber�tta f�r den nya vart alla �r
			for(Connection con : mServer.getConnections()){
				GunslingerConnection gCon = (GunslingerConnection)con;
				if(gCon.getID() != connection.getID() && gCon.name != null){
					Player player = mWorld.getPlayerById(gCon.getID());
					PlayerJoinLeave oldPlayer  = new PlayerJoinLeave(gCon.getID(), gCon.name, true);
					connection.sendTCP(oldPlayer); 
					connection.sendTCP(player.getPlayerState());
				}
			}
			
			//Skicka worldstate
			// connection.sendTCP(new EnemySpawn(mWorld.getEnemies()));
			
			//Skicka med alla pickupable objekt
			//for (GameObjectSpawn gos : mWorld.getPickupAbleObjects()) {
			//	connection.sendTCP(gos);
			//}
			
			//�r servern full, skicka ut att spelet startar
			if (mConnections.size() == mSlots)
				GameReady();			
			
		} else { //Server full
			connection.sendTCP(new JoinReply(JoinReplyFlag.DENIED, mSlots));
			connection.close();
		}
	}
	
	public void sendGameObjects() {
		for (GameObjectSpawn gos : mWorld.getGameObjects()) {
			sendMessage(gos);
		}
	}

	private void PlayerState(GunslingerConnection connection, Object message) {
		PlayerState msg = (PlayerState) message;
		mWorld.playerMoved(msg);
		mServer.sendToAllExceptUDP(connection.getID(), msg);
	}
	
	private void PlayerShoots(GunslingerConnection connection, Object message) {
		PlayerShoots msg = (PlayerShoots)message;
		mServer.sendToAllExceptTCP(connection.getID(), msg);
		mWorld.playerShoots((PlayerShoots)message);
	}
	
	public void sendMessageUDP(Object message) {
		mServer.sendToAllUDP(message);
	}
	
	public void sendMessageTo(int id, Object message) {
		mServer.sendToUDP(id, message);
	}
	
	public ServerWorld world() {
		return mWorld;
	}

	public void reset() {
		mWorld.reset();
	}
}
