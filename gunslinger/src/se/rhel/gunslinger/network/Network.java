package se.rhel.gunslinger.network;

import java.util.ArrayList;

import se.rhel.gunslinger.model.Levels;
import se.rhel.gunslinger.model.UpdatePackage;
import se.rhel.gunslinger.model.entities.BossAI.BossState;
import se.rhel.gunslinger.model.entities.Enemy.EnemyState;
import se.rhel.gunslinger.model.entities.Enemy.EnemyType;
import se.rhel.gunslinger.model.entities.GameObject;
import se.rhel.gunslinger.model.entities.GameObject.Type;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;
import se.rhel.gunslinger.screens.Screens;

import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

public class Network {
	
	static public final int port = 6464;
	static public final int portUdp = 6466;
	
	/**
	 * Registrera vilka klasser som ska skickas �ver n�tverk
	 * @param endpoint
	 */
	static public void register(EndPoint endpoint) {
		Kryo kryo = endpoint.getKryo();
		kryo.register(PlayerJoinLeave.class);
		kryo.register(PlayerState.class);
		kryo.register(Vector2.class);
		kryo.register(JoinRequest.class);
		kryo.register(JoinReply.class);
		kryo.register(JoinReplyFlag.class);
		kryo.register(GameReady.class);
		kryo.register(Player.State.class);
		kryo.register(PlayerShoots.class);
		kryo.register(PlayerPickup.class);
		kryo.register(GameObject.Type.class);
		kryo.register(EnemySpawn.class);
		kryo.register(EnemyCredentials.class);
		kryo.register(EnemyUpdateState.class);
		kryo.register(ArrayList.class);
		kryo.register(Object[].class);
		kryo.register(EnemyState.class);
		kryo.register(EnemyAttack.class);
		kryo.register(GameObjectSpawn.class);
		kryo.register(EnemyDead.class);
		kryo.register(StageUpdate.class);
		kryo.register(EnemyHPUpdate.class);
		kryo.register(PlayerHPUpdate.class);
		kryo.register(PlayerThrow.class);
		kryo.register(Levels.class);
		kryo.register(Weapons.class);
		kryo.register(PlayerWeaponSwitch.class);
		kryo.register(PlayerCanSwitchWeapon.class);
		kryo.register(ActorTween.class);
		kryo.register(float[].class);
		kryo.register(ScreenChange.class);
		kryo.register(Screens.class);
		kryo.register(StageCleared.class);
		kryo.register(EnemyType.class);
		kryo.register(StageReset.class);
		kryo.register(BossFiringCannon.class);
		kryo.register(BossStateType.class);
		kryo.register(BossState.class);
		kryo.register(UpdatePackage.class);
	}
	
	static public class BossFiringCannon extends UpdatePackage {
		public int mBossID;
		public Vector2 mHitPos;
		
		public BossFiringCannon() {}
		public BossFiringCannon(int id, Vector2 hitpos) {
			mBossID = id;
			mHitPos = hitpos;
		}
	}
	
	/**
	 * Klass f�r f�rfarande Join/Leave av klient
	 */
	static public class PlayerJoinLeave extends UpdatePackage {
		public int mPlayerID;
		public String mName;
		public boolean mHasJoined;

		public PlayerJoinLeave() {}
		public PlayerJoinLeave(int playerId, String name, boolean hasJoined) {
			mPlayerID = playerId;
			mName = name;
			mHasJoined = hasJoined;
		}
	}
	
	/**
	 * N�r hela stagen ska resetas, inklusive spelare
	 */
	static public class StageReset extends UpdatePackage {
		public StageReset() {}
	}
	
	/**
	 * N�r stagen blir cleared p� servern
	 */
	static public class StageCleared {
		public StageCleared() {}
	}
	
	/**
	 * N�r det ska bytas screen p� klienten
	 */
	static public class ScreenChange {
		public Screens toScreen;
		public ScreenChange() {}
		public ScreenChange(Screens toScreen) {
			this.toScreen = toScreen;
		}
	}
	
	/**
	 * Aktor tweening
	 */
	static public class ActorTween {
		
		public float targetX;
		public float targetY;
		public float[] wps;
		public boolean left;
		
		public ActorTween() {
		}
		
		public ActorTween(float targetX, float targetY, boolean left, float... wps) {
			this.targetX = targetX;
			this.targetY = targetY;
			this.wps = wps;
			this.left = left;
		}
	}

	/**
	 * Klass f�r hantering av r�relse
	 */
	static public class PlayerState extends UpdatePackage {
		public int mPlayerID;
		public Vector2 mPosition;
		public Vector2 mDirection;
		public Vector2 mGotoPos;
		public boolean mIsFacingLeft;

		public PlayerState() {}
		public PlayerState(int playerId, Vector2 position, Vector2 direction, Vector2 gotoPos, boolean isFacingLeft) {
			mPlayerID = playerId;
			mPosition = position;
			mDirection = direction;
			mGotoPos = gotoPos;
			mIsFacingLeft = isFacingLeft;
		}
	}
	
	static public class PlayerWeaponSwitch {
		public int mPlayerID;
		
		public PlayerWeaponSwitch() {}
		public PlayerWeaponSwitch(int id) {
			mPlayerID = id;
		}
	}
	
	static public class PlayerCanSwitchWeapon extends UpdatePackage {
		public boolean mCanSwitchWeapon;
		public Weapons mWeaponType;
	
		public PlayerCanSwitchWeapon() {}
		public PlayerCanSwitchWeapon(boolean canSwitchWeapon, Weapons type) {
			mCanSwitchWeapon = canSwitchWeapon;
			mWeaponType = type;
		}
	}
	
	static public class PlayerThrow extends UpdatePackage {
		public int mID;
		public Vector2 mThrowToPos;
		
		public PlayerThrow() {}
		public PlayerThrow(int id, Vector2 pos) {
			mID = id; 
			mThrowToPos = pos;
		}
	}
	
	static public class BossStateType extends UpdatePackage {
		public BossState mState;
		
		public BossStateType() {}
		public BossStateType(BossState bs) {
			mState = bs;
		}
	}
	
	static public class EnemySpawn {
		public ArrayList<EnemyCredentials> mEnemies;
		
		public EnemySpawn() {}
		public EnemySpawn(ArrayList<EnemyCredentials> ec) {
			mEnemies = ec;
		}
	}
	
	static public class EnemyCredentials extends UpdatePackage {
		public Vector2 mPosition;
		public EnemyType mType;
		public int mID;
		
		public EnemyCredentials() {}
		public EnemyCredentials(int id, Vector2 position, EnemyType type) {
			mID = id;
			mPosition = position;
			mType = type;
		}
	}
	
	static public class EnemyUpdateState extends UpdatePackage {
		public int mEnemyID;
		public Vector2 mPosition;
		public boolean mIsFacingLeft;
		public EnemyState mState;
		public boolean mIsAttacking;
		public Vector2 mDirection;
		
		public EnemyUpdateState() {}
		public EnemyUpdateState(int id, Vector2 position, EnemyState state, boolean isAttacking, Vector2 direction) {
			mEnemyID = id;
			mPosition = position;
			mState = state;
			mIsAttacking = isAttacking;
			mDirection = direction;
		}
	}
	
	static public class EnemyAttack extends UpdatePackage {
		public int mEnemyID;
		public Vector2 mDirection;
		public boolean mIsFacingLeft;
		
		public EnemyAttack() {}
		public EnemyAttack(int id, Vector2 direction, boolean isFacingLeft) {
			mEnemyID = id;
			mDirection = direction;
			mIsFacingLeft = isFacingLeft;
		}
	}
	
	static public class EnemyDead extends UpdatePackage {
		public int mEnemyID;
		
		public EnemyDead() {}
		public EnemyDead(int id) {
			mEnemyID = id;
		}
	}
	
	/**
	 * Token f�r klientuppkoppling mot server
	 */
	public enum JoinReplyFlag {
		ACCEPTED,
		DENIED,
		WAITING
	}
	
	/**
	 * Klass som klient skickar till server
	 * vid uppkoppling.
	 */
	static public class JoinRequest {
		public String mName;
		
		public JoinRequest() {}
		public JoinRequest(String name) {
			mName = name;
		}
	}
	
	/**
	 * Klass som server skickar till klient
	 * med svar vid uppkoppling.
	 */
	static public class JoinReply {
		public JoinReplyFlag mFlag;
		public int mServerSlots;
		
		public JoinReply() {}
		public JoinReply(JoinReplyFlag flag, int slots) {
			mFlag = flag;
			mServerSlots = slots;
		}
	}
	
	static public class PlayerShoots extends UpdatePackage {
		public int mPlayerID;
		public Vector2 mDirection;

		public PlayerShoots() {}
		public PlayerShoots(int playerID, Vector2 direction) {
			mPlayerID = playerID;
			mDirection = direction;
		}
	}
	
	static public class PlayerPickup extends UpdatePackage {
		public int mPlayerID;
		public GameObject.Type mObjType;
		public Weapons wepType;
		public int mObjID;
		public boolean isGameState = false;
		public int removeAmount = 0;
		public int howMany = 0;
		
		public PlayerPickup() {}
		public PlayerPickup(int howMany, int playerID, int objID, Type objType, Weapons wepType, boolean gameState) {
			mPlayerID = playerID;
			mObjID = objID;
			mObjType = objType;
			isGameState = gameState;
			this.howMany = howMany;
			this.wepType = wepType;
		}
		
		// Kan anv�ndas n�r item ska tas bort ocks�
		public PlayerPickup(int playerID, int objID, Type objType, boolean gameState, int removeAmount) {
			mPlayerID = playerID;
			mObjID = objID;
			mObjType = objType;
			isGameState = gameState;
			this.removeAmount = removeAmount;
		}
		
		public boolean isRemove() {
			return removeAmount > 0 ? true : false;
		}
	}
	
	static public class GameObjectSpawn extends UpdatePackage {
		public int mId;
		public GameObject.Type mObjType;
		public Vector2 mPosition;
		public Weapons mWeaponType;
		
		public GameObjectSpawn() {}
		public GameObjectSpawn(int id, Vector2 position, GameObject.Type type) {
			mId = id;
			mPosition = position;
			mObjType = type;
		}
		public GameObjectSpawn(int id, Vector2 position, GameObject.Type type, Weapons weapontype) {
			mId = id;
			mPosition = position;
			mObjType = type;
			mWeaponType = weapontype;
		}
	}
	
	static public class StageUpdate {
		public int mStageNumber;
		
		public StageUpdate() {}
		public StageUpdate(int stageNumber) {
			mStageNumber = stageNumber;
		}
	}
	
	static public class EnemyHPUpdate extends UpdatePackage {
		public int mEnemyID;
		public int mDamage;
		public int mShooterID;
		public boolean mIsCrit;
		
		public EnemyHPUpdate() {}
		public EnemyHPUpdate(int enemyID, int damage, boolean crit, int shooterID) {
			mEnemyID = enemyID;
			mDamage = damage;
			mShooterID = shooterID;
			mIsCrit = crit;
		}
	}
	
	static public class PlayerHPUpdate extends UpdatePackage {
		public int mPlayerID;
		public int mAmount;
		public boolean mIsCrit;
		public boolean mIsDamage;
		
		public PlayerHPUpdate() {}
		public PlayerHPUpdate(int playerID, int amount, boolean isCrit, boolean isDamage) {
			mPlayerID = playerID;
			mAmount = amount;
			mIsCrit = isCrit;
			mIsDamage = isDamage;
		}
	}
	
	static public class GameReady {}
}
