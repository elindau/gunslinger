package se.rhel.gunslinger.sceens.actors;

import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.moribitotech.mtx.AbstractActor;

/**
 * Representerar vapnen man kan k�pa i stagen
 * @author Emil
 *
 */
public class WeaponActor extends AbstractActor {
	
	public Weapons 	TYPE;
	public int 		ID;
	
	public WeaponActor(TextureRegion textureRegion, boolean isTextureRegionActive,
			float posX, float posY, float width, float height) {
		super(textureRegion, isTextureRegionActive, posX, posY, width, height);
	}
	
	public WeaponActor(float posX, float posY, float width, float height, Weapons type, int id) {
		super(posX, posY, width, height);
		
		TYPE = type;
		ID = id;
	}
}
