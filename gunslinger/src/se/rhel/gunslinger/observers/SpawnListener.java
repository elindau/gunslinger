package se.rhel.gunslinger.observers;

public interface SpawnListener extends IListener {
	public void enemySpawned();
}
