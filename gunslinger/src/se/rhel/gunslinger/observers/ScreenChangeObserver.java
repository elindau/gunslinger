package se.rhel.gunslinger.observers;

import se.rhel.gunslinger.screens.Screens;

import com.badlogic.gdx.utils.Array;

public class ScreenChangeObserver {

	Array<ScreenChangeListener> scListener = new Array<ScreenChangeListener>();
	
	public void addListener(ScreenChangeListener toAdd) {
		scListener.add(toAdd);
	}
	
	public void changeScreen() {
		for(ScreenChangeListener scl : scListener) {
			scl.screenChanged();
		}
	}
	
	public void changeScreen(Screens toScreen) {
		for(ScreenChangeListener scl : scListener) {
			scl.screenChange(toScreen);
		}
	}
}
