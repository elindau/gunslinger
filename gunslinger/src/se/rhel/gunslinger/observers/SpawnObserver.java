package se.rhel.gunslinger.observers;

import com.badlogic.gdx.utils.Array;

public class SpawnObserver {
	Array<SpawnListener> spListener = new Array<SpawnListener>();
	
	public void addListener(IListener toAdd) {
		spListener.add((SpawnListener)toAdd);
	}

	public void enemySpawn() {
		for(SpawnListener spl : spListener) {
			spl.enemySpawned();
		}
	}
}
