package se.rhel.gunslinger.observers;

import se.rhel.gunslinger.model.HUD.HUDText;

public interface HUDTextListener extends IListener {
	public void addHudText(HUDText obj);
}
