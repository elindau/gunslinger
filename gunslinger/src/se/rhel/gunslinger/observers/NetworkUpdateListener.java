package se.rhel.gunslinger.observers;

import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

public interface NetworkUpdateListener {
	public void playerDamaged(int id, int damage, boolean isCrit);
	public void playerHealed(int id, int amount);
	public void enemyDamaged(int id, int damage, boolean isCrit, int playerID);
	public void playerWeaponCollision(int id, boolean swapAvailable, Weapons weapons);
}
