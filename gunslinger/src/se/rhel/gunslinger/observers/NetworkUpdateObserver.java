package se.rhel.gunslinger.observers;

import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.utils.Array;

public class NetworkUpdateObserver {
	Array<NetworkUpdateListener> NUListener = new Array<NetworkUpdateListener>();
	
	public void addListener(NetworkUpdateListener toAdd) {
		NUListener.add((NetworkUpdateListener)toAdd);
	}

	public void playerDamaged(int id, int damage, boolean isCrit) {
		for(NetworkUpdateListener nul : NUListener) {
			nul.playerDamaged(id, damage, isCrit);
		}
	}
	
	public void enemyDamaged(int id, int damage, boolean isCrit, int playerID) {
		for(NetworkUpdateListener nul : NUListener) {
			nul.enemyDamaged(id, damage, isCrit, playerID);
		}
	}
	
	public void playerHealed(int id, int amount) {
		for(NetworkUpdateListener nul : NUListener) {
			nul.playerHealed(id, amount);
		}
	}
	
	public void playerWeaponSwap(int id, boolean swapAvailable, Weapons weapons) {
		for(NetworkUpdateListener nul : NUListener) {
			nul.playerWeaponCollision(id, swapAvailable, weapons);
		}
	}
}
