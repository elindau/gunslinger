package se.rhel.gunslinger.observers;

public interface Observer {

	public void addListener(IListener toAdd);
	public void event();
	
}
