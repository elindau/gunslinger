package se.rhel.gunslinger.observers;

import se.rhel.gunslinger.model.HUD.HUDText;

import com.badlogic.gdx.utils.Array;

public class HUDTextObserver {
	Array<HUDTextListener> pListener = new Array<HUDTextListener>();
	
	public void addListener(IListener toAdd) {
		pListener.add((HUDTextListener)toAdd);
	}

	public void event(HUDText obj) {
		for(HUDTextListener pl : pListener) {
			pl.addHudText(obj);
		}
	}

}
