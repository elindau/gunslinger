package se.rhel.gunslinger.observers;

import se.rhel.gunslinger.model.Bourbon;
import se.rhel.gunslinger.model.entities.Boss;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.GameObject;
import se.rhel.gunslinger.model.entities.GameObject.State;
import se.rhel.gunslinger.model.entities.PickupAbleObject;
import se.rhel.gunslinger.model.entities.PickupAbleWeapon;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.entities.Sign;
import se.rhel.gunslinger.model.weapons.Bullet;
import se.rhel.gunslinger.model.weapons.IMeleeWeapon;
import se.rhel.gunslinger.model.weapons.IWeapon;
import se.rhel.gunslinger.model.weapons.IWeapon.Type;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

public class CollisionListener implements ContactListener {

	private Fixture mFixA;
	private Fixture mFixB;
	private Body 	mBodyA;
	private Body	mBodyB;
	
	private NetworkUpdateObserver nuObs;
	
	public CollisionListener(NetworkUpdateObserver networkUpdateObs) {
		nuObs = networkUpdateObs;
	}

	@Override
	public void beginContact(Contact contact) {
		// System.out.println("CONTACT: Begin Contact");
		
		mFixA = contact.getFixtureA();
		mFixB = contact.getFixtureB();

		mBodyA = mFixA.getBody();
		mBodyB = mFixB.getBody();
		
		collisionHandlerBegin(mBodyA, mBodyB);
	}
	
	private void collisionHandlerBegin(Body bodyA, Body bodyB) {
		// Check mot kollisionskropp
		if(bodyA != null && bodyB != null) {

			// Objekt kan bara kollidera med players och beh�ver d�f�r inte kollas mot fiender 
			if(bodyA.getUserData() instanceof GameObject) {
				
					if (bodyB.getUserData() instanceof Player) {
						switch(((GameObject)bodyA.getUserData()).getType()) {
						case WEAPON:
							Player p = (Player)bodyB.getUserData();
							PickupAbleWeapon paw = (PickupAbleWeapon)bodyA.getUserData();
							
							p.setAvailableWeaponToSwap(paw);
							if (!p.getCanSwapWeapon()) {
								p.setCanSwapWeapon(true);
								nuObs.playerWeaponSwap(p.getID(), true, paw.getWeaponType());
							}
							break;
						case BOURBON:
						case COIN:
							((PickupAbleObject)bodyA.getUserData()).pickUp(((Player)bodyB.getUserData()).getID());
							break;
							
						case SIGN:
							// Skylten �r aktiverad
							((Sign)bodyA.getUserData()).setState(State.ACTIVE);
							break;
					}
				}
			}
			
			if(bodyB.getUserData() instanceof GameObject) {

				// Objekt kan bara kollidera med fiender och beh�ver d�f�r inte kollas 
				if(bodyB.getUserData() instanceof GameObject) {
					
						if (bodyA.getUserData() instanceof Player) {
							switch(((GameObject)bodyB.getUserData()).getType()) {
							case WEAPON:
								Player p = (Player)bodyA.getUserData();
								PickupAbleWeapon paw = (PickupAbleWeapon)bodyB.getUserData();
								p.setAvailableWeaponToSwap(paw);	
								
								if (!p.getCanSwapWeapon()) {
									p.setCanSwapWeapon(true);
									nuObs.playerWeaponSwap(p.getID(), true, paw.getWeaponType());
								}
								break;
							case BOURBON:
							case COIN:
								((PickupAbleObject)bodyB.getUserData()).pickUp(((Player)bodyA.getUserData()).getID());
								break;
								
							case SIGN:
								// Skylten �r aktiverad
								((Sign)bodyB.getUserData()).setState(State.ACTIVE);
								break;
						}
					}
				}
			}
			
			// Kollision med fiende
			if(bodyA.getUserData() instanceof Enemy) {
				if(bodyB.getUserData() instanceof Player) {
					// S�tt fiende till lite idle s� han inte forts�tter springa in i spelare
					// och kan attackera
					Player p = (Player)bodyB.getUserData();
					((Enemy)bodyA.getUserData()).playerContact(p.isAlive() ? true : false);
					
					if (bodyA.getUserData() instanceof Boss) {
						Boss b = (Boss)bodyA.getUserData();
						if (b.isRushing() && p.getPosition().y >= b.getPosition().y - 1f && p.getPosition().y <= b.getPosition().y + 1f) {
							nuObs.playerDamaged(p.getID(), b.getRushDmg(), false);
						}
					}
				}
				// Kolliderar fiende med en annan fiende
				if(bodyB.getUserData() instanceof Enemy) {
					((Enemy)bodyA.getUserData()).enemyContact(true);
				}
			}
			if(bodyB.getUserData() instanceof Enemy) {
				if(bodyA.getUserData() instanceof Player) {
					Player p = (Player)bodyA.getUserData();			
					((Enemy)bodyB.getUserData()).playerContact(p.isAlive() ? true : false);
					
					if (bodyB.getUserData() instanceof Boss) {
						Boss b = (Boss)bodyB.getUserData();

						if (b.isRushing() && p.getPosition().y >= b.getPosition().y - 1f && p.getPosition().y <= b.getPosition().y + 1f) {
							nuObs.playerDamaged(p.getID(), b.getRushDmg(), false);
						}
					}
				}
				if(bodyA.getUserData() instanceof Enemy) {
					// ((Enemy)bodyB.getUserData()).enemyContact(true);
				}
			}
			
			// Kollision med melee-vapen
			if(bodyA.getUserData() instanceof IWeapon) {
				if(((IWeapon)bodyA.getUserData()).getType() == Type.MELEE) {
					if(bodyB.getUserData() instanceof Player) {
						nuObs.playerDamaged(((Player)bodyB.getUserData()).getID(), ((IMeleeWeapon)bodyA.getUserData()).getHitDamage(), ((IMeleeWeapon)bodyA.getUserData()).isCrit());
					}
				}
			}
			if(bodyB.getUserData() instanceof IWeapon) {
				if(((IWeapon)bodyB.getUserData()).getType() == Type.MELEE) {
					if(bodyA.getUserData() instanceof Player) {
						nuObs.playerDamaged(((Player)bodyA.getUserData()).getID(), ((IMeleeWeapon)bodyB.getUserData()).getHitDamage(), ((IMeleeWeapon)bodyB.getUserData()).isCrit());
					}
				}
			}
			
			// D�da kulor om de tr�ffar n�got
			if(bodyA.getUserData() instanceof Bullet) {
				// Kolla kollision mot fiende
				if(bodyB.getUserData() instanceof Enemy) {
					BulletEnemyCollision((Bullet)bodyA.getUserData(), (Enemy)bodyB.getUserData());
				}
				else if(bodyB.getUserData() instanceof Player) {
					BulletPlayerCollision((Bullet)bodyA.getUserData(), (Player)bodyB.getUserData());
				}
				
				// Oavsett vad kulan kolliderar med ska den d�das
				((Bullet)bodyA.getUserData()).setState(State.DEAD);
			}
			else if(bodyB.getUserData() instanceof Bullet) {
					if(bodyA.getUserData() instanceof Enemy) {
						BulletEnemyCollision((Bullet)bodyB.getUserData(), (Enemy)bodyA.getUserData());
					}
					else if(bodyA.getUserData() instanceof Player) {
						BulletPlayerCollision((Bullet)bodyB.getUserData(), (Player)bodyA.getUserData());
					}
					
				((Bullet)bodyB.getUserData()).setState(State.DEAD);
			}
			
			// Kollision med kastade Bourbon-flaskor
			if(bodyA.getUserData() instanceof Bourbon) {
				if(bodyB.getUserData() instanceof Player) {
					PlayerBourbonCollision((Bourbon)bodyA.getUserData(), 
							(Player)bodyB.getUserData());
				}
					
			}
			if(bodyB.getUserData() instanceof Bourbon) {
				if(bodyA.getUserData() instanceof Player) {
					PlayerBourbonCollision((Bourbon)bodyB.getUserData(), 
							(Player)bodyA.getUserData());
				}	
			}
		}
	}

	@Override
	public void endContact(Contact contact) {
		// System.out.println("CONTACT: End Contact");
		
		mFixA = contact.getFixtureA();
		mFixB = contact.getFixtureB();
		
		mBodyA = mFixA.getBody();
		mBodyB = mFixB.getBody();
		
		collisionHandlerEnd(mBodyA, mBodyB);
		//collisionHandlerEnd();
	}
	
	private void collisionHandlerEnd(Body bodyA, Body bodyB) {
		// Check mot kollisionskropp
		if(bodyA != null && bodyB != null) {
			
			if(bodyA.getUserData() instanceof GameObject) {
				// Cast
				switch(((GameObject)bodyA.getUserData()).getType()) {
				case WEAPON:
					if (bodyB.getUserData() instanceof Player) {
						Player p = (Player)bodyB.getUserData();
						nuObs.playerWeaponSwap(p.getID(), false, Weapons.UNDEFINED);
						p.setCanSwapWeapon(false);
						p.setAvailableWeaponToSwap(null);
					}
					break;
				case BOURBON:
					break;
				case SIGN:
						((Sign)bodyA.getUserData()).setState(State.DEACTIVE);
					break;
				}
			}
			
			if(bodyB.getUserData() instanceof GameObject) {
				// Cast
				switch(((GameObject)bodyB.getUserData()).getType()) {
				case WEAPON:
					if (bodyA.getUserData() instanceof Player) {
						Player p = (Player)bodyA.getUserData();
						nuObs.playerWeaponSwap(p.getID(), false, Weapons.UNDEFINED);
						p.setCanSwapWeapon(false);
						p.setAvailableWeaponToSwap(null);
					}
					break;
				case BOURBON:
					break;
				case SIGN:
						((Sign)bodyB.getUserData()).setState(State.DEACTIVE);
					break;
				}
			}
			
			if(bodyA.getUserData() instanceof Enemy) {
				if(bodyB.getUserData() instanceof Player) {
					// S�tt fiende till lite idle s� han inte forts�tter springa in i spelare
					// och kan attackera
					((Enemy)bodyA.getUserData()).playerContact(false);
				}
				// Kolliderar fiende med en annan fiende
				if(bodyB.getUserData() instanceof Enemy) {
					// ((Enemy)bodyA.getUserData()).enemyContact(false);
				}
			}
			if(bodyB.getUserData() instanceof Enemy) {
				if(bodyA.getUserData() instanceof Player) {
					((Enemy)bodyB.getUserData()).playerContact(false);
				}
				if(bodyA.getUserData() instanceof Enemy) {
					// ((Enemy)bodyB.getUserData()).enemyContact(false);
				}
			}
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// System.out.println("CONTACT: Pre Solve");
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// System.out.println("CONTACT: Post Solve");
	}
	
    private void BulletEnemyCollision(Bullet b, Enemy e) {
    	if (e instanceof Boss) {
    		Boss boss = (Boss)e;
    		
    		if (!boss.isVulnerable()) {
    			nuObs.enemyDamaged(boss.getID(), -1, false, b.getPlayerWhoFired().getID());
    			return;
    		}
    	}
    	
	    nuObs.enemyDamaged(e.getID(), b.getDamage(), b.isCrit(), b.getPlayerWhoFired().getID());      
    	b.setState(State.DEAD);
    }

    private void BulletPlayerCollision(Bullet b, Player p) {
        nuObs.playerDamaged(p.getID(), b.getDamage(), b.isCrit());
        b.setState(State.DEAD);
    }

    private void PlayerBourbonCollision(Bourbon b, Player p) {
        p.hitByBourbon(b.getHealAmount());
        nuObs.playerHealed(p.getID(), b.getHealAmount());
        b.kill();
    }

}
