package se.rhel.gunslinger.observers;

import com.badlogic.gdx.utils.Array;

public class SoundObserver {
	Array<SoundListener> sListener = new Array<SoundListener>();
	
	public void addListener(IListener toAdd) {
		sListener.add((SoundListener) toAdd);
	}
	
	public void damageTaken(int id) {
		for(SoundListener sl : sListener)  {
			sl.damageTaken(id);
		}
	}
}
