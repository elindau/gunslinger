package se.rhel.gunslinger.observers;

import se.rhel.gunslinger.model.Bourbon;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.GameObject;
import se.rhel.gunslinger.model.entities.PickupAbleObject;
import se.rhel.gunslinger.model.entities.PickupAbleWeapon;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.entities.Sign;
import se.rhel.gunslinger.model.entities.GameObject.State;
import se.rhel.gunslinger.model.weapons.Bullet;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

public class ClientCollisionListener implements ContactListener {
	
	private Fixture mFixA;
	private Fixture mFixB;
	private Body 	mBodyA;
	private Body	mBodyB;

	@Override
	public void beginContact(Contact contact) {
		mFixA = contact.getFixtureA();
		mFixB = contact.getFixtureB();

		mBodyA = mFixA.getBody();
		mBodyB = mFixB.getBody();
		
		collisionHandlerBegin(mBodyA, mBodyB);
	}

	private void collisionHandlerBegin(Body bodyA, Body bodyB) {
		// Objekt kan bara kollidera med fiender och beh�ver d�f�r inte kollas 
		if(bodyA.getUserData() instanceof GameObject) {
			
				if (bodyB.getUserData() instanceof Player) {
					
					Player p = (Player)bodyB.getUserData();					
					switch(((GameObject)bodyA.getUserData()).getType()) {
					
					case WEAPON:
						break;
					case BOURBON:
					case COIN:
						//if (((PickupAbleObject)bodyA.getUserData()).getState() != State.DEAD)
						//	((PickupAbleObject)bodyA.getUserData()).pickUp(p.getID());
						break;
						
					case SIGN:
						// Skylten �r aktiverad
						if (((Player)bodyB.getUserData()).isLocal()) {
							((Sign)bodyA.getUserData()).setState(State.ACTIVE);
						}
						break;
				}
			}
		}
					
		if(bodyB.getUserData() instanceof GameObject) {

			// Objekt kan bara kollidera med fiender och beh�ver d�f�r inte kollas 
			if(bodyB.getUserData() instanceof GameObject) {
					if (bodyA.getUserData() instanceof Player) {
						
						Player p = (Player)bodyA.getUserData();						
						switch(((GameObject)bodyB.getUserData()).getType()) {
					
						case WEAPON:
							break;
						case BOURBON:
						case COIN:
							//if (((PickupAbleObject)bodyB.getUserData()).getState() != State.DEAD)
							//	((PickupAbleObject)bodyB.getUserData()).pickUp(p.getID());
							break;
							
						case SIGN:
							// Skylten �r aktiverad
							if (p.isLocal()) {
								((Sign)bodyB.getUserData()).setState(State.ACTIVE);
							}
							break;
					}
				}
			}
		}
		
		if(bodyA != null && bodyB != null) {
			// D�da kulor om de tr�ffar n�got
			if(bodyA.getUserData() instanceof Bullet) {
				((Bullet)bodyA.getUserData()).setState(State.DEAD);
			}
			if(bodyB.getUserData() instanceof Bullet) {
				((Bullet)bodyB.getUserData()).setState(State.DEAD);
			}
		}
		
		if(bodyA.getUserData() instanceof Bourbon) {
			if (bodyB.getUserData() instanceof Player) {
				Bourbon b = (Bourbon)bodyA.getUserData();
				b.kill();
			}
		} 
		else if (bodyB.getUserData() instanceof Bourbon) {
			if (bodyA.getUserData() instanceof Player) {
				Bourbon b = (Bourbon)bodyB.getUserData();
				b.kill();
			}
		}
	}

	@Override
	public void endContact(Contact contact) {
		
		mFixA = contact.getFixtureA();
		mFixB = contact.getFixtureB();
		
		mBodyA = mFixA.getBody();
		mBodyB = mFixB.getBody();
		
		collisionHandlerEnd(mBodyA, mBodyB);
	}
	
	private void collisionHandlerEnd(Body bodyA, Body bodyB) {
		// Check mot kollisionskropp
		if(mBodyA.getUserData() instanceof GameObject) {
			// Cast
			if (mBodyB.getUserData() instanceof Player) {
				
				Player p = (Player)mBodyB.getUserData();
				
				switch(((GameObject)mBodyA.getUserData()).getType()) {
				case WEAPON:
					break;
				case BOURBON:
					break;
				case SIGN:
					if(((Player)mBodyB.getUserData()).isLocal()) {
						((Sign)mBodyA.getUserData()).setState(State.DEACTIVE);
					}
					break;
				}
			}
		}
		
		if(mBodyB.getUserData() instanceof GameObject) {
			// Cast
			if (mBodyA.getUserData() instanceof Player) {
				
				Player p = (Player)mBodyA.getUserData();
				
				switch(((GameObject)mBodyB.getUserData()).getType()) {
				case WEAPON:
					break;
				case BOURBON:
					break;
				case SIGN:
					if(((Player)mBodyA.getUserData()).isLocal()) {
						((Sign)mBodyB.getUserData()).setState(State.DEACTIVE);
					}
					break;
				}
			}
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

}
