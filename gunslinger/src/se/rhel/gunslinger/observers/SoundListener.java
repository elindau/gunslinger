package se.rhel.gunslinger.observers;

public interface SoundListener extends IListener {
	public void damageTaken(int id);
}
