package se.rhel.gunslinger.observers;

public interface StageListener extends IListener {
	public void stageChanged();
	public void levelChanged();
}
