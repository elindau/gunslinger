package se.rhel.gunslinger.observers;

import se.rhel.gunslinger.screens.Screens;

public interface ScreenChangeListener {
	public void screenChange(Screens toScreen);
	public void screenChanged();
}
