package se.rhel.gunslinger.observers;

import com.badlogic.gdx.utils.Array;

public class StageObserver {
	Array<StageListener> sListener = new Array<StageListener>();

	public void addStageListener(StageListener toAdd) {
		sListener.add(toAdd);
	}
	
	public void changedStage() {
		for(StageListener sl : sListener) {
			sl.stageChanged();
		}
	}
}


