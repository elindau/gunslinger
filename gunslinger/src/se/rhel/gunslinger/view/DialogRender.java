package se.rhel.gunslinger.view;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.Stage;
import se.rhel.gunslinger.model.entities.Sign;
import se.rhel.gunslinger.model.entities.GameObject.State;
import se.rhel.gunslinger.screens.BaseStage;
import se.rhel.gunslinger.tweens.SplashAccessor;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

public class DialogRender {
	
	// Tween
	private TweenManager mManager;
	private boolean	isTweening;
	private int	isFirst;
	
	// Sign
	private ArrayList<Sign> mSigns;
	private Sprite mMessageSprite;
	private BitmapFont mHudFont;
	private LabelStyle lbs;
	
	private SpriteBatch mSpriteBatch;
	private Camera mCamera;
	private Stage mWorldStage;
	private Label mText;
	
	private BaseStage mStage;
	private Table mTable;

	public DialogRender(SpriteBatch spritebatch, Camera camera, Stage worldstage, TweenManager manager) {
		mHudFont = new BitmapFont(Gdx.files.internal("data/fonts/hud.fnt"), false);
		lbs = new LabelStyle(mHudFont, Color.WHITE);
		mSpriteBatch = spritebatch;
		mCamera = camera;
		mWorldStage = worldstage;
		mSigns = worldstage.signs();
		mManager = manager;
		mStage = new BaseStage(spritebatch);
		mTable = new Table();
		
		mMessageSprite = new Sprite(Resources.getInstance().messageBgTexture, 0, 0, 800, 140);
		mMessageSprite.setSize(Sign.MESSAGE_BG_SIZE_X, Sign.MESSAGE_BG_SIZE_Y);
		mMessageSprite.setColor(1, 1, 1, 0);
		
		mText = new Label("", lbs);
		mText.setWidth(400);
		mText.setHeight(64);
		mText.setX(Gdx.graphics.getWidth() / 2 - mText.getWidth() / 2);
		mText.setY(Gdx.graphics.getHeight() / 2 - mText.getHeight() / 2);
		mText.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
		
		mStage.addActor(mText);
	}
	
	public void draw(float delta) {
		for (Sign s : mSigns) {
			if(s.getState() == State.ACTIVE) {
				// Rita ut texten som finns i Scene2D
				
				setSignIfAny();
				//mManager.update(delta);
				mMessageSprite.setPosition(mCamera.getDisplacement().x, mCamera.getDisplacement().y);
				mMessageSprite.draw(mSpriteBatch);
				
				mText.setText(s.getMessage());
				//mText.setY((mCamera.getScale().y * mMessageSprite.getHeight() / 2) - mText.getTextBounds().height );
				mText.setY(Gdx.graphics.getHeight() / 8);
				mText.setX(Gdx.graphics.getWidth() / 2 - mText.getTextBounds().width / 2);
				//mStage.setViewport(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT, false);
				//mStage.setCamera(mCamera);
				mStage.draw();
	
				if(s.getState() == State.DEACTIVE) {
					isFirst = 0;
				}
			}
		}
	}
	
	public void setSignIfAny() {
		mMessageSprite.setPosition(mWorldStage.getPosition().x, mWorldStage.getPosition().y);
		
		TweenCallback cb = new TweenCallback() {
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				isTweening = false;
				isFirst++;
			}
		};
		
		if(!isTweening) {
			if(isFirst == 0) {
				isTweening = true;
				
				mMessageSprite.setColor(1, 1, 1, 0);
				
				Tween.to(mMessageSprite, SplashAccessor.ALPHA, 0.5f)
				.target(1)
				.ease(TweenEquations.easeInQuad)
				.setCallbackTriggers(TweenCallback.COMPLETE)
				.setCallback(cb)
				.start(mManager);
			}
		}
	}
	
	public void newStage(Stage stage) {
		mSigns = stage.signs();
	}
}
