package se.rhel.gunslinger.view;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.CameraLookAt;
import se.rhel.gunslinger.model.LevelManager;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.entities.Enemy.EnemyType;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class LevelRender {

	private SpriteBatch 		mSpriteBatch;
	private LevelManager		mLevelManager;
	
	private Texture 			mLevelTexture, mLevelBgTexture;
	private Sprite				mLevelSprite, mLevelBgSprite;
	private Sprite				mEnemyPointerSprite;
	private AnimatedSprite		mStageArrow;
	
	private Sprite[]			mParallaxBg;

	private CameraLookAt		mLookAt;
	
	private Vector2				mOldPlayerPos = new Vector2();
	private Vector2				mOldLookAtPos = new Vector2();
		
	public LevelRender(SpriteBatch spriteBatch, LevelManager manager, CameraLookAt lookAt) {
		
		mSpriteBatch = spriteBatch;
		mLevelManager = manager;
		mLookAt = lookAt;

		Resources.getInstance().switchLevel(manager.getCurrentLevel().name().getAbbreviation());
		
		mStageArrow = new AnimatedSprite(Resources.getInstance().stageArrowAtlas, "arrow", 8);
		mStageArrow.setSize(2f, 1f);
		mStageArrow.play();
		mStageArrow.loop(true);
		mStageArrow.setAnimationRate(45);
		
		mParallaxBg = new Sprite[] { new Sprite(Resources.getInstance().levelPx0, 0, 0, 160, 50),
									new Sprite(Resources.getInstance().levelPx1, 0, 0, 160, 50),
									new Sprite(Resources.getInstance().levelPx2, 0, 0, 160, 50) };
		mParallaxBg[0].setSize(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT / 2);
		mParallaxBg[1].setSize(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT / 2);
		mParallaxBg[2].setSize(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT / 2);
		
		mParallaxBg[0].setPosition(-Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT / 2);
		mParallaxBg[1].setPosition(0, Camera.CAMERA_HEIGHT / 2);
		mParallaxBg[2].setPosition(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT / 2);
		
		mLevelTexture = Resources.getInstance().level;
		mLevelBgTexture = Resources.getInstance().levelBg;
		
		// mLevelTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		// mLevelBgTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		// mLevelSprite = new Sprite(mLevelTexture, 0, 0, mLevelManager.getCurrentLevel().allStages().size() * 2560, 768);
		mLevelSprite = new Sprite(mLevelTexture, 0, 0, 4 * 160, 100);
		mLevelBgSprite = new Sprite(mLevelBgTexture, 0, 0, 160, 100);
		
		mLevelBgSprite.setSize(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT);

		// mLevelSprite.setSize(Camera.CAMERA_WIDTH * mLevelManager.getCurrentLevel().allStages().size() * 2, Camera.CAMERA_HEIGHT);
		mLevelSprite.setSize(Camera.CAMERA_WIDTH * 4, Camera.CAMERA_HEIGHT);
		
		mEnemyPointerSprite = new Sprite(Resources.getInstance().enemyPointerTexture, 0, 0, 12, 12);
		mEnemyPointerSprite.setSize(0.5f, 0.5f);
	}
	
	public void draw(float delta, Player player) {
		mLevelBgSprite.setX(mLookAt.position().x - Camera.CAMERA_WIDTH / 2);

		if ((float)Math.round(player.getPosition().y * 100) / 100 != (float)Math.round(mOldPlayerPos.y * 100) / 100) {		
			mLevelBgSprite.setY(mLookAt.position().y / 3f + ((player.getVelocity().y * 0.1f) * delta) );
		}
		
		mLevelBgSprite.draw(mSpriteBatch);
		
		// Om den sista bilden �verskrider kameran * antal bilder
		// s� ska den placeras f�rst i arrayn		

		if(mParallaxBg[1].getX() > mLookAt.position().x) {
			mParallaxBg[2].setPosition(mParallaxBg[0].getX() - Camera.CAMERA_WIDTH, mParallaxBg[2].getY());
			
			Sprite[] clone = mParallaxBg.clone();
			clone[0] = mParallaxBg[2];
			clone[1] = mParallaxBg[0];
			clone[2] = mParallaxBg[1];
			mParallaxBg = clone;
		}
		

		if(mParallaxBg[2].getX() + Camera.CAMERA_WIDTH < mLookAt.position().x + Camera.CAMERA_WIDTH) {
			mParallaxBg[0].setPosition(mParallaxBg[2].getX() + Camera.CAMERA_WIDTH, mParallaxBg[0].getY());
			
			Sprite[] clone = mParallaxBg.clone();
			clone[0] = mParallaxBg[1];
			clone[1] = mParallaxBg[2];
			clone[2] = mParallaxBg[0];
			mParallaxBg = clone;
		}
		
		for(int i = 0; i < mParallaxBg.length; i++) {
			
			if ((float)Math.round(player.getPosition().y * 100) / 100 != (float)Math.round(mOldPlayerPos.y * 100) / 100) {
				if (player.getPosition().y > Camera.CAMERA_HEIGHT / 2) {
					mParallaxBg[i].setY(mParallaxBg[i].getY() + ((player.getVelocity().y * 0.1f) * delta));
				
					if (mParallaxBg[i].getY() < Camera.CAMERA_HEIGHT / 3) {
						mParallaxBg[i].setY(Camera.CAMERA_HEIGHT / 3);
					} else if (mParallaxBg[i].getY() > Camera.CAMERA_HEIGHT / 2 + 0.5f) {
						mParallaxBg[i].setY(Camera.CAMERA_HEIGHT / 2 + 0.5f);
					}
				}
			} 
			
			if ((float)Math.round(player.getPosition().x * 100) / 100 != (float)Math.round(mOldPlayerPos.x * 100) / 100 && mOldLookAtPos.x != mLookAt.position().x) {
				mParallaxBg[i].setX(mParallaxBg[i].getX() - ((player.getVelocity().x * 0.1f) * delta));
			}
			mParallaxBg[i].draw(mSpriteBatch);
		}
		// mLevelPxBgSprite.draw(mSpriteBatch);
		mLevelSprite.draw(mSpriteBatch);
		
		// Ska rita ut pointers till vart fienderna �r om de �r utanf�r kameran
		for(Enemy e : mLevelManager.getCurrentLevel().getCurrentStage().enemies().values()) {
			if(e.isAlive() && e.getTypeOfEnemy() != EnemyType.BOSS) {
				if(e.getPosition().x < mLookAt.position().x - Camera.CAMERA_WIDTH / 2) {
					// Till v�nster		
					mEnemyPointerSprite.setPosition(mLookAt.position().x - Camera.CAMERA_WIDTH / 2, e.getPosition().y);
					mEnemyPointerSprite.draw(mSpriteBatch);
				} else if(e.getPosition().x > mLookAt.position().x + Camera.CAMERA_WIDTH / 2) {
					// Till H�ger
					mEnemyPointerSprite.setPosition(mLookAt.position().x + Camera.CAMERA_WIDTH / 2 - 0.5f, e.getPosition().y);
					mEnemyPointerSprite.draw(mSpriteBatch);
				}
			}

		}
		
		// Ajjabajja, m�ste kolla mot servern
		if(mLevelManager.isCurrentServerStageCleared()) {
			mStageArrow.setPosition( (mLookAt.position().x + Camera.CAMERA_WIDTH * 0.5f) - mStageArrow.getWidth(), mLookAt.position().y);
			mStageArrow.update(delta);
			mStageArrow.draw(mSpriteBatch);
		}
		
		mOldPlayerPos = player.getPosition().cpy();
		mOldLookAtPos = mLookAt.position().cpy();
	}
}
