package se.rhel.gunslinger.view;

import java.util.ArrayList;
import java.util.Iterator;

import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.ClientWorld;
import se.rhel.gunslinger.model.entities.Decal;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.GameObject;
import se.rhel.gunslinger.model.entities.IEntity;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.observers.SoundObserver;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class BubbleRender {

	private SpriteBatch mSpriteBatch;
	private ClientWorld mWorld;
	private PlayerRender pRender;
	private EnemyRender eRender;
	private ObjectRender oRender;
	private DecalRender dRender;
	
	private ArrayList<IEntity> mEntities = new ArrayList<IEntity>();
	
	public BubbleRender(SpriteBatch spritebatch, ClientWorld world, Camera camera, SoundObserver sObs) {
		mSpriteBatch = spritebatch;
		mWorld = world;
		
		pRender = new PlayerRender(mSpriteBatch, world.player(), world.getPlayers());
		eRender = new EnemyRender(mSpriteBatch);
		oRender = new ObjectRender(mSpriteBatch);
		dRender = new DecalRender(mSpriteBatch);
		
		sObs.addListener(pRender);
	}
	
	public void drawScene(float delta) {
		if (mWorld.levelManager().getCurrentLevel() != null) {
			mEntities.addAll(mWorld.levelManager().getCurrentLevel().getCurrentStage().signs());
			mEntities.addAll(mWorld.levelManager().getCurrentLevel().decals());
		}
		
		
		//Sortera
		sort();
		
		//M�la ut alla
		for (Iterator<IEntity> it = mEntities.iterator(); it.hasNext();) {
			IEntity e = it.next();
			
			if (e instanceof GameObject) {
				oRender.draw(delta, (GameObject)e);
			} else {
				dRender.draw(delta, (Decal)e);
			}
		}
		
		//T�m arrayen
		mEntities.clear();
	}
	
	public void draw(float delta) {
		if (mWorld.levelManager().getCurrentLevel() != null) {
			mEntities.addAll(mWorld.getPlayers().values());
			mEntities.addAll(mWorld.levelManager().getCurrentLevel().getCurrentStage().enemies().values());
			mEntities.addAll(mWorld.levelManager().getCurrentLevel().getCurrentStage().objects());
			mEntities.addAll(mWorld.levelManager().getCurrentLevel().getCurrentStage().signs());
			mEntities.addAll(mWorld.levelManager().getCurrentLevel().decals());
		}
		
		
		//Sortera
		sort();
		
		//M�la ut alla
		for (Iterator<IEntity> it = mEntities.iterator(); it.hasNext();) {
			IEntity e = it.next();
			
			if (e instanceof Player)  {
				pRender.draw(delta, (Player)e);
			} else if (e instanceof Enemy) {
				eRender.draw(delta, (Enemy)e);
			} else if (e instanceof GameObject) {
				oRender.draw(delta, (GameObject)e);
			} else if (e instanceof Decal) {
				dRender.draw(delta, (Decal)e);
			}
		}
		
		//T�m arrayen
		mEntities.clear();
	}
	
	private void sort() {
		//Bubblesort f�r vem som ska renderas fr�mst
		if (mEntities.size() > 1) {
			int length = mEntities.size();
			boolean swapped = true;
			IEntity temp;
			
			do {
				swapped = false;
				for (int i = 1; i <= length-1; i++) {
						if (mEntities.get(i-1).compare(mEntities.get(i)) < 0) {
							temp = mEntities.get(i-1);
							mEntities.set(i-1, mEntities.get(i));
							mEntities.set(i, temp);
							swapped = true;
						}
				}
				
				length -= 1;
			} while(swapped);
		}
	}
	
	public ObjectRender getObjectRender() {
		return oRender;
	}
}
