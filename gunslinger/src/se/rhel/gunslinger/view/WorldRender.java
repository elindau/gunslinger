package se.rhel.gunslinger.view;

import java.util.Map;

import se.rhel.gunslinger.controller.WorldController;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.CameraLookAt;
import se.rhel.gunslinger.model.ClientWorld;
import se.rhel.gunslinger.model.HUD.HUDText;
import se.rhel.gunslinger.model.LevelManager;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.weapons.Gun;
import se.rhel.gunslinger.model.weapons.IRangedWeapon;
import se.rhel.gunslinger.model.weapons.RangedWeapon;
import se.rhel.gunslinger.model.weapons.IWeapon.Type;
import se.rhel.gunslinger.observers.HUDTextListener;
import se.rhel.gunslinger.observers.HUDTextObserver;
import se.rhel.gunslinger.observers.SoundObserver;
import se.rhel.gunslinger.tweens.SplashAccessor;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

public class WorldRender implements HUDTextListener {
	
	private ClientWorld 			mWorld;
	private Camera 				mCamera;
	private SpriteBatch 		mSpriteBatch;
	
	// Renderers
	private TargetRender		mTargetRender;
	private PlayerRender 		mPlayerRender;
	private EnemyRender			mEnemyRender;
	private Box2DDebugRenderer 	mDebugRenderer;
	private ObjectRender 		mObjectRender;
	private ForegroundRender	mForegroundRender;
	private BulletRender 		mBulletRender;
	private HUDRender			mHudRender;
	private LevelRender			mLevelRender;
	private UIRender			mUIRender;
	private BubbleRender		mBubbleRender;
	
	//Observer
	private HUDTextObserver		mPickupObserver;
	
	private TweenManager		mManager;
	
	// H�ll koll p� vilken level som ska ritas ut
	private LevelManager		mLevelManager;
	private DialogRender  		mDialogRender;
	
	public WorldRender(ClientWorld world, Camera camera, CameraLookAt lookAt, SoundObserver sObs) {
		mWorld = world;
		mCamera = camera;
		mSpriteBatch = new SpriteBatch();
		mManager = new TweenManager();
		
		mPickupObserver = new HUDTextObserver();
		mPickupObserver.addListener(this);
		mWorld.setHUDTextObserver(mPickupObserver);
		
		mTargetRender = new TargetRender(mSpriteBatch, mWorld.player());
		mBubbleRender = new BubbleRender(mSpriteBatch, mWorld, camera, sObs);
		mDebugRenderer = new Box2DDebugRenderer();
		mBulletRender = new BulletRender(mSpriteBatch);
		mLevelManager = mWorld.levelManager();
		mDialogRender = new DialogRender(mSpriteBatch, mCamera, mWorld.levelManager().getCurrentLevel().getCurrentStage(), mManager);
		mUIRender = new UIRender(mSpriteBatch, mCamera, mWorld.player());
		mHudRender = new HUDRender(mSpriteBatch, mCamera);
		mLevelRender = new LevelRender(mSpriteBatch, mLevelManager, lookAt);

		Tween.registerAccessor(Sprite.class, new SplashAccessor());
		
		mForegroundRender = new ForegroundRender(mSpriteBatch, mManager, lookAt);
	}
	
	public void draw(float delta) {
		
		mSpriteBatch.setProjectionMatrix(mCamera.combined);

		mSpriteBatch.begin();
		
		mLevelRender.draw(delta, mWorld.player());	
		mTargetRender.draw(delta);
		mBubbleRender.draw(delta);
		
		for (Map.Entry<Integer, Player> playerEntry : mWorld.getPlayers().entrySet()) {
			Player player = (Player)playerEntry.getValue();
			
			if(player.getInventory().gotWeapon())
				if (player.getInventory().weapon().getType() == Type.RANGED) {
					IRangedWeapon re = ((IRangedWeapon)player.getInventory().weapon());
					mBulletRender.draw(delta, re.getBullets());
				}
		}
		
		for (Map.Entry<Integer, Enemy> enemyEntry :  mLevelManager.getCurrentLevel().getCurrentStage().enemies().entrySet()) {
			Enemy e = (Enemy)enemyEntry.getValue();
			
			if(e.isRanged()) {
				mBulletRender.draw(delta, ((IRangedWeapon)e.getWeapon()).getBullets());
			}
		}
		
		mHudRender.draw(delta);
		mDialogRender.draw(delta);
		mForegroundRender.draw(delta);
		mUIRender.draw(delta);
		
		mManager.update(delta);

		if(WorldController.SHOW_DEBUG)
			mDebugRenderer.render(mWorld.world(), mCamera.combined);
		
		mSpriteBatch.end();
	}

	@Override
	public void addHudText(HUDText obj) {
		mHudRender.addHUDText(obj);
	}
	
	/**
	 * N�r stagen �ndras
	 */
	public void stageChanged() {
		mDialogRender.newStage(mLevelManager.getCurrentLevel().getCurrentStage());
	}

	public ForegroundRender getForegroundRender() {
		return mForegroundRender;
	}
}
