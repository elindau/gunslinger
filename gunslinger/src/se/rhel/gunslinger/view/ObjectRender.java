package se.rhel.gunslinger.view;


import java.util.HashMap;
import java.util.Map;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.entities.GameObject;
import se.rhel.gunslinger.model.entities.GameObject.State;
import se.rhel.gunslinger.model.entities.GameObject.Type;
import se.rhel.gunslinger.model.entities.Sign;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;


public class ObjectRender {
	
	private SpriteBatch 					mSpriteBatch;
	
	// Textures
	private Sprite							mSignSprite;
	
	private TextureAtlas					mBubbleAtlas;
	private AnimatedSprite					mBubbleSprite;
	
	private Map<Integer, AnimatedSprite>	mCoinAnimations, mBourbonAnimations, mObjectPickups, mGunAnimations;
	private AnimatedSprite					mGunAnimation;
	
	private Map<Integer, Boolean>			mPlaySound;
	
	public ObjectRender(SpriteBatch spriteBatch) {
		mSpriteBatch = spriteBatch;
		
		mSignSprite = new Sprite(Resources.getInstance().signTexture, 0, 0, 24, 32);
		mSignSprite.setSize(1.5f, 2f);
		
		mBubbleAtlas = Resources.getInstance().bubbleAtlas;
		mBubbleSprite = new AnimatedSprite(mBubbleAtlas, "bubble", 6);
		mBubbleSprite.setSize(1f, 1f);
		mBubbleSprite.play();
		mBubbleSprite.loop(true);
		mBubbleSprite.setAnimationRate(10);
		
		mGunAnimation = new AnimatedSprite(Resources.getInstance().gunAtlas, "gun", 16);
		mGunAnimation.setSize(1.5f, 1f);
		mGunAnimation.play();
		mGunAnimation.loop(true);
		mGunAnimation.setAnimationRate(15);
		
		mCoinAnimations = new HashMap<Integer, AnimatedSprite>();
		mBourbonAnimations = new HashMap<Integer, AnimatedSprite>();
		mGunAnimations = new HashMap<Integer, AnimatedSprite>();
		mObjectPickups = new HashMap<Integer, AnimatedSprite>();
		mPlaySound = new HashMap<Integer, Boolean>();
	}

	public void draw(float delta, GameObject obj) {
		if(obj.getType() == Type.WEAPON) {	
			if(obj.getState() != State.DEAD) {
				
				AnimatedSprite gunAnimation = mGunAnimations.get(obj.getID());
				if(gunAnimation == null) {
					gunAnimation = new AnimatedSprite(Resources.getInstance().gunAtlas, "gun", 16);
					gunAnimation.setSize(1.5f, 1f);
					gunAnimation.play();
					gunAnimation.loop(true);
					gunAnimation.setAnimationRate(15);
					mGunAnimations.put(obj.getID(), gunAnimation);
				}
				
				AnimatedSprite gunPickup = mObjectPickups.get(obj.getID());
				if(gunPickup == null) {
					gunPickup = new AnimatedSprite(Resources.getInstance().coinPickup, "coinpickup", 6);
					gunPickup.setSize(1f, 1f);
					gunPickup.play();
					gunPickup.loop(false);
					gunPickup.setAnimationRate(10);
					gunPickup.setPosition(obj.getPosition().x, obj.getPosition().y - 0.25f);
					mObjectPickups.put(obj.getID(), gunPickup);
					mPlaySound.put(obj.getID(), true);
				}
				
				gunAnimation.setPosition(obj.getPosition().x - obj.getSize().x / 4, obj.getPosition().y);
				gunAnimation.update(delta);
				gunAnimation.draw(mSpriteBatch);
			} else if(obj.getState() == State.DEAD) {
				
				if(mPlaySound.containsKey(obj.getID())) {
					boolean playSound = mPlaySound.get(obj.getID());
					if(playSound) {
						mPlaySound.put(obj.getID(), false);
						SoundManager.getInstance().playGunPickup();
					}
				}
				
				AnimatedSprite gunPickup = mObjectPickups.get(obj.getID());
				if(gunPickup != null) {					
					gunPickup.update(delta);
					gunPickup.draw(mSpriteBatch);
					
					// N�r all animation �r klar kan objekten tas bort fr�n mappen
					if(gunPickup.isFinished()) {
						mObjectPickups.remove(obj.getID());
						mGunAnimations.remove(obj);
						mPlaySound.remove(obj.getID());
					}
				}
			}
		}
				
		if(obj.getType() == Type.BOURBON) {
			if(obj.getState() != State.DEAD) {
				AnimatedSprite as = mBourbonAnimations.get(obj.getID());
				
				AnimatedSprite bourbonPickup = mObjectPickups.get(obj.getID());
				
				if(bourbonPickup == null) {
					bourbonPickup = new AnimatedSprite(Resources.getInstance().coinPickup, "coinpickup", 6);
					bourbonPickup.setSize(1f, 1f);
					bourbonPickup.play();
					bourbonPickup.loop(false);
					bourbonPickup.setAnimationRate(10);
					bourbonPickup.setPosition(obj.getPosition().x, obj.getPosition().y - 0.25f);
					mObjectPickups.put(obj.getID(), bourbonPickup);
					mPlaySound.put(obj.getID(), true);
				}
				
				if(as == null) {
					as = new AnimatedSprite(Resources.getInstance().bourbonAtlas, "bourbon", 15);
					mBourbonAnimations.put(obj.getID(), as);
					as.setSize(1.5f, 1.5f);
					as.play();
					as.loop(true);
					as.setAnimationRate(15);
					// mPlaySound.put(obj.getID(), true);
				}
				
				as.setPosition(obj.getPosition().x - obj.getSize().x / 4, obj.getPosition().y);
				as.update(delta);
				as.draw(mSpriteBatch);
			} else if(obj.getState() == State.DEAD) {
				
				if(mPlaySound.containsKey(obj.getID())) {
					boolean playSound = mPlaySound.get(obj.getID());
					if(playSound) {
						mPlaySound.put(obj.getID(), false);
						SoundManager.getInstance().playBourbonPickup();
					}
					// mPlaySound.remove(obj.getID());
				}
				
				AnimatedSprite bourbonPickup = mObjectPickups.get(obj.getID());
				if(bourbonPickup != null) {					
					
					bourbonPickup.update(delta);
					bourbonPickup.draw(mSpriteBatch);
					
					// N�r all animation �r klar kan objekten tas bort fr�n mappen
					if(bourbonPickup.isFinished()) {
						mObjectPickups.remove(obj.getID());
						mBourbonAnimations.remove(obj);
						mPlaySound.remove(obj.getID());
					}
				}
			}
		}
				
		if(obj.getType() == Type.COIN) {
			if(obj.getState() != State.DEAD) {
				AnimatedSprite as = mCoinAnimations.get(obj.getID());
				// L�gg till coinpickupanimation ocks�
				AnimatedSprite coinPickup = mObjectPickups.get(obj.getID());
				
				if(coinPickup == null) {
					coinPickup = new AnimatedSprite(Resources.getInstance().coinPickup, "coinpickup", 6);
					coinPickup.setSize(1f, 1f);
					coinPickup.play();
					coinPickup.loop(false);
					coinPickup.setAnimationRate(10);
					coinPickup.setPosition(obj.getPosition().x, obj.getPosition().y - 0.25f);
					mObjectPickups.put(obj.getID(), coinPickup);
					mPlaySound.put(obj.getID(), true);
				}
				
				if (as == null) {
					as = new AnimatedSprite(Resources.getInstance().coinAtlas, "coin", 9);
					mCoinAnimations.put(obj.getID(), as);
					as.setSize(1.5f, 1.5f);
					as.play();
					as.loop(true);
					as.setAnimationRate(15);
				}
				
				as.setPosition(obj.getPosition().x - obj.getSize().x / 4, obj.getPosition().y);
				as.update(delta);
				as.draw(mSpriteBatch);
				
			} else if(obj.getState() == State.DEAD) {
				
				if(mPlaySound.containsKey(obj.getID())) {
					boolean playSound = mPlaySound.get(obj.getID());
					if(playSound) {
						mPlaySound.put(obj.getID(), false);
						SoundManager.getInstance().playCoinPickup();
					}	
				}
				
				AnimatedSprite coinPickup = mObjectPickups.get(obj.getID());
				if(coinPickup != null) {					
					
					// Resources.getInstance().coinSounds[1].play();
					
					coinPickup.update(delta);
					coinPickup.draw(mSpriteBatch);
					
					// N�r all animation �r klar kan objekten tas bort fr�n mappen
					if(coinPickup.isFinished()) {
						mObjectPickups.remove(obj.getID());
						mCoinAnimations.remove(obj.getID());
						mPlaySound.remove(obj.getID());
					}
				}
			}
		}
		
		if (obj.getType() == Type.SIGN) {
			mSignSprite.setPosition(obj.getPosition().x - obj.getSize().x / 2, obj.getPosition().y);
			
			mBubbleSprite.setPosition(obj.getPosition().x - mBubbleSprite.getWidth() / 2, obj.getPosition().y + mSignSprite.getHeight());
			mBubbleSprite.update(delta);
			mBubbleSprite.draw(mSpriteBatch);
			// 1.5f, 2f
			
			mSignSprite.draw(mSpriteBatch);
		}
	}
}
