package se.rhel.gunslinger.view;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.entities.Decal;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class DecalRender {
	
	private SpriteBatch mSpriteBatch;
	private Sprite 		mLarge_Cactus;
	private Sprite		mSmall_Cactus;
	private Sprite		mDeadCowboySprite;
	private Sprite		mShadow;
	private Sprite		mStone;

	public DecalRender(SpriteBatch sb) {
		mSpriteBatch = sb;
		
		mStone = new Sprite(Resources.getInstance().stone, 0, 0, 25, 17);
		mSmall_Cactus = new Sprite(Resources.getInstance().small_cactus, 0, 0, 17, 26);
		mLarge_Cactus = new Sprite(Resources.getInstance().cactus, 0, 0, 20, 32);
		mDeadCowboySprite = new Sprite(Resources.getInstance().deadCowboy, 0, 0, 32, 32);
		mShadow = new Sprite(Resources.getInstance().characterShadow, 0, 0, 15, 5);
		mShadow.setColor(0,0,0,0.4f);
	}
	
	public void draw(float delta, Decal d) {
		Sprite sprite;
		mShadow.setSize(d.getSize().x, d.getSize().x / 3f);
		
		switch (d.getType()) {	
			case LARGE_CACTUS:
				mShadow.setSize(mShadow.getWidth() / 1.6f, mShadow.getHeight() / 1.6f);
				mLarge_Cactus.setSize(d.getSize().x, d.getSize().y);
				mLarge_Cactus.setPosition(d.getPosition().x - d.getSize().x / 2, d.getPosition().y);
				sprite = mLarge_Cactus;
				break;
			case SMALL_CACTUS:
				mShadow.setSize(mShadow.getWidth() / 1.6f, mShadow.getHeight() / 1.6f);
				mSmall_Cactus.setSize(d.getSize().x, d.getSize().y);
				mSmall_Cactus.setPosition(d.getPosition().x - d.getSize().x / 2f, d.getPosition().y);
				sprite = mSmall_Cactus;
				break;
			case DEAD_GUY:
				mShadow.setSize(0, 0);
				mDeadCowboySprite.setSize(d.getSize().x, d.getSize().y);
				mDeadCowboySprite.setPosition(d.getPosition().x - d.getSize().x / 2f, d.getPosition().y - d.getSize().y / 4f);
				sprite = mDeadCowboySprite;
				break;
			case STONE:
				mStone.setSize(d.getSize().x, d.getSize().y);
				mStone.setPosition(d.getPosition().x - d.getSize().x / 2f, d.getPosition().y);
				sprite = mStone;
				break;
			default:
				sprite = null;
		}

		mShadow.setPosition(d.getPosition().x - mShadow.getWidth() / 2, d.getPosition().y - mShadow.getHeight() / 3f);
		mShadow.draw(mSpriteBatch);
		sprite.draw(mSpriteBatch);
	}
}
