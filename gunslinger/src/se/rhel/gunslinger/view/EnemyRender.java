package se.rhel.gunslinger.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.entities.Boss;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.Enemy.EnemyState;
import se.rhel.gunslinger.model.entities.Enemy.EnemyType;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.entities.Player.State;
import se.rhel.gunslinger.model.weapons.Cannon;
import se.rhel.gunslinger.model.weapons.Cannon.CannonBall;
import se.rhel.gunslinger.model.weapons.Pitchfork;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;


public class EnemyRender {

	private SpriteBatch 					mSpriteBatch;
	
	// Sprites
	private Sprite 							mEnemySprite;
	private Sprite							mHealthbar;
	private Sprite							mMeleeWeaponSprite;
	private Sprite							mShadow;
	private Sprite							mGun;
	
	private ParticleEffect 					mBlood;
	private ParticleEffect 					mExplosion;
	
	private boolean 						mStartBlood = true;
	private Vector2							mLastKnownLocation;
	private boolean							mSetLocationOnce = true;
	
	private Map<Integer, Boolean>			mPlaySounds = new HashMap<Integer, Boolean>();
	private Map<Integer, Boolean>			mIsAttacking = new HashMap<Integer, Boolean>();
	
	// Animation
	private AnimatedSprite					mEnemyAnimation;
	private Map<Integer, AnimatedSprite> 	mAnimations = new HashMap<Integer, AnimatedSprite>();
	private AnimatedSprite 					mEnemyDeadAnimation;
	private AnimatedSprite					mBloodSplatter;
	private Map<Integer, Boolean>			mPlayDeadSound = new HashMap<Integer, Boolean>();
	private Map<Integer, Boolean>			mPlayDamageTaken = new HashMap<Integer, Boolean>();
	private ArrayList<AnimatedSprite>		mMuzzles = new ArrayList<AnimatedSprite>();
	private ArrayList<AnimatedSprite>		mSmokes = new ArrayList<AnimatedSprite>();
	
	
	//Renderers
	private BossRender mBossRender;

	private AnimatedSprite mMuzzle;

	private AnimatedSprite mSmoke;
	
	public EnemyRender(SpriteBatch spriteBatch) {
		mSpriteBatch = spriteBatch;
		mBossRender = new BossRender(mSpriteBatch);
			
		mHealthbar = new Sprite(Resources.getInstance().health, 0, 0, 1, 6);
		mMeleeWeaponSprite = new Sprite( Resources.getInstance().pitchForkTexture, 0, 0, 32, 32);
		mMeleeWeaponSprite.setSize(0.5f, 1f);
		mMeleeWeaponSprite.setOrigin(mMeleeWeaponSprite.getU() + mMeleeWeaponSprite.getWidth() / 2, mMeleeWeaponSprite.getV() + mMeleeWeaponSprite.getHeight() / 2);
		
		mShadow = new Sprite(Resources.getInstance().characterShadow);
		mShadow.setColor(0, 0, 0, 0.4f);
		
		mBlood = Resources.getInstance().blood;
		mBlood.setDuration(1000);
		mBlood.allowCompletion();
		mBlood.reset();
		
		mExplosion = Resources.getInstance().explosion;
		mExplosion.allowCompletion();
		mExplosion.reset();
		
		mEnemyDeadAnimation = new AnimatedSprite(Resources.getInstance().enemyDie, "enemydie", 9);
		mEnemyDeadAnimation.setSize(2f, 2f);
		mEnemyDeadAnimation.setAnimationRate(20);
		mEnemyDeadAnimation.play();
		mEnemyDeadAnimation.loop(false);
		
		mBloodSplatter = new AnimatedSprite(Resources.getInstance().bloodSplatter, "blood", 8);
		mBloodSplatter.setSize(1f, 1f);
		mBloodSplatter.setAnimationRate(20);
		mBloodSplatter.play();
		mBloodSplatter.loop(false);
		
		mGun = new Sprite(Resources.getInstance().gunTexture);
		mGun.setSize(1.5f, 0.6f);
		
		mMuzzle = new AnimatedSprite(Resources.getInstance().muzzle, "muzzle", 3);
		mMuzzle.setSize(0.5f, 0.5f);
		mMuzzle.play();
		mMuzzle.loop(false);
		mMuzzle.setAnimationRate(20);
		
		mSmoke = new AnimatedSprite(Resources.getInstance().smoke, "smoke", 10);
		mSmoke.setSize(0.5f, 0.5f);
		mSmoke.play();
		mSmoke.loop(false);
		mSmoke.setAnimationRate(10);
		
		// mBlood.allowCompletion();
	}
	
	public void draw(float delta, Enemy e) {
		if (e instanceof Boss) {
			if (e.isAlive())
				mBossRender.draw(delta, (Boss) e);
		} else {
			if (mAnimations.get(e.getID()) == null) {
				mEnemyAnimation = new AnimatedSprite(Resources.getInstance().farmerAtlas, "farmer", 5);
				mEnemyAnimation.setAnimationRate(10);
				mEnemyAnimation.setSize(2f,  2f);
				mAnimations.put(e.getID(), mEnemyAnimation);
				mPlayDeadSound.put(e.getID(), true);
				mPlayDamageTaken.put(e.getID(), true);
			}
			
			mEnemyAnimation = mAnimations.get(e.getID());
			
			if(e != null) {
				if(e.isAlive()) {
					
					if (mIsAttacking.get(e.getID()) == null)
						mIsAttacking.put(e.getID(), false);
					
					mLastKnownLocation = e.getPosition();
					
					if(e.getState() != EnemyState.DYING) {
						if(e.getState() == EnemyState.IDLE) {
							mEnemyAnimation.freeze(0);
							mPlaySounds.remove(e.getID());
						} else if(e.getState() == EnemyState.MOVING){
							if(!mEnemyAnimation.mIsPlaying)
								mEnemyAnimation.play();
						} else {
							mEnemyAnimation.freeze(0);
						}

						mShadow.setSize(e.getSize().x, e.getSize().x / 3f);
						mShadow.setPosition(e.getPosition().x - mShadow.getWidth() / 2f, e.getPosition().y - mShadow.getHeight() / 2f - e.getSize().y / 2);
						
						if(e.isFacingLeft()) {
							if (!mEnemyAnimation.isFlipX())
								mEnemyAnimation.flip(true, false);
						} else {
							if (mEnemyAnimation.isFlipX())
								mEnemyAnimation.flip(false, false);
						}
						
						mShadow.draw(mSpriteBatch);
						mEnemyAnimation.setPosition(e.getPosition().x - Player.SIZE / 2f, e.getPosition().y - Player.SIZE / 2f);
						mEnemyAnimation.update(delta);
						mEnemyAnimation.setColor(e.isRanged() ? new Color(1.17f, .75f, .1f, 1f) : Color.WHITE);
						mEnemyAnimation.draw(mSpriteBatch);
						
						
						if (e.isRanged()) {
							mGun.setSize(1.4f, 0.8f);
							if (e.isFacingLeft()) {
								if (!mGun.isFlipX())
									mGun.flip(true, false);
								mGun.setPosition(e.getPosition().x - 1f, e.getPosition().y - 0.4f);
							} else {
								if (mGun.isFlipX())
									mGun.flip(true, false);
								mGun.setPosition(e.getPosition().x - 0.4f, e.getPosition().y - 0.4f);
							}
							
							if (e.isAttacking()) {
								if (mIsAttacking.get(e.getID()) == false) {					
									mIsAttacking.put(e.getID(), true);
									
									if (mPlaySounds.get(e.getID()) == null || mPlaySounds.get(e.getID()) == false)
										mPlaySounds.put(e.getID(), true);
									
									AnimatedSprite mMuzzle = new AnimatedSprite(Resources.getInstance().muzzle, "muzzle", 3);
									mMuzzle.setSize(0.5f, 0.5f);
									mMuzzle.play();
									mMuzzle.loop(false);
									mMuzzle.setAnimationRate(20);
									
									AnimatedSprite mSmoke = new AnimatedSprite(Resources.getInstance().smoke, "smoke", 10);
									mSmoke.setSize(0.5f, 0.5f);
									mSmoke.play();
									mSmoke.loop(false);
									mSmoke.setAnimationRate(10);
									
									Vector2 pos = e.isFacingLeft() ? new Vector2(mGun.getX() - 0.35f, mGun.getY() + 0.2f) : new Vector2(mGun.getX() + 1.25f, mGun.getY() + 0.2f);
									mMuzzle.setPosition(pos.x, pos.y);
									mSmoke.setPosition(pos.x, pos.y);
											
									mMuzzles.add(mMuzzle);
									mSmokes.add(mSmoke);
								}
							}
							
							mGun.draw(mSpriteBatch);
							
							//Muzzles
							for (Iterator<AnimatedSprite> it = mMuzzles.iterator(); it.hasNext();) {
								AnimatedSprite as = it.next();
								
								as.update(delta);
								as.draw(mSpriteBatch);
								
								if (as.isFinished()) {
									as.restart();
									it.remove();
									
									if (mIsAttacking.get(e.getID())) {
										mIsAttacking.put(e.getID(), false);
									}
									
									if (mPlaySounds.get(e.getID()) != null && mPlaySounds.get(e.getID())) {
										SoundManager.getInstance().playExplosionSmall();
										mPlaySounds.put(e.getID(), false);
									}
								}
							}
							
							//Smokes
							for (Iterator<AnimatedSprite> it = mSmokes.iterator(); it.hasNext();) {
								AnimatedSprite as = it.next();
								
								as.update(delta);
								as.draw(mSpriteBatch);
								
								as.setPosition(as.getX(), as.getY() + delta / 2f);
								
								if (as.isFinished()) {									
									as.restart();
									it.remove();
								}
							}
						}
						
						// Rita ut melee-vapen
						if(!(e instanceof Boss) && !e.isRanged()) {
							mMeleeWeaponSprite.setOrigin(0.5f, 0.1f);
							mMeleeWeaponSprite.setRotation((float) Math.toDegrees(e.getWeaponBody().getAngle()));
							mMeleeWeaponSprite.setSize(1f, 2f);
							
							if(e.isFacingLeft()) {
								mMeleeWeaponSprite.setPosition(e.getPosition().x - 0.1f, e.getPosition().y - 0.3f);
							} else {
								mMeleeWeaponSprite.setPosition(e.getPosition().x + 0.1f - e.getSize().x, e.getPosition().y - 0.3f);
							}
							mMeleeWeaponSprite.draw(mSpriteBatch);
						}
						
						mHealthbar.setColor(Color.RED);
						mHealthbar.setSize((float)((float)e.getHealth()/(float)e.getMaxHealth())*e.getSize().x, 0.1f);
						mHealthbar.setPosition(e.getPosition().x - e.getSize().x / 2 + (e.getSize().x-mHealthbar.getBoundingRectangle().width) / 2, e.getPosition().y + e.getSize().y / 2 + mHealthbar.getHeight());
						mHealthbar.draw(mSpriteBatch);
					
						if(e.hasEnemyTakenDamage()) {
							
							if(mPlayDamageTaken.containsKey(e.getID())) {
								if(mPlayDamageTaken.get(e.getID())) {
									mPlayDamageTaken.put(e.getID(), false);
									SoundManager.getInstance().playDamageTaken();
								}
							}
							
							if(mStartBlood) {
								mBloodSplatter.setPosition(e.getPosition().x, e.getPosition().y);
								mBloodSplatter.update(delta);
								mBloodSplatter.draw(mSpriteBatch);
							}
							if(mBloodSplatter.isFinished()) {
								mBloodSplatter.restart();
								e.setShootAt(false);
								mPlayDamageTaken.put(e.getID(), true);
							}
						}
					} else {
						if(e.getState() == EnemyState.DYING) {
							
							if(mPlayDeadSound.containsKey(e.getID())) {
								if(mPlayDeadSound.get(e.getID())) {
									mPlayDeadSound.put(e.getID(), false);
									SoundManager.getInstance().playEnemyDeath();
								}
							}
							
							// State DYING finns bara p� klienten och vi kan manipulera en explosion
							if(mSetLocationOnce) {
								mEnemyDeadAnimation.setPosition(mLastKnownLocation.x - Player.SIZE / 2f, mLastKnownLocation.y - Player.SIZE / 2f);
								mSetLocationOnce = false;
							}
							
							mEnemyDeadAnimation.update(delta);
							mEnemyDeadAnimation.draw(mSpriteBatch);	
							
							// �ndra staten sen igen s� �r allt som vanligt
							if(mEnemyDeadAnimation.isFinished()) {
								e.setState(EnemyState.DEAD);
								mEnemyDeadAnimation.restart();
								mSetLocationOnce = true;
								mPlayDeadSound.remove(e.getID());
								mPlayDamageTaken.remove(e.getID());
							}
						}
					}
				}
			}
		}
	}
}
