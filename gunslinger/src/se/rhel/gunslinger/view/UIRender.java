package se.rhel.gunslinger.view;

import java.util.Map;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.GUI;
import se.rhel.gunslinger.model.GUI.ButtonType;
import se.rhel.gunslinger.model.GUI.UIButton;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.weapons.WeaponStats;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;
import se.rhel.gunslinger.model.weapons.WeaponStats.WeaponStatComparer;
import se.rhel.gunslinger.screens.BaseStage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;


public class UIRender {
	
	private SpriteBatch 	mSpriteBatch;
	private Player 			mPlayer;
	private Sprite 			mSwitchButton, mHealingButton, mShootButton;
	private Sprite[]		mHealingButtonTypes;
	private Sprite[]		mSwitchButtonTypes;

	private Sprite 			mCash;
	private Sprite			mBourbon;
	private Label 			mCashLabel;
	private Label			mBourbonLabel;
	private Camera 			mCamera;
	private Stage 			mStage;
	private UIButton 		shootButton, switchButton, healingButton;

	private Table 			mWeaponSwapTable = new Table();
	private BitmapFont 		mUIFont;
	private Sprite 			mWeaponInfoBackground;
	private Sprite 			mHealthbar;
	private Sprite[]		mShootButtons;
	private Sprite			mUI;
	
	private Label			mAttackLabel;
	private Label			mCritDmgLabel;
	private Label			mCritChanceLabel;
	
	private LabelStyle 		lbs;
	private LabelStyle 		namestyle;
	
	private WeaponStats 	oldWeapon;
	private WeaponStats 	newWeapon;
	private WeaponStatComparer wsc;
	
	
	public UIRender(SpriteBatch spritebatch, Camera camera, Player player) {
		mSpriteBatch = spritebatch;
		mPlayer = player;
		mCamera = camera;
		mStage = new BaseStage(mSpriteBatch);
		mWeaponSwapTable = new Table();
		GUI ui = new GUI(camera);
		Map<ButtonType, UIButton> buttons = ui.getUIComponents();
		
		
		mUIFont = Resources.getInstance().font5x5;
		switchButton = buttons.get(ButtonType.SWITCH);
		shootButton = buttons.get(ButtonType.SHOOT);
		healingButton = buttons.get(ButtonType.HEALING);
		mHealthbar = new Sprite(Resources.getInstance().health);
		mUI = new Sprite(Resources.getInstance().ui);
		mUI.setSize(16f, 2.86f);
		
		// Skapa upp en sprite f�r hur m�nga kulor
		mShootButtons = new Sprite[7];
		for(int i = 0; i < mShootButtons.length; i++) {
			mShootButtons[i] = new Sprite(Resources.getInstance().shoot.createSprite("shoot", i));
			mShootButtons[i].setSize(shootButton.mSize.x, shootButton.mSize.y);
		}
		
		lbs = new LabelStyle(new BitmapFont(Gdx.files.internal("data/fonts/roto.fnt"), false), Color.WHITE);
		namestyle = new LabelStyle(mUIFont, Color.WHITE);
		
		mShootButton = mShootButtons[6];
		mWeaponInfoBackground = new Sprite(Resources.getInstance().weaponInfo);
		
		mCash = new Sprite(Resources.getInstance().coinAtlas.createSprite("coin", 0));
		mCash.setSize(1f, 1f);
		
		mCashLabel = new Label(""+player.getInventory().getCoins(), namestyle);
		
		mBourbon = new Sprite(Resources.getInstance().bourbonAtlas.createSprite("bourbon", 0));
		mBourbonLabel = new Label(""+player.getInventory().getCurrentBourbons(), namestyle);
		
		
		mShootButton.setSize(shootButton.mSize.x, shootButton.mSize.y);
		
		mSwitchButtonTypes = new Sprite[] { Resources.getInstance().shopbuttons.createSprite("addbutton"),
											Resources.getInstance().shopbuttons.createSprite("addbuttonpressed")};
		for(Sprite s : mSwitchButtonTypes) {
			s.setSize(switchButton.mSize.x, switchButton.mSize.y);
		}
		
		mSwitchButton = mSwitchButtonTypes[0];
		
		mHealingButtonTypes = new Sprite[] {Resources.getInstance().healingButton.createSprite("healingbutton"), 
											Resources.getInstance().healingButton.createSprite("healingbutton_pressed"),
											Resources.getInstance().healingButton.createSprite("healingbutton_grey")};
		for(Sprite s : mHealingButtonTypes) {
			s.setSize(healingButton.mSize.x, healingButton.mSize.y);
		}
		
		mHealingButton = mHealingButtonTypes[0];
		mHealingButton.setSize(healingButton.mSize.x, healingButton.mSize.y);
		
		mAttackLabel = new Label("attack damage", lbs);
		mCritChanceLabel = new Label("crit chance", lbs);
		mCritDmgLabel = new Label("crit damage", lbs);
		mWeaponSwapTable.debug();
	}
	
	public void draw(float delta) {
		mUI.setSize(9.636f, 1f);
		mUI.setPosition(mCamera.getDisplacement().x + Camera.CAMERA_WIDTH / 2 - mUI.getWidth() / 2, mCamera.getDisplacement().y + Camera.CAMERA_HEIGHT - mUI.getHeight());
		mUI.draw(mSpriteBatch);

		if (mPlayer.isAlive()) {
			Color color = Color.GREEN;
			if (mPlayer.getHealth() < 35) {
				color = Color.RED;
			} else if (mPlayer.getHealth() < 75) {
				color = Color.YELLOW;
			}
			
			int bullets = mPlayer.getCurrentBullets();
			mShootButton = mShootButtons[bullets];
			
			int width = mPlayer.getHealth(); 
			
			mHealthbar.setBounds(mCamera.getDisplacement().x + 5.545f, mCamera.getDisplacement().y + 9.275f, (float)(width/100f)*4.82f, 0.54f);
			mHealthbar.setColor(color);
			mHealthbar.draw(mSpriteBatch);
		}

		
		// Rita ut cash
		mCash.setSize(1f, 1f);
		mCash.setPosition(mCamera.getDisplacement().x + 3.1f, mCamera.getDisplacement().y + 9.53f);
		mCash.draw(mSpriteBatch);
		mCashLabel.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
		mCashLabel.setText(String.valueOf(mPlayer.getInventory().getCoins()) + "$");
		mCashLabel.setAlignment(Align.bottom, Align.left);
		mCashLabel.setPosition(4.4f * mCamera.getScale().x - mCashLabel.getWidth() / 2, mCamera.getScale().y * 9.55f);
		mStage.addActor(mCashLabel);
		
		// Rita ut bourbon
		mBourbon.setSize(0.7f, 0.7f);
		mBourbon.setPosition(mCamera.getDisplacement().x + 11f, mCamera.getDisplacement().y + 9.52f);
		mBourbon.draw(mSpriteBatch);
		mBourbonLabel.setText(String.valueOf(mPlayer.getInventory().getCurrentBourbons()));
		mBourbonLabel.setAlignment(Align.bottom, Align.left);
		mBourbonLabel.setPosition(11.9f * mCamera.getScale().x - mBourbonLabel.getWidth() / 2, mCamera.getScale().y * 9.55f);
		mBourbonLabel.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
		mStage.addActor(mBourbonLabel);
		
		// Rita ut skjutknapp
		if (mPlayer.getInventory().gotWeapon()) {
			mShootButton.setPosition(shootButton.mPos.x + mCamera.getDisplacement().x, shootButton.mPos.y + mCamera.getDisplacement().y);
			mShootButton.draw(mSpriteBatch);
		}
		
		// Rita ut healingknapp
		if(mPlayer.getInventory().gotBourbon()) {
			if(healingButton.isTouchedDown()) {
				mHealingButton = mHealingButtonTypes[1];
			} else {
				mHealingButton = mHealingButtonTypes[0];
			}
		} else {
			// Disabled
			mHealingButton = mHealingButtonTypes[2];
		}
		
		mHealingButton.setPosition(mCamera.getDisplacement().x + healingButton.mPos.x, healingButton.mPos.y + mCamera.getDisplacement().y);
		mHealingButton.draw(mSpriteBatch);
		
		// Rita ut vapeninfo och pickupknapp
		if (mPlayer.getCanSwapWeapon()) {
			
			if(switchButton.isTouchedDown()) {
				mSwitchButton = mSwitchButtonTypes[1];
			} else {
				mSwitchButton = mSwitchButtonTypes[0];
			}
			mSwitchButton.setPosition(mCamera.getDisplacement().x + switchButton.mPos.x, switchButton.mPos.y + mCamera.getDisplacement().y);
			mSwitchButton.draw(mSpriteBatch);
			
			//Har player ett vapen visa information om det nya vapnets egenskaper
			if (mPlayer.getInventory().gotWeapon() && mPlayer.getAvailableWeaponSwap() != null && mPlayer.getAvailableWeaponSwap().getWeaponType() != Weapons.UNDEFINED) {

                mWeaponInfoBackground.setSize(6.5f, 2.2f);
				mWeaponInfoBackground.setPosition(mCamera.getDisplacement().x + Camera.CAMERA_WIDTH / 2 - mWeaponInfoBackground.getWidth() / 2, mCamera.getDisplacement().y);
				mWeaponInfoBackground.draw(mSpriteBatch);
				
				oldWeapon = WeaponStats.getWeaponStats(mPlayer.getInventory().weapon().getWeaponType());
				newWeapon = WeaponStats.getWeaponStats(mPlayer.getAvailableWeaponSwap().getWeaponType());
				
				wsc = WeaponStatComparer.getComapredStats(oldWeapon, newWeapon);
				
				Label weaponName = new Label(String.valueOf(mPlayer.getAvailableWeaponSwap().getWeaponType()), namestyle);
				weaponName.setAlignment(Align.bottom, Align.right);
				weaponName.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
				weaponName.setPosition((Gdx.graphics.getWidth() / 2) - weaponName.getWidth() / 2, 1.55f * mCamera.getScale().y);
				weaponName.setColor(Color.YELLOW);
				mStage.addActor(weaponName);			

				lbs.font.setScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
				Label dmg = new Label((wsc.mDmg > 0 ? "+" : "" ) + String.valueOf((int)(wsc.mDmg)), lbs);
				Color color = wsc.mDmg < 0 ? Color.RED : Color.GREEN;
				dmg.setColor(color);
				
				mWeaponSwapTable.setPosition((Gdx.graphics.getWidth() / 2) - mWeaponSwapTable.getWidth() / 2, 0.85f * mCamera.getScale().y);
				mWeaponSwapTable.add(dmg).padRight(0.2f * mCamera.getScale().x).align(Align.right);
				mWeaponSwapTable.add(mAttackLabel).align(Align.left);
				mWeaponSwapTable.row();

				Label crit = new Label((wsc.mCrit > 0 ? "+" : "" ) + String.valueOf((int)(wsc.mCrit * 100)) + "%", lbs);
				color = wsc.mCrit < 0 ? Color.RED : Color.GREEN;
				crit.setColor(color);
				
				mWeaponSwapTable.add(crit).padRight(0.2f * mCamera.getScale().x).align(Align.right);
				mWeaponSwapTable.add(mCritChanceLabel).align(Align.left);
				mWeaponSwapTable.row();

				Label critmulti = new Label((wsc.mCritMultiplier > 0 ? "+" : "" ) + String.valueOf(wsc.mCritMultiplier) + "%", lbs);
				color = wsc.mCritMultiplier < 0 ? Color.RED : Color.GREEN;
				critmulti.setColor(color);
				
				mWeaponSwapTable.add(critmulti).padRight(0.2f * mCamera.getScale().x).align(Align.right);
				mWeaponSwapTable.add(mCritDmgLabel).align(Align.left);			
				}
			}
		
		
		mStage.addActor(mWeaponSwapTable);
		mStage.draw();
		mWeaponSwapTable.clear();
		//mWeaponSwapTable.drawDebug(mStage);
		mStage.clear();
	}

}
