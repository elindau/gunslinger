package se.rhel.gunslinger.view;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.CameraLookAt;
import se.rhel.gunslinger.screens.GameScreen;
import se.rhel.gunslinger.tweens.SplashAccessor;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Sine;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class ForegroundRender {

	private SpriteBatch 	mSpriteBatch;
	private Texture 		mForegroundTexture1;
	private Texture			mForegroundTexture2;
	private Sprite 			mForegroundSprite1;
	private Sprite 			mForegroundSprite2;
	private Sprite 			mForegroundShadowSprite;
	private TweenManager 	mManager;
	private CameraLookAt	mLookAt;
	
	private Sprite[]		mParallaxFg;
	
	public ForegroundRender(SpriteBatch spriteBatch, TweenManager manager, CameraLookAt lookAt) {
		mSpriteBatch = spriteBatch;
		mForegroundTexture1 = Resources.getInstance().foregroundGrassTexture1;
		mForegroundTexture2 = Resources.getInstance().foregroundGrassTexture2;
		mLookAt = lookAt;
		
		mForegroundSprite1 = new Sprite(mForegroundTexture1, 0, 0, 160, 24);
		mForegroundSprite1.setSize(Camera.CAMERA_WIDTH, 2f);
		
		mForegroundSprite2 = new Sprite(mForegroundTexture2, 0, 0, 160, 24);
		mForegroundSprite2.setSize(Camera.CAMERA_WIDTH, 2f);
		mForegroundSprite2.setPosition(mLookAt.position().x - Camera.CAMERA_WIDTH / 2, mForegroundSprite2.getY() - 0.3f);
		
		mManager = manager;
		// Tween.registerAccessor(Sprite.class, new SplashAccessor());
		Tween.call(windCallback).start(mManager);
		
		mParallaxFg = new Sprite[] {new Sprite(mForegroundTexture1, 0, 0, 160, 24),
				new Sprite(mForegroundTexture1, 0, 0, 160, 24),
				new Sprite(mForegroundTexture1, 0, 0, 160, 24)};
		mParallaxFg[0].setSize(Camera.CAMERA_WIDTH, 2f);
		mParallaxFg[1].setSize(Camera.CAMERA_WIDTH, 2f);
		mParallaxFg[2].setSize(Camera.CAMERA_WIDTH, 2f);
		
		mParallaxFg[0].setPosition(-Camera.CAMERA_WIDTH, mParallaxFg[0].getY());
		mParallaxFg[1].setPosition(0, mParallaxFg[1].getY());
		mParallaxFg[2].setPosition(Camera.CAMERA_WIDTH, mParallaxFg[2].getY());
	}
	
	public void draw(float delta) {
		// mManager.update(delta);
		
		mForegroundSprite2.setPosition(mLookAt.position().x - Camera.CAMERA_WIDTH / 2, mForegroundSprite2.getY());
		mForegroundSprite1.setPosition(mLookAt.position().x - Camera.CAMERA_WIDTH / 2, mForegroundSprite1.getY());
		
		mForegroundSprite2.draw(mSpriteBatch);
		// mForegroundSprite1.draw(mSpriteBatch);
		//mForegroundShadowSprite.draw(mSpriteBatch);
		
		if(mParallaxFg[1].getX() > mLookAt.position().x) {
			mParallaxFg[2].setPosition(mParallaxFg[0].getX() - Camera.CAMERA_WIDTH, mParallaxFg[2].getY());
			
			Sprite[] clone = mParallaxFg.clone();
			clone[0] = mParallaxFg[2];
			clone[1] = mParallaxFg[0];
			clone[2] = mParallaxFg[1];
			mParallaxFg = clone;
		}
		

		if(mParallaxFg[2].getX() + Camera.CAMERA_WIDTH < mLookAt.position().x + Camera.CAMERA_WIDTH) {
			mParallaxFg[0].setPosition(mParallaxFg[2].getX() + Camera.CAMERA_WIDTH, mParallaxFg[0].getY());
			
			Sprite[] clone = mParallaxFg.clone();
			clone[0] = mParallaxFg[1];
			clone[1] = mParallaxFg[2];
			clone[2] = mParallaxFg[0];
			mParallaxFg = clone;
		}
		
		for(int i = 0; i < mParallaxFg.length; i++) {
			mParallaxFg[i].draw(mSpriteBatch);
		}
	}
	
    private final TweenCallback windCallback = new TweenCallback() {
        @Override
        public void onEvent(int type, BaseTween<?> source) {
            float d = MathUtils.random() * 0.5f + 0.5f;    // duration
            float t = -0.5f * mForegroundSprite1.getHeight();    // amplitude
 
            /*Tween.to(mForegroundSprite1, SplashAccessor.SKEW_X2X3, d)
                .target(t, t)
                .ease(Sine.INOUT)
                .repeatYoyo(1, 0)
                .setCallback(windCallback)
                .start(mManager);*/
            
            Timeline.createParallel()
            	.push(Tween.to(mParallaxFg[1], SplashAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).repeatYoyo(1, 0).setCallback(windCallback))
            	.push(Tween.to(mForegroundSprite2, SplashAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3).repeatYoyo(1, 0))
            	.push(Tween.to(mParallaxFg[0], SplashAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3).repeatYoyo(1, 0))
            	.push(Tween.to(mParallaxFg[2], SplashAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3).repeatYoyo(1, 0))
            	.start(mManager);
        }
    };
    
    
    //F�r menyerna
    public void setSpriteBatch(SpriteBatch sp) {
    	mSpriteBatch = sp;
    }
    
    public TweenManager getManager() {
    	return mManager;
    }
}
