package se.rhel.gunslinger.view;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.Enemy.EnemyState;
import se.rhel.gunslinger.model.entities.Player;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TargetRender {
	private SpriteBatch 	mSpriteBatch;
	private Sprite			mTargetRingSprite;
	private Player			mPlayer;
	
	public TargetRender(SpriteBatch spritebatch, Player player) {
		mSpriteBatch = spritebatch;
		mPlayer = player;
		
		mTargetRingSprite = new Sprite(Resources.getInstance().targetRing);
	}

	public void draw(float delta) {	
		if (mPlayer.getTarget() != null && mPlayer.getTarget().isAlive()) {
			if(mPlayer.getTarget() instanceof Enemy) {
				if(((Enemy)mPlayer.getTarget()).getState() != EnemyState.DYING) {
					mTargetRingSprite.setSize(mPlayer.getTarget().getSize().x * 1.5f , mPlayer.getTarget().getSize().y / 3f);
					mTargetRingSprite.setPosition(mPlayer.getTarget().getPosition().x - mTargetRingSprite.getWidth() / 2,
							mPlayer.getTarget().getPosition().y - mPlayer.getTarget().getSize().y + mTargetRingSprite.getHeight());
					mTargetRingSprite.draw(mSpriteBatch);	
				}
			} else {
				mTargetRingSprite.setSize(mPlayer.getTarget().getSize().x * 1.5f , mPlayer.getTarget().getSize().y / 3f);
				mTargetRingSprite.setPosition(mPlayer.getTarget().getPosition().x - mTargetRingSprite.getWidth() / 2,
						mPlayer.getTarget().getPosition().y - mPlayer.getTarget().getSize().y + mTargetRingSprite.getHeight());
				mTargetRingSprite.draw(mSpriteBatch);	
			}
		}
	}
}
