package se.rhel.gunslinger.view;

import java.util.Random;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.options.Options;

public class SoundManager {

	public enum MusicType {
		GAMETHEME, MENUTHEME, ENDTHEME, ALL;
	}
	
	private Options 	mOptions;
	private Music 		mTheme;
	private Music 		mMenu;
	private Music 		mEnd;
	
	private Sound[] 	mEnemyDeath;
	private Sound[]		mShots;
	private Sound[]		mCoins;
	private Sound[]		mExplosions;
	private Sound[]		mDamageTaken;
	private Sound[]		mExplosionsSmall;
	
	private Sound		mPlayerDeath;
	private Sound		mBourbon;
	private Sound		mGunPickup;
	private Sound		mDenied;
	
	private static final SoundManager INSTANCE = new SoundManager();
	
	public SoundManager() {
		mOptions = new Options();
		mTheme = Resources.getInstance().theme;
		mMenu = Resources.getInstance().menu;
		mEnd = Resources.getInstance().end;
		mEnemyDeath = Resources.getInstance().enemyDeaths;
		mShots = Resources.getInstance().shots;
		mCoins = Resources.getInstance().coinSounds;
		mBourbon = Resources.getInstance().bourbonPickup;
		mExplosions = Resources.getInstance().explosions;
		mDamageTaken = Resources.getInstance().damageTaken;
		mExplosionsSmall = Resources.getInstance().explosions_small;
		mPlayerDeath = Resources.getInstance().playerDeath;
		mGunPickup = Resources.getInstance().gunPickup;
		mDenied = Resources.getInstance().denied;
	}
	
	public void playMusic(boolean looping, float volume, MusicType type) {
		switch(type) {
		case GAMETHEME:
			stopMusic(MusicType.ALL);
			if(mTheme.isPlaying() == false && mOptions.getMusic() != false) {
				mTheme.setLooping(looping);
				mTheme.setVolume(volume);
				mTheme.play();
			}
			break;
		case MENUTHEME:
			stopMusic(MusicType.ALL);
			// Problem p� android ibland ifall ingen null-check
			if(mMenu != null && mOptions != null) {
				if(mMenu.isPlaying() == false && mOptions.getMusic() != false) {
					mMenu.setLooping(looping);
					mMenu.setVolume(volume);
					mMenu.play();
				}	
			}
			break;
		case ENDTHEME:
			stopMusic(MusicType.ALL);
			// Problem p� android ibland ifall ingen null-check
			if(mEnd != null && mOptions != null) {
				if(mEnd.isPlaying() == false && mOptions.getMusic() != false) {
					mEnd.setLooping(looping);
					mEnd.setVolume(volume);
					mEnd.play();
				}	
			}
			break;
			
		case ALL:
			break;
		}
	}
	
	public void pauseMusic(MusicType type) {
		switch(type) {
		case GAMETHEME:
			if(mTheme.isPlaying()) {
				mTheme.pause();
			}
			break;
		case MENUTHEME:
			if(mMenu.isPlaying()) {
				mMenu.pause();
			}
			break;
		case ENDTHEME:
			if(mEnd.isPlaying()) {
				mEnd.pause();
			}
		case ALL:
			if(mMenu.isPlaying()) {
				mMenu.pause();
			}
			if(mTheme.isPlaying()) {
				mTheme.pause();
			}
			if(mEnd.isPlaying()) {
				mEnd.pause();
			}
			break;
		}
	}
	
	public void stopMusic(MusicType type) {
		switch(type) {
		case GAMETHEME:
			if(mTheme.isPlaying()) {
				mTheme.stop();
			}
			break;
		case MENUTHEME:
			if(mMenu.isPlaying()) {
				mMenu.stop();
			}
			break;
		case ENDTHEME:
			if(mEnd.isPlaying()) {
				mEnd.stop();
			}
			break;
		case ALL:
			if(mMenu.isPlaying()) {
				mMenu.stop();
			}
			if(mTheme.isPlaying()) {
				mTheme.stop();
			}
			if(mEnd.isPlaying()) {
				mEnd.stop();
			}
			break;
		}
	}
	
	public boolean isPlaying(MusicType type) {
		boolean b = false;
		
		switch(type) {
		case GAMETHEME:
			b = mTheme.isPlaying();
			break;
		case MENUTHEME:
			b = mMenu.isPlaying();
			break;
		default:
			b = false;
			break;
		}
		return b;
	}
	
	public void playDenied() {
		if(mOptions.getSound()) {
			mDenied.play(mOptions.getSoundVolume());
		}
	}
	
	public void playPlayerDeath() {
		if(mOptions.getSound()) {
			mPlayerDeath.play(mOptions.getSoundVolume());
		}
	}
	
	public void playExplosionSmall() {
		if(mOptions.getSound()) {
			mExplosionsSmall[getRandomPlayNumber(0, mExplosionsSmall.length)].play(mOptions.getSoundVolume());
		}
	}
	
	public void playDamageTaken() {
		if(mOptions.getSound()) {
			mDamageTaken[getRandomPlayNumber(0, mDamageTaken.length)].play(mOptions.getSoundVolume());
		}
	}
	
	public void playCoinPickup() {
		if(mOptions.getSound()) {
			mCoins[getRandomPlayNumber(0, mCoins.length)].play(mOptions.getSoundVolume());
		}
	}
	
	public void playExplosion() {
		if(mOptions.getSound()) {
			mExplosions[getRandomPlayNumber(0, mExplosions.length)].play(mOptions.getSoundVolume());
		}
	}
	
	public void playEnemyDeath() {
		if(mOptions.getSound()) {
			mEnemyDeath[getRandomPlayNumber(0, mEnemyDeath.length)].play(mOptions.getSoundVolume());	
		}
	}
	
	public void playShot() {
		if(mOptions.getSound()) {
			int randomnumber = getRandomPlayNumber(0, mShots.length);
			System.out.println("Shotnr: " + randomnumber);
			mShots[randomnumber].play(mOptions.getSoundVolume());	
		}
	}
	
	public void playBourbonPickup() {
		if(mOptions.getSound()) {
			mBourbon.play(mOptions.getSoundVolume());
		}
	}
	
	public void playGunPickup() {
		if(mOptions.getSound()) {
			mGunPickup.play(mOptions.getSoundVolume());
		}
	}
	
	public static SoundManager getInstance() {
		return INSTANCE;
	}

	public void setVolume(float value) {
		mTheme.setVolume(value);
		mMenu.setVolume(value);
	}
	
	public void setSoundVolume(float value) {
		mOptions.setSoundVolume(value);
	}

	public void dispose() {
		stopMusic(MusicType.ALL);
		mTheme.dispose();
		mMenu.dispose();
		
		for(Sound s : mEnemyDeath) {
			s.dispose();
		}
		for(Sound s : mShots) {
			s.dispose();
		}
		for(Sound s : mCoins) {
			s.dispose();
		}
		for(Sound s : mExplosions) {
			s.dispose();
		}
		for(Sound s : mDamageTaken) {
			s.dispose();
		}
		for(Sound s : mExplosionsSmall) {
			s.dispose();
		}
		
		mPlayerDeath.dispose();
		mBourbon.dispose();
		mGunPickup.dispose();
	}
	
	public int getRandomPlayNumber(int min, int max) {
		// F�r inklusive max max += 1;
		Random rand = new Random();
		int n = (int) (max - min);
		int x = (int) (rand.nextInt(n) + min);
		
		return x;
	}
}
