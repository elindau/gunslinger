package se.rhel.gunslinger.view;

import java.util.ArrayList;
import java.util.Iterator;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.entities.Boss;
import se.rhel.gunslinger.model.entities.BossAI.BossState;
import se.rhel.gunslinger.model.entities.Enemy.EnemyState;
import se.rhel.gunslinger.model.entities.Player.State;
import se.rhel.gunslinger.model.weapons.Cannon.CannonBall;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class BossRender {

	private SpriteBatch 				mSpriteBatch;
	
	// Sprites
	private Sprite 						mCannonBallSprite;
	private Sprite 						mCannonSprite;
	private Sprite 						mShadow;
	private Sprite 						mHealthbar;

	// Animation
	private Animation 					mAnimation;
	private AnimatedSprite 				mExplosion;
	private ArrayList<AnimatedSprite> 	mExplosions = new ArrayList<AnimatedSprite>();
	private AnimatedSprite				mBossRush;
	private AnimatedSprite				mBossTrip;
	private AnimatedSprite				mBossUp;
	private boolean 					mPlaySound = true;;
	
	private BossState					mCurrentState = BossState.RUSHING;
	
	private AnimatedSprite mSprite;
	private AnimatedSprite mMuzzle;
	
	private ArrayList<AnimatedSprite> mMuzzles = new ArrayList<AnimatedSprite>();
	
	public BossRender(SpriteBatch spritebatch) {
		mSpriteBatch = spritebatch;
		
		mBossRush = new AnimatedSprite(Resources.getInstance().boss, "bossrush", 5);
		mBossTrip = new AnimatedSprite(Resources.getInstance().boss, "bosstrip", 3);
		mBossUp = new AnimatedSprite(Resources.getInstance().boss, "bossup", 2);
		
		mBossRush.setAnimationRate(15);
		mBossRush.loop(true);
		
		mBossTrip.setAnimationRate(5);
		mBossTrip.loop(false);
		
		mBossUp.setAnimationRate(2);
		mBossUp.loop(false);
		
		mCannonSprite = new Sprite(Resources.getInstance().cannon, 0, 0, 48, 48);
		mCannonBallSprite = new Sprite(Resources.getInstance().cannon_ball, 0,0, 26,26);
		mShadow = new Sprite(Resources.getInstance().characterShadow);
		mShadow.setColor(0, 0, 0, 0.4f);	
		mHealthbar = new Sprite(Resources.getInstance().health, 0, 0, 1, 6);
		
		mExplosion = new AnimatedSprite(Resources.getInstance().cannon_explosion, "explosion", 10);
		mExplosion.setSize(2f,  2f);
		mExplosion.setAnimationRate(20);
		mExplosion.play();
		mExplosion.loop(false);
	}
	
	public void draw(float delta, Boss b) {
		
		if (mSprite == null)
			mSprite = mBossRush;
		
		if (mCurrentState != b.getBossState()) {
			mCurrentState = b.getBossState();
			
			if (b.getBossState() == BossState.PRETRIP) {
				mSprite = new AnimatedSprite(Resources.getInstance().boss, "bosstrip", 3);
				mSprite.setAnimationRate(5);
				mSprite.loop(false);
			} else if (b.getBossState() == BossState.UP) {
				mSprite = new AnimatedSprite(Resources.getInstance().boss, "bossup", 2);
				mSprite.setAnimationRate(2);
				mSprite.loop(false);
			} else if (b.getBossState() == BossState.RUSHING || b.getBossState() == BossState.WALKING) {
				mSprite = mBossRush;
				mSprite.setAnimationRate(15);
			}
		}
		
		mSprite.update(delta);
		
		if(b.isFacingLeft()) {
			if (!mSprite.isFlipX())
				mSprite.flip(true, false);
		} else {
			if (mSprite.isFlipX())
				mSprite.flip(false, false);
		}
		
		mSprite.setSize(3f, 3f);
		mSprite.setPosition(b.getPosition().x - mSprite.getWidth() / 2f, b.getPosition().y - mSprite.getHeight() / 2f);
		
		if (!mSprite.mIsPlaying)
			mSprite.play();
		
		
		//Kanonen
		mCannonSprite.setSize(1f, 1f);
		mCannonSprite.setPosition(b.getCannon().getPosition().x, b.getCannon().getPosition().y);
		mCannonSprite.draw(mSpriteBatch);
		
		//Dess kulor
		if (b.getCannon().getBalls().size() > 0) {
			for (CannonBall cb : b.getCannon().getBalls()) {
				
				if(cb.getState() == State.SPAWNING) {
					// Kulan har precis spawnat
					SoundManager.getInstance().playExplosionSmall();
					cb.setState(State.DYING);
					
					mMuzzle = new AnimatedSprite(Resources.getInstance().muzzle, "muzzle", 3);
					mMuzzle.setSize(0.8f, 0.8f);
					mMuzzle.setPosition(b.getCannon().getPosition().x + mMuzzle.getWidth() / 4f, b.getCannon().getPosition().y + mCannonSprite.getHeight() / 2 + mMuzzle.getHeight() / 6f);
					mMuzzle.play();
					mMuzzle.setAnimationRate(15);
					mMuzzle.loop(false);
					mMuzzles.add(mMuzzle);
				}
				
				if (cb.isAlive()) {
					if (cb.isFalling()) {
						mShadow.setSize(cb.getSize() * cb.scaleToHitPos(), (cb.getSize() / 3f)* cb.scaleToHitPos());
						mShadow.setPosition(cb.getHitPosition().x - mShadow.getWidth() / 2, cb.getHitPosition().y);
						mShadow.draw(mSpriteBatch);
					}
					mCannonBallSprite.setSize(cb.getSize(), cb.getSize());
					mCannonBallSprite.setPosition(cb.getPosition().x - mCannonBallSprite.getWidth() / 2, cb.getPosition().y - mCannonBallSprite.getHeight() / 2);
					mCannonBallSprite.draw(mSpriteBatch);
				} else {
					mExplosion.setPosition(cb.getHitPosition().x - mExplosion.getWidth() / 2, cb.getHitPosition().y - mCannonBallSprite.getHeight());
					mExplosions.add(mExplosion);
				}
			}
		}
		
		//Muzzleflash kanon
		for (Iterator<AnimatedSprite> it = mMuzzles.iterator(); it.hasNext();) {
			AnimatedSprite as = it.next();
			
			as.update(delta);
			as.draw(mSpriteBatch);
			
			if (as.isFinished()) {
				as.restart();
				it.remove();
			}
		}
		
		//Explosioner
		for (Iterator<AnimatedSprite> it = mExplosions.iterator(); it.hasNext();) {
			AnimatedSprite as = it.next();
			
			if(mPlaySound) {
				SoundManager.getInstance().playExplosion();
				mPlaySound = false;
			}
			as.update(delta);
			as.draw(mSpriteBatch);
			if(as.isFinished()) {
				as.restart();
				it.remove();
				mPlaySound = true;
			}
		}
		
		//Skugga f�r boss
		if (b.getBossState() == BossState.TRIP) {
			mShadow.setSize(mSprite.getWidth(), mSprite.getWidth() / 6f);
			mShadow.setPosition(b.getPosition().x - mShadow.getWidth() / 2f, b.getPosition().y - mShadow.getHeight() / 3f - b.getSize().y / 2.2f);
		} else {
			mShadow.setSize(mSprite.getWidth() / 2, mSprite.getWidth() / 6f);
			mShadow.setPosition(b.getPosition().x - mShadow.getWidth() / 2f, b.getPosition().y - mShadow.getHeight() / 3f - b.getSize().y / 2);
		}
		
		mShadow.draw(mSpriteBatch);
		
		//Boss
		mSprite.draw(mSpriteBatch);
		
		//Healthbar
		mHealthbar.setColor(Color.RED);
		mHealthbar.setSize((float)((float)b.getHealth()/(float)b.getMaxHealth())*b.getSize().x, 0.1f);
		mHealthbar.setPosition(b.getPosition().x - b.getSize().x / 2 + (b.getSize().x-mHealthbar.getBoundingRectangle().width) / 2, b.getPosition().y + b.getSize().y / 2 + mHealthbar.getHeight());
		mHealthbar.draw(mSpriteBatch);
	}
}
