package se.rhel.gunslinger.view;


import java.util.HashMap;
import java.util.Map;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Bourbon;
import se.rhel.gunslinger.model.entities.PickupAbleObject;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.entities.Player.State;
import se.rhel.gunslinger.observers.SoundListener;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

public class PlayerRender implements SoundListener {

	private SpriteBatch 				mSpriteBatch;
	
	// Textures
	private Color						mTint;
	
	private Map<Integer, Player>		mRenderOrder = new HashMap<Integer, Player>();
	
	private TextureAtlas 				mGotoTextureAtlas;
	private AnimatedSprite				mGotoSprite;
	private AnimatedSprite				mMuzzle;
	private AnimatedSprite[]			mDusts = new AnimatedSprite[2];
	private AnimatedSprite				mSmoke;
	private AnimatedSprite[] 			mSmokes = new AnimatedSprite[3]; // Kan vara tre aktiva smokes �t g�ngen
	
	private Map<Integer, AnimatedSprite> mPlayerAnimation, mTombStones, mRespawns, mTakingDamages;
	
	private Vector2 					mLastTarget;
	
	private Player						mLocalPlayer;
	private Sprite						mShadow;

	private Sprite 						mBourbonSprite;
	
	private Map<Integer, Boolean>       mDrawDusts = new HashMap<Integer, Boolean>();
	private Map<Integer, Boolean>		mIsFacingLeft = new HashMap<Integer, Boolean>();
	private Map<Integer, Boolean>		mPlaySounds = new HashMap<Integer, Boolean>();
	private Map<Integer, Boolean>		mDamageTakens = new HashMap<Integer, Boolean>();
	
	private float						mDustElapsed = 0f;
	private float						mDustMaxOffset = 0.2f;
	
	private boolean						mDrawSmoke = false;
	private float						mSmokeSpeedY = 0.7f;
	
	private static final Map<Integer, Color> mPlayerColors;
	static {
		mPlayerColors = new HashMap<Integer, Color>();
		mPlayerColors.put(1, Color.WHITE);
		mPlayerColors.put(2, Color.YELLOW);
		mPlayerColors.put(3, Color.GREEN);
		mPlayerColors.put(4, Color.RED);
	}
	
	public PlayerRender(SpriteBatch spriteBatch, Player localplayer,  Map<Integer, Player> players) {
		mSpriteBatch = spriteBatch;
		
		mTombStones = new HashMap<Integer, AnimatedSprite>();
		mPlayerAnimation = new HashMap<Integer, AnimatedSprite>();
		mRespawns = new HashMap<Integer, AnimatedSprite>();
		mTakingDamages = new HashMap<Integer, AnimatedSprite>();
		
		mLocalPlayer = localplayer;
		mRenderOrder.putAll(players);
		
		for (Player player : players.values()) {
			mDrawDusts.put(player.getID(), false);
			mPlaySounds.put(player.getID(), true);
			mIsFacingLeft.put(player.getID(), player.isFacingLeft());
			mDamageTakens.put(player.getID(), false);
		}
		
		mShadow = new Sprite(Resources.getInstance().characterShadow);
		mShadow.setColor(0, 0, 0, 0.4f);
		
		mGotoTextureAtlas = Resources.getInstance().gotoAtlas;
		mGotoSprite = new AnimatedSprite(mGotoTextureAtlas, "goto", 9);
		mGotoSprite.setSize(1f, 1f);
		mGotoSprite.play();
		mGotoSprite.loop(false);
		mGotoSprite.setAnimationRate(20);
		
		mMuzzle = new AnimatedSprite(Resources.getInstance().muzzle, "muzzle", 3);
		mMuzzle.setSize(0.5f, 0.5f);
		mMuzzle.play();
		mMuzzle.loop(false);
		mMuzzle.setAnimationRate(20);
		
		mSmoke = new AnimatedSprite(Resources.getInstance().smoke, "smoke", 10);
		mSmoke.setSize(0.5f, 0.5f);
		mSmoke.play();
		mSmoke.loop(false);
		mSmoke.setAnimationRate(10);
		
		for(int i = 0; i < 3; i++) {
			AnimatedSprite smoke = new AnimatedSprite(Resources.getInstance().smoke, "smoke", 10);
			smoke.setSize(0.5f, 0.5f);
			smoke.play();
			smoke.loop(false);
			smoke.setAnimationRate(20);
			mSmokes[i] = smoke;
		}
		
		for(int i = 0; i < 2; i++) {
			AnimatedSprite dust = new AnimatedSprite(Resources.getInstance().dust, "dust", 10);
			dust.setSize(0.5f, 0.5f);
			dust.play();
			dust.loop(false);
			dust.setAnimationRate(20);
			mDusts[i] = dust;
		}
		
		mBourbonSprite = new Sprite(Resources.getInstance().borbounTexture, 0, 0, 32, 32);
		mBourbonSprite.setSize(PickupAbleObject.BOURBON_SIZE, PickupAbleObject.BOURBON_SIZE);		
	}
	
	public void draw(float delta, Player player) {
		if (mPlayerAnimation.get(player.getID()) == null) {
			//Annars l�gg till en ny animation
			AnimatedSprite cowboy = new AnimatedSprite(Resources.getInstance().cowboyAtlas, "cowboy", 5);
			cowboy.setAnimationRate(15);
			cowboy.setSize(2f, 2f);
			mPlayerAnimation.put(player.getID(), cowboy);
			
			AnimatedSprite ts = new AnimatedSprite(Resources.getInstance().tombstoneAtlas, "tomb", 6);
			ts.setSize(1f, 2f);
			ts.play();
			ts.loop(false);
			ts.setAnimationRate(10);
			mTombStones.put(player.getID(), ts);
			
			AnimatedSprite rs = new AnimatedSprite(Resources.getInstance().respawnAtlas, "respawn", 9);
			rs.setSize(1f, 2f);
			rs.play();
			rs.loop(false);
			rs.setAnimationRate(10);
			mRespawns.put(player.getID(), rs);
			
			AnimatedSprite td = new AnimatedSprite(Resources.getInstance().playerDamage, "playerdamage", 5);
			td.setSize(1f, 1f);
			td.play();
			td.loop(false);
			td.setAnimationRate(15);
			mTakingDamages.put(player.getID(), td);
		}
		
		//H�mta ut f�rgv�rde
		mTint = mPlayerColors.get(player.getID());
		if(player.state() == Player.State.IDLE) {
			mPlayerAnimation.get(player.getID()).freeze(0);
		} else if(player.state() == Player.State.WALKING){
			if(!mPlayerAnimation.get(player.getID()).mIsPlaying)
				mPlayerAnimation.get(player.getID()).play();
		}
		
		if(player.isFacingLeft()) {
			if (!mPlayerAnimation.get(player.getID()).isFlipX())
				mPlayerAnimation.get(player.getID()).flip(true, false);
		} else {
			if (mPlayerAnimation.get(player.getID()).isFlipX())
				mPlayerAnimation.get(player.getID()).flip(false, false);
		}

		if (player.equals(mLocalPlayer)) {
			// Animering av vart man ska g�
			if(player.state() != State.IDLE) {
				if(mLastTarget == null)
					mLastTarget = player.getGotoPos();
				
				mGotoSprite.setPosition(player.getGotoPos().x - 1f / 2, player.getGotoPos().y);
				mGotoSprite.update(delta);
				mGotoSprite.draw(mSpriteBatch);
				
				if(mLastTarget != player.getGotoPos()) {
					mGotoSprite.restart();
					mLastTarget = player.getGotoPos();
				}
			}
		}
		
		for (Bourbon b : player.getInventory().getBourbon()) {
			if(b.isAlive()) {
				if(b.getPosition() != null) {
					mBourbonSprite.setPosition(b.getPosition().x - Bourbon.BOURBON_SIZE / 2, b.getPosition().y - Bourbon.BOURBON_SIZE / 2);
					mBourbonSprite.draw(mSpriteBatch);
				}
			}
		}		

		if(player.state() == Player.State.DEAD || player.state() == Player.State.DYING) {
			if(mPlaySounds.get(player.getID())) {
				SoundManager.getInstance().playPlayerDeath();
				mPlaySounds.put(player.getID(), false);
			}
			mTombStones.get(player.getID()).setPosition(player.getPosition().x - player.getSize().x / 2, player.getPosition().y - 0.5f);
			mTombStones.get(player.getID()).update(delta);
			mTombStones.get(player.getID()).draw(mSpriteBatch);
			
		} else if(player.state() == Player.State.SPAWNING) {
			mRespawns.get(player.getID()).setPosition(player.getPosition().x - player.getSize().x / 2, player.getPosition().y - player.getSize().y / 2);
			mRespawns.get(player.getID()).update(delta);
			mRespawns.get(player.getID()).draw(mSpriteBatch);

			mPlaySounds.put(player.getID(), true);
		} else {
			mTombStones.get(player.getID()).restart();
			mRespawns.get(player.getID()).restart();
			
			mShadow.setSize(player.getSize().x, player.getSize().x / 3f);
			mShadow.setPosition(player.getPosition().x - mShadow.getWidth() / 2f, player.getPosition().y - mShadow.getHeight() / 2f - player.getSize().y / 2);
			mShadow.draw(mSpriteBatch);
			
			mPlayerAnimation.get(player.getID()).setPosition(player.getPosition().x - Player.SIZE / 2, player.getPosition().y - Player.SIZE / 2);
			mPlayerAnimation.get(player.getID()).update(delta);
			mPlayerAnimation.get(player.getID()).setColor(mTint);
			mPlayerAnimation.get(player.getID()).draw(mSpriteBatch);
			
			if(player.isFiring()) {
				if(mPlaySounds.get(player.getID())) {
					SoundManager.getInstance().playShot();
					mPlaySounds.put(player.getID(), false);
				}
				
				if(player.isFacingLeft()) {
					mMuzzle.setPosition(mPlayerAnimation.get(player.getID()).getX(), mPlayerAnimation.get(player.getID()).getY() + 1f);
					mSmoke.setPosition(mPlayerAnimation.get(player.getID()).getX(), mPlayerAnimation.get(player.getID()).getY() + 1f);
				} else {
					mMuzzle.setPosition(mPlayerAnimation.get(player.getID()).getX() + 1.5f, mPlayerAnimation.get(player.getID()).getY() + 1f);
					mSmoke.setPosition(mPlayerAnimation.get(player.getID()).getX() + 1.5f, mPlayerAnimation.get(player.getID()).getY() + 1f);
				}
				
				mMuzzle.draw(mSpriteBatch);
				mMuzzle.update(delta);
				
				mDrawSmoke = true;
				
			}
			
			if(mMuzzle.isFinished()) {
				mMuzzle.restart();
				mPlaySounds.put(player.getID(), true);
				player.setFiring(false);
			}
			
			// R�k fr�n pistolen ska ritas ut
			if(mDrawSmoke) {
				mSmoke.setPosition(mSmoke.getX(), mSmoke.getY() + mSmokeSpeedY * delta);
				mSmoke.draw(mSpriteBatch);
				mSmoke.update(delta);
			}
			
			if(mSmoke.isFinished()) {
				mSmoke.restart();
				mDrawSmoke = false;
			}
			
			// �r f�rra staten skiljt mot nuvarande
			if(player.getFormerState() != player.state() || mIsFacingLeft.get(player.getID()) != player.isFacingLeft()) {
				// �r nuvarande, nya staten walking
				if(player.state() == Player.State.WALKING) {
					// D� ska lite dust m�las ut
					mDrawDusts.put(player.getID(), true);
					
					if(player.isFacingLeft()) {
						mDusts[0].setPosition(mPlayerAnimation.get(player.getID()).getX() + 1.25f, mPlayerAnimation.get(player.getID()).getY() - 0.5f / 2);
						mDusts[1].setPosition(mPlayerAnimation.get(player.getID()).getX() + 1.25f + 0.2f, mPlayerAnimation.get(player.getID()).getY() - 0.5f / 2);
					} else {
						mDusts[1].setPosition(mPlayerAnimation.get(player.getID()).getX() + 0.5f / 2, mPlayerAnimation.get(player.getID()).getY() - 0.5f / 2);
						mDusts[0].setPosition(mPlayerAnimation.get(player.getID()).getX() + 0.5f / 2 + 0.2f, mPlayerAnimation.get(player.getID()).getY() - 0.5f / 2);
					}
				}
			}
			
			if(mDrawDusts.get(player.getID())) {				
				mDusts[1].draw(mSpriteBatch);
				mDusts[1].update(delta);
				
				mDustElapsed += delta;
				if(mDustElapsed >= mDustMaxOffset) {
					mDusts[0].draw(mSpriteBatch);
					mDusts[0].update(delta);
				}
				
				if(mDusts[0].isFinished() && mDusts[1].isFinished()) {
					mDrawDusts.put(player.getID(), false);
					mDustElapsed = 0f;
					mDusts[1].restart();
					mDusts[0].restart();
				}
			}
			
			if(mDamageTakens.get(player.getID())) {
				mTakingDamages.get(player.getID()).setPosition(player.getPosition().x - 0.5f, player.getPosition().y - 0.5f);
				mTakingDamages.get(player.getID()).update(delta);
				mTakingDamages.get(player.getID()).draw(mSpriteBatch);
			}
			
			if(mTakingDamages.get(player.getID()).isFinished()) {
				mDamageTakens.put(player.getID(), false);
				mTakingDamages.get(player.getID()).restart();
			}
			
			mIsFacingLeft.put(player.getID(), player.isFacingLeft());
		}
	}
	
	@Override
	public void damageTaken(int id) {
		SoundManager.getInstance().playDamageTaken();
		mDamageTakens.put(id, true);
	}
}