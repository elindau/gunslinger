package se.rhel.gunslinger.view;

import se.rhel.gunslinger.model.entities.Player;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Animation {

	private int			mFramesX, mFramesY;
	private int 		mFrameHeight, mFrameWidth;
	private Sprite 		mSprite;
	private SpriteBatch mSpriteBatch;
	
	private float		mMaxTime;
	private float		mTimeElapsed;
	private int 		mCurrentFrameX = 0, mCurrentFrameY = 0;
	private boolean		mRunning = true;
	private boolean		mLooping = true;
	
	private Vector2		mSize;
	
	public Animation(int framesX, int framesY, Sprite sprite, float animationTime, Vector2 size, 
			SpriteBatch spriteBatch) {
		mFrameHeight = (int) sprite.getHeight();
		mFrameWidth = (int) sprite.getWidth();
		mSprite = sprite;
		mMaxTime = animationTime / framesX;
		mFramesX = framesX;
		mFramesY = framesY;
		mSprite.setSize(size.x, size.x);
		mSpriteBatch = spriteBatch;
		mSize = size;
	}
	
	public void draw(float delta, Vector2 pPosition, Color tint) {
		mTimeElapsed += delta;
		
		if(mRunning) {
			if(mTimeElapsed > mMaxTime) {
				mCurrentFrameX++;
				
				if(mCurrentFrameX >= mFramesX) {
					if(mLooping)
						mCurrentFrameX = 0;
				}
				mTimeElapsed = 0f;
			}
		}

		mSprite.setRegion(mCurrentFrameX * mFrameWidth, mCurrentFrameY * mFrameHeight, mFrameWidth, mFrameHeight);
		mSprite.setPosition(pPosition.x - mSize.x / 2 , pPosition.y - mSize.y / 2);
		mSprite.setColor(tint);
		mSprite.draw(mSpriteBatch);
	}
	
	public void addY() {
		mCurrentFrameY = 1;
		if(mCurrentFrameY == mFramesY) {
			mCurrentFrameY = 0;
		}
	}
	
	public void resetY() {
		mCurrentFrameY = 0;
	}
	
	public void freezeAt(int frameX) {
		mRunning = false;
		mCurrentFrameX = frameX;
	}
	
	public void continueAnimation() {
		mRunning = true;
	}
	
	public void setLooping(boolean looping) {
		mLooping = looping;
	}
}
