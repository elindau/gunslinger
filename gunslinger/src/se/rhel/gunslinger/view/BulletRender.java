package se.rhel.gunslinger.view;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.entities.GameObject.State;
import se.rhel.gunslinger.model.weapons.Bullet;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class BulletRender {

	private SpriteBatch 	mSpriteBatch;
	
	private Texture 		mBulletTexture;
	private Sprite 			mBulletSprite;
	
	public BulletRender(SpriteBatch spriteBatch) {
		mSpriteBatch = spriteBatch;
		
		mBulletTexture = Resources.getInstance().bulletTexture;
		mBulletSprite = new Sprite(mBulletTexture, 0, 0, 2, 1);
		mBulletSprite.setSize(Bullet.BULLET_SIZE, 0.1f);
	}
	
	public void draw(float delta, Array<Bullet> bullets) {
		for(Bullet b : bullets) {
			if(b.getState() == State.ACTIVE) {
				mBulletSprite.setPosition(b.getPosition().x - Bullet.BULLET_SIZE / 2, b.getPosition().y - Bullet.BULLET_SIZE / 2);
				mBulletSprite.draw(mSpriteBatch);
			}
		}
	}
}
