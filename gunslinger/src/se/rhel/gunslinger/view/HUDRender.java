package se.rhel.gunslinger.view;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.HUD.HUDText;
import se.rhel.gunslinger.screens.BaseStage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;


public class HUDRender {
	
	private Map<HUDText, Label> mCurrentlyRender = new HashMap<HUDText, Label>();
	private Map<HUDText, Label> mCopy = new HashMap<HUDText, Label>();
	private BitmapFont mHudFont;
	private BaseStage mStage;
	private LabelStyle lbs;
	private Camera mCamera;
	private Random mRand = new Random();
	
	public HUDRender(SpriteBatch spritebatch, Camera camera) {
		mHudFont = new BitmapFont(Gdx.files.internal("data/fonts/hud.fnt"), false);
		lbs = new LabelStyle(new BitmapFont(Gdx.files.internal("data/fonts/roto.fnt"), false), Color.WHITE);
		mStage = new BaseStage(spritebatch);
		mCamera = camera;
	}
	
	public synchronized void draw(float delta) {
		mCurrentlyRender.putAll(mCopy);
		mCopy.clear();
		//FIXME: Har crashat h�r ConcurrentModification, hj�lper synchronized (?)
		for (Iterator<Entry<HUDText, Label>> it = mCurrentlyRender.entrySet().iterator(); it.hasNext();) {
			Map.Entry<HUDText, Label> entry = it.next();
			
			HUDText curText = entry.getKey();
			Label curLabel = entry.getValue();
			
			curLabel.setY(curLabel.getY() + delta * mCamera.getScale().y / 2);
			
			if ((curText.mTime -= delta) < 0) {
				it.remove();
				mStage.getActors().removeValue(curLabel, true);
			}	
		}
		
		mStage.draw();
	}
	
	public synchronized void addHUDText(HUDText obj) {
		int bias = mRand.nextInt(30)-30;
		Label label = new Label(obj.getText(), lbs);
		Vector2 visual = mCamera.getViewCoordinats(new Vector2(obj.getPosition().x, obj.getPosition().y + 1f));
		label.setFontScale(obj.getSize() * Camera.FONT_SCALE_X * 1.5f, obj.getSize() * Camera.FONT_SCALE_Y * 1.5f);
		label.setPosition(visual.x + bias, visual.y);
		label.setColor(obj.getColor());
		mStage.addActor(label);
		mCopy.put(obj, label);
	}
}
