package se.rhel.gunslinger.controller;

import java.util.HashMap;
import java.util.Map;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.ClientWorld;
import se.rhel.gunslinger.model.GUI;
import se.rhel.gunslinger.model.GUI.ButtonType;
import se.rhel.gunslinger.model.GUI.UIButton;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.screens.GameScreen;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

/**
 * Kontrollerar v�rldens kontroller
 * @author Emil
 *
 */
public class WorldController implements InputProcessor {
	
	public static boolean SHOW_FPS;
	public static boolean SHOW_DEBUG;
	
	private GameScreen					mScreen;
	private ClientWorld 				mWorld;
	private Player 						mPlayer;
	private Camera						mCamera;
	private Map<ButtonType, UIButton> 	mUIComponents;
	
	public enum MapKeys {
		LEFT, RIGHT, UP, DOWN;
	}
	
	static Map<MapKeys, Boolean> keys = new HashMap<MapKeys, Boolean>();
	static {
		keys.put(MapKeys.LEFT, false);
		keys.put(MapKeys.RIGHT, false);
		keys.put(MapKeys.UP, false);
		keys.put(MapKeys.DOWN, false);
	}
	
	public WorldController(ClientWorld world, Camera cam, GameScreen screen) {
		mWorld = world;
		mCamera = cam;
		mPlayer = world.player();
		GUI ui = new GUI(mCamera);
		mScreen = screen;
		mUIComponents = ui.getUIComponents();
	}
	
	// Key presses and touches
	public void leftPressed() {
		keys.get(keys.put(MapKeys.LEFT, true));
	}
	
	public void leftReleased() {
		keys.get(keys.put(MapKeys.LEFT, false));
	}
	
	public void rightPressed() {
		keys.get(keys.put(MapKeys.RIGHT, true));
	}
	
	public void rightReleased() {
		keys.get(keys.put(MapKeys.RIGHT, false));
	}
	
	public void upPressed() {
		keys.get(keys.put(MapKeys.UP, true));
	}
	
	public void upReleased() {
		keys.get(keys.put(MapKeys.UP, false));
	}
	
	public void downPressed() {
		keys.get(keys.put(MapKeys.DOWN, true));
	}
	
	public void downReleased() {
		keys.get(keys.put(MapKeys.DOWN, false));
	}
	
	// The current input from user
	public void processCurrentInput() {
//		
//		if(keys.get(MapKeys.LEFT)) {
//			mPlayer.setFacingLeft(true);
//			mPlayer.setState(Player.State.WALKING);
//			mPlayer.getDirection().x = Player.DIRECTION_LEFT;
//		}
//		else if(keys.get(MapKeys.RIGHT)) {
//			mPlayer.setFacingLeft(false);	
//			mPlayer.setState(Player.State.WALKING);
//			mPlayer.getDirection().x = Player.DIRECTION_RIGHT;
//		}
//		
//		if(keys.get(MapKeys.UP)) {
//			mPlayer.setState(Player.State.WALKING);
//			mPlayer.getDirection().y = Player.DIRECTION_UP;
//		}
//		else if(keys.get(MapKeys.DOWN)) {
//			mPlayer.setState(Player.State.WALKING);
//			mPlayer.getDirection().y = Player.DIRECTION_DOWN;
//		}
//		
//		if(!keys.get(MapKeys.DOWN) && !keys.get(MapKeys.UP)) {
//			mPlayer.getVelocity().y = 0;
//		}
//		
//		if(!keys.get(MapKeys.RIGHT) && !keys.get(MapKeys.LEFT)) {
//			mPlayer.getVelocity().x = 0;
//		}
//		
//		// If both or none direction, player is idle
//		if(keys.get(MapKeys.LEFT) && keys.get(MapKeys.RIGHT) ||
//				(!keys.get(MapKeys.LEFT)) && (!keys.get(MapKeys.RIGHT)) &&
//				(!keys.get(MapKeys.DOWN)) && (!keys.get(MapKeys.UP))) {
//			mPlayer.setState(Player.State.IDLE);
//			mPlayer.getVelocity().x = 0;
//			mPlayer.getVelocity().y = 0;
//		}
	}
	
	// ---------------------------------
	// INPUT HANDLING
	// Tells the controller what's happening on the screen
	// TODO: Implement touchDragged event
	// ---------------------------------

	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Keys.LEFT)
			leftPressed();
		if(keycode == Keys.RIGHT)
			rightPressed();
		if(keycode == Keys.UP)
			upPressed();
		if(keycode == Keys.DOWN)
			downPressed();
		if(keycode == Keys.BACK) {
			// Ska visa pausmeny / ska det pausas?
			mScreen.pauseGame();
		}
		if(keycode == Keys.ESCAPE)
			mScreen.pauseGame();
		
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Keys.LEFT)
			leftReleased();
		if(keycode == Keys.RIGHT)
			rightReleased();
		if(keycode == Keys.UP)
			upReleased();
		if(keycode == Keys.DOWN)
			downReleased();
		if(keycode == Keys.F)
			SHOW_FPS = !SHOW_FPS;
		if(keycode == Keys.D)
			SHOW_DEBUG = !SHOW_DEBUG;
		if(keycode == Keys.SPACE)
			mWorld.firePlayerGun();
		if(keycode == Keys.B)
			mWorld.throwPlayerBourbon();
		if(keycode == Keys.A)
			mPlayer.newBourbon();
		if (keycode == Keys.S)
			for (int i = 0; i < 1000; i++) {
			mPlayer.getInventory().newCoin();
		}
			
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		Vector2 clickPos = mCamera.getModelCoordinats(new Vector2(screenX, screenY));

		//St�r player p� ett vapen kolla om trycket �r p� switch knappen
		if (mPlayer.getCanSwapWeapon()) {
			UIButton btn = mUIComponents.get(ButtonType.SWITCH);
			
			if (clickPos.x - mCamera.getDisplacement().x <= btn.mPos.x + btn.mSize.x && clickPos.x - mCamera.getDisplacement().x >= btn.mPos.x
					&& clickPos.y - mCamera.getDisplacement().y <= btn.mPos.y + btn.mSize.y && clickPos.y - mCamera.getDisplacement().y >= btn.mPos.y) {
				mWorld.playerWeaponSwap();
				return true;
			}
		}
		
		//Kolla om trycket �r p� skjut knappen
		UIButton shootbtn = mUIComponents.get(ButtonType.SHOOT);
		if (clickPos.x - mCamera.getDisplacement().x <= shootbtn.mPos.x + shootbtn.mSize.x && clickPos.x - mCamera.getDisplacement().x >= shootbtn.mPos.x
				&& clickPos.y - mCamera.getDisplacement().y <= shootbtn.mPos.y + shootbtn.mSize.y && clickPos.y - mCamera.getDisplacement().y >= shootbtn.mPos.y) {
			mWorld.firePlayerGun();
			return true;
		}
		
		// Kasta bourbon / Healing
		UIButton healingButton = mUIComponents.get(ButtonType.HEALING);
		if (clickPos.x - mCamera.getDisplacement().x <= healingButton.mPos.x + healingButton.mSize.x && clickPos.x - mCamera.getDisplacement().x >= healingButton.mPos.x
				&& clickPos.y - mCamera.getDisplacement().y <= healingButton.mPos.y + healingButton.mSize.y && clickPos.y - mCamera.getDisplacement().y >= healingButton.mPos.y) {
			mWorld.throwPlayerBourbon();
			healingButton.touchDown();
			return true;
		}
		
		 System.out.println("ClickPos X: " + clickPos.x + "ClickPos Y: " + clickPos.y);
		float max = mWorld.levelManager().getCurrentLevel().getCurrentStage().getMaxY();
		float min = mWorld.levelManager().getCurrentLevel().getCurrentStage().getMinY();
		float med = (max + min) / 2;
		//Annars flytta spelaren till positionen han trycker
		
		mPlayer.screenClick(clickPos, med);
		
		// If it's not Android
		if(!Gdx.app.getType().equals(ApplicationType.Android))
			return false;

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		
		// Oavsett vart man touchar 'upp' s� kan ingen knapp
		// vara nedtryckt
		for(UIButton b : mUIComponents.values()) {
			b.touchUp();
		}
		
		// If it's not Android
		if(!Gdx.app.getType().equals(ApplicationType.Android))
			return false;
		
		/*
		if(screenX < mWidth / 2 && screenY > mHeight / 2) {
			leftReleased();
		}
		if(screenX > mWidth / 2 && screenY > mHeight / 2) {
			rightReleased();
		}
		*/
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		mCamera.zoom += amount;
		return true;
	}
}
