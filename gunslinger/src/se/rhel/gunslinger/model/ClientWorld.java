package se.rhel.gunslinger.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.ws.soap.AddressingFeature.Responses;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.controller.WorldController;
import se.rhel.gunslinger.model.HUD.HUDText;
import se.rhel.gunslinger.model.entities.Boss;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.GameObject;
import se.rhel.gunslinger.model.entities.Enemy.EnemyType;
import se.rhel.gunslinger.model.entities.GameObject.State;
import se.rhel.gunslinger.model.entities.GameObject.Type;
import se.rhel.gunslinger.model.entities.PickupAbleObject;
import se.rhel.gunslinger.model.entities.PickupAbleWeapon;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.options.GameState.GameStatus;
import se.rhel.gunslinger.model.weapons.Bullet;
import se.rhel.gunslinger.model.weapons.Cannon;
import se.rhel.gunslinger.model.weapons.Cannon.CannonBall;
import se.rhel.gunslinger.model.weapons.WeaponCreator;
import se.rhel.gunslinger.network.KryoClient;
import se.rhel.gunslinger.network.Network.BossFiringCannon;
import se.rhel.gunslinger.network.Network.EnemyAttack;
import se.rhel.gunslinger.network.Network.EnemyCredentials;
import se.rhel.gunslinger.network.Network.EnemyDead;
import se.rhel.gunslinger.network.Network.EnemyHPUpdate;
import se.rhel.gunslinger.network.Network.EnemyUpdateState;
import se.rhel.gunslinger.network.Network.GameObjectSpawn;
import se.rhel.gunslinger.network.Network.PlayerCanSwitchWeapon;
import se.rhel.gunslinger.network.Network.PlayerHPUpdate;
import se.rhel.gunslinger.network.Network.PlayerJoinLeave;
import se.rhel.gunslinger.network.Network.PlayerPickup;
import se.rhel.gunslinger.network.Network.PlayerShoots;
import se.rhel.gunslinger.network.Network.PlayerState;
import se.rhel.gunslinger.network.Network.PlayerThrow;
import se.rhel.gunslinger.network.Network.PlayerWeaponSwitch;
import se.rhel.gunslinger.network.Network.StageUpdate;
import se.rhel.gunslinger.observers.ClientCollisionListener;
import se.rhel.gunslinger.observers.CollisionListener;
import se.rhel.gunslinger.observers.HUDTextObserver;
import se.rhel.gunslinger.observers.StageObserver;
import se.rhel.gunslinger.screens.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class ClientWorld  {

	protected Player 				mPlayer;
	private FPSLogger 				fps;
	protected LevelManager 			mLevelManager;
	private Stage					mCurrentStage;
	protected Map<Integer, Player> 	mPlayers = new HashMap<Integer, Player>();
	private HUD 					mHud = new HUD();
	private StageObserver 			sObs;
	protected boolean				mBossTime = false;
	protected NetworkUpdate			mNetworkUpdate;
	private GameStatus				mGameState;
	
	//Multiplayer
	private KryoClient mClient;
	
	//Observer
	protected HUDTextObserver hObs;
	
	// Physics
	public World 		mPhysicsWorld;
	
	private boolean 	mIsTimeToChangeScreen = false;
	private Screens		mToScreen;
	
	//Klient konstruktor
	public ClientWorld(KryoClient client) {
		mClient = client;
		
		init();
	}
	
	public void init() {
		mPhysicsWorld = new World(new Vector2(0, 0), true);
		mPhysicsWorld.setContactListener(new ClientCollisionListener());
		fps = new FPSLogger();
		mLevelManager = new LevelManager(mPhysicsWorld, true, null, null);
		mNetworkUpdate = new NetworkUpdate(this);
		
		mGameState = GameStatus.PAUSED;
	}
	
	public void clear(Levels startLevel) {
		mGameState = GameStatus.PAUSED;
		boolean weMustRunThis = true;
		while(!mPhysicsWorld.isLocked() && weMustRunThis) {
			// Ta bort nuvarande fysikv�rd och skapa en ny
			mPhysicsWorld.dispose();
			mPhysicsWorld = new World(new Vector2(0, 0), true);
			mPhysicsWorld.setContactListener(new ClientCollisionListener());

			for(Player p : getPlayers().values()) {
				p.newBody(mPhysicsWorld, new Vector2(5,5));
			}
			weMustRunThis = false;
		}
		
		mLevelManager = new LevelManager(mPhysicsWorld, true, null, null);
		mPlayer.reset(new Vector2(5,5), mPhysicsWorld, false);
		mLevelManager.setStartLevel(startLevel);
		
		mPhysicsWorld.step(1/60f, 6, 2);
		
		mGameState = GameStatus.RUNNING;
	}
	
	public void reset() {
		mBossTime = false;
		
		for (Player p : mPlayers.values()) {
			
		}
	}

	public synchronized void update(float delta) {
		// Mest f�r debugging
		if(WorldController.SHOW_FPS)
			fps.log();
		
		mNetworkUpdate.update(delta);
		if(mGameState == GameStatus.RUNNING) {
			mPhysicsWorld.step(1/60f, 6, 2);
			
			if (Gdx.input.isKeyPressed(Keys.F1)) {
				mLevelManager.getCurrentLevel().initializeLevelDecals(mLevelManager.getCurrentLevel().readLevelDecals(mLevelManager.getCurrentLevel().name()));
			}
			
			if (mClient != null && mPlayer != null) {
				Iterator<Body> bi = mPhysicsWorld.getBodies();
				while(bi.hasNext()) {
					Body b = bi.next();
					
					if (b != null && b.getType() == BodyType.DynamicBody) {
						if (b.getUserData() instanceof Player) {
							Player p = (Player) b.getUserData();
													
							if(p != null) {
								
								//Kolla om hash-map inte inneh�ller player med ID
								if (!mPlayers.containsKey(p.getID())) {
									mPhysicsWorld.destroyBody(b);
								}
								
								//Kolla om p �r den lokala playern
								if (p.equals(mPlayer)) {
									if (p.hasThrownBourbon()) {
										if(mPlayer.tryThrowBourbon(mPlayer.getPosition(), true)) {
											mClient.sendMessageUDP(new PlayerThrow(mPlayer.getID(), mPlayer.getTargetPosition()));
										}
									}
									
									if (p.hasMoved()) {
										updatePlayerState();
									}
								}
												
								p.update(delta);	
							}
						}
						
						if (b.getUserData() instanceof Enemy) {
							Enemy e = (Enemy)b.getUserData();
							
							e.updateMovement();
							
							//FIXME: j�vligt illa
							if (e.getTypeOfEnemy() == EnemyType.BOSS) {
								Boss boss = (Boss)e;
								
								boss.getCannon().update(delta);
							}
							
							if (!e.isAlive()) {
								if(e.getTypeOfEnemy() == EnemyType.BOSS)
									mBossTime = false;
								
								if(e.getTypeOfEnemy() != EnemyType.BOSS && !e.isRanged()) {
									// Ska bara ta bort den extra kroppen vid melee
									mPhysicsWorld.destroyJoint(e.getJoint());
									mPhysicsWorld.destroyBody(e.getWeaponBody());	
								}
								mPhysicsWorld.destroyBody(b);
								
		                          if (mPlayer.getTarget() != null) {
		                          	if (mPlayer.getTarget().equals(e))
		                                  mPlayer.setTarget(null);
		                  		}
							}
						}
						
						// Uppdatera kulor
						if(b.getUserData() instanceof Bullet) {
							// Do something
							Bullet bullet = (Bullet)b.getUserData();
							if(bullet.getState() == State.DEAD) {
								mPhysicsWorld.destroyBody(b);
							} else {
								bullet.update(delta);
							}
						}
						
						// Uppdatera kanonkulor
						if(b.getUserData() instanceof CannonBall) {
							CannonBall ball = (CannonBall)b.getUserData();
							if (!ball.isAlive()) {
								mPhysicsWorld.destroyBody(b);
							}
						}
						
						// Kolla om vi hittar n�gra objekt som ska tas bort
						if(b.getUserData() instanceof Bourbon) {
							Bourbon bon = (Bourbon)b.getUserData();
							if(!bon.isAlive())
								mPhysicsWorld.destroyBody(b);
						}
						
					} else {
						// Statiska kroppar
						if(b != null && b.getUserData() instanceof GameObject) {
							GameObject go = (GameObject) b.getUserData();
							
							if(go != null) {
								if(go.getState() == State.DEAD) {
									mPhysicsWorld.destroyBody(b);
								}
							}
						}
					}
				}
			}
		}
	}

	// ---------------------------------
	// GETTERS AND SETTERS
	// ---------------------------------
	
	public Player player() { return mPlayer; }
	
	public World world(){ return mPhysicsWorld; }

	public LevelManager levelManager() {
        return mLevelManager;
	}
	
	public Map<Integer, Player> getPlayers() {
		return mPlayers;
	}
	
	public Array<Player> getPlayersArray() {
		Array<Player> players = new Array<Player>();
		for(Player p : mPlayers.values()) {
			players.add(p);
		}
		return players;
	}
	
	public void setHUDTextObserver(HUDTextObserver hudTextObs) {
		hObs = hudTextObs;
	}
	
	public void setStageObservers(StageObserver stageObserver) {
		sObs = stageObserver;
	}

	//-----------------------------------
	// MULTIPLAYER METHODS
	//-----------------------------------
	
	public KryoClient getClient() {
		return mClient;
	}
	
	public void updatePlayerState() {
		mClient.sendMessageUDP(mPlayer.getPlayerState());
	}
	
	public void onDisconnect() {
		mClient = null;
		mPlayers.clear();
	}

	public void onConnect(String name) {
		System.out.println("connected as: " + name);
		if (mPlayer == null) {
			mPlayer = new Player(new Vector2(5, 5), mPhysicsWorld, true);
			mPlayer.setID(mClient.mId);
			mPlayers.put(mClient.mId, mPlayer);
		}
	}

	public Player getPlayerById(int id) {
		return mPlayers.get(id);
	}
	
	public synchronized void clientSendMessage(Object msg) {
		mClient.sendMessageUDP(msg);
	}
	
	public void firePlayerGun() {
		if (mPlayer.fireGun(new Vector2(), true)) {
			mClient.sendMessageUDP(new PlayerShoots(mPlayer.getID(), mPlayer.getTargetDirection()));
		}
	}
	
	public void throwPlayerBourbon() {
		mPlayer.setThrowBourbon();
	}
	
	public void stageUpdate(StageUpdate msg) {
		// Resetta spelarna som �r d�da
		for (Player p : mPlayers.values()) {
			System.out.println(p.getID() + "  " + p.isAlive());
			if (!p.isAlive()) {
				p.reset(new Vector2(new Vector2(mLevelManager.getCurrentLevel().getSpecificStage(msg.mStageNumber).getPosition().x, mPlayer.getPosition().y)), mPhysicsWorld, true);
			}
		}
		mPlayer.stageUpdatePosition(new Vector2(mLevelManager.getCurrentLevel().getSpecificStage(msg.mStageNumber).getPosition().x, mPlayer.getPosition().y));	
		
//		if(!mPlayer.isAlive()) {
//			mPlayer.reset(new Vector2(new Vector2(mLevelManager.getCurrentLevel().getSpecificStage(msg.mStageNumber).getPosition().x, mPlayer.getPosition().y)), mPhysicsWorld, true);
//		} else {
//			mPlayer.stageUpdatePosition(new Vector2(mLevelManager.getCurrentLevel().getSpecificStage(msg.mStageNumber).getPosition().x, mPlayer.getPosition().y));	
//		}
		
		sObs.changedStage();
	}

	public void playerWeaponSwap() {
		//Skicka att player vill byta vapen
		mPlayer.setCanSwapWeapon(false);
		mClient.sendMessageUDP(new PlayerWeaponSwitch(mPlayer.getID()));
	}

	/**
	 * Kallas n�r det �r dags att byta screen
	 * @param message
	 */
	public void setScreen(Screens message) {
		System.out.println("TILL SK�RM: " + message);
		mIsTimeToChangeScreen = true;
		mToScreen = message;
	}
	
	/**
	 * Apbra namn
	 * @return
	 */
	public boolean isItTimeToChangeScreen() {
		return mIsTimeToChangeScreen;
	}
	
	public void setIsItTimeToChangeScreen(boolean b) {
		mIsTimeToChangeScreen = b;
	}
	
	public Screens getToScreen() {
		return mToScreen;
	}
	
	public boolean isBossTime() {
		return mBossTime;
	}
	
	public NetworkUpdate getNetworkUpdate() {
		return mNetworkUpdate;
	}
}
