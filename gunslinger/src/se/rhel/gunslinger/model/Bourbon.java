package se.rhel.gunslinger.model;

import se.rhel.gunslinger.model.entities.IEntity;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.tweens.BodyAccessor;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;

import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.TweenPaths;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

/**
 * Representerar objekt som kan heala
 * dig eller medspelare
 * @author Emil
 *
 */
public class Bourbon implements IEntity {

	public static final float BOURBON_SIZE = 0.3f;
	private int mHealAmount = 35;
	private Vector2 mSize = new Vector2(0.3f, 0.3f);
	private Body 			mBody;	
	private TweenManager 	mManager;
	
	public enum State {
		DEAD, ALIVE;
	}
	
	private State mState;
	
	public Bourbon() {
		mState = State.ALIVE;
		
		Tween.setWaypointsLimit(2);
		Tween.registerAccessor(Body.class, new BodyAccessor());
	}
	
	public boolean isAlive() {
		return mState == State.ALIVE ? true : false;
	}
	
	public void kill() {
		// Flaskan kan inte l�ngre kastas
		mState = State.DEAD;
	}
	
	@Override
	public Vector2 getPosition() {
		if(mBody != null)
			return mBody.getPosition();
		return null;
	}
	
	public int getHealAmount() {
		return mHealAmount;
	}

	// Kastar en flaska fr�n en position, till en position
	public void throwBottle(World world, TweenManager manager, Vector2 fromPos, Vector2 toPos) {
		mManager = manager;
		
		// �t vilket h�ll den ska kastas
		if(fromPos.x > toPos.x) {
			fromPos.x -= 1.5f;
		} else if(fromPos.x < toPos.x){
			fromPos.x += 1.5f;
		} 
		
		// Skapa upp fysiken
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		mBody = world.createBody(def);

		PolygonShape poly = new PolygonShape();
		poly.setAsBox(BOURBON_SIZE, BOURBON_SIZE);

		FixtureDef fd = new FixtureDef();
		fd.density = 1f;
		fd.filter.categoryBits = Filter.CATEGORY_OBJECT;
		fd.filter.maskBits = Filter.MASK_OBJECT;
		fd.shape = poly;
		fd.isSensor = true;
				
		mBody.createFixture(fd);
				
		mBody.setBullet(true);
		mBody.setFixedRotation(false);
		mBody.setTransform(fromPos, 0);

		mBody.setUserData(this);
						
		poly.dispose();
		
		System.out.println("Throwing bourbon");
		
		// Koll f�r att f� ut en Quadratic Bezier Curve
		/**
		 * http://en.wikipedia.org/wiki/B%C3%A9zier_curve  
			Quadratic Bezier curves 
			B(t) = (1-t)^2*p0+2(1-t)t*p1+(t^2)*p2 
		 */
		float bX = 5f;
		float bY = 7f;
		
		// Startpunkt
		float startX = mBody.getPosition().x;
		float startY = mBody.getPosition().y;
		
		// Slutpunkt
		float endX = toPos.x;
		float endY = toPos.y;
		
		// F� fram v�rden l�ngs med en kurva
		Array<Vector2> points = new Array<Vector2>();
		for(float t = 0.0f; t <= 1; t+= 0.25f) {
			float x = (float)((1-t)*(1-t)*startX + 2*(1-t)*t*bX+t*t*endX);
			float y = (float)((1-t)*(1-t)*startY + 2*(1-t)*t*bY+t*t*endY);  
			
			// L�gg till v�rden (�ven om pos0 och pos4 �r v�rdel�sa)
			points.add(new Vector2(x, y));
		}
		
		// Skapa tween fr�n ut�ngspunkt till slutpunkt 
		// med hj�lp av pos1-3 fr�n points
		/*
		Timeline.createSequence()
			.push(
					Tween.to(mBody, BodyAccessor.POSITION_XY, 0.1f)
						.target(new float[] {points.get(1).x, points.get(1).y})
						.path(TweenPaths.linear)
						.delay(0.0f)
						)
			.push(
					Tween.to(mBody, BodyAccessor.POSITION_XY, 0.2f)
						.target(new float[] {points.get(2).x, points.get(2).y})
						.path(TweenPaths.linear)
						.delay(0.0f)
						)
			.push(
					Tween.to(mBody, BodyAccessor.POSITION_XY, 0.2f)
						.target(new float[] {points.get(3).x, points.get(3).y})
						.path(TweenPaths.linear)
						.delay(0.0f)
						)
			.push(
					Tween.to(mBody, BodyAccessor.POSITION_XY, 0.1f)
						.target(new float[] {toPos.x, toPos.y})
						.path(TweenPaths.linear)
						.delay(0.0f)
						)
			.start(manager);
		*/
		// Tween away
		Tween.setWaypointsLimit(10);
		Tween.to(mBody, BodyAccessor.POSITION_XY, 0.5f)
			.target(toPos.x, toPos.y)
			.path(TweenPaths.catmullRom)
			.waypoint(new float[] {points.get(1).x, points.get(1).y, points.get(2).x, points.get(2).y, points.get(3).x, points.get(3).y})
			.delay(0.0f)
			.start(manager);
	}

	@Override
	public Vector2 getSize() {
		return mSize;
	}

	@Override
	public int compare(IEntity entity) {
		if (getBottomY() < entity.getBottomY()) return -1;
		else if (getBottomY() > entity.getBottomY()) return 1;
		else return 0;
	}

	@Override
	public float getBottomY() {
		return getPosition().y - getSize().y / 2;
	}
}
