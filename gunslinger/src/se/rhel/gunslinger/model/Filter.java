package se.rhel.gunslinger.model;

public class Filter {
	public static final short CATEGORY_PLAYER = 0x0001;
	public static final short CATEGORY_ENEMY = 0x0002;
	public static final short CATEGORY_SCENERY = 0x0004;
	public static final short CATEGORY_OBJECT = 0x0008;
	public static final short CATEGORY_PLAYER_BULLETS = 0x0010;
	public static final short CATEGORY_ENEMY_BULLETS = 0x0020;
	public static final short CATEGORY_WALLS = 0x0040;
	public static final short CATEGORY_PLAYER_COLLIDER = 0x080;
	// 0x0020
	// 0x0040
	// 0x0080
	
	public static final short MASK_PLAYER = CATEGORY_ENEMY | CATEGORY_OBJECT | CATEGORY_ENEMY_BULLETS | CATEGORY_WALLS;
	public static final short MASK_ENEMY = CATEGORY_PLAYER | CATEGORY_SCENERY | CATEGORY_PLAYER_BULLETS | CATEGORY_ENEMY;
	public static final short MASK_PLAYER_BULLET = CATEGORY_SCENERY | CATEGORY_ENEMY | CATEGORY_WALLS;
	public static final short MASK_ENEMY_BULLET = CATEGORY_PLAYER | CATEGORY_SCENERY | CATEGORY_WALLS;
	public static final short MASK_OBJECT = CATEGORY_PLAYER | CATEGORY_PLAYER_COLLIDER;
	public static final short MASK_WALLS = CATEGORY_PLAYER | CATEGORY_PLAYER_BULLETS | CATEGORY_ENEMY_BULLETS | CATEGORY_OBJECT;
	public static final short MASK_SCENERY = CATEGORY_PLAYER_COLLIDER | CATEGORY_OBJECT;
	public static final short MASK_PLAYER_COLLIDER = CATEGORY_SCENERY | CATEGORY_OBJECT ;
}
