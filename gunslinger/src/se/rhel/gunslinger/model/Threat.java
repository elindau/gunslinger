package se.rhel.gunslinger.model;

public class Threat {
	
	private static final int MAX_THREAT = 200;
	private static final int MIN_THREAT = 0;
	
	private int mCurrentThreat;
	
	public Threat() {
		mCurrentThreat = MIN_THREAT;
	}

	public int getCurrentThreat() {
		return mCurrentThreat;
	}

	public void reset() {
		mCurrentThreat = MIN_THREAT;
	}
	
	public void addThreat(int amount) {
		mCurrentThreat += amount;
		if(mCurrentThreat > MAX_THREAT)
			mCurrentThreat = MAX_THREAT;
	}
}
