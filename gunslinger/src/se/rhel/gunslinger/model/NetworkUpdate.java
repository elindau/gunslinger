package se.rhel.gunslinger.model;

import java.util.ArrayList;
import java.util.Iterator;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.HUD.HUDText;
import se.rhel.gunslinger.model.entities.Boss;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.Enemy.EnemyType;
import se.rhel.gunslinger.model.entities.GameObject;
import se.rhel.gunslinger.model.entities.GameObject.State;
import se.rhel.gunslinger.model.entities.GameObject.Type;
import se.rhel.gunslinger.model.entities.PickupAbleObject;
import se.rhel.gunslinger.model.entities.PickupAbleWeapon;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.weapons.WeaponCreator;
import se.rhel.gunslinger.network.Network.BossFiringCannon;
import se.rhel.gunslinger.network.Network.BossStateType;
import se.rhel.gunslinger.network.Network.EnemyAttack;
import se.rhel.gunslinger.network.Network.EnemyCredentials;
import se.rhel.gunslinger.network.Network.EnemyDead;
import se.rhel.gunslinger.network.Network.EnemyHPUpdate;
import se.rhel.gunslinger.network.Network.EnemyUpdateState;
import se.rhel.gunslinger.network.Network.GameObjectSpawn;
import se.rhel.gunslinger.network.Network.PlayerCanSwitchWeapon;
import se.rhel.gunslinger.network.Network.PlayerHPUpdate;
import se.rhel.gunslinger.network.Network.PlayerJoinLeave;
import se.rhel.gunslinger.network.Network.PlayerPickup;
import se.rhel.gunslinger.network.Network.PlayerShoots;
import se.rhel.gunslinger.network.Network.PlayerState;
import se.rhel.gunslinger.network.Network.PlayerThrow;
import se.rhel.gunslinger.network.Network.StageReset;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class NetworkUpdate {
	
	private ClientWorld mWorld;
	private ArrayList<UpdatePackage> mUpdates = new ArrayList<UpdatePackage>();
	
	public NetworkUpdate(ClientWorld world) {
		mWorld = world;
	}
	
	public synchronized void update(float delta) {
		for (Iterator<UpdatePackage> it = mUpdates.iterator(); it.hasNext();) {
			UpdatePackage up = it.next();
			
			if (up instanceof BossFiringCannon) {
				//Boss skjuter kannonen
				BossFiringCannon bfc = (BossFiringCannon) up;
				Boss b = (Boss) mWorld.mLevelManager.getCurrentLevel().getCurrentStage().enemies().get(bfc.mBossID);
				b.getCannon().fire(bfc.mHitPos);
			}
			else if (up instanceof EnemyCredentials) {
				//Skapa upp enemies
				EnemyCredentials e = (EnemyCredentials) up;
				
				if (e.mType == EnemyType.BOSS) {
					mWorld.mLevelManager.getCurrentLevel().getCurrentStage().addEnemy(new Boss(e.mID, e.mPosition, mWorld.mPhysicsWorld));
					mWorld.mBossTime = true;
				} else {
					mWorld.mLevelManager.getCurrentLevel().getCurrentStage().addEnemy(new Enemy(e.mID, e.mPosition, mWorld.mPhysicsWorld, e.mType));
				}
			}
			else if (up instanceof EnemyAttack) {
				//Attackera med fiende
				EnemyAttack ea = (EnemyAttack) up;
				
				Enemy e = mWorld.mLevelManager.getCurrentLevel().getCurrentStage().enemies().get(ea.mEnemyID);
				if(e != null) {
					e.tryAttack(ea.mDirection);
					e.setFacingLeft(ea.mIsFacingLeft);
				}
			}
			else if (up instanceof EnemyUpdateState) {
				//Uppdatera fiender
				EnemyUpdateState eus = (EnemyUpdateState) up;
				
				Enemy e = mWorld.mLevelManager.getCurrentLevel().getCurrentStage().enemies().get(eus.mEnemyID);
				
				if(e != null) {
					e.updateEnemyState(eus);
					e.updateMovement();	
				}
			}
			else if (up instanceof GameObjectSpawn) {
				//Skapa up GameObjects
				GameObjectSpawn gos = (GameObjectSpawn)	up;
				
				if (gos.mObjType == GameObject.Type.WEAPON) {
					mWorld.mLevelManager.getCurrentLevel().getCurrentStage().addGameObject(
							new PickupAbleWeapon(gos.mId, gos.mPosition, mWorld.mPhysicsWorld, false, gos.mWeaponType));
				} else {
					mWorld.mLevelManager.getCurrentLevel().getCurrentStage().addGameObject(
							new PickupAbleObject(gos.mId, gos.mPosition, gos.mObjType, false, mWorld.mPhysicsWorld));
				}
			}
			else if (up instanceof PlayerThrow) {
				//Kasta bourbon
				PlayerThrow pt = (PlayerThrow) up;
				
				Player p = mWorld.getPlayers().get(pt.mID);
				p.tryThrowBourbon(pt.mThrowToPos, false);
			}
			else if (up instanceof PlayerPickup) {
				//Spelare plockar upp objekt
				PlayerPickup pickup = (PlayerPickup) up;		
				Player p = mWorld.mPlayers.get(pickup.mPlayerID);
				
				// �r det en fejkad pickup?
				if(pickup.isGameState) {
					// M�ste l�ggas till i objekten
					if(pickup.mObjType == Type.WEAPON) {
						System.out.println("VI har f�tt ett vapen! " + pickup.wepType);
						if(mWorld.mPhysicsWorld != null) {
							p.newWeapon(WeaponCreator.createWeapon(pickup.wepType, p, mWorld.mPhysicsWorld));	
						}
					}
					if(pickup.mObjType == Type.COIN) {
						if(pickup.isRemove()) {
							p.getInventory().removeCoins(pickup.removeAmount);
						} else {
							for(int i = 0; i < pickup.howMany; i++) {
								p.getInventory().newCoin();	
							}
						}
					}
					if(pickup.mObjType == Type.BOURBON) {
						for(int i = 0; i < pickup.howMany; i++) {
							p.getInventory().addBourbon();	
						}
					}
				} else {
					// Annars �r det en vanlig pickup
					for (Iterator<GameObject> go = mWorld.mLevelManager.getCurrentLevel().getCurrentStage().objects().iterator(); go.hasNext();) {
						GameObject obj = go.next();
						
						if (obj.getID() == pickup.mObjID) {			
							if(pickup.mObjType == Type.WEAPON) {
								PickupAbleWeapon paw = (PickupAbleWeapon)obj;
								p.newWeapon(WeaponCreator.createWeapon(paw.getWeaponType(), mWorld.mPlayers.get(pickup.mPlayerID), mWorld.mPhysicsWorld));
								
								if (p.getID() == mWorld.mPlayer.getID())
									mWorld.hObs.event(new HUDText("+" +paw.getWeaponType(), 1f, Color.WHITE, mWorld.mPlayer.getPosition(), 1f));
							} else if (pickup.mObjType == Type.BOURBON) {
								p.newBourbon();
								
								if (p.getID() == mWorld.mPlayer.getID())
									if (mWorld.hObs != null)
										mWorld.hObs.event(new HUDText("+" +pickup.mObjType, 1f, Color.WHITE, mWorld.mPlayer.getPosition(), 1f));
							} else if(pickup.mObjType == Type.COIN) {
								p.getInventory().newCoin();
								
								if (p.getID() == mWorld.mPlayer.getID())
									mWorld.hObs.event(new HUDText("+1$", 1f, Color.YELLOW, mWorld.mPlayer.getPosition(), 1f));
							}
							
							obj.setState(State.DEAD);
						}
					}	
				}
			}
			else if (up instanceof PlayerShoots) {
				//Spelare skjuter
				PlayerShoots ps = (PlayerShoots) up;
				
				Player player = mWorld.mPlayers.get(ps.mPlayerID);	
				if (player != null)
					player.fireGun(ps.mDirection, false);
			}
			else if (up instanceof PlayerState) {
				//Spelare f�rflyttar sig
				PlayerState ps = (PlayerState) up;
				
				Player player = mWorld.mPlayers.get(ps.mPlayerID);
				if (player != null) {
					player.setPlayerState(ps);
				}
			}
			else if (up instanceof PlayerHPUpdate) {
				//HP Update
				PlayerHPUpdate phu = (PlayerHPUpdate) up;
				
				Player p = mWorld.mPlayers.get(phu.mPlayerID);
				String message = "";
				float size = 1f;
				Color color = Color.BLACK;
				
				if (phu.mIsDamage) {
					p.hitByEnemy(phu.mAmount);
					
					color = Color.RED;
					if (phu.mIsCrit) { size = 1.5f; }	
					message = "-" + phu.mAmount;
				} else {
					p.hitByBourbon(phu.mAmount);
					color = Color.GREEN;
					message = "+" + phu.mAmount;
				}
				
				// Om spelaren d�r
				if(!p.isAlive()) {
					color = Color.RED;
					size = 2f;
					message = "DEAD";
				}
				
				if (p.getID() == mWorld.mPlayer.getID())
					mWorld.hObs.event(new HUDText(message, 0.5f, color, mWorld.mPlayer.getPosition(), size));
			}
			else if (up instanceof EnemyHPUpdate) {
				EnemyHPUpdate ehu = (EnemyHPUpdate) up;
				
				Enemy e = mWorld.mLevelManager.getCurrentLevel().getCurrentStage().enemies().get(ehu.mEnemyID);
				
				if (ehu.mDamage > 0) {
					e.damageEnemy(ehu.mDamage);
				}
				
				Color color = Color.WHITE;
				float size = 1f;
				
				if (ehu.mIsCrit) {
					color = Color.YELLOW;
					size = 1.5f;
				}

				if (ehu.mShooterID == mWorld.mPlayer.getID()) {
					mWorld.hObs.event(new HUDText(ehu.mDamage < 0 ? "Invulnerable" : String.valueOf(ehu.mDamage), 1f, color, e.getPosition(), size));
				}
			}
			else if (up instanceof PlayerCanSwitchWeapon) {
				PlayerCanSwitchWeapon pcs = (PlayerCanSwitchWeapon) up;
				
				mWorld.mPlayer.setCanSwapWeapon(pcs.mCanSwitchWeapon);
				mWorld.mPlayer.setAvailableWeaponToSwap(new PickupAbleWeapon(-1, Vector2.Zero, mWorld.mPhysicsWorld, false, pcs.mWeaponType));
			}
			else if (up instanceof EnemyDead) {
				EnemyDead ed = (EnemyDead) up;
				
				if(mWorld.mLevelManager.getCurrentLevel().getCurrentStage().enemies().get(ed.mEnemyID) != null)
					mWorld.mLevelManager.getCurrentLevel().getCurrentStage().enemies().get(ed.mEnemyID).setDead(false);
			}
			else if (up instanceof StageReset) {
				//Kallas fr�n servern n�r alla spelare �r d�da och stagen ska d� resettas
				Vector2 resetVector = new Vector2(mWorld.levelManager().getCurrentLevel().getCurrentStage().getPosition().x + Player.SIZE,
						mWorld.levelManager().getCurrentLevel().getCurrentStage().getPosition().y + Camera.CAMERA_HEIGHT / 2);
				mWorld.player().reset(resetVector, mWorld.mPhysicsWorld, true);
			}
			else if (up instanceof BossStateType) {

				BossStateType bst = (BossStateType) up;
				for (Iterator<Enemy> bossit = mWorld.mLevelManager.getCurrentLevel().getCurrentStage().enemies().values().iterator(); bossit.hasNext();) {
					Enemy e = bossit.next();
				
					if (e instanceof Boss) {
						Boss boss = (Boss)e;
						boss.setBossState(bst.mState);
					}
					
				}
			}
			
			it.remove();
		}
	}
	
	public synchronized void addUpdatePackage(UpdatePackage np) {
		mUpdates.add(np);
	}
	
	public void playerJoinLeave(PlayerJoinLeave pjl) {
		if (pjl.mHasJoined) {
			Player newPlayer = new Player(new Vector2(5,5), mWorld.mPhysicsWorld, false);
			newPlayer.setID(pjl.mPlayerID);
			mWorld.mPlayers.put(pjl.mPlayerID, newPlayer);
		} else {
			mWorld.mPlayers.remove(pjl.mPlayerID);
		}
	}
}
