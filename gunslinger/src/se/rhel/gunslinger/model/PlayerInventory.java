package se.rhel.gunslinger.model;

import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.weapons.IWeapon;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class PlayerInventory {

	private World mWorld;
	
	private Array<Bourbon> 	mBourbons;
	private IWeapon			mWeapon;
	private Player			mOwner;
	private int				mCoins;
	
	public PlayerInventory(World world, Player player) {
		mWorld = world;
		mBourbons = new Array<Bourbon>();
		mOwner = player;
		mCoins = 0;
	}
	
	public void setNewWorld(World world) {
		mWorld = world;
	}
	
	public void addWeapon(IWeapon weapon) {
		mWeapon = weapon;
	}
	
	public void addBourbon() {
		mBourbons.add(new Bourbon());
	}
	
	// F�rs�ker kasta iv�g en flaska bourbon
	public boolean throwBourbon(Vector2 from, Vector2 to, TweenManager manager) {
		for(Bourbon b : mBourbons) {
			if(b.isAlive()) {
				b.throwBottle(mWorld, manager, from, to);
				return true;
			} else {
				mBourbons.removeValue(b, false);
			}
		}
		return false;
	}
	
	public void resetCoins() {
		mCoins = 0;
	}
	
	public void removeCoins(int amount) {
		mCoins -= amount;
	}
	
	public void newCoin() {
		mCoins++;
	}
	
	public int getCoins() {
		return mCoins;
	}
	
	public IWeapon weapon() {
		return mWeapon;
	}
	
	public Array<Bourbon> getBourbon() {
		return mBourbons;
	}
	
	public int getCurrentBourbons() {
		int amount = 0;
		for(Bourbon b : mBourbons) {
			if(b.isAlive()) {
				amount++;
			}
		}
		return amount;
	}
	
	public boolean gotWeapon() {
		if(mWeapon == null)
			return false;
		return true;
	}
	
	public boolean gotBourbon() {
		if(mBourbons != null) {
			if(mBourbons.size != 0) {
				for(Bourbon b : mBourbons) {
					if(b.isAlive())
						return true;
				}	
			}
		}
		return false;
	}
}
