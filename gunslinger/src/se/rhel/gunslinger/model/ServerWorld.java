package se.rhel.gunslinger.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.entities.Boss;
import se.rhel.gunslinger.model.entities.CoinCreator;
import se.rhel.gunslinger.model.entities.Enemy;
import se.rhel.gunslinger.model.entities.Enemy.EnemyType;
import se.rhel.gunslinger.model.entities.GameObject;
import se.rhel.gunslinger.model.entities.GameObject.State;
import se.rhel.gunslinger.model.entities.GameObject.Type;
import se.rhel.gunslinger.model.entities.PickupAbleObject;
import se.rhel.gunslinger.model.entities.PickupAbleWeapon;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.options.GameState;
import se.rhel.gunslinger.model.options.GameState.GameStatus;
import se.rhel.gunslinger.model.weapons.Bullet;
import se.rhel.gunslinger.model.weapons.Cannon.CannonBall;
import se.rhel.gunslinger.model.weapons.IWeapon;
import se.rhel.gunslinger.model.weapons.WeaponCreator;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;
import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.network.Network.BossFiringCannon;
import se.rhel.gunslinger.network.Network.BossStateType;
import se.rhel.gunslinger.network.Network.EnemyAttack;
import se.rhel.gunslinger.network.Network.EnemyCredentials;
import se.rhel.gunslinger.network.Network.EnemyDead;
import se.rhel.gunslinger.network.Network.EnemyHPUpdate;
import se.rhel.gunslinger.network.Network.EnemySpawn;
import se.rhel.gunslinger.network.Network.EnemyUpdateState;
import se.rhel.gunslinger.network.Network.GameObjectSpawn;
import se.rhel.gunslinger.network.Network.PlayerCanSwitchWeapon;
import se.rhel.gunslinger.network.Network.PlayerHPUpdate;
import se.rhel.gunslinger.network.Network.PlayerJoinLeave;
import se.rhel.gunslinger.network.Network.PlayerPickup;
import se.rhel.gunslinger.network.Network.PlayerShoots;
import se.rhel.gunslinger.network.Network.PlayerState;
import se.rhel.gunslinger.network.Network.PlayerThrow;
import se.rhel.gunslinger.network.Network.PlayerWeaponSwitch;
import se.rhel.gunslinger.network.Network.ScreenChange;
import se.rhel.gunslinger.network.Network.StageCleared;
import se.rhel.gunslinger.network.Network.StageUpdate;
import se.rhel.gunslinger.network.Network.StageReset;
import se.rhel.gunslinger.observers.CollisionListener;
import se.rhel.gunslinger.observers.NetworkUpdateListener;
import se.rhel.gunslinger.observers.NetworkUpdateObserver;
import se.rhel.gunslinger.observers.ScreenChangeListener;
import se.rhel.gunslinger.observers.ScreenChangeObserver;
import se.rhel.gunslinger.observers.SpawnListener;
import se.rhel.gunslinger.observers.SpawnObserver;
import se.rhel.gunslinger.screens.Screens;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;


public class ServerWorld implements SpawnListener, NetworkUpdateListener, ScreenChangeListener {

	private LevelManager 					mLevelManager;
	private Stage							mCurrentStage;
	private Map<Integer, Player> 			mPlayers = new HashMap<Integer, Player>();
	
	private KryoServer 						mServer;
	
	// Physics
	private World 							mPhysicsWorld;
	
	private float 							sendTimer = 0.1f;
	private boolean							mSendOnce = false;
	private boolean							mFirstAddedPlayer = true;
	
	// Observer
	private SpawnObserver					mSpawnObserver;

	private NetworkUpdateObserver			mNUObs;
	private ScreenChangeObserver			mScreenObserver;
	
	//N�tverkslistor
	private ArrayList<PlayerWeaponSwitch>  	mPws = new ArrayList<PlayerWeaponSwitch>();
	private ArrayList<PlayerThrow> 			mPt = new ArrayList<PlayerThrow>();
	
	// State
	private PlayerPickup					mPlayerStatePickup;
	
	private GameStatus mGameState;
	
	//Server konstruktor
	public ServerWorld(KryoServer server) {
		mServer = server;
		mNUObs = new NetworkUpdateObserver();
		mNUObs.addListener(this);
		
		mPhysicsWorld = new World(new Vector2(0, 0), false);
		mPhysicsWorld.setContactListener(new CollisionListener(mNUObs));
		
		mGameState = GameStatus.PAUSED;

		// init();
	}
	
	public void init(Levels startLevel) {
		
		mGameState = GameStatus.PAUSED;
		boolean weMustRunThis = true;
		while(!mPhysicsWorld.isLocked() && weMustRunThis) {

			// Ta bort nuvarande fysikv�rd och skapa en ny
			mPhysicsWorld.dispose();
			mPhysicsWorld = new World(new Vector2(0, 0), false);
			mPhysicsWorld.setContactListener(new CollisionListener(mNUObs));
			
			// Spelarnas kroppar m�ste tas bort / �terinstansieras
			for(Player p : getPlayers().values()) {
				p.newBody(mPhysicsWorld, new Vector2(5, 5));
			}
			weMustRunThis = false;
		}
		
		mSpawnObserver = new SpawnObserver();
		mSpawnObserver.addListener(this);
		
		mScreenObserver = new ScreenChangeObserver();
		mScreenObserver.addListener(this);
		
		mLevelManager = new LevelManager(mPhysicsWorld, false, mSpawnObserver, mScreenObserver);
		mLevelManager.setStartLevel(startLevel);
		mServer.sendGameObjects();
		
		mGameState = GameStatus.RUNNING;
	}
	
	/**
	 * F�rs�ker resetta server-v�rlden
	 */
	public void reset() {
		for (Iterator<Body> it = mPhysicsWorld.getBodies(); it.hasNext();) {
			Body b = it.next();
			
			if (b != null && (b.getUserData() instanceof Enemy)) {
				((Enemy)b.getUserData()).setDead(false);
			}
		}
		
		mLevelManager.getCurrentLevel().getCurrentStage().reset();	
	}

	public synchronized void update(float delta) {
		
		if(mGameState == GameStatus.RUNNING) {
			mPhysicsWorld.step(1/60f, 6, 2);

			// Uppdatera stagen
			stageUpdate(delta);
			
			// Kolla om alla spelare �r d�da
			deadCheck();
			
			// Cannonballcheck
			updateCannonballs(delta);

			
			for (Iterator<PlayerWeaponSwitch> it = mPws.iterator(); it.hasNext();) {
				PlayerWeaponSwitch pws = it.next();

				Player p = mPlayers.get(pws.mPlayerID);
				
				if (p.getCanSwapWeapon() && p.swapWeapon()) {
					if (p.getInventory().gotWeapon()) {
						IWeapon wep = p.getInventory().weapon();
						
						int id = Id.getInstance().get();
						Vector2 spawnpos = p.isFacingLeft() ? new Vector2(p.getPosition().x + 0.5f, p.getPosition().y - 0.7f) : new Vector2(p.getPosition().x - 1.3f, p.getPosition().y - 0.7f);
						mLevelManager.getCurrentLevel().getCurrentStage().addGameObject(new PickupAbleWeapon(id, spawnpos, mPhysicsWorld, false, wep.getWeaponType()));
						mServer.sendMessage(new GameObjectSpawn(id, spawnpos, Type.WEAPON, wep.getWeaponType()));
						playerWeaponCollision(pws.mPlayerID, false, Weapons.UNDEFINED);
					}
				}
				
				it.remove();
			}
			
			for (Iterator<PlayerThrow> it = mPt.iterator(); it.hasNext();) {
				PlayerThrow pt = it.next();
				
				Player p = mPlayers.get(pt.mID);
				p.tryThrowBourbon(pt.mThrowToPos, false);
				
				it.remove();
			}
				
			Iterator<Body> bi = mPhysicsWorld.getBodies();
			while(bi.hasNext()) {
				Body b = bi.next();
				
				if (b != null && b.getType() == BodyType.DynamicBody) {
					if (b.getUserData() instanceof Player) {
						Player p = (Player) b.getUserData();
												
						if(p != null) {
							
							// Fejka pickup f�r spelarens sparade state
							if(p.isHost() && mPlayerStatePickup != null) {
								mServer.sendMessage(mPlayerStatePickup);
								mPlayerStatePickup = null;
							}
							
							//Kolla om hash-map inte inneh�ller player med ID
							if (!mPlayers.containsKey(p.getID())) {
								mPhysicsWorld.destroyBody(b);
							}
							
							p.update(delta);
							}
						}
					
					// Uppdatera kulor
					if(b.getUserData() instanceof Bullet) {
						// Do something
						Bullet bullet = (Bullet)b.getUserData();
						
						if(bullet.getState() == State.DEAD) {
							mPhysicsWorld.destroyBody(b);
						} else {
							bullet.update(delta);
						}
					}
					
					if(b.getUserData() instanceof CannonBall) {
						CannonBall ball = (CannonBall)b.getUserData();
						if (!ball.isAlive()) {
							//Skapa explosion
							for (Player p : mPlayers.values()) {
								if (ball.isWithingExplosionRadius(p.getPosition())) {
									mNUObs.playerDamaged(p.getID(), ball.getDamage(), false);
								}
							}
							mPhysicsWorld.destroyBody(b);
						} else {
							ball.update(delta);
						}
					}
					
					// Uppdatera fiender
					if(b.getUserData() instanceof Enemy) {
						Enemy e = (Enemy)b.getUserData();
						if (e instanceof Boss) { 
							Boss boss = (Boss)e;
							
							//System.out.println(boss.getBossState());
							if(boss.isFiringCannon()) {
								mServer.sendMessageUDP(new BossFiringCannon(boss.getID(), boss.getCannon().getBalls().get(boss.getCannon().getBalls().size()-1).getHitPosition()));
							}
							
							mServer.sendMessageUDP(new BossStateType(boss.getBossState()));
						}
						
						if(e.isAlive()) {

							if ((sendTimer -= delta) < 0 || e.hasChanged()) {
								sendTimer = sendTimer < 0 ? 0.1f : sendTimer;
								mServer.sendMessageUDP(new EnemyUpdateState(e.getID(), e.getPosition(), e.getState(), e.isAttacking(), e.getDirection()));
							}

							//Kolla om enemy g�r en attack
							if (e.isAttacking()) {
								mServer.sendMessageUDP(new EnemyAttack(e.getID(), e.getBulletDirection(), e.isFacingLeft()));
							}
											
							e.update(delta, getPlayersArray(), levelManager().getCurrentLevel().getCurrentStage());

						} else {
							
							// Lite loot ska spawna - h�rdkodat hur m�nga
							ArrayList<PickupAbleObject> coins = CoinCreator.spawn(e.getPosition(), e.getDropAmount(), mPhysicsWorld);
							
							for(PickupAbleObject c : coins) {
								mLevelManager.getCurrentLevel().getCurrentStage().addGameObject(c);
								mServer.sendMessage(new GameObjectSpawn(c.getID(), c.getPosition(), c.getType()));
							}
							
							// Fiende �r d�d
							if (e.getTypeOfEnemy() != EnemyType.BOSS) {
								if(!e.isRanged()) {
									// Ska bara ta bort den extra kroppen vid melee
									mPhysicsWorld.destroyJoint(e.getJoint());
									mPhysicsWorld.destroyBody(e.getWeaponBody());	
								}
							}
							
							mPhysicsWorld.destroyBody(b);
							mServer.sendMessage(new EnemyDead(e.getID()));
							
							// Ska byta screen om bossen �r d�d
							if(e.getTypeOfEnemy() == EnemyType.BOSS) {
								if(e.getDropAmount() != 0) {
									// Om dropamount �r noll s� �r det moi, programmeraren, som d�dat honom
									mServer.sendMessage(new ScreenChange(Screens.CUTSCENE));
								}
							}
						}
					}
					
					// Kolla om vi hittar n�gra objekt som ska tas bort
					if(b.getUserData() instanceof Bourbon) {
						Bourbon bon = (Bourbon)b.getUserData();
						if(!bon.isAlive())
							mPhysicsWorld.destroyBody(b);
					}
					
				} else {
					// Statiska kroppar
					if(b != null && b.getUserData() instanceof GameObject) {
						GameObject go = (GameObject) b.getUserData();
						
						if(go != null) {
							if(go.getState() == State.DEAD) {
								mPhysicsWorld.destroyBody(b);
							}
						}
					}

				}
			}
		}
	}
	
	private void updateCannonballs(float delta) {
		if(mLevelManager.getLevelID() == Levels.LEVEL2) {
			for(Enemy e : mLevelManager.getCurrentLevel().getCurrentStage().enemies().values()) {
				if(e instanceof Boss) {
					for(CannonBall b : ((Boss)e).getCannon().getBalls()) {
						if(!b.isAlive()) {
							// Skapa expl
							//Skapa explosion
							for (Player p : mPlayers.values()) {
								if (b.isWithingExplosionRadius(p.getPosition())) {
									mNUObs.playerDamaged(p.getID(), b.getDamage(), false);
								}
							}
						} else {
							b.update(delta);
						}
					}
					
				}
			}
		}
	}
	
	private void stageUpdate(float delta) {
		if(mLevelManager != null) {
			if(mLevelManager.getCurrentLevel() != null) {
				mCurrentStage = mLevelManager.getCurrentLevel().getCurrentStage();
			}
				
			// Koll f�r att skicka meddelande om utritning av pil till klient
			if(mLevelManager.isCurrentStageCleared()) {
				if(!mSendOnce) {
					mServer.sendMessage(new StageCleared());
					mSendOnce = true;
				}
			}
		}
		
		for (Map.Entry<Integer, Player> playerEntry : mPlayers.entrySet()) {
			Player player = playerEntry.getValue();
				
			if (player.getPosition().x + player.getSize().x / 2 + 0.1f >= mCurrentStage.getPosition().x + mCurrentStage.length()) {
			
				if (mLevelManager != null && mLevelManager.trySwitchStage()) {

					// Stagen �r bytt och d�rf�r borde vi kolla om det finns n�gra d�da spelare
					for(Player p : mPlayers.values()) {
						if(!p.isAlive()) {
							p.reset(mLevelManager.getCurrentLevel().getCurrentStage().getPosition(), mPhysicsWorld, true);
						}
					}
					 
					mServer.sendMessage(new StageUpdate(mLevelManager.getCurrentLevel().currentStage().getAbbreviation()));
					mSendOnce = false;
				}
			}
		}

		if(mCurrentStage != null) {
			// Uppdatera nuvarande stage
			mCurrentStage.update(delta, getPlayersArray());
			
			if(mCurrentStage.isThereObjects()) {

				for (Iterator<GameObject> it = mCurrentStage.objects().iterator(); it.hasNext();) {
					GameObject obj = it.next();
					
					if(obj.getType() == Type.WEAPON) {
						PickupAbleWeapon paw = (PickupAbleWeapon)obj;
						if(paw.isPickedUp()) {
							paw.setState(State.DEAD);
							
							mServer.sendMessage(new PlayerPickup(1, paw.getPickupID(), paw.getID(), paw.getType(), paw.getWeaponType(), false));
							mPlayers.get(paw.getPickupID()).newWeapon(WeaponCreator.createWeapon(paw.getWeaponType(), mPlayers.get(paw.getPickupID()), mPhysicsWorld));
						}
					} else if(obj.getType() == Type.BOURBON) {
						PickupAbleObject pao = (PickupAbleObject)obj;
						if(pao.isPickedUp()) {
							pao.setState(State.DEAD);
							mServer.sendMessage(new PlayerPickup(1, pao.getPickupID(), pao.getID(), pao.getType(), null, false));
							mPlayers.get(pao.getPickupID()).newBourbon();
						}
					} else if(obj.getType() == Type.COIN) {
						
						PickupAbleObject pao = (PickupAbleObject)obj;
						if(pao.isPickedUp()) {
							pao.setState(State.DEAD);
							mServer.sendMessage(new PlayerPickup(1, pao.getPickupID(), pao.getID(), pao.getType(), null, false));
							mPlayers.get(pao.getPickupID()).getInventory().newCoin();
					}
				}
			}
		}
	}
	}

	/**
	 * Koll om alla spelare �r d�da
	 * @param delta
	 */
	private void deadCheck() {
		boolean allDead = true;
		for(Player p : mPlayers.values()) {
			if(p.isAlive()) {
				allDead = false;
			}
		}
		
		// �r alla spelare d�da
		if(allDead) {
			mServer.sendMessage(new StageReset());
			reset();
			
			for(Player p : mPlayers.values()) {
				Vector2 resetVector = new Vector2(mLevelManager.getCurrentLevel().getCurrentStage().getPosition().x + p.getSize().x,
						mLevelManager.getCurrentLevel().getCurrentStage().getPosition().y + Camera.CAMERA_HEIGHT / 2);
				p.reset(resetVector, mPhysicsWorld, true);
			}
		}
	}
	
	// ---------------------------------
	// GETTERS AND SETTERS
	// ---------------------------------	
	
	public World world(){ return mPhysicsWorld; }

	public LevelManager levelManager() {
        return mLevelManager;
	}
	
	public Map<Integer, Player> getPlayers() {
		return mPlayers;
	}
	
	public Array<Player> getPlayersArray() {
		Array<Player> players = new Array<Player>();
		for(Player p : mPlayers.values()) {
			players.add(p);
		}
		return players;
	}

	//-----------------------------------
	// MULTIPLAYER METHODS
	//-----------------------------------

	public void setStatus(String string) {
		System.out.println(string);
	}

	/**
	 * L�gg till spelare
	 * @param msg
	 */
	public synchronized void addPlayer(PlayerJoinLeave msg) {
		Player newPlayer = new Player(new Vector2(5,5), mPhysicsWorld, false);
		newPlayer.setID(msg.mPlayerID);
		
		
		// Om det �r spelaren som har servern, singleplayer eller multiplayer
		// ska hans state laddas
		if(mFirstAddedPlayer) {
			
			//FIXME: F�r tungt att loopa igenom alla pengar och bourbon f�r att skicka! crashar om de �r 1000tals
			// Spelaren �r host
			newPlayer.setHost(true);
			
			// S�tt state
			GameState state = Resources.getInstance().gameState;
			// Kan vara satt till null om det �r f�rsta g�ngen applikationen k�rs
			if(state != null) {
				if(state.gotGun()) {
					
					// Fejka pickup
					mPlayerStatePickup = new PlayerPickup(1, newPlayer.getID(), Id.getInstance().get(), Type.WEAPON, state.type(), true);
					newPlayer.newWeapon(WeaponCreator.createWeapon(state.type(), newPlayer, mPhysicsWorld));
//					switch(state.type()) {
//					case OMEGAGUN:
//					case SUPERGUN:
//					case GUN:
//						// Fejka pickup
//						mPlayerStatePickup = new PlayerPickup(newPlayer.getID(), Id.getInstance().get(), Type.WEAPON, true);
//						newPlayer.newWeapon(WeaponCreator.createWeapon(state.type(), newPlayer, mPhysicsWorld));
//						break;
//					default:
//						// Do nothing
//						break;
//					}
				} else {
					// Visserligen ska spelaren f� pistolen bara p� f�rsta leveln
					// men genom att k�ra det varenda g�ng s�kerhetsst�ller vi att
					// en spelare aldrig st�r utan pistol
					PlayerPickup p = new PlayerPickup(1, newPlayer.getID(), Id.getInstance().get(), Type.WEAPON, Weapons.GUN, true);
					newPlayer.newWeapon(WeaponCreator.createWeapon(Weapons.GUN, newPlayer, mPhysicsWorld));
					mServer.sendMessage(p);
				}
				
				// Lite fejkad coin-pickup ocks�
				for(int i = 0; i < state.coins(); i++) {
					newPlayer.getInventory().newCoin();	
				}
				PlayerPickup p = new PlayerPickup(state.coins(), newPlayer.getID(), Id.getInstance().get(), Type.COIN, null, true);
				mServer.sendMessage(p);
				
				// .. och bourbon
				for(int i = 0; i < state.bourbons(); i++) {
					newPlayer.getInventory().addBourbon();
				}
				PlayerPickup p2 = new PlayerPickup(state.bourbons(), newPlayer.getID(), Id.getInstance().get(), Type.BOURBON, null,  true);
				mServer.sendMessage(p2);
				
				System.out.println("Coins: " + state.coins());
				System.out.println("Bourbons: " + state.bourbons());
				System.out.println("Levels cleared: " + state.levelsCleared());
			}
			
			mFirstAddedPlayer = false;
		} else {
			// Visserligen ska spelaren f� pistolen bara p� f�rsta leveln
			// men genom att k�ra det varenda g�ng s�kerhetsst�ller vi att
			// en spelare aldrig st�r utan pistol
			PlayerPickup p = new PlayerPickup(1, newPlayer.getID(), Id.getInstance().get(), Type.WEAPON, Weapons.GUN, true);
			newPlayer.newWeapon(WeaponCreator.createWeapon(Weapons.GUN, newPlayer, mPhysicsWorld));
			mServer.sendMessage(p);
		}
		
		mPlayers.put(msg.mPlayerID, newPlayer);
	}


	public synchronized void playerMoved(PlayerState msg) {
		Player player = mPlayers.get(msg.mPlayerID);
		if (player != null) {
			player.setPlayerState(msg);
		}
	}

	public Player getPlayerById(int id) {
		return mPlayers.get(id);
	}

	public synchronized void removePlayer(PlayerJoinLeave msg) {
		mPlayers.remove(msg.mPlayerID);
	}
	
	
	public synchronized void serverSendMessage(Object msg) {
		mServer.sendMessage(msg);
	}

	public synchronized void playerShoots(PlayerShoots msg) {
		Player player = mPlayers.get(msg.mPlayerID);	
		if (player != null)
			player.fireGun(msg.mDirection, false);
	}
	
	public ArrayList<EnemyCredentials> getEnemies() {
		ArrayList<EnemyCredentials> ec = new ArrayList<EnemyCredentials>();
		for (Map.Entry<Integer, Enemy> enemy : mLevelManager.getCurrentLevel().getCurrentStage().enemies().entrySet()) {
			Enemy e = enemy.getValue();
			ec.add(new EnemyCredentials(e.getID(), e.getPosition(), e.getTypeOfEnemy()));
		}
		
		return ec;
	}
	
	public ArrayList<GameObjectSpawn> getGameObjects() {
		ArrayList<GameObjectSpawn> gos = new ArrayList<GameObjectSpawn>();
		
		if(mLevelManager != null) {
			for (GameObject obj : mLevelManager.getCurrentLevel().getCurrentStage().objects()) {
				if (obj.getType() == Type.WEAPON) {
					gos.add(new GameObjectSpawn(obj.getID(), obj.getPosition(), obj.getType(), ((PickupAbleWeapon)obj).getWeaponType()));
				} else {
					gos.add(new GameObjectSpawn(obj.getID(), obj.getPosition(), obj.getType()));
				}
			}	
		}
		
		return gos;
	}

	@Override
	public void enemySpawned() {
		//Skicka worldstate
		mServer.sendMessage(new EnemySpawn(getEnemies()));
	}

	@Override
	public void playerDamaged(int id, int amount, boolean isCrit) {
		Player p = mPlayers.get(id);
		p.hitByEnemy(amount);
		
		mServer.sendMessage(new PlayerHPUpdate(id, amount, isCrit, true));	
	}

	@Override
	public void enemyDamaged(int id, int damage, boolean isCrit, int playerID) {
		if (damage > 0) {
			Enemy e = mLevelManager.getCurrentLevel().getCurrentStage().enemies().get(id);
			e.gunHit(mPlayers.get(playerID), damage);
		}
		mServer.sendMessage(new EnemyHPUpdate(id, damage, isCrit, playerID));
	}

	@Override
	public void playerHealed(int id, int amount) {
		Player p = mPlayers.get(id);
		p.hitByBourbon(amount);
		mServer.sendMessage(new PlayerHPUpdate(id, amount, false, false));
	}

	public void playerThrow(PlayerThrow msg) {
		//Player p = mPlayers.get(msg.mID);
		mPt.add(msg);
		//p.tryThrowBourbon(msg.mThrowToPos);
	}

	public void PlayerSwappedWeapon(PlayerWeaponSwitch msg) {
		mPws.add(msg);
	}
	
	/**
	 * N�r screen ska �ndras,
	 * exempelvis vid level-val
	 */
	@Override
	public void screenChange(Screens toScreen) {
		mServer.sendMessage(new ScreenChange(toScreen));
		// reset();
	}

	@Override
	public void playerWeaponCollision(int id, boolean swapAvailable, Weapons weapon) {
		mServer.sendMessageTo(id, new PlayerCanSwitchWeapon(swapAvailable, weapon));
	}
	
	/**
	 * N�r spelaren har k�pt vapen i shoppen
	 */
	public boolean playerBoughtGun(Weapons type, int cost, int playerId) {
		// Fejka pickup
		PlayerPickup weaponPickup = new PlayerPickup(1, playerId, Id.getInstance().get(), Type.WEAPON, type, true);
		PlayerPickup coinDecrease = new PlayerPickup(playerId, Id.getInstance().get(), Type.COIN, true, cost);
		
		Player p = getPlayerById(playerId);
		if(p != null) {
			p.getInventory().removeCoins(cost);
			p.newWeapon(WeaponCreator.createWeapon(type, p, mPhysicsWorld));
			mServer.sendMessage(weaponPickup);
			mServer.sendMessage(coinDecrease);
			return true;
		}
		return false;
	}

	@Override
	public void screenChanged() {
		// TODO Auto-generated method stub
		
	}
}
