package se.rhel.gunslinger.model.options;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Klass f�r att spara ner anv�ndarens preferenser
 * @author Emil
 *
 */
public class Options {
	
	private static final String PREF_NAME = "myPreferences";
	private static final String PREF_MUSIC = "musicOn";
	private static final String PREF_SOUND = "soundOn";
	private static final String PREF_MUSIC_VOLUME = "musicVolume";
	private static final String PREF_SOUND_VOLUME = "soundVolume";
	
	private static boolean 		DEFAULT_MUSIC = true;
	private static boolean 		DEFAULT_SOUND = true;
	private static float   		DEFAULT_MUSIC_VOLUME = 0.3f;
	private static float   		DEFAULT_SOUND_VOLUME = 0.3f;
	
	private Preferences mPref;
	
	public Options() {
		mPref = Gdx.app.getPreferences(PREF_NAME);
	}
	
	public void save() {
		mPref.flush();
	}
	
	public void setMusic(boolean music) {
		mPref.putBoolean(PREF_MUSIC, music);
		save();
	}
	
	public void setSound(boolean sound) {
		mPref.putBoolean(PREF_SOUND, sound);
		save();
	}
	
	public boolean getMusic() {
		return mPref.getBoolean(PREF_MUSIC, DEFAULT_MUSIC);
	}
	
	public boolean getSound() {
		return mPref.getBoolean(PREF_SOUND, DEFAULT_SOUND);
	}
	
	public float getMusicVolume() {
		return mPref.getFloat(PREF_MUSIC_VOLUME, DEFAULT_MUSIC_VOLUME);
	}
	
	public void setMusicVolume(float volume) {
		mPref.putFloat(PREF_MUSIC_VOLUME, volume);
		save();
	}
	
	public float getSoundVolume() {
		return mPref.getFloat(PREF_SOUND_VOLUME, DEFAULT_SOUND_VOLUME);
	}
	
	public void setSoundVolume(float volume) {
		mPref.putFloat(PREF_SOUND_VOLUME, volume);
		save();
	}
}
