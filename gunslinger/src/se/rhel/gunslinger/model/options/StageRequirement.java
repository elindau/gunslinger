package se.rhel.gunslinger.model.options;

import com.badlogic.gdx.utils.Array;

import se.rhel.gunslinger.model.entities.Enemy.EnemyType;
import se.rhel.gunslinger.model.entities.GameObject;

/**
 * Klass som representerar
 * det som l�ses in fr�n .json filerna
 * @author Emil
 *
 */
public class StageRequirement {

	private int 						NO_OF_ENEMIES;
	private int 						NO_OF_SIGNS;
	private int 						NO_OF_NON_OPTIONAL_OBJECTS;
	private Array<GameObject.Type> 		TYPES;
	private Array<String>				MESSAGES;
	private Array<EnemyType>			ENEMY_TYPE;
	
	public int enemies() {
		return NO_OF_ENEMIES;
	}
	
	public int signs() {
		return NO_OF_SIGNS;
	}
	
	public int objects() {
		return NO_OF_NON_OPTIONAL_OBJECTS;
	}
	
	public Array<GameObject.Type> types() {
		return TYPES;
	}
	
	public Array<String> messages() {
		return MESSAGES;
	}
	
	public Array<EnemyType> enemyTypes() {
		return ENEMY_TYPE;
	}
}
