package se.rhel.gunslinger.model.options;

import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

/**
 * F�r att l�sa / skriva GameState
 * @author Emil
 *
 */
public class GameState {
	
	public static final String FILENAME = "gamestate.json";
	public static final String PATH = "data/state/" + FILENAME;

	private int 	LEVELS_CLEARED;
	private int 	BOURBONS;
	private int		COINS;
	private boolean GUN;
	private Weapons TYPE = Weapons.UNDEFINED;
	
	public GameState() {
		LEVELS_CLEARED = 0;
		BOURBONS = 0;
		GUN = false;
	}
	
	// SETTERS
	public void newLevelCleared(int completedLevelNo) {
		if(completedLevelNo > LEVELS_CLEARED) {
			LEVELS_CLEARED++;
			// Or LEVELS_CLEARED = completedLevelNo, men borde inte vara skillnad
		}
	}
	
	public void modifyCoins(int amount) {
		COINS = amount;
	}
	
	public void modifyBourbon(int amount) {
		BOURBONS = amount;
	}
	
	public void setGun(boolean gun) {
		GUN = gun;
	}
	
	public void setGunType(Weapons wep) {
		TYPE = wep;
	}
	
	// GETTERS
	public int levelsCleared() {
		return LEVELS_CLEARED;
	}
	
	public int coins() {
		return COINS;
	}
	
	public int bourbons() {
		return BOURBONS;
	}
	
	public boolean gotGun() {
		return GUN;
	}
	
	public Weapons type() {
		return TYPE;
	}
	
	public enum GameStatus {
		RUNNING, PAUSED;
	}
	
}
