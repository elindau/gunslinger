package se.rhel.gunslinger.model;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class HUD {
	//TODO: skapa n�gon form av HUD h�r
	
	public HUD() {
		
	}
	
	public static class HUDText {
		private String mText;
		private Vector2 mPosition;
		public float mTime;
		private Color mColor;
		private float mSize;
		
		public HUDText(String text, float displayTime, Color color, Vector2 position, float size) {
			mText = text;
			mTime = displayTime;
			mPosition = position;
			mColor = color;
			mSize = size;
		}
		
		public String getText() {
			return mText;
		}
		
		public Vector2 getPosition() {
			return mPosition;
		}

		public Color getColor() {
			return mColor;
		}

		public float getSize() {
			return mSize;
		}	
	}
}
