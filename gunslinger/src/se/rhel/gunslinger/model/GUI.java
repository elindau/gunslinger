package se.rhel.gunslinger.model;

import java.util.HashMap;
import java.util.Map;
import com.badlogic.gdx.math.Vector2;

public class GUI {

	private static Map<ButtonType, UIButton> 	mUIComponents = new HashMap<ButtonType, UIButton>();
	static {
		mUIComponents.put(ButtonType.SWITCH, UIButton.createButton(new Vector2(6f,2.2f), new Vector2(1f, 1f)));
		mUIComponents.put(ButtonType.SHOOT, UIButton.createButton(new Vector2(13.9f,0.1f), new Vector2(2f, 2f)));
		mUIComponents.put(ButtonType.HEALING, UIButton.createButton(new Vector2(0.1f,0.1f), new Vector2(2f, 2f)));
	}
	
	private Camera mCamera;
	
	public GUI(Camera camera) {
		mCamera = camera;
	}
	
	public Map<ButtonType, UIButton> getUIComponents() {
		return mUIComponents;
	}
	
	public static class UIButton {
		public Vector2 mPos;
		public Vector2 mSize;
		
		private boolean mTouched;

		private UIButton(Vector2 pos, Vector2 size) {
			mPos = pos;
			mSize = size;
		}
		
		public static UIButton createButton(Vector2 pos, Vector2 size) {
			return new UIButton(pos, size);
		}
		
		public boolean isTouchedDown() {
			return mTouched;
		}
		
		public void touchDown() {
			mTouched = true;
		}
		
		public void touchUp() {
			mTouched = false;
		}
	}
	
	public enum ButtonType {
		SHOOT, SWITCH, HEALING;
	}
}
