package se.rhel.gunslinger.model.entities;

import java.util.Random;

import se.rhel.gunslinger.model.Filter;
import se.rhel.gunslinger.model.Stage;
import se.rhel.gunslinger.model.entities.BossAI.BossState;
import se.rhel.gunslinger.model.weapons.Cannon;
import se.rhel.gunslinger.model.weapons.Gun;
import se.rhel.gunslinger.model.weapons.Pitchfork;
import sun.nio.cs.ext.MSISO2022JP;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Array;

public class Boss extends Enemy {

	private World		mPhysicsWorld;
	private Cannon		mCannon;
	protected boolean 	mFiringCannon = false;
	private Random 		rand = new Random();
	protected boolean	mVulnerable = false;
	protected boolean 	mRushing = false;
	protected BossState mBossState = BossState.UP;
	
	public Boss(int id, Vector2 position, World world) {
		super(id, position, world, EnemyType.BOSS);
		mSpawnPosition = position;
		mPhysicsWorld = world;

		mHealth = 350;
		mMaxHealth = mHealth;
		mSize = new Vector2(2f, 3f);
		
		super.SPEED = 1f;
		super.SIZE = 3f;
		
		mCannon = new Cannon(new Vector2(30, 6.3f), world);
		
		resetBoss();
	}
	
	private void resetBoss() {
		reset();
		mHealth = 1000;
		mMaxHealth = mHealth;
	}
	
	@Override
	public void spawn(boolean attachAI) {
		resetBoss();
		//Skapa upp fysiken
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		mBody = mPhysicsWorld.createBody(def);

		PolygonShape poly = new PolygonShape();
		poly.setAsBox(0.75f, 1.5f);

		FixtureDef fd = new FixtureDef();
		
		fd.density = 50f;
		fd.filter.categoryBits = Filter.CATEGORY_ENEMY;
		fd.filter.maskBits = Filter.MASK_ENEMY;
		fd.shape = poly;
		fd.isSensor = true;
		
		mBody.createFixture(fd);
		mBody.setBullet(true);
		mBody.setFixedRotation(true);
		mBody.setTransform(mSpawnPosition, 0);
		mBody.setUserData(this);
		
		poly.dispose();
		
		if (attachAI) {
			mAI = new BossAI(this);
		}
	}

	public void update(float delta, Array<Player> players, Stage stage) {
		super.update(delta, players, stage);

		mCannon.update(delta);
	}
	
	public Cannon getCannon() {
		return mCannon;
	}
	
	public boolean isFiringCannon() {
		if (mFiringCannon) {
			mFiringCannon = false;
			return true;
		}
		return false;
	}
	
	public boolean isRushing() {
		return mBossState == BossState.RUSHING;
	}
	
	public int getRushDmg() {
		return rand.nextInt(20)+20;
	}
	
	public boolean isVulnerable() {
		return mBossState == BossState.TRIP;
	}
	
	public BossState getBossState() {
		return mBossState;
	}
	
	public void setBossState(BossState bs) {
		mBossState = bs;
	}
	
}
