package se.rhel.gunslinger.model.entities;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import se.rhel.gunslinger.model.Stage;
import se.rhel.gunslinger.model.Threat;

public class BaseAI {
	
	private int 					mThreatIgnore;
	protected Player 				mCurrentTarget;
	private Map<Player, Threat> 	mThreatList;
	
	public BaseAI(int threatIgnore) {
		mThreatIgnore = threatIgnore;
		mThreatList = new HashMap<Player, Threat>();
	}
	
	public Player getCurrentTarget() {
		return mCurrentTarget;
	}
	
	/**
	 * Retunera spelare med h�gst threat som �r �ver ignoregr�nsen
	 * @return
	 */
	public Player getPlayerWithHighestThreat() {
		int highestThreat = 0;
		Player player = null;
		
		for(Map.Entry<Player, Threat> threats : mThreatList.entrySet()) {
			
			if(threats.getValue().getCurrentThreat() > highestThreat) {
				highestThreat = threats.getValue().getCurrentThreat();
				// Ignorera threat som �r under 40
				if(highestThreat > mThreatIgnore) {
					if (threats.getKey().isAlive()) {
						player = threats.getKey();
					}
				}
			}
		}
		return player;
	}
	
	/**
	 * Fienden har blivit tr�ffad av en spelare
	 * och ska generera lite threat till den
	 * @param deliveredFrom
	 */
	public void addPlayerToThreat(Player deliveredFrom) {
		
		if(mThreatList.containsKey(deliveredFrom)) {
			// �ka threat p� spelaren
			((Threat)mThreatList.get(deliveredFrom)).addThreat(20);
		} else {
			mThreatList.put(deliveredFrom, new Threat());
		}
	}

	void update(float delta, Array<Player> players, Stage currentStage) {
	}

	public Vector2 getCalculatedTargetDirection() {
		return Vector2.Zero;
	}

	public boolean isRushing() {
		return false;
	}
}
