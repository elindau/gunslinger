package se.rhel.gunslinger.model.entities;

import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class PickupAbleWeapon extends GameObject {

	private Weapons mWeaponType;
	
	public PickupAbleWeapon(int id, Vector2 spawn, World world, boolean isRequired, Weapons weapontype) {
		super(id, spawn, Type.WEAPON, world, isRequired);

		mWeaponType = weapontype;
	}
	
	public Weapons getWeaponType() {
		return mWeaponType;
	}
	
	public void pickUp(int id) {
		super.mCurrentState = State.ACTIVE;
		super.mPickedUpByID = id;
	}

	public int getPickupID() {
		return super.mPickedUpByID;
	}
}
