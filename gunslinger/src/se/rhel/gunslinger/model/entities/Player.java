package se.rhel.gunslinger.model.entities;

import java.util.Iterator;
import se.rhel.gunslinger.model.Filter;
import se.rhel.gunslinger.model.PlayerInventory;
import se.rhel.gunslinger.model.weapons.IWeapon;
import se.rhel.gunslinger.network.Network.PlayerState;
import se.rhel.gunslinger.observers.SoundObserver;
import aurelienribon.tweenengine.TweenManager;
import se.rhel.gunslinger.model.weapons.RangedWeapon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;


public class Player implements ICharacter {

	public enum State {
		IDLE, WALKING, JUMPING, DEAD, DYING, SPAWNING;
	}

	public static final int 	DIRECTION_UP = 1;
	public static final int 	DIRECTION_DOWN = -1;
	public static final int 	DIRECTION_RIGHT = 1;
	public static final int 	DIRECTION_LEFT = -1;
	
	private static final float	OFFSET_Y_CLICK = 1f;

	public static float 		SPEED = 2.5f;
	public static final float 	SIZE = 2f;
	private Vector2 			mSize = new Vector2(1f, 2f);

	private int 				mPlayerId;
	private Vector2 			mVelocity;
	private Vector2 			mDirection;

	private Vector2 			mGotoPos = new Vector2();
	private Vector2 			mOldPos = new Vector2();
	private int 				mOldHealth;
	
	private boolean 			mIsLocal;
	private boolean 			mFacingLeft;
	private State 				mState;
	private State				mOldState;

	private Body 				mBody;
	private Body				mBodySensor;
	private World 				mPhysicsWorld;
	private Fixture 			mSensorFixture;
	private Fixture 			playerPhysicsFixture;
	
	private ICharacter 			mTarget = null;

	private boolean 			mLock;
	private boolean 			mIsHost = false;
	private int 				mHealth;
	
	private float 				baseCrit = 0.1f; //10%
	
	private boolean 			mSwappedWeapon = false;
	private float 				mWeaponSwapTime = 0f;
	
	// Dying time
	private float				mDyingMaxTime = 3f;
	private float				mDyingElapsedTime = 0f;
	private PostReset			mPostReset;	
	private boolean 			mIsTherePostReset = false;
	
	// Spawning time
	private float				mSpawningMaxTime = 0.7f;
	private float				mSpawnElapsedTime = 0f;
	
	// G� in i obstacletimer
	private float				mObstacleMaxTime = 0.5f;
	private float				mObstacleElapsedTime = 0f;
	
	// Spelarens inventory
	private PlayerInventory 	mInventory;
	private TweenManager 		mManager;
	
	private boolean 			mCanSwapWeapon = false;
	private PickupAbleWeapon 	mAvailableWeaponSwap;
	private boolean 			mThrownBourbon = false;
	
	private boolean				mFired = false;
	
	private SoundObserver		mSoundObserver;
	
	private float				mFiringTimeElapsed = 0f;
	private float				mFiringMaxTime = 0.5f;
	private boolean				mIsAllowedToFire = false;
	
	private float				mThrowingTimeElapsed = 0f;
	private float				mThrowingMaxTime = 0.5f;
	private boolean				mIsAllowedToThrow = false;
	
	// Reload
	private float 				mReloadTimeElapsed = 0f;
	private float 				mReloadMaxTime = 1.5f;
	private float				mReloadFinalTimeElapsed = 0f;
	private float				mReloadFinalMaxTime = 2.0f;
	private static final int	MAX_NUMBER_BULLETS = 6;
	private static final int    MIN_NUMBER_BULLETS = 0;
	private int					mCurrentBullets = 6;
	
	// Combat-memory
	private float				mCombatTimeElapsed = 0f;
	private float				mComatMaxTime = 1f;
	private boolean				mInCombat = false;
	
	
	public Player(Vector2 position, World world, boolean isLocal) {
		reset(position, world, false);
		mIsLocal = isLocal;
		mInventory = new PlayerInventory(world, this);
		mManager = new TweenManager();
	}
	
	/**
	 * Hj�lpklass vid sl�pande reset
	 * @author Emil
	 *
	 */
	private class PostReset {
		public Vector2 	POSITION_RESET;
		public World	WORLD_RESET;
		public PostReset(Vector2 pos, World world) {
			POSITION_RESET = pos;
			WORLD_RESET = world;
		}
	}
	
	/**
	 * Metod f�r att skapa body till en spelare
	 * @param mPhysicsWorld2
	 */
	public void newBody(World world, Vector2 position) {
		// Physics
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		mBody = world.createBody(def);
		mBodySensor = world.createBody(def);
		
		//Collisionbody
		CircleShape circle = new CircleShape();
		circle.setPosition(new Vector2(0, -.75f));
		circle.setRadius(.25f);				

		FixtureDef fd = new FixtureDef();
		fd.density = 1f;
		fd.isSensor = false;
		fd.shape = circle;
		fd.friction = 0f;
		fd.filter.categoryBits = Filter.CATEGORY_PLAYER_COLLIDER;
		fd.filter.maskBits = Filter.MASK_PLAYER_COLLIDER;
		
		mBody.createFixture(fd);
		
		//Sensor
		PolygonShape poly = new PolygonShape();
		poly.setAsBox(0.5f, 1f);
		fd.isSensor = true;
		fd.shape = poly;
		fd.filter.categoryBits = Filter.CATEGORY_PLAYER;
		fd.filter.maskBits = Filter.MASK_PLAYER;
		
		mBody.createFixture(fd);
		
		mBody.setBullet(true);
		mBody.setFixedRotation(true);
		mBody.setTransform(position, 0);
		mBody.setUserData(this);
		
		circle.dispose();
		poly.dispose();
		
		mPhysicsWorld = world;
		
		if(getInventory() != null) {
			getInventory().setNewWorld(mPhysicsWorld);
			// M�ste �ndra referenserna hos objekten
			if(getInventory().gotWeapon()) {
				IWeapon g = getInventory().weapon();
				((RangedWeapon) g).setNewWorld(mPhysicsWorld);	
			}	
		}
		
		mState = State.SPAWNING;
		mHealth = 100;
	}
	
	public void reset(Vector2 position, World world, boolean hasDied) {
		// Ska inte resetas under Dying-state
		if(mState == State.DYING) {
			mPostReset = new PostReset(position, world);
			mIsTherePostReset = true;
		} else {
			
			if(mBody != null) {
				// world.destroyBody(mBody);
				mBody.setTransform(position, 0);
				mVelocity = Vector2.Zero;
				mTarget = null;
				mGotoPos = new Vector2(mBody.getPosition().x, mBody.getPosition().y - OFFSET_Y_CLICK);
			} else {
				newBody(world, position);
			}
			
			if(hasDied) {
				getInventory().resetCoins();	
			}
			
			mDyingElapsedTime = 0f;
			mSpawnElapsedTime = 0f;
			mCombatTimeElapsed = 0f;
			mInCombat = false;
			
			mDirection = new Vector2();
			mFacingLeft = false;
			mState = State.SPAWNING;
			mVelocity = new Vector2(0, 0);
			mPhysicsWorld = world;
			mHealth = 100;
			
			mGotoPos = new Vector2(mBody.getPosition().x, mBody.getPosition().y - OFFSET_Y_CLICK);
		}
	}
	
	public void setObserver(SoundObserver sObs) {
		mSoundObserver = sObs;
	}


	public void update(float delta) {
		mBodySensor.setTransform(mBody.getPosition(), 0);
		
		mOldState = mState;
		
		if(isLocked()) {
			SPEED = 0;
		} else {
			SPEED = 2.5f;
		}
		
		if(isUpdateAble()) {
			mOldHealth = mHealth;
			
			// Flytta spelaren till goto-pos eller ocks� idle
			float fakeY = mBody.getPosition().y - OFFSET_Y_CLICK;
			if((mBody.getPosition().x < mGotoPos.x + 0.1f && mBody.getPosition().x > mGotoPos.x - 0.1f) &&
					(fakeY < mGotoPos.y + 0.1f && fakeY > mGotoPos.y - 0.1f)) {
				mDirection.set(0f, 0f);
				if(mState != State.DYING && mState != State.SPAWNING) {
					mState = State.IDLE;
				}
			} else {
				Vector2 gotop = new Vector2(mBody.getPosition().x, fakeY);
				mDirection.set(mGotoPos.cpy().sub(gotop));
				mState = State.WALKING;
			}
			
			mVelocity.set(mDirection.nor().scl(SPEED));
			mBody.setLinearVelocity(mVelocity);

			//S�tt facing beroende p� om man har target eller ej
			if (mTarget != null && !(mTarget instanceof Player)) {
				if (mTarget.getPosition().x > mBody.getPosition().x)
					mFacingLeft = false;
				else
					mFacingLeft = true;
			} else {
				if (mVelocity.x < 0)
					mFacingLeft = true;
				else if (mVelocity.x > 0)
					mFacingLeft = false;
			}
			
			//fulhack f�r att undvika vapenduplicering
			if (mSwappedWeapon) {
				if((mWeaponSwapTime -= delta) < 0) {
					mSwappedWeapon = false;
				}
			}
		} else {
			mBody.setLinearVelocity(Vector2.Zero);
		}
		
		// N�r spelaren resettas
		if(mState == State.SPAWNING) {
			mSpawnElapsedTime += delta;
			if(mSpawnElapsedTime > mSpawningMaxTime) {
				mState = State.IDLE;
			}
		}
		
		// Anv�nds State.Dying n�r tombstone ska visas
		if(mState == State.DYING) {
			mDyingElapsedTime += delta;
			if(mDyingElapsedTime >= mDyingMaxTime) {
				mState = State.DEAD;
			}
		}
		
		// Finns det en sl�pande reset?
		//System.out.println("POSTRESET: " + mIsTherePostReset);
		if(mState == State.DEAD && mIsTherePostReset) {
			reset(mPostReset.POSITION_RESET, mPostReset.WORLD_RESET, true);
			mPostReset = null;
			mIsTherePostReset = false;
		}
		
		// Koll mot n�r spelare f�r skjuta
		mFiringTimeElapsed += delta; 
		if(mFiringTimeElapsed > mFiringMaxTime) {
			mIsAllowedToFire = true;
		}
		
		// Koll mot n�r spelare f�r kasta bourbon
		mThrowingTimeElapsed += delta;
		if(mThrowingTimeElapsed > mThrowingMaxTime) {
			mIsAllowedToThrow = true;
		}
		
		// Reload f�rfarande
		if(mCurrentBullets != MAX_NUMBER_BULLETS) {
			if(mCurrentBullets == MIN_NUMBER_BULLETS) {
				// Inga kulor ska resetta alla efter lite l�ngre tid
				mReloadFinalTimeElapsed += delta;
				if(mReloadFinalTimeElapsed > getReloadFinalMaxTime()) {
					mReloadFinalTimeElapsed = 0f;
					mCurrentBullets = MAX_NUMBER_BULLETS;
				}
			} else {
				// Annars l�gg till ny kula d� och d�
				mReloadTimeElapsed += delta;
				if(mReloadTimeElapsed > mReloadMaxTime) {
					mReloadTimeElapsed = 0f;
					mCurrentBullets++;
					if(mCurrentBullets > MAX_NUMBER_BULLETS) {
						mCurrentBullets = MAX_NUMBER_BULLETS;
					}
				}	
			}
		}
		
		// Koll om spelaren g�tt p� samma st�lle f�r l�nge, ex g�tt in i v�gg
		if(mState == State.WALKING) {
			// Om gamla positionen �r snarlik nya 
			if((mOldPos.x == mBody.getPosition().x) ||
					(mOldPos.y == mBody.getPosition().y)) {
				// .. starta timer
				mObstacleElapsedTime += delta;
				if(mObstacleElapsedTime > mObstacleMaxTime) {
					mObstacleElapsedTime = 0f;
					// och i s�dana fall s�tt nuvarande position till goto-pos
					mGotoPos = new Vector2(mBody.getPosition().x, mBody.getPosition().y - OFFSET_Y_CLICK);
				}
			}
		}
		
		// Combat
		if(mInCombat) {
			mCombatTimeElapsed += delta;
			if(mCombatTimeElapsed > mComatMaxTime) {
				mCombatTimeElapsed = 0f;
				mInCombat = false;
			}	
		}
		
		mOldPos = mBody.getPosition().cpy();

		// System.out.println("PLAYER STATE: " + mState + " !!!!!");

		mManager.update(delta);
	}
	
	public boolean isInCombat() {
		return mInCombat;
	}
	
	public float getReloadFinalMaxTime() {
		if(mInCombat) {
			return 3f;
		} else {
			return 1.5f;
		}
	}

	public boolean hasMoved() {
		return mOldPos.x != mBody.getPosition().x || mOldPos.y != mBody.getPosition().y;
	}

	public Vector2 getPosition() {
		return mBody.getPosition();
	}

	public Vector2 getVelocity() {
		return mVelocity;
	}

	public void setFacingLeft(boolean facing) {
		mFacingLeft = facing;
	}

	public boolean isFacingLeft() {
		return mFacingLeft;
	}

	public void setState(State state) {
		mState = state;
	}

	public State state() {
		return mState;
	}

	public Vector2 getDirection() {
		return mDirection;
	}
	
	// Ska spelarens position etc. uppdateras
	private boolean isUpdateAble() {
		if(mState != State.DEAD && mState != State.DYING && mState != State.SPAWNING) {
			return true;
		}
		return false;
	}
	
	public void moveTo(Vector2 clickPosition, boolean teleport) {
		if(teleport) {
			mBody.setTransform(clickPosition, 0);
			mGotoPos = new Vector2(mBody.getPosition().x, mBody.getPosition().y - OFFSET_Y_CLICK);
		} else {
			mGotoPos = clickPosition;
		}
	}
	
	public Vector2 getGotoPos() {
		return mGotoPos;
	}

	public void setPlayerState(PlayerState msg) {
		mBody.setTransform(msg.mPosition, 0);
		mDirection.set(msg.mDirection);
		mGotoPos = msg.mGotoPos;
		mFacingLeft = msg.mIsFacingLeft;
	}

	public PlayerState getPlayerState() {
		return new PlayerState(mPlayerId, mBody.getPosition(), mDirection, mGotoPos, mFacingLeft);
	}

	public void setID(int playerId) {
		mPlayerId = playerId;
	}

	public int getID() {
		return mPlayerId;
	}

	public void setLock(boolean state) {
		mLock = state;
	}

	public boolean isLocked() {
		return mLock;
	}
	
	public int getCurrentBullets() {
		return mCurrentBullets;
	}

	// N�r spelare g�r p� vissa objekt ska dessa
	// l�ggas till i hans 'inventory' s�
	// att de kan anv�ndas
	public PlayerInventory getInventory() {
		return mInventory;
	}
	
	public void newWeapon(IWeapon weapon) {
		mInventory.addWeapon(weapon);
	}

	public void setTarget(ICharacter target) {
		mTarget = target;
	}
	
	public ICharacter getTarget() {
		return mTarget;
	}

	public void newBourbon() {
		System.out.println("ADDAR BOURBON TILL ID: " + mPlayerId);
		mInventory.addBourbon();
	}

	public boolean fireGun(Vector2 direction, boolean isFromLocal) {
		if (mInventory.gotWeapon()) {
			
			// Ska kolla hur m�nga kulor spelaren har innan reload kr�vs
			// samt att han inte kan skjuta f�r ofta
			if(isFromLocal) {
				if(mIsAllowedToFire) {
					mFiringTimeElapsed = 0f;
					mIsAllowedToFire = false;
				} else {
					return false;
				}
			}
			
			if(mCurrentBullets <= MIN_NUMBER_BULLETS) {
				return false;
			}
			
			Vector2 bulletDir = direction;
			if (mTarget != null && !(mTarget instanceof Player))
				bulletDir = mTarget.getPosition().cpy().sub(mBody.getPosition());
			
			Vector2 spawnPos = getPosition();
			spawnPos.y += 0.25f;
			if(isFacingLeft()) {
				spawnPos.x -= 1f;
			} else {
				spawnPos.x += 1f;
			}
			
			mCurrentBullets--;
			if(mCurrentBullets < MIN_NUMBER_BULLETS) {
				mCurrentBullets = MIN_NUMBER_BULLETS;
			}
			mInventory.weapon().attack(spawnPos, isFacingLeft(), bulletDir, baseCrit);
			setFiring(true);
			return true;
		}
		return false;
	}
	
	public boolean isFiring() {
		return mFired;
	}
	
	public void setFiring(boolean b) {
		mFired = b;
	}
	
	public boolean tryThrowBourbon(Vector2 pos, boolean isFromLocal) {
		
		if(isFromLocal) {
			if(mIsAllowedToThrow) {
				mThrowingTimeElapsed = 0f;
				mIsAllowedToThrow = false;
			} else {
				return false;
			}	
		}
		
		mThrownBourbon = false;
		// Har spelaren n�gon bourbon
		if(mInventory.gotBourbon()) {
			Vector2 throwToPos = pos;
			
			// Har spelaren n�got target
			if(mTarget != null && mTarget instanceof Player)
				throwToPos = mTarget.getPosition();
			mInventory.throwBourbon(getPosition(), throwToPos, mManager);
			return true;
		}
		return false;
	}
	
	public void hitByBourbon(int amount) {
		int healed = mHealth + amount > 100 ? 100-mHealth : amount;
		mHealth += healed;
		if(mHealth > 100)
			mHealth = 100;
		System.out.println("HP: " + mHealth + "/100");
	}
	
	// Spelare har blivit tr�ffad av fiendes vapen
	public void hitByEnemy(int damage) {
		mInCombat = true;
		mHealth -= damage;
		if(mHealth <= 0) {
			mState = State.DYING;
			mHealth = 0;
		}
		
		if(mSoundObserver != null) {
			mSoundObserver.damageTaken(getID());
		}
	}
	
	@Override
	public boolean isAlive() {
		return mState == State.DEAD ? false : true;
	}
	
	@Override
	public Vector2 getSize() {
		return mSize;
	}
	
	public void screenClick(Vector2 position, float maxUpRange) {
		
		boolean touch = false;
		//Kolla om klicket tr�ffar n�gon body av typen ICharacter
		Iterator<Body> bi = mPhysicsWorld.getBodies();
		
		while(bi.hasNext()) {
			Body b = bi.next();

			if (b != null && b.getType() == BodyType.DynamicBody && b.getUserData() instanceof ICharacter) {
				ICharacter bc = (ICharacter)b.getUserData();
				
				if (bc != null) {
					if (position.x >= bc.getPosition().x - bc.getSize().x / 2 && position.x <= bc.getPosition().x + bc.getSize().x / 2
							&& position.y <= bc.getPosition().y + (bc.getSize().y / 2) && position.y >= bc.getPosition().y - (bc.getSize().y / 2)) {
						
						touch = true;
						//Om player inte har n�got target
						if (getTarget() == null) {
							setTarget(bc);
						} else if (!getTarget().equals(bc)) { //�r target inte samma, s�tt nytt target
							//S�tt target hos player
							setTarget(bc);
						} else { //Annars ta bort target
							setTarget(null);
						}
					}
				}
			}
		}
		
		if(position.y < maxUpRange) {
			if (touch == false) {
				moveTo(position, false);
			}	
		}
	}
	
	public Vector2 getTargetDirection() {
		Vector2 dir = new Vector2();
		if (mTarget != null && !(mTarget instanceof Player)) {
			dir = mTarget.getPosition().cpy().sub(mBody.getPosition());
		}
		
		return dir;
	}
	
	public Vector2 getTargetPosition() {
		if (mTarget != null) {
			return mTarget.getPosition();
		}
		return getPosition();
	}

	public void stageUpdatePosition(Vector2 newpos) {
		mBody.setTransform(newpos, 0);
	}
	
	public boolean isLocal() {
		return mIsLocal;
	}

	public int hasPlayerLostHP() {
		if (mOldHealth > mHealth) {
			return mOldHealth - mHealth;
		}
		return 0;
	}

	public int getHealth() {
		return mHealth;
	}
	
	public boolean getCanSwapWeapon() {
		return mCanSwapWeapon;
	}
	
	public void setCanSwapWeapon(boolean b) {
		mCanSwapWeapon = b;
	}
	
	public void setAvailableWeaponToSwap(PickupAbleWeapon wep) {
		mAvailableWeaponSwap = wep;
	}

	public PickupAbleWeapon getAvailableWeaponSwap() {
		return mAvailableWeaponSwap;
	}
	
	public boolean swapWeapon() {
		if (mAvailableWeaponSwap != null && mWeaponSwapTime <= 0) {
			mSwappedWeapon = true;
			mCanSwapWeapon = false;
			mAvailableWeaponSwap.pickUp(mPlayerId);
			mWeaponSwapTime = 0.5f;
			return true;
		}
		return false;
	}

	public void setHost(boolean b) {
		mIsHost = b;
	}
	
	public boolean isHost() {
		return mIsHost;
	}

	public boolean hasThrownBourbon() {
		return mThrownBourbon;
	}
	
	public void setThrowBourbon() {
		mThrownBourbon = true;
	}
	
	public State getFormerState() {
		return mOldState;
	}

	@Override
	public int compare(IEntity entity) {
		if (getBottomY() < entity.getBottomY()) return -1;
		else if (getBottomY() > entity.getBottomY()) return 1;
		else return 0;
	}

	@Override
	public float getBottomY() {
		return getPosition().y - getSize().y / 2;
	}

}
