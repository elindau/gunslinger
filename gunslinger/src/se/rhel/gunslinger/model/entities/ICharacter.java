package se.rhel.gunslinger.model.entities;

import com.badlogic.gdx.math.Vector2;

public interface ICharacter extends IEntity {

	public Vector2 getPosition();
	public Vector2 getSize();
	public boolean isAlive();
	public boolean isFacingLeft();
}
