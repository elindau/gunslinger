package se.rhel.gunslinger.model.entities;

import se.rhel.gunslinger.model.Filter;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * Representerar skyltar
 * @author Emil
 *
 */
public class Sign extends GameObject {

	public static final float	SIGN_SIZE = 2.5f;
	public static final float	MESSAGE_BG_SIZE_X = 16f;
	public static final float   MESSAGE_BG_SIZE_Y = 3f;
	
	private State 	mState;
	private String 	mMessage;
	private Vector2 mSize = new Vector2(1.5f, 2f);
	private Vector2 mPosition;
	
	private Body 	mBody;
	
	public Sign(int id, Vector2 spawn, String message, World world) {
		super(id, spawn, Type.SIGN, world, false);
		System.out.println(spawn);
		mPosition = spawn;
		mState = State.DEACTIVE;
		mMessage = message;
		
		// Skapa upp sensor-kropp speciellt f�r sign
		// Skapa upp fysiken
		BodyDef def = new BodyDef();
		def.type = BodyType.StaticBody;
		def.position.set(spawn.x, spawn.y);
		mBody = world.createBody(def);

		PolygonShape poly = new PolygonShape();
		poly.setAsBox(1f, 0.2f);

		FixtureDef fd = new FixtureDef();
		fd.density = 1f;

		fd.filter.categoryBits = Filter.CATEGORY_OBJECT;
		fd.filter.maskBits = Filter.MASK_OBJECT;
		fd.shape = poly;
		fd.isSensor = true;
		
		mBody.createFixture(fd);
		mBody.setFixedRotation(true);

		mBody.setUserData(this);

		poly.dispose();
	}
	
	@Override
	public int compare(IEntity entity) {
		if (getBottomY() < entity.getBottomY()) return -1;
		else if (getBottomY() > entity.getBottomY()) return 1;
		else return 0;
	}

	@Override
	public float getBottomY() {
		return mPosition.y;
	}

	public String getMessage() {
		return mMessage;
	}

	@Override
	public State getState() {
		return mState;
	}

	@Override
	public void setState(State newState) {
		mState = newState;
	}

	@Override
	public Vector2 getSize() {
		return mSize;
	}
	
	
}
