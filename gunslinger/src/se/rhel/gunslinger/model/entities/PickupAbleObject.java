package se.rhel.gunslinger.model.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Extendar GameObject
 * och representerar objekt som kan plockas
 * upp av spelare
 * @author Emil
 *
 */
public class PickupAbleObject extends GameObject {

	public static final float	GUN_SIZE = 0.8f;
	public static final float 	BOURBON_SIZE = 1f;
	public static final float	COIN_SIZE = 1f;
	
	public PickupAbleObject(int id, Vector2 spawn, Type type, boolean isRequired, World world) {
		super(id, spawn, type, world, isRequired);
	}

	public void pickUp(int id) {
		super.mCurrentState = State.ACTIVE;
		super.mPickedUpByID = id;
	}
	
	public int getPickupID() {
		return super.mPickedUpByID;
	}
}
