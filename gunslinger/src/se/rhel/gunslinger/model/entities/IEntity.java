package se.rhel.gunslinger.model.entities;

import com.badlogic.gdx.math.Vector2;

public interface IEntity {
	public Vector2 getPosition();
	public Vector2 getSize();
	public int compare(IEntity entity);
	public float getBottomY();
}
