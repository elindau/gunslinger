package se.rhel.gunslinger.model.entities;

import se.rhel.gunslinger.model.Filter;
import se.rhel.gunslinger.model.Id;
import aurelienribon.bodyeditor.BodyEditorLoader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Basklass f�r mer statiska objekt
 * i spelv�rlden
 * @author Emil
 *
 */
public abstract class GameObject implements IEntity {

	private static final String FILENAME_GUN = "gun";
	private static final String FILENAME_BOURBON = "bourbon";
	private static final String FILENAME_SIGN = "sign";
	private static final String FILENAME_COIN = "coin";

	private int 	mId;
	private Vector2 mPosition;
	private Type	mType;
	private World	mWorld;
	private boolean mIsRequired;
	protected State mCurrentState;
	protected int 	mPickedUpByID;
	private Vector2 mSize = new Vector2();
	
	public GameObject(int id, Vector2 position, Type type, World world, boolean isReq) {
		mId = id;
		mPosition = position;
		mType = type;
		mWorld = world;
		mIsRequired = isReq;	
		
		createStageCollisionObjects();
	}
	
	/**
	 * Skapar kroppar och l�gger till dem
	 * i fysikv�rlden
	 */
	private void createStageCollisionObjects() {
		// 0. Ladda in .json filen fr�n Physics body editor
		String filename = buildFilename();
		// .. namnet f�r vi fr�n filnamnet - .json
		BodyEditorLoader loader = new BodyEditorLoader(Gdx.files.internal(filename));
					
		// 1. Skapa body def (borde ha namnkonvention p� sj�lva banans stage-namn)
		BodyDef bd = new BodyDef();
		bd.position.set(mPosition);	
		bd.type = BodyType.StaticBody;
					
		// 2. Skapa FixtureDef
		FixtureDef fd = new FixtureDef();
		fd.density = 1;
		fd.friction = 0.0f;
		fd.restitution = 0.0f;
		boolean sensor = mType.equals(Type.SIGN) ? false : true;
		fd.isSensor = sensor;
		fd.filter.categoryBits = Filter.CATEGORY_OBJECT;
		fd.filter.maskBits = Filter.MASK_OBJECT;

		// 3. Skapa kroppen
		Body groundModel = mWorld.createBody(bd);
		if(!mType.equals(Type.SIGN)) {
			groundModel.setUserData(this);	
		}
		
		// 4. Ska body fixture automatiskt genom bodyeditorloader
		String name = "";
		float size = 0f;
		
		if(mType.equals(Type.WEAPON)) {
			name = FILENAME_GUN;
			size = PickupAbleObject.GUN_SIZE;
			mSize = new Vector2(size, size);
		}
		if(mType.equals(Type.BOURBON)) {
			name = FILENAME_BOURBON;
			size = PickupAbleObject.BOURBON_SIZE;
			mSize = new Vector2(size, size);
		}
		if(mType.equals(Type.SIGN)) {
			name = FILENAME_SIGN;
			size = Sign.SIGN_SIZE;
			mSize = new Vector2(size, size);
		}
		if(mType.equals(Type.COIN)) {
			name = FILENAME_COIN;
			size = PickupAbleObject.COIN_SIZE;
			mSize = new Vector2(size, size);
		}
		
		loader.attachFixture(groundModel, name, fd, size);
					
		// 5. L�gg till stagen i arrayen
		// level.getStage(level.allStages().get(i)).initialize(bd.position, mPhysicsWorld);
	}
	
	/**
	 * Hj�lpmetod f�r att snickra ihop filnamn
	 * beroende p� typ av objekt
	 * @return
	 */
	private String buildFilename() {
		String path = "data/objects/";
		String json = ".json";
		String file = "";
		
		switch(mType) {
		case WEAPON:
				file = FILENAME_GUN;
			break;
		case BOURBON:
				file = FILENAME_BOURBON;
			break;
		case SIGN:
				file = FILENAME_SIGN;
			break;
		case COIN:
				file = FILENAME_COIN;
			break;
		}
		
		return path + file + json;
	}
	
	public State getState() {
		return mCurrentState;
	}
	public void setState(State newState) {
		mCurrentState = newState;
	}
	
	public Type getType() {
		return mType;
	}
	
	@Override
	public Vector2 getPosition() {
		return mPosition;
	}
	
	public static boolean isSign(Type type) {
		if(type.equals(Type.SIGN))
			return true;
		return false;
	}
	
	public static boolean isPickupAbleObject(Type type) {
		if(type.equals(Type.WEAPON) || type.equals(Type.BOURBON)) 
			return true;
		return false;
	}
	
	public boolean isPickedUp() {
		return mCurrentState == State.ACTIVE ? true : false;
	}
	
	public int getID() {
		return mId;
	}
	
	public enum State {
		ACTIVE, DEACTIVE, DEAD;
	}
	
	public enum Type {
		WEAPON, BOURBON, SIGN, COIN;
	}

	public boolean isRequired() {
		return mIsRequired;
	}
	
	@Override
	public Vector2 getSize() {
		return mSize;
	}

	@Override
	public int compare(IEntity entity) {
		if (getBottomY() < entity.getBottomY()) return -1;
		else if (getBottomY() > entity.getBottomY()) return 1;
		else return 0;
	}
	
	@Override
	public float getBottomY() {
		return getPosition().y - getSize().y;
	}
}
