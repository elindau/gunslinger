package se.rhel.gunslinger.model.entities;

import java.util.Random;
import java.util.logging.Level;

import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.LevelManager;
import se.rhel.gunslinger.model.Stage;
import se.rhel.gunslinger.model.entities.Enemy.EnemyState;
import se.rhel.gunslinger.model.weapons.Cannon;
import se.rhel.gunslinger.model.weapons.MeleeWeapon;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;


public class BossAI extends BaseAI{	
	private Random 		rand = new Random();
	
	private Boss		mBoss;
	
	//Cannon
	private float		mFireCanonTimer = 0f;
	private float		mFireCannonWaitTimer = 5f;
	private int			mTimesToFireCannon = 10;
	private Vector2 	mFirePosition;
	
	//Rush
	private float 		mRushTimer = 5f;
	private float 		mRushWaitTimer = 2f;
	//private boolean 	mRushing = false;
	private Vector2		mRushTargetPos = new Vector2();
	private boolean		firstRush = false;
	private boolean		secondRush = false;
	private float 		mTripTimer = 5f;
	private boolean 	mTrip = false;
	private boolean		mFireCannonNormal = true;
	private boolean		mIsFireCannonSet = false;
	
	private Vector2		mLastHitPos = new Vector2();
	private float 		mCannonOffset = 2f;
	private Vector2 	mHitPos;
	
	public BossAI(Boss boss) {
		super(0);
		
		mBoss = boss;
		mFirePosition = new Vector2(Camera.CAMERA_WIDTH * 2 + 2f, 4f);
	}

	@Override
	public void update(float delta, Array<Player> players, Stage currentStage) {	
		
		for(Player p : players) {
			if(mCurrentTarget == null) {
				mCurrentTarget = p;
			}
			
			if(p.getPosition().cpy().sub(mBoss.getPosition()).len() <  mCurrentTarget.getPosition().cpy().sub(mBoss.getPosition()).len()) {
				mCurrentTarget = p;
			}
		}

		// Kolla mot threaten
		if(getPlayerWithHighestThreat() != null) {
			mCurrentTarget = getPlayerWithHighestThreat();
		}
		
		if (mBoss.getBossState() == BossState.TRIP) {
			if ((mTripTimer -= delta) < 0) {
				mTripTimer = 5f;
				mBoss.setState(EnemyState.IDLE);
				mBoss.mBossState = BossState.UP;
				mBoss.mVulnerable = false;
				mFireCanonTimer = 2f;
			}
			
		} else if (mBoss.getBossState() == BossState.PRETRIP) {
			if (isWithinBoundry(mRushTargetPos, 1f)) {
				mBoss.setState(EnemyState.IDLE);
				mBoss.setBossState(BossState.TRIP);
				mRushTimer = 10f;
				mBoss.SPEED = 2f;	
			} else {
				mBoss.mVelocity.x -= 1f;
			}
		} else {
			if ((mFireCanonTimer -= delta) <= 0 && mBoss.getBossState() != BossState.RUSHING) {		
				//Kolla om man st�r vi kanonen	
				if (isWithinBoundry(mFirePosition, 2f)) {
					mBoss.setState(EnemyState.IDLE);
					
					if (!mIsFireCannonSet) {
						mIsFireCannonSet = true;
						mFireCannonNormal = rand.nextFloat() > 0.5f ? true : false;
					}
					
					//Avfyra x antal g�nger
					if (mTimesToFireCannon > 0 && (mFireCannonWaitTimer -= delta) < 0) {
						mTimesToFireCannon--;
						mFireCannonWaitTimer = mFireCannonNormal ? 1f : 0.3f;
						mBoss.mFiringCannon = true;
						
						if (mFireCannonNormal) {
							//halvrandom position utifr�n current target
							mHitPos = new Vector2(mCurrentTarget.getPosition().x + (rand.nextFloat() * 2) -1, mCurrentTarget.getBottomY() + (rand.nextFloat() - 0.5f));
						} else {
							//Skjut ifr�n h�ger till v�nster
							if (mLastHitPos.equals(Vector2.Zero)) {
								mLastHitPos = mBoss.getPosition().cpy();
							}
						
							mHitPos = new Vector2(mLastHitPos.x - mCannonOffset, mCurrentTarget.getPosition().y + rand.nextFloat() - 0.5f);
							mLastHitPos = mHitPos.cpy();
						}
						
						mBoss.getCannon().fire(mHitPos.cpy());
					}
					
					//resetta cd's f�r n�sta f�rfarande
					if (mTimesToFireCannon == 0) {
						mIsFireCannonSet = false;
						mTimesToFireCannon = 10;
						mBoss.SPEED = 2f;
						mFireCanonTimer = 30f;
						mLastHitPos = Vector2.Zero;
					}
					
				} else { // Annars g� mot den
					mBoss.setBossState(BossState.WALKING);
					mBoss.mState = EnemyState.MOVING;
					mBoss.SPEED = 6f;
					mBoss.setDirection(mFirePosition.cpy().sub(mBoss.getPosition()));
				}
			} else if ((mRushTimer -= delta) <= 0) {
				//Kolla om han befinner sig utanf�r sk�rmen till h�ger
				if (mBoss.getBossState() != BossState.RUSHING) {
					if (isWithinBoundry(mFirePosition, 1f)) {
						//�r han utanf�r, rusha till v�nster
						mBoss.SPEED = 10f;
						mBoss.setBossState(BossState.RUSHING);
						//mBoss.mRushing = true;
						firstRush = true;
						
						if (rand.nextBoolean()) {
							mRushTargetPos = new Vector2(mFirePosition.x - Camera.CAMERA_WIDTH -5f, mFirePosition.y + rand.nextInt(2)-1);
						} else {
							mRushTargetPos = new Vector2(mFirePosition.x - Camera.CAMERA_WIDTH -5f, mCurrentTarget.getPosition().y + 1);
						}
						
						mBoss.mBody.setTransform(new Vector2(mBoss.mBody.getPosition().x, mRushTargetPos.y), 0);
						mBoss.setDirection(mRushTargetPos.cpy().sub(mBoss.getPosition()));
					} else {
						//Annars g� dit
						mBoss.setState(EnemyState.MOVING);
						mBoss.setDirection(mFirePosition.cpy().sub(mBoss.getPosition()));
						mBoss.SPEED = 8f;
					}
				} else {
				//Om vi rushar
					//�r vi vid v�r position, �ndra y och rusha tillbaka
					if (isWithinBoundry(mRushTargetPos, 1f) && firstRush) {
						mBoss.setState(EnemyState.IDLE);
						if ((mRushWaitTimer -= delta) < 0) {
							mRushWaitTimer = 1f;
							mBoss.moveBody(new Vector2(mBoss.getPosition().x, mFirePosition.y + (rand.nextFloat() * 2) - 1));
							mRushTargetPos = new Vector2(mBoss.getPosition().x + Camera.CAMERA_WIDTH + 4f, mBoss.getPosition().y);
							secondRush = true;
							firstRush = false;
						}
					} 
					else if (isWithinBoundry(mRushTargetPos, 1f) && secondRush) {
						mBoss.moveBody(new Vector2(mBoss.getPosition().x, mCurrentTarget.getPosition().y));
						mRushTargetPos = new Vector2(mBoss.getPosition().x - Camera.CAMERA_WIDTH / 2 - 2f , mCurrentTarget.getPosition().y);
						secondRush = false;
					}
					else {
						mBoss.setState(EnemyState.MOVING);
						mBoss.setDirection(mRushTargetPos.cpy().sub(mBoss.getPosition()));
										
						if (!firstRush && !secondRush && isWithinBoundry(mRushTargetPos, 6f)) {
							mBoss.setBossState(BossState.PRETRIP);
						}
					}
				}
			}
		}
	}

	@Override
	public Vector2 getCalculatedTargetDirection() {
		return mCurrentTarget.getPosition().cpy().sub(mBoss.getPosition());
	}
	
	private boolean isWithinBoundry(Vector2 gotoPos, float marginSize) {
		return (mBoss.getPosition().x <= gotoPos.x + marginSize && mBoss.getPosition().x >= gotoPos.x);
	}
	
	public enum BossState {
		RUSHING,
		PRETRIP,
		TRIP,
		UP,
		WALKING
	}
}
