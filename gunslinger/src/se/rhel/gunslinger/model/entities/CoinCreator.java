package se.rhel.gunslinger.model.entities;

import java.util.ArrayList;
import java.util.Random;

import se.rhel.gunslinger.model.Id;
import se.rhel.gunslinger.model.entities.GameObject.Type;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Skapar ett visst antal coins runt en fiende som
 * just d�tt
 * @author Emil
 *
 */
public class CoinCreator {
	
	public static ArrayList<PickupAbleObject> spawn(Vector2 p, int amount, World world) {
		ArrayList<PickupAbleObject> coins = new ArrayList<PickupAbleObject>();
		
		for(int i = 0; i < amount; i++) {
			coins.add(new PickupAbleObject(Id.getInstance().get(), getRandomPosition(p.x - 1f, p.x + 1f, p.y - 1f, p.y + 1f), Type.COIN, false, world));
		}
		
		return coins;
	}
	
	private static Vector2 getRandomPosition(float minX, float maxX, float minY, float maxY) {
		Random rand = new Random();
		
		double randomX = minX + (maxX - minX) * rand.nextDouble();
		double randomY = minY + (maxY - minY) * rand.nextDouble();
		
		
		return new Vector2((float)randomX, (float)randomY);
	}
}

