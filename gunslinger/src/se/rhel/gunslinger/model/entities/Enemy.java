package se.rhel.gunslinger.model.entities;

import se.rhel.gunslinger.model.Filter;
import se.rhel.gunslinger.model.Stage;
import se.rhel.gunslinger.model.weapons.Gun;
import se.rhel.gunslinger.model.weapons.IMeleeWeapon;
import se.rhel.gunslinger.model.weapons.IWeapon;
import se.rhel.gunslinger.model.weapons.Pitchfork;
import se.rhel.gunslinger.model.weapons.Shotgun;

import se.rhel.gunslinger.network.Network.EnemyUpdateState;
import se.rhel.gunslinger.observers.HUDTextObserver;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import com.badlogic.gdx.utils.Array;

public class Enemy implements ICharacter {
	
	public enum EnemyState { ATTACKING, IDLE, MOVING, DYING, DEAD; }
	
	public enum EnemyType { RANGED, MELEE, BOSS; }

	public static float 	SIZE = 1f;
	protected Vector2 		mSize = new Vector2(1f, 2f);
	protected static float	SPEED = 0.8f;
	
	private static final float SWING_SPEED_LEFT = (float) Math.toRadians(360);
	private static final float SWING_ANGLE_LEFT = (float) Math.toRadians(180);
	
	private int				mEnemyID;
	protected Vector2 		mSpawnPosition;
	private IEntity 		mEntityToAttack = null;
	protected EnemyState 	mState = EnemyState.IDLE;
	protected Vector2 		mDirection = new Vector2();
	protected Vector2 		mVelocity;
	protected int			mMaxHealth;
	protected int 			mHealth;
	protected boolean		mFacingLeft;
	private int 			mOldHealth;
	private EnemyType		mType;
	
	protected Vector2		mOldDirection = new Vector2();;
	
	// Fysik
	protected Body 			mBody;
	private World 			mPhysicsWorld;

	protected IWeapon		mWeapon;

	protected BaseAI		mAI;
	private boolean			mCollide = false;
	private boolean			mEnemyCollide = false;
	
	private boolean			mRanged;
	private boolean			mAttacking;
	private boolean			mBeingShotAt = false;
	private boolean			mIdleAttack;
	private boolean 		mHasBeenShot = false;
	
	// Tid
	private float			mMemoryMaxTime;
	private float			mMemoryTimeElapsed;
	
	private float			mBaseCrit = 0.05f; //5%
	
	private Vector2 		mOldPos = new Vector2();
	private EnemyState 		mOldState = EnemyState.IDLE;
	private Vector2 		mOldVelocity = new Vector2();
	
	private int				mDropAmount;
	
	public Enemy(int enemyID, Vector2 aPosition, World aWorld, EnemyType typeOfEnemy) {
		mEnemyID = enemyID;
		
		this.SPEED = 1.25f;
		this.SIZE = 2f;
		
		if(typeOfEnemy == EnemyType.RANGED) {
			mRanged = true;
		} else {
			mRanged = false;
		}
		mType = typeOfEnemy;
		mPhysicsWorld = aWorld;
		mSpawnPosition = aPosition;
		
		reset();
	}
	
	public void reset() {
		mVelocity = new Vector2(SPEED, 0);
		mHealth = 100;
		mMaxHealth = mHealth;
		mMemoryMaxTime = 2f;
		mMemoryTimeElapsed = 0f;
		mDropAmount = 5;
		mCollide = false;
	}
	
	public void spawn(boolean attachAI) {
		
		reset();
		mHealth = 100;
		
		// Skapa upp fysiken
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		mBody = mPhysicsWorld.createBody(def);

		PolygonShape poly = new PolygonShape();
		poly.setAsBox(0.5f, 1f);

		FixtureDef fd = new FixtureDef();
		fd.density = 50f;

		fd.filter.categoryBits = Filter.CATEGORY_ENEMY;
		fd.filter.maskBits = Filter.MASK_ENEMY;
		fd.shape = poly;
		fd.isSensor = true;
		
		mBody.createFixture(fd);
		
		mBody.setBullet(true);
		mBody.setFixedRotation(true);
		mBody.setTransform(mSpawnPosition, 0);

		mBody.setUserData(this);
		
		// Skapa olika vapen beroende
		// p� om fiende �r ranged eller icke
		if(isRanged()) {
			mWeapon = new Shotgun(mPhysicsWorld, false, this);
		} else {
			mWeapon = new Pitchfork(mSpawnPosition, mBody, mPhysicsWorld);
		}
		
		poly.dispose();
	
		// Koppla AI'n
		if(attachAI) {
			if(mAI == null) {
				mAI = new EnemyAI(this);	
			}
		}
		
		mState = EnemyState.MOVING;
	}
	

	public void update(float delta, Array<Player> players, Stage currentStage) {
		mAI.update(delta, players, currentStage);
		
		mOldPos = mBody.getPosition().cpy();
		updateMovement();
		
		// Fiendens 'minne'
		mMemoryTimeElapsed += delta;
		if(mMemoryTimeElapsed > mMemoryMaxTime) {
			mMemoryTimeElapsed = 0f;
			// F�rs�ker radera minnet att han blivit skjuten
			mBeingShotAt = false;
		}
		
		mOldHealth = mHealth;
	}
	
	public void updateMovement() {
		mOldDirection = mDirection.cpy();
		mOldVelocity = mVelocity.cpy();
		
		if (mState == EnemyState.MOVING) {
			mVelocity = mDirection.nor().scl(SPEED);
			mBody.setLinearVelocity(mVelocity);
			
			// S�tt r�tt facing
			if (mDirection.x < 0) {
				mFacingLeft = true;			
			} else {
				mFacingLeft = false;
			}
		}
		
		if(mState == EnemyState.IDLE || mState == EnemyState.ATTACKING) {
			mBody.setLinearVelocity(new Vector2(0, 0));
		}
		
		mOldState = mState;
	}
	
	// LOGIK
	
	/**
	 * F�rs�k attackera
	 */
	public void tryAttack(Vector2 direction) {
		mAttacking = true;

		Vector2 spawn = new Vector2(mBody.getPosition().cpy());
		
		if (getTypeOfEnemy() == EnemyType.RANGED) {
			if(isFacingLeft()) {
				spawn.x -= 1f;
			} else {
				spawn.x += 1f;
			}
		}
		mWeapon.attack(spawn, mFacingLeft, direction, mBaseCrit);
	}
	
	/**
	 * Sluta attackera
	 */
	public void stopAttack() {
		mAttacking = false;

		if (mWeapon instanceof IMeleeWeapon)
			((IMeleeWeapon)mWeapon).stopAttack();
	}
	
	/**
	 * @return Om fiender kolliderat eller inte
	 */
	public boolean hasCollidedWithPlayer() {
		return mCollide;
	}
	
	/**
	 * N�r en spelare kolliderat med fiende
	 */
	public void playerContact(boolean bool) {
		mCollide = bool;
	}
	
	public void makeIdleAttack(boolean b) {
		mIdleAttack = b;
	}
	
	public boolean isIdleAttacking() {
		return mIdleAttack;
	}
	
	public void enemyContact(boolean b) {
		mEnemyCollide = b;
	}
	
	public boolean hasCollidedWithEnemy() {
		return mEnemyCollide;
	}
	
	/**
	 * Fiender har blivit tr�ffad av spelarskott
	 */
	public void gunHit(Player deliveredFrom, int damage) {
		mHealth -= damage;
		mBeingShotAt = true;
		mAI.addPlayerToThreat(deliveredFrom);
	}
	
	public boolean isBeingShotAt() {
		return mBeingShotAt;
	}
	
	public boolean isFacingLeft() {
		return mFacingLeft;
	}
	
	public boolean isAttacking() {
		return mAttacking;
	}
	
	@Override
	public boolean isAlive() {
		if(mHealth > 0 || mState == EnemyState.DYING) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isRanged() {
		return mRanged;
	}
	
	// GETTERS & SETTERS
	
	public void setDirection(Vector2 direction) {
		mDirection = direction;
	}
	
	public EnemyType getTypeOfEnemy() {
		return mType;
	}
	
	public Vector2 getDirection() {
		return mDirection;
	}
	
	public void setState(EnemyState state) {
		mState = state;
	}
	
	public EnemyState getState() {
		return mState;
	}
	
	public IWeapon getWeapon() {
		return mWeapon;
	}
	
	// F�r att kunna f�rst�ra jointen
	public Joint getJoint() {

		return ((IMeleeWeapon)mWeapon).getJoint(); // mJoint;
	}
	
	public Body getWeaponBody() {

		return ((IMeleeWeapon)mWeapon).getBody(); // mWeaponBody;
	}

	@Override
	public Vector2 getSize() {
		return mSize;
	}
	
	@Override
	public Vector2 getPosition() {
		return mBody.getPosition();
	}

	public int getID() {
		return mEnemyID;
	}
	
	public boolean hasEnemyMoved() {
		return mOldPos.x != mBody.getPosition().x || mOldPos.y != mBody.getPosition().y;
	}
	
	public int hasEnemyLostHP() {
		if (mOldHealth != mHealth) {
			return mOldHealth - mHealth;
		}
		return 0;
	}
	
	protected void moveBody(Vector2 pos) {
		mBody.setTransform(pos, 0);
	}

	public void updateEnemyState(EnemyUpdateState message) {
		mBody.setTransform(message.mPosition, 0);
		mState = message.mState;
		mDirection = message.mDirection;
		
		if (!message.mIsAttacking) {
			stopAttack();
		}
	}
	
	public Vector2 getBulletDirection() {
		return mAI.getCalculatedTargetDirection();
	}

	public Vector2 getTargetDirection() {
		Vector2 targetDir = new Vector2();
		
		if (mAI.getCurrentTarget() != null) {
			targetDir = mAI.getCurrentTarget().getPosition().cpy().sub(mBody.getPosition());
		}
		
		return targetDir;
	}

	public void setFacingLeft(boolean isFacingLeft) {
		mFacingLeft = isFacingLeft;
	}
	
	public int getDropAmount() {
		return mDropAmount;
	}
	
	public void setDead(boolean drop) {
		// Ska fienden droppa eller inte
		if(!drop) {
			// Ta bort ev. drop
			mDropAmount = 0;
		}
		mHealth = 0;
	}

	public void damageEnemy(int hp) {
		if(mState != EnemyState.DYING) {
			mHealth -= hp;
			mHasBeenShot = true;
		}
		
		if(mHealth <= 0) {
			mState = EnemyState.DYING;
		}
	}
	
	public boolean hasEnemyTakenDamage() {
		return mHasBeenShot;
	}
	
	public void setShootAt(boolean b) {
		mHasBeenShot = b;
	}
	
	public boolean hasChanged() {
		return 	mOldVelocity.x != mVelocity.x || 
				mOldVelocity.y != mVelocity.y || 
				mState != mOldState || 
				mOldDirection.x != mDirection.x || 
				mOldDirection.y != mDirection.y;
	}

	public int getHealth() {
		return mHealth;
	}

	public int getMaxHealth() { 
		return mMaxHealth;
	}

	@Override
	public int compare(IEntity entity) {
		if (getBottomY() < entity.getBottomY()) return -1;
		else if (getBottomY() > entity.getBottomY()) return 1;
		else return 0;
	}

	@Override
	public float getBottomY() {
		return getPosition().y - mSize.y / 2;
	}
}
