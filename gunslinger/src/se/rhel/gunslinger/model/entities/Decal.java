package se.rhel.gunslinger.model.entities;


import se.rhel.gunslinger.model.Filter;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Decal implements IEntity {
	
	public static final Vector2 LARGE_CACTUS_SIZE = new Vector2(1.875f, 3f);
	public static final Vector2 SMALL_CACTUS_SIZE = new Vector2(1.3f, 2f);
	public static final Vector2 DEAD_GUY_SIZE = new Vector2(2f, 2f);
	public static final Vector2 STONE_SIZE = new Vector2(2f, 1.47f);
	
	private Vector2 mPosition;
	private Vector2 mSize;
	private DecalType mType;
	private Body	mBody;

	public Decal(Vector2 position, DecalType type, World world) {
		mPosition = position;
		mSize = size(type);
		mType = type;
		
		BodyDef def = new BodyDef();
		def.type = BodyType.StaticBody;
		
		mBody = world.createBody(def);
		
		PolygonShape ps = new PolygonShape();
		
		if(type == DecalType.LARGE_CACTUS || type == DecalType.SMALL_CACTUS) {
			ps.setAsBox(mSize.x / 4f, mSize.x / 20f, new Vector2(position.x, position.y + mSize.y / 10f), 0);
		}
		else {
			ps.setAsBox(mSize.x / 2f, mSize.x / 20f, new Vector2(position.x, position.y + mSize.y / 5f), 0);
		}
		
		FixtureDef fd = new FixtureDef();
		fd.density = 0.1f;
		fd.isSensor = false;
		fd.shape = ps;
		fd.friction = 0f;
		fd.filter.categoryBits = Filter.CATEGORY_OBJECT;
		fd.filter.maskBits = Filter.MASK_OBJECT;
		
		mBody.createFixture(fd);
		mBody.setBullet(true);
		mBody.setFixedRotation(false);
		//mBody.setTransform(new Vector2(position.x, position.y + mSize.y / 10f), 0);
		mBody.setUserData(this);
	}
	
	public DecalType getType() {
		return mType;
	}
	
	public Vector2 getSize() {
		return mSize;
	}
	
	public Vector2 getPosition() {
		return mPosition;
	}

	@Override
	public int compare(IEntity entity) {
		if (getBottomY() < entity.getBottomY()) return -1;
		else if (getBottomY() > entity.getBottomY()) return 1;
		else return 0;
	}
	
	public enum DecalType {
		LARGE_CACTUS,
		SMALL_CACTUS,
		DEAD_GUY,
		STONE
	}

	@Override
	public float getBottomY() {
		return mPosition.y;
	}
	
	private Vector2 size(DecalType type) {
		Vector2 size;
		
		switch(type) {
		case LARGE_CACTUS:
			size = LARGE_CACTUS_SIZE;
			break;
		case SMALL_CACTUS:
			size = SMALL_CACTUS_SIZE;
			break;
		case DEAD_GUY:
			size = DEAD_GUY_SIZE; 
			break;
		case STONE:
			size = STONE_SIZE;
			break;
		default:
			size = Vector2.Zero;
			break;
		}
		
		return size;
	}
	
	public static class DecalParse {
		private DecalType TYPE;
		private float POSITION_X;
		private float POSITION_Y;
		
		public DecalType getType() {
			return TYPE;
		}
		
		public Vector2 getPosition() {
			return new Vector2(POSITION_X, POSITION_Y);
		}
	}
}
