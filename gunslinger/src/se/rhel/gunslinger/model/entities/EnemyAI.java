package se.rhel.gunslinger.model.entities;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import se.rhel.gunslinger.model.Stage;
import se.rhel.gunslinger.model.Threat;
import se.rhel.gunslinger.model.entities.Enemy.EnemyState;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class EnemyAI extends BaseAI {

	private static final float MAX_DISTANCE = 9f;
	private static final float MIN_DISTANCE = 0.6f;
	private static final float MELEE_ATTACK_RANGE = 4f;
	private static final float RANGED_ATTACK_RANGE = 10f;
	private static final float RANGED_CLOSE_MAX_RANGE = 4.5f;
	private static final int THREAT_IGNORE = 10;

	private Enemy mEnemy;

	// Max-tid på vissa States
	private float mIdleMaxTime;
	private float mIdleTimeElapsed;
	private float mMovingMaxTime;
	private float mMovingTimeElapsed;
	private float mRangedAttackTimeElapsed;
	private float mRangedAttackMaxTime;
	private float mAdjustMaxtime;
	private float mAdjustTimeElapsed;

	private boolean mHaveShooted = false;
	private boolean mFirstContact = false;

	private Vector2 mCalculatedDirection = new Vector2();

	private Random mRand;

	public EnemyAI(Enemy enemy) {
		super(THREAT_IGNORE);
		mEnemy = enemy;

		if (enemy.isRanged()) {
			mIdleMaxTime = 3f;
			mMovingMaxTime = 2f;
			mRangedAttackMaxTime = 1.5f;
			System.out.println("RANGED");
		} else {
			mIdleMaxTime = 2f;
			mMovingMaxTime = 7f;
			System.out.println("MELEE");
		}

		mRangedAttackTimeElapsed = 0f;
		mIdleTimeElapsed = 0f;
		mMovingTimeElapsed = 0f;

		mAdjustMaxtime = 3.5f;
		mAdjustTimeElapsed = 0f;

		mRand = new Random();

	}

	@Override
	public void update(float delta, Array<Player> players, Stage currentStage) {
		for (Player p : players) {
			if (mCurrentTarget == null) {
				mCurrentTarget = p;
			}

			if (p.getPosition().cpy().sub(mEnemy.getPosition()).len() < mCurrentTarget
					.getPosition().cpy().sub(mEnemy.getPosition()).len()) {
				
				if (p.isAlive()) {
					mCurrentTarget = p;
				}
			}
		}

		// Kolla mot threaten
		if (getPlayerWithHighestThreat() != null) {
			mCurrentTarget = getPlayerWithHighestThreat();
		}

		// Är spelaren levande
		if (mCurrentTarget.state() != Player.State.DEAD
				&& mCurrentTarget.state() != Player.State.DYING) {

			if (mEnemy.isIdleAttacking()) {
				mEnemy.setState(EnemyState.ATTACKING);
				mIdleTimeElapsed += delta;
				if (mIdleTimeElapsed > mIdleMaxTime) {
					mIdleTimeElapsed = 0;
					mEnemy.makeIdleAttack(false);
				}
			}

			if (mEnemy.hasCollidedWithEnemy()) {
				// Koll om fiende går in i annan fiende
				// Testa byta riktning - ska bara köras en gång
				mEnemy.setState(EnemyState.IDLE);

				mAdjustTimeElapsed += delta;
				if (mAdjustTimeElapsed > mAdjustMaxtime) {
					mAdjustTimeElapsed = 0f;

					mEnemy.enemyContact(false);
				}
			}

			// Kolla om fiende är nära nog att attackera,
			// eller om fiende har träffat spelare
			if (mEnemy.hasCollidedWithPlayer()) {
				// Fiende får chilla lite
				mEnemy.setState(EnemyState.ATTACKING);
			}

			if (!mEnemy.hasCollidedWithEnemy()
					&& !mEnemy.hasCollidedWithPlayer()
					&& !mEnemy.isIdleAttacking()) {

				float range = (mEnemy.getPosition().cpy().sub(mCurrentTarget
						.getPosition())).len();
				if (range > RANGED_CLOSE_MAX_RANGE || !mEnemy.isRanged()) {
					// Annars ska fiende röra sig mot spelare
					mEnemy.setState(EnemyState.MOVING);
					mEnemy.setDirection(mCurrentTarget.getPosition().cpy()
							.sub(mEnemy.getPosition()));
					// .. Även sluta attackera
					mEnemy.stopAttack();
				} else {
					if (mEnemy.getState() == EnemyState.IDLE) {
						mEnemy.makeIdleAttack(true);
					}
				}

			}

			// Om fiende rört på sig för länge
			// och spelare är någorlunda nära
			// försök attackera
			if (mEnemy.getState() == EnemyState.MOVING) {

				mMovingTimeElapsed += delta;
				if (mMovingTimeElapsed > mMovingMaxTime) {
					mMovingTimeElapsed = 0f;

					// Spelare är inom ATTACK_RANGE(4m) och fiende kan börja
					// veva lite om han är melee och han INTE blir skjuten av
					// spelare
					// (dumt att stå still om det viner kulor runt huvudet).
					// Ranged fiender lackar och skjuter oavsett range och om de
					// blir skjutna

					// (mCurrentTarget.getPosition().cpy().sub(mEnemy.getPosition()).len()

					if (!mEnemy.isBeingShotAt()) {
						// Fejka spelarkontakt
						// mEnemy.playerContact(true);
						// mEnemy.setState(EnemyState.IDLE);
						mEnemy.makeIdleAttack(true);
					}
				}
			}

			// Om fiende idlar sĺ ska han attackera
			if (mEnemy.getState() == EnemyState.ATTACKING) {
				// Koll om fienden är utanför stagesen
				float range = (mEnemy.getPosition().cpy().sub(mCurrentTarget
						.getPosition())).len();

				if (mEnemy.getPosition().x > currentStage.getPosition().x
						&& mEnemy.getPosition().x < currentStage.getPosition().x
								+ currentStage.length()) {

					if (mEnemy.isRanged()) {

						// Mĺste hantera hur fiende skjuter
						if (!mHaveShooted) {
							if (range < RANGED_ATTACK_RANGE) {
								// Attackens riktning
								Vector2 direction = mCurrentTarget
										.getPosition().cpy()
										.sub(mEnemy.getPosition());

								// Avstĺnd till target
								float distance = mCurrentTarget.getPosition()
										.cpy().sub(mEnemy.getPosition()).len();

								// Konvertera min-max till 0-1 ..
								float dstRange = MAX_DISTANCE - MIN_DISTANCE;
								float hitRange = 1 - 0;
								// .. och fĺ pĺ sĺ sätt fram ett miss-värde
								float missChance = (((distance - MIN_DISTANCE) * hitRange) / dstRange) + 0;

								if (missChance < 0)
									missChance = 0;

								// Räkna ut om fiende ska missa eller inte
								float random = (float) mRand.nextDouble();

								// DEBUG
								// System.out.println("DISTANCE TO PLAYER: " +
								// distance + " m");
								// System.out.println("MISSCHANCE: " +
								// missChance + " %");
								// System.out.println("RANDOM VALUE: " + random
								// + " %");

								if (random < missChance) {
									// true = skjuter över, false = under
									if (mRand.nextBoolean()) {
										direction.y += 1.3f;
									} else {
										direction.y -= 1.3f;
									}
								}

								// Används för att skicka direction till
								// klienten
								mCalculatedDirection = direction;

								// Vänd fiende mot player
								boolean facing = direction.x < 0 ? true : false;
								mEnemy.setFacingLeft(facing);

								mEnemy.tryAttack(new Vector2(direction));
								mHaveShooted = true;
							}
						} else {
							mEnemy.stopAttack();
							mRangedAttackTimeElapsed += delta;
							if (mRangedAttackTimeElapsed > mRangedAttackMaxTime) {
								mRangedAttackTimeElapsed = 0f;
								mHaveShooted = false;
							}
						}
					} else {
						// Annars är han melee och kan slĺ lite som han vill
						if (range < MELEE_ATTACK_RANGE) {
							mEnemy.tryAttack(null);
						} else {
							mEnemy.stopAttack();
							mEnemy.setState(EnemyState.IDLE);
						}
					}
				}
			}
		} else {
			// Nuvarande target är död
			mEnemy.stopAttack();
			mEnemy.setState(EnemyState.IDLE);
		}
		
		float range = (mEnemy.getPosition().cpy().sub(mCurrentTarget
				.getPosition())).len();
	}

	public Vector2 getCalculatedTargetDirection() {
		return mCalculatedDirection;
	}

}
