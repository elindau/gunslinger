package se.rhel.gunslinger.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

public class Camera extends OrthographicCamera {
	
	private Vector2 	mScale = new Vector2();
	private float		mFontScale = 0f;
	private Vector2 	mDisplacement = new Vector2();
	
	public static float 		CAMERA_WIDTH = 16f;
	public static float 		CAMERA_HEIGHT = 10f;
	public static float 	PIXELS_PER_METER = 0f;
	public static float			FONT_SCALE_X = 0f;
	public static float			FONT_SCALE_Y = 0f;
	
	public Camera(float width, float height) {
		super();
		mScale.x = width / CAMERA_WIDTH;
		mScale.y = height / CAMERA_HEIGHT;
		
		PIXELS_PER_METER = width / CAMERA_WIDTH;
		FONT_SCALE_X = (float)(Math.round((width / 800f) * 1000) / 1000f);
		FONT_SCALE_Y = (float)(Math.round((height / 480f) * 1000) / 1000f);
	}
	
	public Vector2 getViewCoordinats(Vector2 modelCoords) {
		modelCoords.x -= mDisplacement.x;
		modelCoords.y -= mDisplacement.y;
		Vector2 visualCoordinats = modelCoords.scl(mScale);
		return visualCoordinats;
	}
	
	public Vector2 getModelCoordinats(Vector2 viewCoords) {
		Vector2 modelCoords = viewCoords.div(mScale);
		modelCoords.x += mDisplacement.x;
		modelCoords.y = getReversedYAxis(modelCoords.y) + mDisplacement.y;
		return modelCoords;
	}
	
	private float getReversedYAxis(float oldY) {
		return (CAMERA_HEIGHT - oldY);
	}
	
	public Vector2 getScale() {
		return mScale;
	}
	
	public Vector2 getDisplacement() {
		return mDisplacement;
	}

	public void setDisplacement(Vector2 displacement) {
		mDisplacement = displacement;
	}
}
