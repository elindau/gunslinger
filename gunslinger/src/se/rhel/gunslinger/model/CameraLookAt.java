package se.rhel.gunslinger.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class CameraLookAt {

	private Vector2 mPosition;
	
	public CameraLookAt(Vector2 position) {
		mPosition = position;
	}
	
	// Since the Orthocam is Vector3
	public CameraLookAt(Vector3 position) {
		mPosition = new Vector2(position.x, position.y);
	}
	
	public CameraLookAt(float x, float y) {
		mPosition = new Vector2(x, y);
	}

	public Vector2 position() {
		return mPosition;
	}

	public void setPosition(Vector2 mPosition) {
		this.mPosition = mPosition;
	}

	public void setPosition(Vector3 position) {
		mPosition =new Vector2(position.x, position.y);
	}
}
