package se.rhel.gunslinger.model.weapons;

import java.util.Random;

import se.rhel.gunslinger.model.entities.ICharacter;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class RangedWeapon implements IRangedWeapon {
	
	private Array<Bullet> 	mBullets = new Array<Bullet>();
	private World 			mWorld;
	
	private boolean			mOriginPlayer;
	private ICharacter		mOrigin;
	private Random			rand = new Random();
	private Weapons mWeaponsType;
	private WeaponStats mStats;
	
	public RangedWeapon(World world, boolean originPlayer, ICharacter origin, WeaponStats stats, Weapons weapon) {
		mWorld = world;
		mOriginPlayer = originPlayer;
		mOrigin = origin;
		mWeaponsType = weapon;
		mStats = stats;	
	}
	
	public void setNewWorld(World world) {
		mWorld = world;
	}

	@Override
	public Type getType() {
		return Type.RANGED;
	}

	@Override
	public void attack(Vector2 pos, boolean facingLeft, Vector2 direction, float critChance) {
		boolean crit = rand.nextFloat() < critChance + mStats.getCritChance() ? true : false;
		int damage = crit == true ? 
				(int)((rand.nextInt(mStats.getMaxDmg()- mStats.getMinDmg()) + mStats.getMinDmg()) * mStats.getCritMultiplier()) :
				rand.nextInt(mStats.getMaxDmg()-mStats.getMinDmg()) + mStats.getMinDmg();

		Bullet b = new Bullet(pos, mWorld, mOriginPlayer, mOrigin, damage, crit);

		b.fire(facingLeft, direction);
		mBullets.add(b);
	}

	public Array<Bullet> getBullets() {
		return mBullets;
	}

	@Override
	public Weapons getWeaponType() {
		return mWeaponsType;
	}
	
	public static int mMinDmg;
	public static int mMaxDmg;
	public static float mCrit;
	public static float mCritMultiplier;
}
