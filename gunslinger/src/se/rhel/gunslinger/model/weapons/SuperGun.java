package se.rhel.gunslinger.model.weapons;

import se.rhel.gunslinger.model.entities.ICharacter;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.physics.box2d.World;

public class SuperGun extends RangedWeapon {

	public static int mMinDmg = 100;
	public static int mMaxDmg = 200;
	public static float mCrit = 0.25f; // 25%
	public static float mCritMultiplier = 5f;
	private static WeaponStats mStats = new WeaponStats(mMinDmg, mMaxDmg, mCrit, mCritMultiplier);
	
	public SuperGun(World world, boolean originPlayer, ICharacter origin) {
		super(world, originPlayer, origin, mStats, Weapons.SUPERGUN);
	}
	
	public static WeaponStats getStats() {
		return mStats;
	}
}
