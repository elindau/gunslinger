package se.rhel.gunslinger.model.weapons;

import com.badlogic.gdx.utils.Array;

public interface IRangedWeapon extends IWeapon {
	Array<Bullet> getBullets();
}
