package se.rhel.gunslinger.model.weapons;

import se.rhel.gunslinger.model.entities.ICharacter;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.physics.box2d.World;

public class OmegaGun extends RangedWeapon {

		private static int mMinDmg = 500;
		private static int mMaxDmg = 1000;
		private static float mCrit = 0.45f;
		private static float mCritMultiplier = 10f;
		private static WeaponStats mStats = new WeaponStats(mMinDmg, mMaxDmg, mCrit, mCritMultiplier);
		
		public OmegaGun(World world, boolean originPlayer, ICharacter origin) {
			super(world, originPlayer, origin, mStats, Weapons.OMEGAGUN);
		}

		public static WeaponStats getStats() {
			return mStats;
		}
}
