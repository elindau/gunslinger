package se.rhel.gunslinger.model.weapons;

import java.util.Random;

import se.rhel.gunslinger.model.Filter;
import se.rhel.gunslinger.model.entities.ICharacter;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;

public class MeleeWeapon implements IMeleeWeapon {

	private Random rand = new Random();
	private int[] mDamageRange;
	private int mLastHitDamage = 0;
	private boolean mIsCrit = false;
	private float mWeaponBaseCrit;
	private float mWeaponCritMultiplier;
	
	private Body			mWeaponBody;
	protected RevoluteJoint 	mJoint;
	
	public MeleeWeapon(World world, Body attachTo, Vector2 weaponSize, int[] damageRange, float crit, float critMultiplier) {
		mDamageRange = damageRange;
		mWeaponBaseCrit = crit;
		mWeaponCritMultiplier = critMultiplier;
		
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		mWeaponBody = world.createBody(def);

		PolygonShape poly = new PolygonShape();
		poly.setAsBox(weaponSize.x, weaponSize.y);
		
		FixtureDef fd = new FixtureDef();
		fd.density = 0.1f;
		fd.filter.categoryBits = Filter.CATEGORY_ENEMY;
		fd.filter.maskBits = Filter.MASK_ENEMY;
		fd.shape = poly;
		fd.isSensor = true;
		
		mWeaponBody.createFixture(fd);
					
		mWeaponBody.setBullet(true);
		mWeaponBody.setTransform(attachTo.getPosition(), 0);
		mWeaponBody.setUserData(this);
					
		// Koppla ihop 'sv�rds'-kroppen med fiende-kroppen
		RevoluteJointDef rjdef = new RevoluteJointDef();
		rjdef.bodyA = attachTo;
		rjdef.bodyB = mWeaponBody;
		rjdef.collideConnected = false;
		rjdef.localAnchorA.x = 0f;
		rjdef.localAnchorA.y = 0f;
		rjdef.localAnchorB.x = 0f;
		rjdef.localAnchorB.y = -0.5f;
				
		// Facing left
		rjdef.enableLimit = true;

		rjdef.enableMotor = true;
		rjdef.maxMotorTorque = 20;
		rjdef.motorSpeed = 0;
					
		mJoint = (RevoluteJoint) world.createJoint(rjdef);
		mWeaponBody.setUserData(this);
		poly.dispose();
	}
	
	@Override
	public Type getType() {
		return Type.MELEE;
	}

	@Override
	public void attack(Vector2 pos, boolean facingLeft, Vector2 direction, float critChance) {
		boolean crit = rand.nextFloat() < critChance + mWeaponBaseCrit ? true : false;
		int damage = crit == true ? 
				(int)((rand.nextInt(mDamageRange[1])+mDamageRange[0]) * mWeaponCritMultiplier) : 
				rand.nextInt(mDamageRange[1])+mDamageRange[0];
		
		mLastHitDamage = damage;
		mIsCrit = crit;
	}

	@Override
	public void stopAttack() {
	}

	@Override
	public int getHitDamage() {
		return mLastHitDamage;
	}

	@Override
	public boolean isCrit() {
		return mIsCrit;
	}

	@Override
	public Body getBody() {
		return mWeaponBody;
	}

	@Override
	public Joint getJoint() {
		return mJoint;
	}

	@Override
	public Weapons getWeaponType() {
		return null;
	}
	
	public void setWeaponFacing(boolean left) {
		if (left) {
			mJoint.setLimits((float)Math.toRadians(75), (float)Math.toRadians(90));
			mJoint.setMotorSpeed((float)Math.toRadians(-720));
		} else {
			mJoint.setLimits((float)Math.toRadians(-90), (float)Math.toRadians(-75));
			mJoint.setMotorSpeed((float)Math.toRadians(720));
		}
	}
}
