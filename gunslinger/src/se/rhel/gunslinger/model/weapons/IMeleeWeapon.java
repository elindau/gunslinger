package se.rhel.gunslinger.model.weapons;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Joint;

public interface IMeleeWeapon extends IWeapon {
	public void stopAttack();
	public int getHitDamage();
	public boolean isCrit();
	public Body getBody();
	public Joint getJoint();
}
