package se.rhel.gunslinger.model.weapons;

import se.rhel.gunslinger.model.entities.ICharacter;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;
import com.badlogic.gdx.physics.box2d.World;


/**
 * Representerar en pistol
 * innehåller bullets
 * @author Emil
 *
 */
public class Gun extends RangedWeapon {

	private static int mMinDmg = 8;
	private static int mMaxDmg = 16;
	private static float mCrit = 0.1f;
	private static float mCritMultiplier = 2f;
	private static WeaponStats mStats = new WeaponStats(mMinDmg, mMaxDmg, mCrit, mCritMultiplier);
	
	public Gun(World world, boolean originPlayer, ICharacter origin) {
		super(world, originPlayer, origin, mStats, Weapons.GUN);
	}
	
	public static WeaponStats getStats() {
		return mStats;
	}
}


