package se.rhel.gunslinger.model.weapons;

import se.rhel.gunslinger.model.entities.ICharacter;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class WeaponCreator {
	
	public static IWeapon createWeapon(Weapons type, ICharacter character, World world) {
		IWeapon weapon;
		
		switch(type) {
		case GUN:
			weapon = new Gun(world, true, character);
			break;
		case SUPERGUN:
			weapon = new SuperGun(world, true, character);
			break;
		case OMEGAGUN:
			weapon = new OmegaGun(world, true, character);
			break;
		default:
			weapon = new Gun(world, true, character);
			break;
		}
		
		return weapon;
	}
	
	public enum Weapons {
		UNDEFINED,
		GUN,
		SUPERGUN,
		OMEGAGUN, 
		SHOTGUN
	}
}
