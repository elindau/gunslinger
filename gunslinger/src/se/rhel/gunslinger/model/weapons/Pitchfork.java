package se.rhel.gunslinger.model.weapons;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

public class Pitchfork extends MeleeWeapon {
	public static Vector2 DRAWSIZE = new Vector2(0.3f, 1.3f);
	
	private static final float SWING_SPEED_LEFT = (float) Math.toRadians(720);
	
	private static float SWING_UPPER_LEFT_LIMIT = (float) Math.toRadians(135);
	private static float SWING_LOWER_LEFT_LIMIT = (float) Math.toRadians(0);
	
	private static float SWING_UPPER_RIGHT_LIMIT = (float) Math.toRadians(0);
	private static float SWING_LOWER_RIGHT_LIMIT = (float) Math.toRadians(-135);
		
	//Vapenstats
	private static int[]	DAMAGE_RANGE = new int[]{1,4};
	private static float	CRIT = 0.3f;
	private static float	CRITMULTI_PLIER = 3f;
	private static Vector2 	WEAPONSIZE = new Vector2(0.05f, 0.5f);
	
	private boolean			mFirstSwing;
	
	public Pitchfork(Vector2 pos, Body attachTo, World world) {
		super(world, attachTo, WEAPONSIZE, DAMAGE_RANGE, CRIT, CRITMULTI_PLIER);
	}

	@Override
	public void attack(Vector2 pos, boolean facingLeft, Vector2 direction, float critChance) {			
		super.attack(pos, facingLeft, direction, critChance);
		attackAnimation(facingLeft);
	}
	
	@Override
	public void stopAttack() {
		mFirstSwing = false;
		mJoint.setMotorSpeed(0);
		mJoint.setLimits((float)Math.toRadians(-1), (float)Math.toRadians(1));
	}
	
	private void attackAnimation(boolean facingLeft) {
		if(!facingLeft) {
			mJoint.setLimits(SWING_LOWER_RIGHT_LIMIT, SWING_UPPER_RIGHT_LIMIT);
			
			if(mJoint.getJointAngle() >= (float) Math.toRadians(0)) {
				mFirstSwing = false;
			}
			
			if(!mFirstSwing) {
				mJoint.setMotorSpeed(-SWING_SPEED_LEFT);
				mFirstSwing = true;
			}
			
			if(mJoint.getJointAngle() <= (float) Math.toRadians(-135)) {
				mJoint.setMotorSpeed(SWING_SPEED_LEFT);
			}
			
		} else {
			
			mJoint.setLimits(SWING_LOWER_LEFT_LIMIT, SWING_UPPER_LEFT_LIMIT);
			
			if(mJoint.getJointAngle() <= (float) Math.toRadians(0)) {
				mFirstSwing = false;
			}
			
			if(!mFirstSwing) {
				mJoint.setMotorSpeed(SWING_SPEED_LEFT);
				mFirstSwing = true;
			}
			
			if(mJoint.getJointAngle() >= (float) Math.toRadians(135)) {
				mJoint.setMotorSpeed(SWING_SPEED_LEFT * -1);
			}
		}
	}
}
