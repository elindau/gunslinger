package se.rhel.gunslinger.model.weapons;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.Filter;
import se.rhel.gunslinger.model.entities.Player.State;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Cannon {
	
	private ArrayList<CannonBall> mBalls = new ArrayList<CannonBall>();
	private Vector2 mPosition;
	private Random rand = new Random();
	private World mPhysWorld;
	private Vector2 mFirePos;
	public static float CANNON_SIZE = 1f;
	private float mBallStartSize = 0.3f;

	public Cannon(Vector2 position, World world) {
		mPosition = position;
		mPhysWorld = world;

		mFirePos = new Vector2(mPosition.x + CANNON_SIZE / 2, mPosition.y + CANNON_SIZE);
	}
	
	public Vector2 getPosition() {
		return mPosition;
	}
	
	public void fire(Vector2 hitPos) {
		mBalls.add(new CannonBall(mPhysWorld, mFirePos.cpy(), hitPos));
	}
	
	public ArrayList<CannonBall> getBalls() {
		return mBalls;
	}
	
	public void removeBall(CannonBall ball) {
		mBalls.remove(ball);
	}
	
	public void update(float delta) {
		for (Iterator<CannonBall> it = mBalls.iterator(); it.hasNext();) {
			CannonBall cb = it.next();
			
			if (cb.isAlive()) {
				cb.update(delta);	
			} else {
				it.remove();
			}
		}
	}
	
	public class CannonBall {
		private static final float SPEED = 10f;
		private static final float SIZE = 0.5f;
		
		private float mSize = 0.3f;
		private Vector2 mHitPos;
		private boolean mIsAlive = true;

		private boolean mMaxHeightAchieved = false;
		private World mPhysWorld;
	//	private Body mBody;

		private Vector2 mVelocity = new Vector2();
		private Vector2 mDirection = new Vector2();
		private Vector2 mCbPosition = new Vector2();
		private float mExplosionRadius = 3f;
		private State	mState; // Kan anv�nda player-staten

		public CannonBall(World world, Vector2 startPos, Vector2 hitPos) {
			mPhysWorld = world;
			mHitPos = hitPos;
			mState = State.SPAWNING;
			mCbPosition = startPos;
			
			// Skapar upp kulans kropp
//			BodyDef def = new BodyDef();
//			def.type = BodyType.DynamicBody;
//
//			def.position.set(startPos);
//			def.bullet = true;
//			
//			mBody = mPhysWorld.createBody(def);
//			CircleShape shape = new CircleShape();
//			shape.setRadius(0.3f);
//			
//			FixtureDef fd = new FixtureDef();
//			fd.shape = shape;
//			fd.isSensor = true;
//			fd.density = 1;
//
//			fd.filter.categoryBits = -1;
//			
//
//			mBody.createFixture(fd);
//			
//			shape.dispose();
//			
//			mBody.setUserData(this);
		}
		
		public void update(float delta) {
			if (mCbPosition.y < Camera.CAMERA_HEIGHT + 5f && mMaxHeightAchieved == false) {
				mDirection = new Vector2(0,1);
				mVelocity = mDirection.nor().scl(SPEED);
				
				if (mCbPosition.y >= Camera.CAMERA_HEIGHT + 4f) {
					// mBody.setTransform(new Vector2(mHitPos.x, mCbPosition.getPosition().y), 0);
					mCbPosition = new Vector2(mHitPos.x, mCbPosition.y);
					
					mMaxHeightAchieved = true;
					mSize = 0.7f;
				}

			} else {
				mDirection = new Vector2(0, -1);
				mVelocity = mDirection.nor().scl(SPEED);
				
				//Explosionsposition uppn�dd
				if (mCbPosition.y <= mHitPos.y + 0.1f && mCbPosition.y >= mHitPos.y - 0.1f) {
					mIsAlive = false;
				}
			}
			
			if (mIsAlive) {
				// mBody.setLinearVelocity(mVelocity);
				// mCbPosition.setPosition(mSmoke.getX(), mSmoke.getY() + mSmokeSpeedY * delta);
				mCbPosition.set(new Vector2(mCbPosition.x, mCbPosition.y + mVelocity.y * delta));
				
			}
		}
		
		public void setState(State newState) {
			mState = newState;
		}
		
		public State getState() {
			return mState;
		}
		
		public boolean isAlive() {
			return mIsAlive;
		}

		public Vector2 getPosition() {
			return mCbPosition;
		}
		
		public Vector2 getHitPosition() {
			return mHitPos;
		}
		
		public float getSize() {
			return mSize;
		}
		
		public boolean isWithingExplosionRadius(Vector2 playerPos) {
			if (playerPos.x <= (mCbPosition.x - 0.3f) + mExplosionRadius / 2 && playerPos.x >= (mCbPosition.x - 0.3f) - mExplosionRadius / 2
					&& playerPos.y <= (mCbPosition.y - 0.2f)  + mExplosionRadius / 2 && playerPos.y >= (mCbPosition.y - 0.2f) - mExplosionRadius / 2) {
				return true;
			}
			return false;
		}
		
		public boolean isFalling() {
			return mMaxHeightAchieved;
		}
		
		public float scaleToHitPos() {
			float dstRange = (Camera.CAMERA_HEIGHT + 5f) - mHitPos.y;
			float hitRange = 1 - 0;
			float scale = (((dstRange - mCbPosition.y) * hitRange) / dstRange) + 0.2f;
			return scale;
		}
		
		public int getDamage() {
			return rand.nextInt(20)+20;
		}
	}
}
