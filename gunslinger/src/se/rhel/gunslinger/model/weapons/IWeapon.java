package se.rhel.gunslinger.model.weapons;

import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.math.Vector2;

public interface IWeapon {
	enum Type {
		RANGED, MELEE;
	}
	
	public Type getType();
	public Weapons getWeaponType();
	public void attack(Vector2 pos, boolean facingLeft, Vector2 direction, float critChance);
}
