package se.rhel.gunslinger.model.weapons;

import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

public class WeaponStats {
	
	private int mMinDmg;
	private int mMaxDmg;
	private float mCrit;
	private float mCritMultiplier;

	public WeaponStats(int minDmg, int maxDmg, float critchance, float critmultiplier) {
		mMinDmg = minDmg;
		mMaxDmg = maxDmg;
		mCrit = critchance;
		mCritMultiplier = critmultiplier;
	}

	public float getCritChance() {
		return mCrit;
	}

	public float getCritMultiplier() {
		return mCritMultiplier;
	}

	public int getMaxDmg() {
		return mMaxDmg;
	}
	
	public int getMinDmg() {
		return mMinDmg;
	}
	
	public static WeaponStats getWeaponStats(Weapons type) {
		WeaponStats stats;	
		
		switch(type) {
			case GUN:
				stats = Gun.getStats();
				break;
			case SUPERGUN:
				stats = SuperGun.getStats();
				break;
			case OMEGAGUN:
				stats = OmegaGun.getStats();
				break;
			default:
				stats = null;
				break;
			}
		
		return stats;
	}
	
	public static class WeaponStatComparer {
		
		public float mDmg;
		public float mCrit;
		public float mCritMultiplier;
		
		private WeaponStatComparer(float dmg, float crit, float critmultiplier) {
			mDmg = dmg;
			mCrit = crit;
			mCritMultiplier = critmultiplier;
		}
		
		/** 
		 * @param w1 = nya vapnet
		 * @param w2 = gamla vapnet
		 * @return
		 */
		public static WeaponStatComparer getComapredStats(WeaponStats w1, WeaponStats w2) {
			float dmgdif = (w2.mMinDmg + w2.mMaxDmg) / 2 - (w1.mMinDmg + w1.mMaxDmg) / 2;
			float critdif = w2.mCrit - w1.mCrit;
			float critMultiplierdif = w2.mCritMultiplier - w1.mCritMultiplier;
			
			return new WeaponStatComparer(dmgdif, critdif, critMultiplierdif);
		}
	}
}
