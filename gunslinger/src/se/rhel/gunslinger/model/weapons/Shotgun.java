package se.rhel.gunslinger.model.weapons;

import se.rhel.gunslinger.model.entities.ICharacter;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;

import com.badlogic.gdx.physics.box2d.World;

public class Shotgun extends RangedWeapon {

	public static int mMinDmg = 4;
	public static int mMaxDmg = 8;
	public static float mCrit = 0.15f; // 10%
	public static float mCritMultiplier = 3f;
	private static WeaponStats mStats = new WeaponStats(mMinDmg, mMaxDmg, mCrit, mCritMultiplier);
	
	public Shotgun(World world, boolean originPlayer, ICharacter origin) {
		super(world, originPlayer, origin, mStats, Weapons.SHOTGUN);
	}
	
	public static WeaponStats getStats() {
		return mStats;
	}
}
