package se.rhel.gunslinger.model.weapons;

import se.rhel.gunslinger.model.Filter;
import se.rhel.gunslinger.model.entities.ICharacter;
import se.rhel.gunslinger.model.entities.IEntity;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.entities.GameObject.State;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Bullet implements IEntity {

	public static final float BULLET_SIZE = 0.15f;
	private static final float SPEED = 25f;
	
	private State 	mState;
	private Vector2 mSize = new Vector2(0.15f, 0.15f);
	private Vector2 mSpawnPosition;
	private Vector2 mVelocity;
	private Vector2 mDirection;
	private Body 	mBody;
	private World 	mWorld;
	private Fixture mBulletFixture;
	private boolean	mOriginPlayer;
	private Player	mPlayer;	
	private boolean mCrit;
	private int mDamage;
	
	// Initeras fr�n spelaren position

	public Bullet(Vector2 spawn, World world, boolean originPlayer, ICharacter origin, int damage, boolean crit) {
		mSpawnPosition = spawn;
		mWorld = world;
		mVelocity = new Vector2(SPEED, 0);
		mDirection = new Vector2(1, 0);
		mState = State.DEACTIVE;
		mDamage = damage;
		mCrit = crit;
		
		mOriginPlayer = originPlayer;
		if(originPlayer)
			mPlayer = (Player) origin;
	}
	
	/**
	 * Skjuter iv�g kulan
	 */
	public void fire(boolean facingLeft, Vector2 direction) {
		if (direction.equals(Vector2.Zero)) {
			if(facingLeft) {
				mDirection.x *= -1;
				mSpawnPosition.x -= 0.1f;
			}
		} else {
			mDirection = direction;
		}
			
		// Aktivera kulan
		mState = State.ACTIVE;
		
		// Skapar upp kulans kropp
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		def.position.x = mSpawnPosition.x;
		def.position.y = mSpawnPosition.y;
		def.bullet = true;
		
		mBody = mWorld.createBody(def);
		CircleShape shape = new CircleShape();
		// Vilken storlek
		shape.setRadius(BULLET_SIZE / 2);
		
		FixtureDef fd = new FixtureDef();
		fd.shape = shape;
		fd.isSensor = false;
		fd.density = 1;
		
		if(mOriginPlayer) {
			fd.filter.categoryBits = Filter.CATEGORY_PLAYER_BULLETS;
			fd.filter.maskBits = Filter.MASK_PLAYER_BULLET;
		} else {
			fd.filter.categoryBits = Filter.CATEGORY_ENEMY_BULLETS;
			fd.filter.maskBits = Filter.MASK_ENEMY_BULLET;
		}


		mBulletFixture = mBody.createFixture(fd);
		
		shape.dispose();
		
		mBody.setUserData(this);
	}
	
	public void update(float delta) {
		mVelocity = mDirection.nor().scl(SPEED);
		mBody.setLinearVelocity(mVelocity);	
	}
	
	public Player getPlayerWhoFired() {
		return mPlayer;
	}
	
	public State getState() {
		return mState;
	}
	
	public void setState(State newState) {
		mState = newState;
	}
	
	@Override
	public Vector2 getPosition() {
		return mBody.getPosition();
	}

	public boolean isCrit() {
		return mCrit;
	}

	public int getDamage() {
		return mDamage;
	}

	@Override
	public Vector2 getSize() {
		return mSize;
	}

	@Override
	public int compare(IEntity entity) {
		if (getBottomY() < entity.getBottomY()) return -1;
		else if (getBottomY() > entity.getBottomY()) return 1;
		else return 0;
	}

	@Override
	public float getBottomY() {
		return getPosition().y - getSize().y / 2;
	}
}