package se.rhel.gunslinger.model;

import java.util.HashMap;

public enum Levels {
	LEVEL1(1), LEVEL2(2);
	
	private final int abbreviation;
	private static final HashMap<Integer, Levels> lookup = new HashMap<Integer, Levels>();
	static {
		for (Levels l : Levels.values())
			lookup.put(l.getAbbreviation(), l);
	}
	
	private Levels(int abbreviation) {
		this.abbreviation = abbreviation;
	}
	
	public int getAbbreviation() {
		return abbreviation;
	}
	
	public static Levels get(int abbreviation) {
		return lookup.get(abbreviation);
	}
}
