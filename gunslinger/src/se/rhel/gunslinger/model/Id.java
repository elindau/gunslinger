package se.rhel.gunslinger.model;

public class Id {
	
	private int ID = 0;
	
	private static final Id INSTANCE = new Id();
	
	public static Id getInstance() {
		return INSTANCE;
	}
	
	public int get() {
		++ID;
		return ID;
	}
}
