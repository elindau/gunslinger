package se.rhel.gunslinger.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * Statisk klass f�r att skapa upp fysikkroppar
 * @author Emil & Rickard
 *
 */

public class PhysicsBodyBuilder {
	
	public static float TO_DEGREES = (1 / (float) Math.PI) * 180;
	public static float TO_RADIANS = (1 / 180.0f) * (float) Math.PI;
	
	private static BodyDef mBodyDef;
	private static Body mBody;
	private static Shape shape;
	
	public static Body createNewCircleBody(World world, BodyType type, Vector2 position, boolean allowSleep, float radius, float density, float friction, float restitution) {
		mBodyDef = new BodyDef();	
		mBodyDef.type = type;
		mBodyDef.position.set(position);
		mBodyDef.allowSleep = allowSleep;
		
		mBody = world.createBody(mBodyDef);
		mBody.createFixture(fixtureDef(createCircle(radius), density, friction, restitution));
		shape.dispose();
		
		return mBody;
	}

	public static Body createNewPolygonBody(World world, BodyType type, Vector2 position, boolean allowSleep, float density, float friction, float restitution) {
		mBodyDef = new BodyDef();	
		mBodyDef.type = type;
		mBodyDef.position.set(position);
		mBodyDef.allowSleep = allowSleep;
		
		mBody = world.createBody(mBodyDef);
		mBody.createFixture(fixtureDef(createBox(1f / 2f), density, friction, restitution));
		shape.dispose();
		
		return mBody;
	}
	
	// �verlagrad med liten rotation
	public static Body createNewPolygonBody(World world, BodyType type, Vector2 position, boolean allowSleep, float density, float friction, float restitution, float angle) {
		mBodyDef = new BodyDef();	
		mBodyDef.type = type;
		//mBodyDef.position.set(position);
		mBodyDef.allowSleep = allowSleep;
		
		angle = angle * TO_RADIANS;
		
		if(angle < 0) {
			position.x -= 0.2f;
			position.y += 0.15f;
		} else {
			position.x += 0.2f;
			position.y += 0.15f;
		}

		mBody = world.createBody(mBodyDef);
		mBody.setTransform(position, angle);
		mBody.createFixture(fixtureDef(createBox(1f / 4f), density, friction, restitution));
		shape.dispose();
		
		mBody = world.createBody(mBodyDef);
		if(angle < 0) {
			position.x += 0.4f;
		} else {
			position.x -= 0.4f;
		}
		
		mBody.setTransform(position, 0);
		mBody.createFixture(fixtureDef(createBox((1f / 2f) * 0.7f), density, friction, restitution));
		shape.dispose();
		
		return mBody;
	}
	
	public static Body createNewPolygonBody(World world, BodyType type, Vector2 position, boolean allowSleep, float density, float friction, float restitution, Vector2[] vertices) {
		mBodyDef = new BodyDef();	
		mBodyDef.type = type;
		mBodyDef.position.set(position);
		mBodyDef.allowSleep = allowSleep;
		
		mBody = world.createBody(mBodyDef);
		mBody.createFixture(fixtureDef(createPolyline(vertices), density, friction, restitution));
		shape.dispose();
		
		return mBody;
	}
	
	public static Shape createPolyline(Vector2[] verts) {
		shape = new PolygonShape();
		((PolygonShape) shape).set(verts);

		/*
        Vector2[] vertices = new Vector2[8];
        vertices[0] = new Vector2(8.2f  , 0.0f  );
        vertices[1] = new Vector2(14.6f , 4.0f  );
        vertices[2] = new Vector2(38.5f , 26.8f);
        vertices[3] = new Vector2(32.2f , 34.1f);
        vertices[4] = new Vector2(22.5f , 32.2f);
        vertices[5] = new Vector2(28.2f , 39.8f);     
        vertices[6] = new Vector2(16.1f , 45.7f);
        vertices[7] = new Vector2(13.5f , 29.8f);
        shape = new PolygonShape();
        ((PolygonShape)shape).set(vertices);
        */
		return shape;
	}
	
	public static Shape createCircle(float radius) {
		shape = new CircleShape();
		shape.setRadius(radius);
		
		return shape;
	}
	
	public static Shape createBox(float size) {
		shape = new PolygonShape();
		((PolygonShape) shape).setAsBox(size, size);
		return shape;
	}
	
	public static FixtureDef fixtureDef(Shape shape, float density, float friction, float restitution) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = density;
		fixtureDef.friction = friction;
		fixtureDef.restitution = restitution;
		
		return fixtureDef;
	}
	
}
