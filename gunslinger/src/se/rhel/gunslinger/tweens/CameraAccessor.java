package se.rhel.gunslinger.tweens;

import se.rhel.gunslinger.model.CameraLookAt;

import com.badlogic.gdx.math.Vector2;
import aurelienribon.tweenengine.TweenAccessor;

public class CameraAccessor implements TweenAccessor<CameraLookAt> {

	public static final int POSITION_X = 1;
	public static final int POSITION_Y = 2;
	public static final int POSITION_XY = 3;
	
	@Override
	public int getValues(CameraLookAt target, int tweenType, float[] returnValues) {
		switch(tweenType) {
		case POSITION_X:
			returnValues[0] = target.position().x; return 1;
		case POSITION_Y:
			returnValues[0] = target.position().y; return 1;
		case POSITION_XY:
			returnValues[0] = target.position().x;
			returnValues[1] = target.position().y;
			return 2;
		default: assert false; return -1;
		}
	}

	@Override
	public void setValues(CameraLookAt target, int tweenType, float[] newValues) {
		switch(tweenType) {
		case POSITION_X:
			target.setPosition(new Vector2(newValues[0], target.position().y));
			break;
		case POSITION_Y:
			target.setPosition(new Vector2(target.position().x, newValues[0]));
			break;
		case POSITION_XY:
			target.setPosition(new Vector2(newValues[0], newValues[1]));
			break;
		default: assert false; break;
		}
		
	}

}
