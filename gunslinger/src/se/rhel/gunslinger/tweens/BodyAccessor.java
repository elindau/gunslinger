package se.rhel.gunslinger.tweens;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import aurelienribon.tweenengine.TweenAccessor;

public class BodyAccessor implements TweenAccessor<Body> {

	public static final int POSITION_X = 1;
	public static final int POSITION_Y = 2;
	public static final int POSITION_XY = 3;
	
	@Override
	public int getValues(Body target, int tweenType, float[] returnValues) {
		switch(tweenType) {
		case POSITION_X:
			returnValues[0] = target.getPosition().x; return 1;
		case POSITION_Y:
			returnValues[0] = target.getPosition().y; return 1;
		case POSITION_XY:
			returnValues[0] = target.getPosition().x;
			returnValues[1] = target.getPosition().y;
			return 2;
		default: assert false; return -1;
		}
	}

	@Override
	public void setValues(Body target, int tweenType, float[] newValues) {
		switch(tweenType) {
		case POSITION_X:
			target.setTransform(new Vector2(newValues[0], target.getPosition().y), 0);
			break;
		case POSITION_Y:
			target.setTransform(new Vector2(target.getPosition().x, newValues[0]), 0);
			break;
		case POSITION_XY:
			target.setTransform(new Vector2(newValues[0], newValues[1]), 0);
			break;
		default: assert false; break;
		}
	}

}
