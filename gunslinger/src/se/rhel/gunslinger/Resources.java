package se.rhel.gunslinger;

import se.rhel.gunslinger.model.options.GameState;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Json;
import com.moribitotech.mtx.settings.AppSettings;

/**
 * Using SINGLETON Pattern
 * @author Emil
 *
 */
public class Resources {

	private static final Resources INSTANCE = new Resources();
	
	public AssetManager assetManager;
	
	public Texture playerTexture;
	public Texture enemyTexture;
	public Texture gunTexture;
	public Texture signTexture;
	public Texture messageBgTexture;
	public Texture borbounTexture;
	public Texture logo;
	public Texture foregroundGrassTexture1;
	public Texture foregroundGrassTexture2;
	public Texture foregroundGrassShadowTexture;
	public Texture bulletTexture;
	public Texture targetRing;
	public Texture pitchForkTexture;
	public Texture health;
	public Texture enemyPointerTexture;
	public Texture playerSpriteSheet;
	public Texture characterShadow;
	public Texture weaponInfo;
	public Texture cannon;
	public Texture cannon_ball;
	public Texture cowboyAnimation;
	public Texture optionsBg;
	public Texture cactus;
	public Texture deadCowboy;
	public Texture small_cactus;
	public Texture wepinfo_full;
	public Texture ui;
	public Texture changeScreen;
	public Texture endBg;
	public Texture stone;
	
	//Avatar
	public Texture player_avatar;
	public Texture sheriff_avatar;
	
	public Texture shootButton;
	public Texture pickupButton;
	
	public Texture level;
	public Texture levelBg;
	public Texture levelPxBg;
	
	public Texture levelPx0;
	public Texture levelPx1;
	public Texture levelPx2;
	
	public Texture levelMap; 
	public Texture shopBg;
	public Texture cutSceneBg;
	
	public TextureAtlas menuAtlas;
	public TextureAtlas checkboxAtlas;
	public TextureAtlas gotoAtlas;
	public TextureAtlas bubbleAtlas;
	public TextureAtlas playerActorAtlas;
	public TextureAtlas chooseAtlas;
	public TextureAtlas stageArrowAtlas;
	public TextureAtlas tombstoneAtlas;
	public TextureAtlas respawnAtlas;
	public TextureAtlas coinAtlas;
	public TextureAtlas cowboyAtlas;
	public TextureAtlas bourbonAtlas;
	public TextureAtlas sliderAtlas;
	public TextureAtlas tiledBgsAtlas;
	public TextureAtlas selectBoxAtlas;
	public TextureAtlas farmerAtlas;
	public TextureAtlas	chooseLevelAtlas;
	public TextureAtlas gunAtlas;
	public TextureAtlas muzzle;
	public TextureAtlas enemyDie;
	public TextureAtlas dust;
	public TextureAtlas	cannon_explosion;
	public TextureAtlas smoke;
	public TextureAtlas	coinPickup;
	public TextureAtlas bloodSplatter;
	public TextureAtlas playerDamage;
	public TextureAtlas shop;
	public TextureAtlas	clerk;
	public TextureAtlas	shopbuttons;
	public TextureAtlas	gunSelect;
	public TextureAtlas shoot;
	public TextureAtlas healingButton;
	public TextureAtlas boss;
	public TextureAtlas sheriff;
	public TextureAtlas leave;
	public TextureAtlas timeline;
	
	public BitmapFont font;
	public BitmapFont font5x5;
	
	public Music theme;
	public Music menu;
	public Music end;
	
	public Sound[] coinSounds;
	public Sound[] enemyDeaths;
	public Sound[] shots;
	public Sound[] explosions;
	public Sound[] damageTaken;
	public Sound[] explosions_small;
	public Sound   bourbonPickup;
	public Sound   playerDeath;
	public Sound   gunPickup;
	public Sound   denied;
	
	public ParticleEffect blood;
	public ParticleEffect explosion;
	public Texture		  particle;
	
	// State
	public GameState gameState;
	
	public void loadGameResources() {
		assetManager.load("gfx/playerSheet.png", Texture.class);
		assetManager.load("gfx/enemySheet.png", Texture.class);
		assetManager.load("gfx/objects/revolver_small.png", Texture.class);
		assetManager.load("gfx/objects/sign.png", Texture.class);
		assetManager.load("gfx/messageBg.png", Texture.class);
		assetManager.load("gfx/logo.png", Texture.class);
		assetManager.load("gfx/grass1.png", Texture.class);
		assetManager.load("gfx/grass2.png", Texture.class);
		assetManager.load("gfx/grass_shadow.png", Texture.class);
		assetManager.load("gfx/objects/bourbon.png", Texture.class);
		assetManager.load("gfx/target_ring.png", Texture.class);
		assetManager.load("gfx/bullet.png", Texture.class);
		assetManager.load("gfx/pitchfork.png", Texture.class);
		assetManager.load("gfx/enemyPointer.png", Texture.class);
		assetManager.load("gfx/player_sprite48.png", Texture.class);
		assetManager.load("gfx/pickupbutton.png", Texture.class);
		assetManager.load("gfx/shootbutton.png", Texture.class);
		assetManager.load("gfx/ui.png", Texture.class);
		assetManager.load("gfx/wepinfo.png", Texture.class);

		assetManager.load("gfx/cannon.png", Texture.class);
		assetManager.load("gfx/cannonball.png", Texture.class);
		assetManager.load("gfx/cowboyAnimation.png", Texture.class);
		assetManager.load("gfx/optionsBg.png", Texture.class);
		assetManager.load("gfx/cactus.png", Texture.class);
		assetManager.load("gfx/stone.png", Texture.class);
		assetManager.load("gfx/dead_cowboy.png", Texture.class);
		assetManager.load("gfx/small_cactus.png", Texture.class);
		assetManager.load("gfx/cutscenebg.png", Texture.class);
		assetManager.load("gfx/wepinfo_full.png", Texture.class);
		assetManager.load("gfx/player_avatar.png", Texture.class);
		assetManager.load("gfx/sheriff_avatar.png", Texture.class);
		assetManager.load("gfx/changescreen.png", Texture.class);
		assetManager.load("gfx/endBg.png", Texture.class);
		
		
		// Ladda in texturena till alla levlar
		// 1
		assetManager.load("gfx/level/1/bg.png", Texture.class);
		assetManager.load("gfx/level/1/level.png", Texture.class);
		assetManager.load("gfx/level/1/plx_bg.png", Texture.class);
		assetManager.load("gfx/level/1/plx_0.png", Texture.class);
		assetManager.load("gfx/level/1/plx_1.png", Texture.class);
		assetManager.load("gfx/level/1/plx_2.png", Texture.class);
		// 2
		assetManager.load("gfx/level/2/level.png", Texture.class);
		assetManager.load("gfx/level/2/bg.png", Texture.class);
		assetManager.load("gfx/level/2/plx_bg.png", Texture.class);
		assetManager.load("gfx/level/2/plx_0.png", Texture.class);
		assetManager.load("gfx/level/2/plx_1.png", Texture.class);
		assetManager.load("gfx/level/2/plx_2.png", Texture.class);
		
		assetManager.load("gfx/health.png", Texture.class);
		assetManager.load("gfx/shadow.png", Texture.class);
		assetManager.load("gfx/levelMap.png", Texture.class);
		assetManager.load("gfx/particle.png", Texture.class);
		assetManager.load("gfx/shopBg.png", Texture.class);
		
		assetManager.load("gfx/packs/button.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/goto.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/bubble.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/playerAnimation.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/choose.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/stagearrow.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/tombstone.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/respawn.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/coin.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/cowboy.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/farmer.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/checkbox.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/bourbon.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/slider.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/tiled.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/selectbox.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/chooselevelbuttons.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/gun.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/muzzle.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/enemydie.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/dust.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/explosion.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/smoke.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/coinpickup.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/bloodsplatter.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/playerdamage.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/shop.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/clerk.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/shopbuttons.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/gunselect.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/shoot.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/healingbutton.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/boss.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/sheriff.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/leave.pack", TextureAtlas.class);
		assetManager.load("gfx/packs/timeline.pack", TextureAtlas.class);
		
		assetManager.load("audio/music/theme.mp3", Music.class);
		assetManager.load("audio/music/menu.mp3", Music.class);
		assetManager.load("audio/music/end.mp3", Music.class);
		
		assetManager.load("audio/sound/coin1.wav", Sound.class);
		assetManager.load("audio/sound/coin2.wav", Sound.class);
		assetManager.load("audio/sound/enemydeath1.wav", Sound.class);
		assetManager.load("audio/sound/enemydeath2.wav", Sound.class);
		assetManager.load("audio/sound/shoot1.wav", Sound.class);
		assetManager.load("audio/sound/shoot2.wav", Sound.class);
		assetManager.load("audio/sound/shoot3.wav", Sound.class);
		assetManager.load("audio/sound/shoot4.wav", Sound.class);
		assetManager.load("audio/sound/shoot5.wav", Sound.class);
		assetManager.load("audio/sound/shot1.wav", Sound.class);
		assetManager.load("audio/sound/shot2.wav", Sound.class);
		assetManager.load("audio/sound/bourbon.wav", Sound.class);
		assetManager.load("audio/sound/explosion1.wav", Sound.class);
		assetManager.load("audio/sound/explosion2.wav", Sound.class);
		assetManager.load("audio/sound/hit1.wav", Sound.class);
		assetManager.load("audio/sound/hit2.wav", Sound.class);
		assetManager.load("audio/sound/hit3.wav", Sound.class);
		assetManager.load("audio/sound/expl1.wav", Sound.class);
		assetManager.load("audio/sound/expl2.wav", Sound.class);
		assetManager.load("audio/sound/expl3.wav", Sound.class);
		assetManager.load("audio/sound/death.wav", Sound.class);
		assetManager.load("audio/sound/gunpickup.wav", Sound.class);
		assetManager.load("audio/sound/denied.wav", Sound.class);
		
		assetManager.load("data/fonts/hud.fnt", BitmapFont.class);
		assetManager.load("data/fonts/5x5.fnt", BitmapFont.class);
		
		//Ladda in inst�llningar f�r MTX
		AppSettings.setUp();
	}
	
	public void setGameInstances() {
		
		// Texturer
		enemyTexture = assetManager.get("gfx/enemySheet.png", Texture.class);
		playerTexture = assetManager.get("gfx/playerSheet.png", Texture.class);
		gunTexture = assetManager.get("gfx/objects/revolver_small.png", Texture.class);
		signTexture = assetManager.get("gfx/objects/sign.png", Texture.class);
		messageBgTexture = assetManager.get("gfx/messageBg.png", Texture.class);
		logo = assetManager.get("gfx/logo.png", Texture.class);
		foregroundGrassTexture1 = assetManager.get("gfx/grass1.png", Texture.class);
		foregroundGrassTexture2 = assetManager.get("gfx/grass2.png", Texture.class);
		foregroundGrassShadowTexture = assetManager.get("gfx/grass_shadow.png", Texture.class);
		borbounTexture = assetManager.get("gfx/objects/bourbon.png", Texture.class);
		targetRing = assetManager.get("gfx/target_ring.png", Texture.class);
		bulletTexture = assetManager.get("gfx/bullet.png", Texture.class);
		pitchForkTexture = assetManager.get("gfx/pitchfork.png", Texture.class);
		health = assetManager.get("gfx/health.png", Texture.class);
		enemyPointerTexture = assetManager.get("gfx/enemyPointer.png", Texture.class);
		playerSpriteSheet = assetManager.get("gfx/player_sprite48.png", Texture.class);
		characterShadow = assetManager.get("gfx/shadow.png", Texture.class);
		shootButton = assetManager.get("gfx/shootbutton.png", Texture.class);
		pickupButton = assetManager.get("gfx/pickupbutton.png", Texture.class);
		ui = assetManager.get("gfx/ui.png", Texture.class);
		changeScreen = assetManager.get("gfx/changescreen.png", Texture.class);
		endBg = assetManager.get("gfx/endBg.png", Texture.class);

		weaponInfo = assetManager.get("gfx/wepinfo.png", Texture.class);

		cannon = assetManager.get("gfx/cannon.png", Texture.class);
		cannon_ball = assetManager.get("gfx/cannonball.png", Texture.class);
		cowboyAnimation = assetManager.get("gfx/cowboyAnimation.png", Texture.class);
		optionsBg = assetManager.get("gfx/optionsBg.png", Texture.class);
		cactus = assetManager.get("gfx/cactus.png", Texture.class);
		stone = assetManager.get("gfx/stone.png", Texture.class);
		deadCowboy = assetManager.get("gfx/dead_cowboy.png", Texture.class);
		small_cactus = assetManager.get("gfx/small_cactus.png", Texture.class);
		particle = assetManager.get("gfx/particle.png", Texture.class);
		shopBg = assetManager.get("gfx/shopBg.png", Texture.class);
		cutSceneBg = assetManager.get("gfx/cutscenebg.png", Texture.class);
		wepinfo_full = assetManager.get("gfx/wepinfo_full.png", Texture.class);
		player_avatar = assetManager.get("gfx/player_avatar.png", Texture.class);
		sheriff_avatar = assetManager.get("gfx/sheriff_avatar.png", Texture.class);
		
		// Level
		levelBg = assetManager.get("gfx/level/1/bg.png", Texture.class);
		level = assetManager.get("gfx/level/1/level.png", Texture.class);
		levelPxBg = assetManager.get("gfx/level/1/plx_bg.png", Texture.class);
		levelPx0 = assetManager.get("gfx/level/1/plx_0.png", Texture.class);
		levelPx1 = assetManager.get("gfx/level/1/plx_1.png", Texture.class);
		levelPx2 = assetManager.get("gfx/level/1/plx_2.png", Texture.class);
		
		levelMap = assetManager.get("gfx/levelMap.png", Texture.class);
		
		// TextureAtlases
		menuAtlas = assetManager.get("gfx/packs/button.pack", TextureAtlas.class);
		gotoAtlas = assetManager.get("gfx/packs/goto.pack", TextureAtlas.class);
		bubbleAtlas = assetManager.get("gfx/packs/bubble.pack", TextureAtlas.class);
		playerActorAtlas = assetManager.get("gfx/packs/playerAnimation.pack", TextureAtlas.class);
		chooseAtlas = assetManager.get("gfx/packs/choose.pack", TextureAtlas.class);
		stageArrowAtlas = assetManager.get("gfx/packs/stagearrow.pack", TextureAtlas.class);
		tombstoneAtlas = assetManager.get("gfx/packs/tombstone.pack", TextureAtlas.class);
		respawnAtlas = assetManager.get("gfx/packs/respawn.pack", TextureAtlas.class);
		coinAtlas = assetManager.get("gfx/packs/coin.pack", TextureAtlas.class);
		cowboyAtlas = assetManager.get("gfx/packs/cowboy.pack", TextureAtlas.class);
		farmerAtlas = assetManager.get("gfx/packs/farmer.pack", TextureAtlas.class);
		checkboxAtlas = assetManager.get("gfx/packs/checkbox.pack", TextureAtlas.class);
		bourbonAtlas = assetManager.get("gfx/packs/bourbon.pack", TextureAtlas.class);
		sliderAtlas = assetManager.get("gfx/packs/slider.pack", TextureAtlas.class);
		tiledBgsAtlas = assetManager.get("gfx/packs/tiled.pack", TextureAtlas.class);
		selectBoxAtlas = assetManager.get("gfx/packs/selectbox.pack", TextureAtlas.class);
		chooseLevelAtlas = assetManager.get("gfx/packs/chooselevelbuttons.pack", TextureAtlas.class);
		gunAtlas = assetManager.get("gfx/packs/gun.pack", TextureAtlas.class);
		muzzle = assetManager.get("gfx/packs/muzzle.pack", TextureAtlas.class);
		enemyDie = assetManager.get("gfx/packs/enemydie.pack", TextureAtlas.class);
		dust = assetManager.get("gfx/packs/dust.pack", TextureAtlas.class);
		cannon_explosion = assetManager.get("gfx/packs/explosion.pack", TextureAtlas.class);
		smoke = assetManager.get("gfx/packs/smoke.pack", TextureAtlas.class);
		coinPickup = assetManager.get("gfx/packs/coinpickup.pack", TextureAtlas.class);
		bloodSplatter = assetManager.get("gfx/packs/bloodsplatter.pack", TextureAtlas.class);
		playerDamage = assetManager.get("gfx/packs/playerdamage.pack", TextureAtlas.class);
		clerk = assetManager.get("gfx/packs/clerk.pack", TextureAtlas.class);
		shop = assetManager.get("gfx/packs/shop.pack", TextureAtlas.class);
		shopbuttons = assetManager.get("gfx/packs/shopbuttons.pack", TextureAtlas.class);
		gunSelect = assetManager.get("gfx/packs/gunselect.pack", TextureAtlas.class);
		shoot = assetManager.get("gfx/packs/shoot.pack", TextureAtlas.class);
		healingButton = assetManager.get("gfx/packs/healingbutton.pack", TextureAtlas.class);
		boss = assetManager.get("gfx/packs/boss.pack", TextureAtlas.class);
		sheriff = assetManager.get("gfx/packs/sheriff.pack", TextureAtlas.class);
		leave = assetManager.get("gfx/packs/leave.pack", TextureAtlas.class);
		timeline = assetManager.get("gfx/packs/timeline.pack", TextureAtlas.class);
		
		// Musik
		theme = assetManager.get("audio/music/theme.mp3", Music.class);
		menu = assetManager.get("audio/music/menu.mp3", Music.class);
		end = assetManager.get("audio/music/end.mp3", Music.class);
		
		// Ljud
		coinSounds = new Sound[] {
				assetManager.get("audio/sound/coin1.wav"), 
				assetManager.get("audio/sound/coin2.wav") 
		};
		enemyDeaths = new Sound[] {
				assetManager.get("audio/sound/enemydeath1.wav"), 
				assetManager.get("audio/sound/enemydeath2.wav") 
		};
		shots = new Sound[] { 
				assetManager.get("audio/sound/shot1.wav"), 
				assetManager.get("audio/sound/shot2.wav") 
		};
		explosions = new Sound[] {
				assetManager.get("audio/sound/explosion1.wav", Sound.class), 
				assetManager.get("audio/sound/explosion2.wav", Sound.class)
		};
		damageTaken = new Sound[] {
				//assetManager.get("audio/sound/hit1.wav", Sound.class), 
				//assetManager.get("audio/sound/hit2.wav", Sound.class)
				assetManager.get("audio/sound/hit3.wav", Sound.class)
		};
		explosions_small = new Sound[] {
				assetManager.get("audio/sound/expl1.wav", Sound.class), assetManager.get("audio/sound/expl2.wav", Sound.class),
				assetManager.get("audio/sound/expl3.wav", Sound.class)
		};
		gunPickup = assetManager.get("audio/sound/gunpickup.wav", Sound.class);
		bourbonPickup = assetManager.get("audio/sound/bourbon.wav", Sound.class);
		playerDeath = assetManager.get("audio/sound/death.wav", Sound.class);
		denied = assetManager.get("audio/sound/denied.wav", Sound.class);
 		
		// Font
		font = assetManager.get("data/fonts/hud.fnt", BitmapFont.class);
		font5x5 = assetManager.get("data/fonts/5x5.fnt", BitmapFont.class);
		
		// Partikeleffekter
		blood = new ParticleEffect();
		blood.load(Gdx.files.internal("data/particle/blood.p"), Gdx.files.internal("data/particle"));
		explosion = new ParticleEffect();
		explosion.load(Gdx.files.internal("data/particle/explosion.p"), Gdx.files.internal("data/particle"));
		
		// Ladda in state - l�s fr�n fil
		if(Gdx.files.local(GameState.PATH).exists()) {
			FileHandle file = Gdx.files.local(GameState.PATH);
			Json json = new Json();
			gameState = json.fromJson(GameState.class, file);
		} else {
			gameState = new GameState();
		}

	}
	
	public void switchLevel(int levelNo) {
		levelBg = assetManager.get("gfx/level/"+levelNo+"/bg.png", Texture.class);
		level = assetManager.get("gfx/level/"+levelNo+"/level.png", Texture.class);
		levelPxBg = assetManager.get("gfx/level/"+levelNo+"/plx_bg.png", Texture.class);
		levelPx0 = assetManager.get("gfx/level/"+levelNo+"/plx_0.png", Texture.class);
		levelPx1 = assetManager.get("gfx/level/"+levelNo+"/plx_1.png", Texture.class);
		levelPx2 = assetManager.get("gfx/level/"+levelNo+"/plx_2.png", Texture.class);
	}
	
	public static void prepareManager() {
		getInstance().assetManager = new AssetManager();
	}
	
	public static Resources getInstance() {
		return INSTANCE;
	}
}
