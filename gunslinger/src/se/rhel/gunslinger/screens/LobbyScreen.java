package se.rhel.gunslinger.screens;

import java.io.IOException;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.network.KryoClient;
import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.network.Network.JoinReplyFlag;
import se.rhel.gunslinger.view.ForegroundRender;
import se.rhel.gunslinger.view.SoundManager;
import se.rhel.gunslinger.view.SoundManager.MusicType;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.moribitotech.mtx.AbstractScreen;

/**
 * Screen f�r n�tverkslobby
 * Ser till att spelet k�rs ig�ng hos alla samtidigt
 * Extendar AbstractScreen (mtx.jar)
 * @author Rickard
 *
 */
public class LobbyScreen extends AbstractScreen {
	
	private static final String SCREEN_NAME = "Lobby Menu";
	
	private Label 			mGameReadyLabel;
	private KryoServer 		mServer;
	private KryoClient 		mClient;
	private boolean 		isHost;
	private String 			mName;
	private String 			mIp;
	private int 			mNumberOfPlayers;
	private float			gameReadyTimer = 0f;
	private TextButton		mLeaveButton;
	private Table			mTable;
	
	private Gunslinger		mGame;
	
	private ForegroundRender mForegroundRender;
	private BaseStage		mStage;
	private Camera			mCamera;
	private TweenManager	mManager;
	
	/**
	 * Kontruktor f�r att hosta ett spel
	 * @param game
	 * @param name
	 * @param maxPlayers
	 */
	public LobbyScreen(Gunslinger game, String name, int maxPlayers, ForegroundRender fgr) {
		super(game, SCREEN_NAME);
		this.isHost = true;
		mName = name;
		mNumberOfPlayers = maxPlayers;
		
		mGame = game;
		
		init(fgr);
	}
	
	/**
	 * Konstruktor f�r att joina ett spel
	 * @param game
	 * @param name
	 * @param ip
	 */
	public LobbyScreen(Gunslinger game, String name, String ip, ForegroundRender fgr) {
		super(game, SCREEN_NAME);
		this.isHost = false;
		mName = name;
		mIp = ip;
		
		mGame = game;
		
		init(fgr);
	}
	
	private void init(ForegroundRender fgr) {
		mStage = new BaseStage(getStage().getSpriteBatch());
		mCamera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		getStage().setViewport(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT, false);
		
		mForegroundRender = fgr;
		mForegroundRender.setSpriteBatch(getStage().getSpriteBatch());
		mManager = fgr.getManager();
	}

	@Override
	public void show() {
		Texture txt = Resources.getInstance().logo;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());		
		setBackgroundTexture(region);
		
		mGameReadyLabel = new Label("", UIcomponents.LabelStyle(Resources.getInstance().font, Color.WHITE));
		mGameReadyLabel.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight());
		
		Skin leaveSkin = new Skin(Resources.getInstance().menuAtlas);
		mLeaveButton = UIcomponents.button("Leave", UIcomponents.BaseTextButtonStyle(leaveSkin, Resources.getInstance().font, Color.WHITE, "button", "buttonpressed"), new Rectangle(0,0, 256, 64));
		
		mLeaveButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});
		
		mTable = new Table();
		mTable.setFillParent(true);
		mTable.add(mGameReadyLabel);
		mTable.row().padTop(20);
		mTable.add(mLeaveButton);
		
		mStage.addActor(mTable);
		
		//Skapa upp en klient
		mClient = new KryoClient(mName);
		
		//Kolla om man hostar server
		if (isHost) {
			try {
				//Skapa upp server med x antal slots och koppla upp sin egen klient
				mServer = new KryoServer(mNumberOfPlayers);
				// Skapa kopplingen i basklassen
				mGame.setServer(mServer);
				mClient.connectLocal();
			} catch (IOException e) {
				e.printStackTrace();
				mServer.shutdown();
			}
		} else {
			//Koppla upp klient mot specad IP-adress
			try {
				mClient.connect(mIp);
			} catch (IOException e) {
				getGame().setScreen(new JoinGameScreen((Gunslinger)getGame(), "No server with that ip", mForegroundRender));
			}
		}
		
		Gdx.input.setInputProcessor(mStage);
		Gdx.input.setCatchBackKey(true);
		
		getStage().addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.BACK) {
					back();
				}
				return true;
			}
		});
	}
	
	private void back() {
		if(mClient != null) {
			mClient.shutdown();
		}
		
		if(isHost) {
			if(mServer != null) {
				mServer.shutdown();
			}
			getGame().setScreen(new HostGameScreen((Gunslinger)getGame(), mForegroundRender));
		} else {
			getGame().setScreen(new JoinGameScreen((Gunslinger)getGame(), mForegroundRender));
		}
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		update(delta);
		
		if (!mClient.isGameReady()) {
			mGameReadyLabel.setText(String.valueOf(mClient.getWorldModel().getPlayers().size() + "/" + mClient.getServerSlots() + " Players"));
		} else {
			mGameReadyLabel.setText("Starting game in: " + Math.round(gameReadyTimer));
			
			// Leave-knappen ska tas bort
			mTable.removeActor(mLeaveButton);
		}
		
		mManager.update(delta);
		
		getStage().getSpriteBatch().begin();
		
		mForegroundRender.draw(delta);	
		mStage.draw();
		
		getStage().getSpriteBatch().end();
	}

	public void update(float delta) {
		if (isHost) {
			//Kolla om serverslotsen �r uppfyllda med spelare
			if (mServer.isGameReady()) {
				if ((gameReadyTimer -= delta) < 0) {
					//getGame().setScreen(new GameScreen((Gunslinger)getGame(), mServer, mClient));
					getGame().setScreen(new ChooseLevelScreen((Gunslinger)getGame(), mServer, mClient, null));
				}
			}
		} else {
			if (mClient.getFlag() != JoinReplyFlag.WAITING) { //Kolla om clienten f�tt svar ifr�n en server
				if (mClient.isClientConnected()) {
					if (mClient.isGameReady()) { //Kolla om spelet �r redo att k�ras ig�ng
						if ((gameReadyTimer -= delta) < 0) {
							// getGame().setScreen(new GameScreen((Gunslinger)getGame(), mClient));
							getGame().setScreen(new ChooseLevelScreen((Gunslinger)getGame(), mClient, null));
						}
					}
				} else {
					//getGame().setScreen(new JoinGameScreen((Gunslinger)getGame(), "Server Full"));
				}
			}
		}
	}

	@Override
	public void hide() {
		SoundManager.getInstance().stopMusic(MusicType.MENUTHEME);
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {}
	
	@Override
	public void resize(int width, int height) {}
}
