package se.rhel.gunslinger.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.HashMap;
import java.util.Map;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.CameraLookAt;
import se.rhel.gunslinger.model.Id;
import se.rhel.gunslinger.model.weapons.WeaponCreator;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;
import se.rhel.gunslinger.network.KryoClient;
import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.observers.SoundObserver;
import se.rhel.gunslinger.sceens.actors.PlayerActor;
import se.rhel.gunslinger.sceens.actors.WeaponActor;
import se.rhel.gunslinger.view.AnimatedSprite;
import se.rhel.gunslinger.view.BubbleRender;
import se.rhel.gunslinger.view.ForegroundRender;
import se.rhel.gunslinger.view.LevelRender;
import se.rhel.gunslinger.view.SoundManager;
import sun.security.action.GetLongAction;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.moribitotech.mtx.AbstractScreen;
import com.moribitotech.mtx.AnimationCreator;

public class IntroScreen extends AbstractScreen {
	
	public enum State {FIRST_WALK, DIALOG1, DIALOG2, DIALOG3, SECOND_WALK, THIRD_WALK, DONE, GUNPICKUP};

	private static final String	SCREEN_NAME = "IntroScene";
	
	private Gunslinger	mGame;
	private KryoServer 	mServer;
	private KryoClient	mClient;
	private boolean		mIsHost;
	private BaseStage	mStage;
	
	private PlayerActor	mPlayer;

	private Animation 	mPlayerActorAnimationRight, mPlayerActorAnimationStillRight;
	private Sprite		mDialogBar;
	private BitmapFont  mFont;
	private Label		mDialog;
	private BaseStage	mDialogStage;
	private Button		mAcceptButton;
	private Camera		mCamera;

	private Vector2		mGunPosition = new Vector2(5, 6);
	private float 		mWaitTimer = 1f;
	
	private WeaponActor mWeapon;
	
	private TweenManager	mManager;
	private ForegroundRender mForegroundRender;
	private BubbleRender mBubbleRender;
	private LevelRender mLevelRender;
	private Sprite 		mShadow;
	private Sprite		playerAvatar;
	
	private Sprite		dialogSprite;
	
	private Map<State, Sprite> AVATARS = new HashMap<State, Sprite>();
	private Map<State, String> DIALOGS = new HashMap<State, String>();
	
	private GameScreen mToScreen;
	
	// H�ll koll p� vilken state
	private State 		mState = State.FIRST_WALK;

	private AnimatedSprite mGunPickup;

	private boolean mGunIsPickedUp;

	private TextButton mSkipButton;
	
	public IntroScreen(Gunslinger game, KryoServer server, KryoClient client, GameScreen screen) {
		super(game, SCREEN_NAME);

		mIsHost = true;
		mGame = game;
		mServer = server;
		mClient = client;
		mCamera = screen.getCamera();
		mToScreen = screen;
		init();
	}
	
	public IntroScreen(Gunslinger game, KryoClient client, GameScreen screen) {
		super(game, SCREEN_NAME);
		mIsHost = false;
		mGame = game;
		mClient = client;
		mCamera = screen.getCamera();
		mToScreen = screen;
		init();
	}
	
	public IntroScreen(Gunslinger game, Camera camera) {
		super(game, SCREEN_NAME);
		mCamera = camera;
		mIsHost = false;
		mGame = game;
	}
	
	public void init() {
		mManager = new TweenManager();
		mStage = new BaseStage(getStage().getSpriteBatch());
		mStage.setCamera(mCamera);
		
		mForegroundRender = new ForegroundRender(getStage().getSpriteBatch(), mManager, new CameraLookAt(mCamera.position.cpy()));
		mLevelRender = new LevelRender(getStage().getSpriteBatch(), mClient.getWorldModel().levelManager(), new CameraLookAt(mCamera.position.cpy()));
		mBubbleRender = new BubbleRender(getStage().getSpriteBatch(), mClient.getWorldModel(), mCamera, new SoundObserver());
		
		populateDialog();
	}
	
	private void populateDialog() {
		String d1 = "No, No, No...";

		String d2 = "This Can't be, they took Juarez...\nSomeone's gonna pay for this!";
		String d3 = "I'll take good care of your gun.\nRest in peace old fella'";
		
		//Spanska :>
		//String d2 = "esto no puede ser, tomaron juarez... que va a pagar por esto";
		//String d3 = "Voy a tomar un buen cuidado de su arma. Descansa en paz viejo t�o";
		
		DIALOGS.put(State.DIALOG1, d1);
		DIALOGS.put(State.DIALOG2, d2);
		DIALOGS.put(State.DIALOG3, d3);
		
		AVATARS.put(State.DIALOG1, playerAvatar);
		AVATARS.put(State.DIALOG2, playerAvatar);
		AVATARS.put(State.DIALOG3, playerAvatar);
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		mManager.update(delta);
		mStage.act(delta);
		
		getStage().getSpriteBatch().begin();		
		getStage().setCamera(mCamera);
		mLevelRender.draw(delta, mClient.getWorldModel().player());
		mBubbleRender.drawScene(delta);
		
		mSkipButton.getLabel().setPosition(-0.15f, 0.2f);
		mShadow.setPosition(mPlayer.getX() + mShadow.getWidth() / 2f, mPlayer.getY() - mShadow.getHeight() / 2f);
		mShadow.draw(getStage().getSpriteBatch());
		mStage.draw();
		mForegroundRender.draw(delta);
		
		mStage.getActors().removeValue(mAcceptButton, true);

		mDialogStage.clear();
		
		switch(mState) {
			case DIALOG1:
			case DIALOG2:
			case DIALOG3:
				mDialogBar.draw(getStage().getSpriteBatch());
				mDialog.setText(DIALOGS.get(mState));
				mDialogStage.addActor(mDialog);
				mStage.addActor(mAcceptButton);
				dialogSprite.draw(getStage().getSpriteBatch());
				break;
			case GUNPICKUP:
				mGunIsPickedUp = true;
				mStage.getActors().removeValue(mWeapon, true);
				break;
			case DONE:
				if ((mWaitTimer -= delta) < 0) {
					back();
				}
				break;
			default:
				break;
		}
		
		if (mGunIsPickedUp) {
			mGunPickup.setPosition(mGunPosition.x + 0.25f, mGunPosition.y);
			mGunPickup.update(delta);
			mGunPickup.draw(getStage().getSpriteBatch());
			
			if(mGunPickup.isFinished()) {
				mGunIsPickedUp = false;

				mState = State.DONE;
			}
		}
		
		mDialog.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
		mDialog.setPosition(3.2f * mCamera.getScale().x,  (float)Math.round(mCamera.getScale().y * 1f - mDialog.getTextBounds().height / 2));
		mDialogStage.draw();
		getStage().getSpriteBatch().end();	
	}

	private void back() {
		if (!mClient.getWorldModel().player().getInventory().gotWeapon()) {
			mClient.getWorldModel().player().getInventory().addWeapon(
					WeaponCreator.createWeapon(	Weapons.GUN, 
												mClient.getWorldModel().player(), 
												mClient.getWorldModel().world()));
		}
		
		if(mIsHost) {
			mGame.setScreen(mToScreen, true);
		} else {
			mGame.setScreen(mToScreen, true);
		}
	}

	@Override
	public void show() {
		super.show();
		
		// Font
		mFont = Resources.getInstance().font;
		
		// Animation
		mGunPickup = new AnimatedSprite(Resources.getInstance().coinPickup, "coinpickup", 6);
		mGunPickup.setSize(1f, 1f);
		mGunPickup.play();
		mGunPickup.loop(false);
		mGunPickup.setAnimationRate(10);
	}
	

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
				
		// Spelaren
		mPlayerActorAnimationRight = AnimationCreator.getAnimationFromSingleTextureMultiRows(Resources.getInstance().playerActorAtlas, "player_sprite48", 5, 5, 2, 0, 0.1f); 

		mPlayerActorAnimationStillRight = AnimationCreator.getAnimationFromSingleTextureMultiRows(Resources.getInstance().playerActorAtlas, "player_sprite48", 1, 5, 2, 0, 0.1f);

		mPlayer = new PlayerActor(-5f, 4f, 2f, 2f);
		mPlayer.setAnimation(mPlayerActorAnimationRight, true, true);
		mPlayer.setSize(2f, 2f);
		
		mPlayer.addAction(sequence(moveTo(4f, 5f, 3f), run(new Runnable() {
			
			@Override
			public void run() {
				mPlayer.setAnimation(mPlayerActorAnimationStillRight, true, false);
				mState = State.DIALOG1;
				dialogSprite = playerAvatar;
			}
		})));
		
		//Spelarskugga
		mShadow = new Sprite(Resources.getInstance().characterShadow);
		mShadow.setSize(mPlayer.getWidth() / 2f, mPlayer.getWidth() / 6f);
		mShadow.setColor(0, 0, 0, 0.4f);
		
		mDialogStage = new BaseStage(getStage().getSpriteBatch());
		
		//Player avatar
		playerAvatar = new Sprite(Resources.getInstance().player_avatar);
		playerAvatar.setSize(1.5f, 1.5f);
		playerAvatar.setPosition(1.3f, 0.2f);
		
		// Dialog-bar
		mDialogBar = new Sprite(Resources.getInstance().wepinfo_full, 0, 0, 160, 20);
		mDialogBar.setSize(16f, 2f);
		mDialogBar.setPosition(0, 0);
		
		LabelStyle lbs = new LabelStyle(mFont, Color.WHITE);
		mDialog = new Label("", lbs);

		mDialog.setWrap(true);
		mDialog.setBounds(3.2f * mCamera.getScale().x, 0.4f * mCamera.getScale().y, (11f * mCamera.getScale().x) / Camera.FONT_SCALE_X, 1f * mCamera.getScale().y);
		mFont.setUseIntegerPositions(false);
		
		mWeapon = new WeaponActor(mGunPosition.x, mGunPosition.y, 1.5f, 1f, Weapons.GUN, Id.getInstance().get());
		Animation ani = AnimationCreator.getAnimationFromMultiTextures(Resources.getInstance().gunAtlas, "gun", 16, 0.1f);
		
		mWeapon.setAnimation(ani, true, true);
		mStage.addActor(mPlayer);
		mStage.addActor(mWeapon);
		
		// Acceptera-knapp
		Skin buttonSkin = new Skin(Resources.getInstance().shopbuttons);
		ButtonStyle bs = new ButtonStyle();
		bs.up = buttonSkin.getDrawable("addbutton");
		bs.down = buttonSkin.getDrawable("addbuttonpressed");
		mAcceptButton = new Button(bs);
		mAcceptButton.setSize(1f, 1f);
		mAcceptButton.setPosition(1f, 2f);
		mAcceptButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				continueDialog();
			}
		});
		
		//Skip-knapp
		TextButtonStyle tbs = UIcomponents.BaseTextButtonStyle(new Skin(Resources.getInstance().menuAtlas), mFont, Color.WHITE, "button", "buttonpressed");
		mSkipButton = UIcomponents.button("Skip", tbs, new Rectangle(
				Camera.CAMERA_WIDTH - 2.75f,
				Camera.CAMERA_HEIGHT - 1f,
				2.5f,
				0.625f));

		mSkipButton.getLabel().setFontScale(Camera.FONT_SCALE_X*(mSkipButton.getLabel().getFontScaleX() / mCamera.getScale().x), Camera.FONT_SCALE_Y*(mSkipButton.getLabel().getFontScaleY() / mCamera.getScale().y));
		mSkipButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});
		
		mStage.addActor(mSkipButton);
		
		// Input
		Gdx.input.setInputProcessor(mStage);
	}
	
	private void continueDialog() {
		if(mState == State.DIALOG1) {
			dialogSprite = playerAvatar;
			mState = State.DIALOG2;
			return;
		}
		if(mState == State.DIALOG2) {
			dialogSprite = playerAvatar;
			mState = State.DIALOG3;
			return;
		}
		if (mState == State.DIALOG3) {
			dialogSprite = playerAvatar;
			SoundManager.getInstance().playGunPickup();
			mState = State.GUNPICKUP;
			return;
		}
	}
	
	@Override
	public void dispose() {

		mServer.shutdown();
		super.dispose();
	}
}
