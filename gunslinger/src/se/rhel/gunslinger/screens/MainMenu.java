package se.rhel.gunslinger.screens;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.CameraLookAt;
import se.rhel.gunslinger.model.options.Options;
import se.rhel.gunslinger.view.ForegroundRender;
import se.rhel.gunslinger.view.SoundManager;
import se.rhel.gunslinger.view.SoundManager.MusicType;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.moribitotech.mtx.AbstractScreen;

public class MainMenu extends AbstractScreen {
	
	private static final String SCREEN_NAME = "Main Menu";
	
	private Skin 			mSkin;
	private TextButton 		mSingleplayerButton, mMultiplayerButton, mOptionsButton, mExitButton;
	private BitmapFont		mFont;
	private Camera			mCamera;
	
	private TweenManager	mManager;
	private ForegroundRender mForegroundRender;
	private BaseStage		mStage;

	private Table table;
	
	public MainMenu(Game game) {
		super(game, SCREEN_NAME);
		
		init();
		
		mManager = new TweenManager();
		mForegroundRender = new ForegroundRender(getStage().getSpriteBatch(), mManager, new CameraLookAt(new Vector2(8,0)));
	}

	public MainMenu(Game game, ForegroundRender fgr) {
		super(game, SCREEN_NAME);
		init();
		
		mForegroundRender = fgr;
		mManager = fgr.getManager();
		mForegroundRender.setSpriteBatch(getStage().getSpriteBatch());
	}
	
	private void init() {
		mCamera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mStage = new BaseStage(getStage().getSpriteBatch());
		getStage().setViewport(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT, false);
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		mManager.update(delta);
		
		getStage().setViewport(16f, 10f, false);
		getStage().getSpriteBatch().begin();
		
		mForegroundRender.draw(delta);
			
		mStage.draw();
		
		getStage().getSpriteBatch().end();
	}
	
	@Override
	public void resize(int width, int height) {	
		table.clear();
		
		TextButtonStyle tbs = UIcomponents.BaseTextButtonStyle(mSkin, mFont, Color.WHITE, "button", "buttonpressed");
		
		//Start singleplayer
		mSingleplayerButton = UIcomponents.button("Singleplayer", tbs, 6f * mCamera.getScale().x, 1.5f * mCamera.getScale().y);
		mSingleplayerButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				SoundManager.getInstance().stopMusic(MusicType.MENUTHEME);
				getGame().setScreen(new LobbyScreen((Gunslinger)getGame(),"SinglePlayer", 1, mForegroundRender));
			}
		});
		
		mSingleplayerButton.setSize(mSingleplayerButton.getWidth() * Camera.FONT_SCALE_X, mSingleplayerButton.getHeight() * Camera.FONT_SCALE_Y);
		
		//Start multiplayer
		mMultiplayerButton = UIcomponents.button("Multiplayer", tbs);	
		mMultiplayerButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				//SoundManager.getInstance().stopMusic(MusicType.MENUTHEME);
				getGame().setScreen(new MultiplayerScreen((Gunslinger)getGame(), mForegroundRender));
			}
		});
		

		mOptionsButton = UIcomponents.button("Options", tbs);	
		mOptionsButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				getGame().setScreen(new OptionsScreen((Gunslinger)getGame(), mForegroundRender));
			}
		});
			
		mExitButton = UIcomponents.button("Exit", tbs);	
		mExitButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});
		
//		TextButton testButton = UIcomponents.button("End credits", tbs);
//		testButton.addListener(new ClickListener() {
//			@Override
//			public void clicked(InputEvent event, float x, float y) {
//				getGame().setScreen(new EndCreditScreen((Gunslinger)getGame(), mCamera));
//			}
//		});
		
		table.add(mSingleplayerButton).left();
		table.row().padTop(10);
		table.add(mMultiplayerButton).left();
		table.row().padTop(10);
		table.add(mOptionsButton).left();
		table.row().padTop(10);
		table.add(mExitButton).left();	
		table.row().padTop(10);
		//table.add(testButton).colspan(2).center();

		mStage.addActor(table);
	}
	
	private void back() {
		SoundManager.getInstance().dispose();
		Gdx.app.exit();
	}

	@Override
	public void show() {
		table = new Table();
		//table.setFillParent(true);
		table.setPosition(Gdx.graphics.getWidth() / 2, 4f * mCamera.getScale().y);
		
		mSkin = new Skin(Resources.getInstance().menuAtlas);
		mFont = Resources.getInstance().font;
		
		Gdx.input.setInputProcessor(mStage);
		Gdx.input.setCatchBackKey(true);
		
		getStage().addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.BACK) {
					back();
				}
				return true;
			}
		});
		
		Texture txt = Resources.getInstance().logo;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());		
		setBackgroundTexture(region);
		
		Options opt = new Options();
		
		if (!SoundManager.getInstance().isPlaying(MusicType.MENUTHEME))
			SoundManager.getInstance().playMusic(true, opt.getMusicVolume(), MusicType.MENUTHEME);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {	}

	@Override
	public void resume() {	}

	@Override
	public void dispose() {
		super.dispose();
		mSkin.dispose();
		mFont.dispose();
	}
}
