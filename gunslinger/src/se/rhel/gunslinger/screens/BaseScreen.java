package se.rhel.gunslinger.screens;

import com.badlogic.gdx.Screen;

/**
 * Basklass f�r att separera draw och update
 * @author Emil & Rickard
 *
 */
public abstract class BaseScreen implements Screen {
	
	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}
	
	public abstract void draw(float delta);
	public abstract void update(float delta);
}
