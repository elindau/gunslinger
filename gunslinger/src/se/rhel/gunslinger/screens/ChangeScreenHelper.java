package se.rhel.gunslinger.screens;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.observers.ScreenChangeObserver;
import se.rhel.gunslinger.tweens.SplashAccessor;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * En slags mellanscreen vid byte av screens f�r att
 * f� en mjuk �verg�ng
 * @author Emil
 *
 */
public class ChangeScreenHelper extends Stage {
	
	private boolean 		mIsChanging;
	private	Sprite			mScreenChangeSprite;
	private TweenManager	mManager;
	
	private boolean			mSecondTweening = false;
	private boolean			mBlackness = false;
	private boolean 		mFirstIsTweening = true;
	private boolean			mIsActive;
	
	private float			mBlacknessMaxTime = 1f;
	private float			mBlacknessTimeElapsed = 0f;
	
	private ScreenChangeObserver mObserver;
	
	public ChangeScreenHelper(ScreenChangeObserver observer) {
		mIsChanging = false;
		mObserver = observer;
		
		Texture tex = new Texture(Gdx.files.internal("gfx/changescreen.png"));
		tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		// Resources.getInstance().changeScreen.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		// mScreenChangeSprite = new Sprite(Resources.getInstance().changeScreen, 0, 0, 160, 100);
		mScreenChangeSprite = new Sprite(tex, 0, 0, 160, 100);
		mScreenChangeSprite.setColor(1, 1, 1, 0);
		mScreenChangeSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		mManager = new TweenManager();
		Tween.setWaypointsLimit(10);
		Tween.registerAccessor(Sprite.class, new SplashAccessor());
		
		mIsActive = false;
		
		// setViewport(16, 10, false);
	}
	
	public void changeScreen() {
		mIsChanging = true;
		mIsActive = true;
		
		if(mFirstIsTweening) {
			mFirstIsTweening = false;
			Tween.to(mScreenChangeSprite, SplashAccessor.ALPHA, 2f)
			.target(1)
			.ease(TweenEquations.easeInQuad)
			.setCallback(new TweenCallback() {
				
				@Override
				public void onEvent(int type, BaseTween<?> source) {
					// System.out.println("done");
					// mFirstIsTweening = false;
					mBlackness = true;
					mObserver.changeScreen();
				}
			})
			.setCallbackTriggers(TweenCallback.COMPLETE)
			.start(mManager);	
		}
	}
	
	@Override
	public void draw() {
		if(mIsActive) {
			getSpriteBatch().begin();
			if(mIsChanging) {
				// Sk�rmen ska vara m�rk ett litet tag
				if(mBlackness) {
					mBlacknessTimeElapsed += Gdx.graphics.getDeltaTime();
					if(mBlacknessTimeElapsed > mBlacknessMaxTime) {
						mBlackness = false;
						// Dags att tweena tillbaka till a0
						mSecondTweening = true;
					}
				}
				
				// Andra tweeningen d�r vi g�r tillbaka till alpha 0
				if(mSecondTweening) {
					mSecondTweening = false;
					
					Tween.to(mScreenChangeSprite, SplashAccessor.ALPHA, 2f)
					.target(0)
					.ease(TweenEquations.easeInQuad)
					.setCallback(new TweenCallback() {
						
						@Override
						public void onEvent(int type, BaseTween<?> source) {
							System.out.println("done");
							reset();
						}
					})
					.setCallbackTriggers(TweenCallback.COMPLETE)
					.start(mManager);
				}
				
				// Uppdatera spriten och managern
				mScreenChangeSprite.draw(getSpriteBatch());
				mManager.update(Gdx.graphics.getDeltaTime());
			}
			getSpriteBatch().end();
		}
	}
	
	private void reset() {
		mSecondTweening = false;
		mBlackness = false;
		mFirstIsTweening = true;
		mIsActive = false;
	}
	
	@Override
	public void dispose() {
		
	}
	
}
