package se.rhel.gunslinger.screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Klass f�r att komma f�rbi att spritebatchen inte aktiveras igen efter Stage.draw()
 * @author Rickard
 *
 */
public class BaseStage extends Stage {

	private SpriteBatch mSpritebatch;

	public BaseStage(SpriteBatch sb) {
		mSpritebatch = sb;
	}
	
	@Override
	public void draw() {
		mSpritebatch.end();
		super.draw();
		mSpritebatch.begin();
	}
}
