package se.rhel.gunslinger.screens;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.tweens.SplashAccessor;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SplashScreen implements Screen {

	private Texture 		mSplashTexture;
	private Sprite 			mSplashSprite;
	private SpriteBatch 	mSpriteBatch;
	private Gunslinger		mGame;
	private TweenManager 	mManager;
	
	public SplashScreen(Gunslinger aGame) {
		mGame = aGame;
		init();
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		mManager.update(delta);
		
		// Is the assets done loading
		if(Resources.getInstance().assetManager.update()) {
			// Hooking up the instances
			Resources.getInstance().setGameInstances();
			
			mSpriteBatch.begin();
				mSplashSprite.draw(mSpriteBatch);
			mSpriteBatch.end();
		}	
	}
	
	private void init() {
		// Prepare the assetmanager
		Resources.prepareManager();
		Resources.getInstance().loadGameResources();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		mSplashTexture = new Texture("gfx/splash.png");
		mSplashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		mSplashSprite = new Sprite(mSplashTexture);
		mSplashSprite.setColor(1, 1, 1, 0);
		mSplashSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		mSpriteBatch = new SpriteBatch();
		
		Tween.registerAccessor(Sprite.class, new SplashAccessor());
		
		mManager = new TweenManager();
		
		TweenCallback cb = new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				tweenCompleted();
			}
		};
		
		Tween.to(mSplashSprite, SplashAccessor.ALPHA, 2f)
			.target(1)
			.ease(TweenEquations.easeInQuad)
			.repeatYoyo(1, 2.5f)
			.setCallback(cb)
			.setCallbackTriggers(TweenCallback.COMPLETE)
			.start(mManager);
	}
	
	private void tweenCompleted() {
		mGame.setScreen(new MainMenu(mGame), true);
		// mGame.setScreen(new MainMenu(mGame));
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {

	}

}
