package se.rhel.gunslinger.screens;

import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.CameraLookAt;
import se.rhel.gunslinger.model.options.Options;
import se.rhel.gunslinger.view.ForegroundRender;
import se.rhel.gunslinger.view.SoundManager;
import se.rhel.gunslinger.view.SoundManager.MusicType;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Slider.SliderStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.moribitotech.mtx.AbstractScreen;

public class OptionsScreen extends AbstractScreen {

	private static final String SCREEN_NAME = "Options";
	
	public static final int 	CHECKBOX_WIDTH = 256;
	public static final int 	CHECKBOX_HEIGHT = 64;
	
	private static CheckBox 	mMusic;

	private static CheckBox 	mSound;
	private static BitmapFont	mFont;
	private static Options		mOptions;
	private static Skin 		mCheckboxSkin;

	private static Skin mButtonSkin;
	private TextButton	mBackButton;
	
	private Camera	mCamera;
	private ForegroundRender mForegroundRender;
	private BaseStage		mStage;
	
	public OptionsScreen(Gunslinger game, ForegroundRender fgr) {
		super(game, SCREEN_NAME);
		
		getStage().setViewport(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT, false);
		
		mCamera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mStage = new BaseStage(getStage().getSpriteBatch());
		
		mStage = new BaseStage(getStage().getSpriteBatch());

		mForegroundRender = fgr;
		mForegroundRender.setSpriteBatch(getStage().getSpriteBatch());
	}
	
	/**
	 * Ska retunera en f�rdig options-stage
	 * till spelet, n�r options v�ljs d�rifr�n
	 * @return
	 */
	public static Table getOptionsTable() {
		
		Table table = new Table();
		table.setFillParent(true);
		
		// Stage returnStage = new Stage();
		// returnStage.addActor(table);
		
		mFont = Resources.getInstance().font;
		mCheckboxSkin = new Skin(Resources.getInstance().checkboxAtlas);
		mButtonSkin = new Skin(Resources.getInstance().menuAtlas);
		mOptions = new Options();
		
		TextButtonStyle bStyle = new TextButtonStyle();
		bStyle.font = mFont;
		bStyle.fontColor = Color.WHITE;
		bStyle.up = mButtonSkin.getDrawable("button");
		bStyle.down = mButtonSkin.getDrawable("buttonpressed");
		
		CheckBoxStyle style = new CheckBoxStyle();
		style.font = mFont;
		style.fontColor = Color.WHITE;
		style.checkboxOff = mCheckboxSkin.getDrawable("checkbox");
		style.checkboxOn = mCheckboxSkin.getDrawable("checkbox_checked");
		
		mMusic = new CheckBox("Music On/Off", style);
		mMusic.setChecked(mOptions.getMusic());
		mMusic.setBounds(Gdx.graphics.getWidth() / 2 - CHECKBOX_WIDTH / 2, Gdx.graphics.getHeight() / 2, CHECKBOX_WIDTH, CHECKBOX_HEIGHT);
		
		mSound = new CheckBox("Sound On/off", style);
		mSound.setChecked(mOptions.getSound());
		mSound.setBounds(Gdx.graphics.getWidth() / 2- CHECKBOX_WIDTH / 2, Gdx.graphics.getHeight() / 2 - 64, CHECKBOX_WIDTH, CHECKBOX_HEIGHT);
		
		Skin sliderSkin = new Skin(Resources.getInstance().sliderAtlas);
		SliderStyle sliderStyle = new SliderStyle();
		sliderStyle.background = sliderSkin.getDrawable("bar");
		sliderStyle.knob = sliderSkin.getDrawable("knob");
		sliderStyle.knobAfter = sliderSkin.getDrawable("bar_unfilled");
		sliderStyle.knobBefore = sliderSkin.getDrawable("bar_filled");
		
		final Slider slider = new Slider(0f, 1f, 0.05f, false, sliderStyle);
		slider.setValue(mOptions.getMusicVolume());
		slider.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// S�tt volumen i options ..
				mOptions.setMusicVolume(slider.getValue());
				// .. och i SoundManagern
				SoundManager.getInstance().setVolume(slider.getValue());
			}
		});
		
		final Slider soundSlider = new Slider(0f, 1f, 0.05f, false, sliderStyle);
		soundSlider.setValue(mOptions.getSoundVolume());
		soundSlider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				mOptions.setSoundVolume(soundSlider.getValue());
				SoundManager.getInstance().setSoundVolume(soundSlider.getValue());
			}
		});
		
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = mFont;
		labelStyle.fontColor = Color.WHITE;
		Label volumeLabel = new Label("Music volume: ", labelStyle);
		Label soundlabel = new Label("Sound volume: ", labelStyle);
		
		mMusic.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				mOptions.setMusic(mMusic.isChecked());
				
				// F�r att f� feedback fr�n meny-musik direkt
				if(mMusic.isChecked()) {
					SoundManager.getInstance().playMusic(true, mOptions.getMusicVolume(), MusicType.GAMETHEME);
				} else {
					SoundManager.getInstance().pauseMusic(MusicType.GAMETHEME);
				}
			}
		});
		
		mSound.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				mOptions.setSound(mSound.isChecked());
			}
		});
		
		table.add(mMusic).left();
		table.row().padTop(10);
		table.add(mSound).left();
		table.row().padTop(10);
		table.add(volumeLabel).left();
		table.row().padTop(1);
		table.add(slider).left().fillX();
		table.row().padTop(10);
		table.add(soundlabel).left();
		table.row().padTop(1);
		table.add(soundSlider).left().fillX();
		
		
		// returnStage.addActor(mMusic);
		// returnStage.addActor(mSound);
		
		Skin bgSkin = new Skin(Resources.getInstance().tiledBgsAtlas);
		table.setBackground(bgSkin.getDrawable("black"));
		
		return table;
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		mForegroundRender.getManager().update(delta);

		getStage().getSpriteBatch().begin();
		
		mForegroundRender.draw(delta);
		mStage.draw();
		
		getStage().getSpriteBatch().end();
		
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			getGame().setScreen(new MainMenu(getGame()));
		}
	}
	
	@Override
	public void resize(int width, int height) {	
		mStage.setViewport(width, height, false);
		
		Table table = new Table();
		table.setPosition(Gdx.graphics.getWidth() / 2f, 4f * mCamera.getScale().y);
		mStage.addActor(table);
		
		TextButtonStyle bStyle = new TextButtonStyle();
		bStyle.font = mFont;
		bStyle.fontColor = Color.WHITE;
		bStyle.up = mButtonSkin.getDrawable("button");
		bStyle.down = mButtonSkin.getDrawable("buttonpressed");
		
		CheckBoxStyle style = new CheckBoxStyle();
		style.font = mFont;
		style.fontColor = Color.BLACK;
		style.checkboxOff = mCheckboxSkin.getDrawable("checkbox");
		style.checkboxOn = mCheckboxSkin.getDrawable("checkbox_checked");
		
		Skin sliderSkin = new Skin(Resources.getInstance().sliderAtlas);
		SliderStyle sliderStyle = new SliderStyle();
		sliderStyle.background = sliderSkin.getDrawable("bar");
		sliderStyle.knob = sliderSkin.getDrawable("knob");
		sliderStyle.knobAfter = sliderSkin.getDrawable("bar_unfilled");
		sliderStyle.knobBefore = sliderSkin.getDrawable("bar_filled");
		
		final Slider slider = new Slider(0f, 1f, 0.05f, false, sliderStyle);
		slider.setValue(mOptions.getMusicVolume());
		slider.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				// S�tt volumen i options ..
				mOptions.setMusicVolume(slider.getValue());
				// .. och i SoundManagern
				SoundManager.getInstance().setVolume(slider.getValue());
			}
		});
		
		final Slider soundSlider = new Slider(0f, 1f, 0.05f, false, sliderStyle);
		soundSlider.setValue(mOptions.getSoundVolume());
		soundSlider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				mOptions.setSoundVolume(soundSlider.getValue());
				SoundManager.getInstance().setSoundVolume(soundSlider.getValue());
			}
		});
		
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = mFont;
		labelStyle.fontColor = Color.BLACK;
		Label volumeLabel = new Label("Music volume: ", labelStyle);
		Label soundVolumeLabel = new Label("Sound volume: ", labelStyle);
		
		
		mMusic = new CheckBox(" Music On/Off", style);
		mMusic.setChecked(mOptions.getMusic());
		mMusic.setBounds(Gdx.graphics.getWidth() / 2 - CHECKBOX_WIDTH / 2, Gdx.graphics.getHeight() / 2, CHECKBOX_WIDTH, CHECKBOX_HEIGHT);
		mMusic.getCells().get(0).setWidgetWidth(300);
		
		mSound = new CheckBox(" Sound On/off", style);
		mSound.setChecked(mOptions.getSound());
		mSound.setBounds(Gdx.graphics.getWidth() / 2- CHECKBOX_WIDTH / 2, Gdx.graphics.getHeight() / 2 - 64, CHECKBOX_WIDTH, CHECKBOX_HEIGHT);
		
		mMusic.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				mOptions.setMusic(mMusic.isChecked());
				
				// F�r att f� feedback fr�n meny-musik direkt
				if(mMusic.isChecked()) {
					SoundManager.getInstance().playMusic(true, mOptions.getMusicVolume(), MusicType.MENUTHEME);
				} else {
					SoundManager.getInstance().pauseMusic(MusicType.MENUTHEME);
				}
			}
		});
		
		mSound.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				mOptions.setSound(mSound.isChecked());
			}
		});
		
		mBackButton = new TextButton("Back", bStyle);
		mBackButton.setBounds(Gdx.graphics.getWidth() / 2- CHECKBOX_WIDTH / 2, Gdx.graphics.getHeight() / 2 - 64 * 2, CHECKBOX_WIDTH, CHECKBOX_HEIGHT);
		
		mBackButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});
		
		table.add(mMusic).left();
		table.row().padTop(10);
		table.add(mSound).left();
		table.row().padTop(10);
		table.add(volumeLabel).left();
		table.row().padTop(1);
		table.add(slider).left().fillX();
		table.row().padTop(10);
		table.add(soundVolumeLabel).left();
		table.row().padTop(1);
		table.add(soundSlider).left().fillX();
		table.row().padTop(10);
		table.add(mBackButton);
	}
	
	private void back() {
		getGame().setScreen(new MainMenu((Gunslinger)getGame(), mForegroundRender));
	}
	
	@Override
	public void show() {
		
		Gdx.input.setInputProcessor(mStage);
		Gdx.input.setCatchBackKey(true);
		
		getStage().addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.BACK) {
					back();
				}
				return true;
			}
		});
		
		mFont = Resources.getInstance().font;
		mCheckboxSkin = new Skin(Resources.getInstance().checkboxAtlas);
		mButtonSkin = new Skin(Resources.getInstance().menuAtlas);
		mOptions = new Options();
		
		Texture txt = Resources.getInstance().optionsBg;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());
		setBackgroundTexture(region);
	}
	
	@Override
	public void hide() {
		super.hide();
	}
	
	@Override
	public void pause() {
		super.pause();
		mOptions.save();
	}
	
	@Override
	public void resume() {
		super.resume();
	}
	
	@Override
	public void dispose() {
		super.dispose();
		mFont.dispose();
	}
}
