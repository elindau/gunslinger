package se.rhel.gunslinger.screens;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.view.ForegroundRender;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessorQueue;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.moribitotech.mtx.AbstractScreen;

/**
 * Screen f�r att Hosta server
 * Extendar AbstractScreen (mtx.jar)
 * @author Rickard
 *
 */
public class HostGameScreen extends AbstractScreen {

	private static final String SCREEN_NAME = "Host Game Menu";
	
	private Skin 				mSkin;
	private BitmapFont 			mFont;
	private Table 				mTable;
	
	private ForegroundRender mForegroundRender;
	private BaseStage		mStage;
	private Camera			mCamera;
	private TweenManager	mManager;

	public HostGameScreen(Gunslinger game, ForegroundRender fgr) {
		super(game, SCREEN_NAME);
		
		mStage = new BaseStage(getStage().getSpriteBatch());
		mCamera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mForegroundRender = fgr;
		mForegroundRender.setSpriteBatch(getStage().getSpriteBatch());
		mManager = fgr.getManager();
		getStage().setViewport(16f, 10f, false);
	}

	@Override
	public void render(float delta) {
		super.render(delta);	
		mManager.update(delta);
		
		if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
			System.out.println(Gdx.input.getX() + " : " + Gdx.input.getY());
		}

		getStage().getSpriteBatch().begin();
		
		mForegroundRender.draw(delta);	
		mStage.draw();
		
		getStage().getSpriteBatch().end();
		
		
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			getGame().setScreen(new HostGameScreen((Gunslinger)getGame(), mForegroundRender));
		}
	}

	@Override
	public void resize(int width, int height) {
		mStage.setViewport(width, height, false);
		mTable = new Table();

		mTable.setPosition(Gdx.graphics.getWidth() / 2, 4f * mCamera.getScale().y);
		mTable.debug();
		mStage.addActor(mTable);
		
		Skin selectSkin = new Skin(Resources.getInstance().selectBoxAtlas);
		
		ListStyle ls = new ListStyle(mFont, Color.WHITE, Color.BLACK, selectSkin.getDrawable("listbg"));
		final List dropdown = new List(new String[] {"  2", "  3", "  4"}, ls);
		
//		SelectBoxStyle sbs = new SelectBoxStyle(mFont, Color.WHITE, selectSkin.getDrawable("bg"), selectSkin.getDrawable("listbg"), selectSkin.getDrawable("selected"));
//		final SelectBox dropdown = new SelectBox(new String[] {"2", "3", "4"}, sbs);
//		sbs.itemSpacing = 10f;
		
		TextButton hostbutton = UIcomponents.button("Host", UIcomponents.BaseTextButtonStyle(mSkin, mFont, Color.WHITE, "button", "buttonpressed"), new Rectangle(0,0, 256, 64));
		hostbutton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				getGame().setScreen(new LobbyScreen((Gunslinger)getGame(), "Host", Integer.parseInt(dropdown.getSelection().trim()), mForegroundRender));
			}
		});
		
		TextButton backButton = UIcomponents.button("Back", UIcomponents.BaseTextButtonStyle(mSkin, mFont, Color.WHITE, "button", "buttonpressed"), new Rectangle(0,0, 256, 64));
		backButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});
		
		mTable.add(new Label("Server slots: ", UIcomponents.LabelStyle(mFont, Color.BLACK)));
		mTable.add(dropdown).align(Align.right).fill(true);
		mTable.row().padTop(10);
		mTable.add(hostbutton).colspan(2);
		mTable.row().padTop(10);
		mTable.add(backButton).colspan(2);
	}
	
	private void back() {
		getGame().setScreen(new MultiplayerScreen((Gunslinger)getGame(), mForegroundRender));
	}
	
	@Override
	public void show() {
		mSkin = new Skin(Resources.getInstance().menuAtlas);
		mFont = Resources.getInstance().font;
		
		Gdx.input.setInputProcessor(mStage);
		Gdx.input.setCatchBackKey(true);
		
//		mStage.addListener(new InputListener() {
//			@Override
//			public boolean keyDown(InputEvent event, int keycode) {
//				if(keycode == Keys.BACK) {
//					back();
//				}
//				return true;
//			}
//		});
		
		Texture txt = Resources.getInstance().logo;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());		
		setBackgroundTexture(region);
	}

	@Override
	public void hide() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		super.dispose();
		mSkin.dispose();
		mFont.dispose();
	}
}
