package se.rhel.gunslinger.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;

/**
 * Klass f�r att hj�lpa till vid uppskapandet
 * av komponenter f�r UI.
 * @author Rickard
 *
 */
public class UIcomponents {

	/**
	 * Skapa upp en vanlig textbutton
	 * @param text
	 * @param style
	 * @param bounds
	 * @return
	 */
	public static TextButton button(String text, TextButtonStyle style, Rectangle bounds) {
		TextButton button = new TextButton(text, style);
		button.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
		return button;
	}
	
	public static TextButton button(String text, TextButtonStyle style, float width, float height) {
		TextButton button = new TextButton(text, style);
		button.setWidth(width);
		button.setHeight(height);
		
		button.setBounds(0, 0, width, height);
		return button;
	}
	
	public static TextButton button(String text, TextButtonStyle style) {
		TextButton button = new TextButton(text, style);
		return button;
	}
	
	public static Button button(ButtonStyle style, Rectangle bounds) {
		Button b = new Button(style);
		b.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
		return b;
	}
	
	public static ButtonStyle BaseButtonStyle(Skin skin, String textureRegionButton, String textureRegionButtonPressed) {
		ButtonStyle style = new ButtonStyle();
		style.up = skin.getDrawable(textureRegionButton);
		style.down = skin.getDrawable(textureRegionButtonPressed);
		
		return style;
	}
	
	/**
	 * Skapa upp TextButtonStyle
	 * @param skin
	 * @param font
	 * @return
	 */
	public static TextButtonStyle BaseTextButtonStyle(Skin skin, BitmapFont font, Color color, String textureRegionButton, String textureRegionButtonPressed) {
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable(textureRegionButton);
		style.down = skin.getDrawable(textureRegionButtonPressed);
		style.font = font;
		style.fontColor = color;
		
		return style;
	}
	
	/**
	 * Skapa upp TextFieldStyle
	 * @param font
	 * @param color
	 * @return
	 */
	public static TextFieldStyle TextFieldStyle(BitmapFont font, Color color) {
		TextFieldStyle tfs = new TextFieldStyle();
		tfs.font = font;
		tfs.fontColor = color;
		
		return tfs;
	}
	
	/**
	 * Skapa upp LabelStyle
	 * @param font
	 * @param color
	 * @return
	 */
	public static LabelStyle LabelStyle(BitmapFont font, Color color) {
		return new LabelStyle(font, color);
	}

	/**
	 * Skapa upp TextField
	 * @param text
	 * @param tfs - TextFieldStyle
	 * @param maxCharacters
	 * @param tff - TextFieldFilter
	 * @return
	 */
	public static TextField TextField(String text, TextFieldStyle tfs, int maxCharacters, TextFieldFilter tff) {
		TextField txtField = new TextField(text, tfs);
		txtField.setMaxLength(maxCharacters);
		txtField.setTextFieldFilter(tff);
		return txtField;
	}
}
