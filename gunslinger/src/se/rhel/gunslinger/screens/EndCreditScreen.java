package se.rhel.gunslinger.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.network.KryoClient;
import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.view.SoundManager;
import se.rhel.gunslinger.view.SoundManager.MusicType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.moribitotech.mtx.AbstractScreen;

public class EndCreditScreen extends AbstractScreen {

	private static final String 	SCREEN_NAME = "Credits";
	
	private static final String		THE_END = 
			"Thanks for playing Gunslinger\n" +
			"\n\n" +
			"This, currently demo (since only two levels),\ngame was created by\n" +
			"\n\n" +
			"Rickard Hansson - Graphics, programming, CEO,\nLead harasser\n\n" +
			"Emil Lindau - Graphics (the bad ones), \nprogramming, CEO, Lead singer\n\n" +
			"Currently residing under the name\n'Null Pointer Inception'\n" +
			"\n\n" +
			"We would like to thank: \n\n" +
			"- LibGDX\n" +
			"- KryoNet\n" +
			"- MorbitoTech\n" +
			"- Aurelien Ribon\n" +
			"- Our mothers\n" +
			"- Our fathers\n" +
			"- Understanding girlfriend\n" +
			"- Everybody else, \n" + 
			"  except the bad guys, \n" +
			"  we do not want to thank the bad guys.\n" +
			"\n\n\n\n" +
			"  Fuck you Darth Vader";
	
	private Gunslinger 				mGame;
	private KryoServer 				mServer;
	private KryoClient 				mClient;
	private boolean					mIsHost;
	
	private BitmapFont				mFont;
	private Camera 					mCamera;
	
	private Image[]					mTimeline;
	private Table					mTable;
	
	public EndCreditScreen(Gunslinger game, KryoServer server, KryoClient client, Camera camera) {
		super(game, SCREEN_NAME);
		
		mGame = game;
		mServer = server;
		mClient = client;
		mIsHost = true;
		mCamera = camera;
		
		getStage().setCamera(mCamera);
		
		// St�ng ner le server
		mServer.shutdown();
	}
	
	public EndCreditScreen(Gunslinger game, KryoClient client, Camera camera) {
		super(game, SCREEN_NAME);
		
		mGame = game;
		mClient = client;
		mIsHost = false;
		mCamera = camera;
		getStage().setCamera(mCamera);
	}
	
	// Testkonstruktor
	public EndCreditScreen(Gunslinger game, Camera camera) {
		super(game, SCREEN_NAME);
		mGame = game;
		mCamera = camera;
		getStage().setCamera(mCamera);
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		getStage().setViewport(width, height, false);
		getStage().setCamera(mCamera);

		// H�mta ut timelinebilderna
		mTimeline = new Image[7];
		for(int i = 0; i < 7; i++) {
			mTimeline[i] = new Image(Resources.getInstance().timeline.createSprite("timeline", i));
		}
		
		LabelStyle lbs = new LabelStyle(mFont, Color.WHITE);
		
		mTable = new Table();
		int computedHeight = 0;
		// mTable.setFillParent(true);
		mTable.setWidth(width);
		
		Label endLabel = new Label(THE_END, lbs);
		endLabel.setAlignment(1);
		endLabel.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
		mTable.add(endLabel).align(2);
		float padTop = mCamera.getScale().y * 3f;
		mTable.row().padTop(padTop);
		
		computedHeight += endLabel.getHeight() + padTop;
		
		String[] headers = new String[] {"2013-04-10", "2013-04-17", "2013-04-25", "2013-05-02", "2013-05-08", "2013-05-15", "2013-05-23"};
		float headerPad = mCamera.getScale().y * 0.1f;
		float picturePad = mCamera.getScale().y * 0.5f;
		for(int i = 0; i < mTimeline.length; i++) {
			Label l = new Label(headers[i], lbs);
			l.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
			mTable.add(l).align(2);
			mTable.row().padTop(headerPad);
			mTable.add(mTimeline[i]).height(mTimeline[i].getHeight());
			mTable.row().padTop(picturePad);
			computedHeight += mTimeline[i].getHeight() + headerPad + picturePad;
		}

		mTable.setPosition(0, -(computedHeight / 2) - height / 2);
		
		final int movePicsto = (computedHeight / 2) + height + 20;
			
		startPics(movePicsto);
		
		getStage().addActor(mTable);
	}
	
	private void startPics(int moveto) {
		mTable.addAction(sequence(moveTo(mTable.getX(), moveto, 120f), run(new Runnable() {
			@Override
			public void run() {
				mGame.setScreen(new MainMenu(mGame), true);
			}
		})));
	}
	
	@Override
	public void show() {
		super.show();
		
		SoundManager.getInstance().playMusic(false, 0.1f, MusicType.ENDTHEME);
		// Font
		mFont = Resources.getInstance().font;
		
		// Bg
		Texture txt = Resources.getInstance().endBg;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());
		setBackgroundTexture(region);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		SoundManager.getInstance().stopMusic(MusicType.ENDTHEME);
	}

}
