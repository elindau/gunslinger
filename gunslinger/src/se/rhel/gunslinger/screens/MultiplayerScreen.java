package se.rhel.gunslinger.screens;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.view.ForegroundRender;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.moribitotech.mtx.AbstractScreen;

/**
 * Screen f�r multiplayermeny
 * Extendar AbstractScreen (mtx.jar)
 * @author Rickard
 *
 */
public class MultiplayerScreen extends AbstractScreen {
	
	private static final String SCREEN_NAME = "Multiplayer Menu";
	
	private Skin 			mSkin;
	private TextButton 		mHostButton, mJoinButton, mBackButton;
	private BitmapFont		mFont;
	
	private ForegroundRender mForegroundRender;
	private BaseStage		mStage;
	private Camera			mCamera;
	private TweenManager	mManager;
	
	public MultiplayerScreen(Gunslinger aGame, ForegroundRender fgr) {
		super(aGame, SCREEN_NAME);
		
		mStage = new BaseStage(getStage().getSpriteBatch());
		mCamera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mForegroundRender = fgr;
		mForegroundRender.setSpriteBatch(getStage().getSpriteBatch());
		mManager = fgr.getManager();
		getStage().setViewport(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT, false);
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		mManager.update(delta);
		
		getStage().setViewport(16f, 10f, false);
		getStage().getSpriteBatch().begin();
		
		mForegroundRender.draw(delta);	
		mStage.draw();
		
		getStage().getSpriteBatch().end();
	}
	
	@Override
	public void resize(int width, int height) {
		
		Table table = new Table();
		table.setPosition(Gdx.graphics.getWidth() / 2, 4f * mCamera.getScale().y);
		mStage.addActor(table);
		
		TextButtonStyle tbs = UIcomponents.BaseTextButtonStyle(mSkin, mFont, Color.WHITE, "button", "buttonpressed");
		
		//Knapp f�r host av server
		mHostButton = UIcomponents.button("Host Server", tbs);
		mHostButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				getGame().setScreen(new HostGameScreen((Gunslinger)getGame(), mForegroundRender));
			}
		});
		
		//Knapp f�r joina server
		mJoinButton = UIcomponents.button("Join Server", tbs);	
		mJoinButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				getGame().setScreen(new JoinGameScreen((Gunslinger)getGame(), mForegroundRender));
			}
		});
		
		mBackButton = UIcomponents.button("Back", tbs);	
		mBackButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});

		table.add(mJoinButton).left();
		table.row().padTop(10);
		table.add(mHostButton).left();	
		table.row().padTop(10);
		table.add(mBackButton).left();
	}
	
	private void back() {
		getGame().setScreen(new MainMenu((Gunslinger)getGame(), mForegroundRender));
	}

	@Override
	public void show() {
		mSkin = new Skin(Resources.getInstance().menuAtlas);
		mFont = Resources.getInstance().font;
		
		Gdx.input.setInputProcessor(mStage);
		Gdx.input.setCatchBackKey(true);
		
		getStage().addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.BACK) {
					back();
				}
				return true;
			}
		});
		
		Texture txt = Resources.getInstance().logo;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());		
		setBackgroundTexture(region);
	}

	@Override
	public void hide() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		super.dispose();
		mSkin.dispose();
		mFont.dispose();
	}
}
