package se.rhel.gunslinger.screens;

import java.util.HashMap;
import java.util.Map;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.CameraLookAt;
import se.rhel.gunslinger.model.Id;
import se.rhel.gunslinger.model.entities.GameObject.Type;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.network.KryoClient;
import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.network.Network.PlayerPickup;
import se.rhel.gunslinger.observers.SoundObserver;
import se.rhel.gunslinger.sceens.actors.PlayerActor;
import se.rhel.gunslinger.screens.ChooseLevelScreen.Waypoint;
import se.rhel.gunslinger.screens.IntroScreen.State;
import se.rhel.gunslinger.view.BubbleRender;
import se.rhel.gunslinger.view.ForegroundRender;
import se.rhel.gunslinger.view.LevelRender;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.moribitotech.mtx.AbstractActor;
import com.moribitotech.mtx.AbstractScreen;
import com.moribitotech.mtx.AnimationCreator;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class CutScenesScreen extends AbstractScreen {
	
	public enum State {FIRST_WALK, DIALOG1, DIALOG2, DIALOG3, SECOND_WALK, THIRD_WALK, DONE};

	private static final String	SCREEN_NAME = "Scene";
	private static final int	GIVE_COIN_AMOUNT = 100;
	
	private Gunslinger	mGame;
	private KryoServer 	mServer;
	private KryoClient	mClient;
	private boolean		mIsHost;
	
	private PlayerActor	mSheriff, mPlayer;
	private Animation   mPlayerActorAnimationLeft, mPlayerActorAnimationStillLeft;
	private Animation 	mSheriffAnimationRight, mSheriffAnimationLeft, mSheriffAnimationStillRight;    
	private Sprite		mDialogBar;
	private Sprite		mSheriffAvatar;
	private Sprite		dialogSprite;
	private BitmapFont  mFont;
	private Label		mDialog;
	private Stage		mDialogStage;
	private Button		mAcceptButton;
	private Camera		mCamera;
	private boolean 	mDoActionOnce = true;

	//Renders
	private ForegroundRender mForegroundRender;
	private BubbleRender mBubbleRender;
	private LevelRender mLevelRender;
	
	private Map<State, String> DIALOGS = new HashMap<State, String>();
	private Map<State, Sprite> AVATARS = new HashMap<State, Sprite>();
	
	// H�ll koll p� vilken state
	private State 		mState;
	
	private TweenManager mManager;
	private BaseStage mStage;
	private Sprite mShadow;
	
	public CutScenesScreen(Gunslinger game, KryoServer server, KryoClient client, Camera camera) {
		super(game, SCREEN_NAME);
		mCamera = camera;
		mIsHost = true;
		mGame = game;
		mServer = server;
		mClient = client;
		init();
	}
	
	public CutScenesScreen(Gunslinger game, KryoClient client, Camera camera) {
		super(game, SCREEN_NAME);
		mCamera = camera;
		mIsHost = false;
		mGame = game;
		mClient = client;
		init();
	}
	
	// Testkonstruktor
	public CutScenesScreen(Gunslinger game, Camera camera) {
		super(game, SCREEN_NAME);
		mCamera = camera;
		mGame = game;
		init();
	}
	
	private void init() {
		mManager = new TweenManager();
		mStage = new BaseStage(getStage().getSpriteBatch());
		mStage.setCamera(mCamera);
		
		mForegroundRender = new ForegroundRender(getStage().getSpriteBatch(), mManager, new CameraLookAt(mCamera.position.cpy()));
		mLevelRender = new LevelRender(getStage().getSpriteBatch(), mClient.getWorldModel().levelManager(), new CameraLookAt(mCamera.position.cpy()));
		mBubbleRender = new BubbleRender(getStage().getSpriteBatch(), mClient.getWorldModel(), mCamera, new SoundObserver());
		
		populateDialogs();
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		mManager.update(delta);
		mStage.act(delta);

		getStage().getSpriteBatch().begin();
		getStage().setCamera(mCamera);
		getStage().setViewport(16f, 10f, false);
				
		mLevelRender.draw(delta, mClient.getWorldModel().player());
		
		//Playershadow
		mShadow.setPosition(mPlayer.getX() + mShadow.getWidth() / 2f, mPlayer.getY() - mShadow.getHeight() / 2f);
		mShadow.draw(getStage().getSpriteBatch());
		
		//Sheriffshadow
		mShadow.setPosition(mSheriff.getX() + mShadow.getWidth() / 2f, mSheriff.getY() - mShadow.getHeight() / 2f);
		mShadow.draw(getStage().getSpriteBatch());
		
		mStage.draw();
		mBubbleRender.drawScene(delta);
		mForegroundRender.draw(delta);
		
		mStage.getActors().removeValue(mAcceptButton, true);
		mDialogStage.clear();
			
		switch(mState) {
		case FIRST_WALK:
			break;
		case SECOND_WALK:
			// Sheriffens f�rsta agerande - g� till spelare
			if(mDoActionOnce) {
				mDoActionOnce = false;
				mSheriff.addAction(sequence(moveTo(mPlayer.getX()-1f, mPlayer.getY(), 2f), run(new Runnable() {
					
					@Override
					public void run() {
						mSheriff.setAnimation(mSheriffAnimationStillRight, true, false);
						mState = State.DIALOG1;
						mDoActionOnce = true;
					}
				})));
			}
			
			break;
		case DIALOG1:
		case DIALOG2:
		case DIALOG3:
			// Ska rita ut dialog
			mDialogBar.draw(getStage().getSpriteBatch());
			mDialog.setText(DIALOGS.get(mState));
			mDialogStage.addActor(mDialog);
			mStage.addActor(mAcceptButton);
			dialogSprite.draw(getStage().getSpriteBatch());
			break;
		case THIRD_WALK:
			if(mDoActionOnce) {
				mDoActionOnce = false;
				mSheriff.addAction(sequence(moveTo(-3f, 5f, 5f)));
				mPlayer.addAction(sequence(moveTo(-3f, 5f, 5f), run(new Runnable() {
					
					@Override
					public void run() {
						mState = State.DONE;
					}
				})));
				
				if(mIsHost) {
					for(Player p : mServer.world().getPlayers().values()) {
						// Lite fejkad coin-pickup ocks�
						for(int i = 0; i < GIVE_COIN_AMOUNT; i++) {
							p.getInventory().newCoin();	
						}
						PlayerPickup pp = new PlayerPickup(GIVE_COIN_AMOUNT, p.getID(), Id.getInstance().get(), Type.COIN, null, true);
						mServer.sendMessage(pp);
					}	
				}
			}
			
			break;
		case DONE:
			
			// Starta shoppen
			if(mIsHost) {
				mGame.setScreen(new ShopScreen(mGame, mServer, mClient, mCamera, true), true);
				// mGame.setScreen(new ShopScreen(mGame, mServer, mClient, mCamera));
			} else {
				mGame.setScreen(new ShopScreen(mGame, mClient, mCamera, true), true);
				// mGame.setScreen(new ShopScreen(mGame, mClient, mCamera));
			}
			break;
		}
	
		mDialog.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
		mDialog.setPosition(3.2f * mCamera.getScale().x,  (float)Math.round(mCamera.getScale().y * 1f - mDialog.getTextBounds().height / 2));
		mDialogStage.draw();
		getStage().getSpriteBatch().end();

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		mDialogStage = new Stage(width, height, false);
		mState = State.FIRST_WALK;
		
		// Spelaren
		mPlayerActorAnimationLeft = AnimationCreator.getAnimationFromSingleTextureMultiRows(Resources.getInstance().playerActorAtlas, "player_sprite48", 5, 5, 2, 1, 0.1f);
		mPlayerActorAnimationStillLeft = AnimationCreator.getAnimationFromSingleTextureMultiRows(Resources.getInstance().playerActorAtlas, "player_sprite48", 1, 5, 2, 1, 0.1f);
		mPlayer = new PlayerActor(17f, 5f, 2f, 2f);
		mPlayer.setAnimation(mPlayerActorAnimationLeft, true, true);
		mPlayer.setSize(2f, 2f);
		
		mPlayer.addAction(sequence(moveTo(7f, 4f, 4f), run(new Runnable() {
			
			@Override
			public void run() {
				mPlayer.setAnimation(mPlayerActorAnimationStillLeft, true, false);
				mSheriff.setAnimation(mSheriffAnimationRight, true, true);
				mState = State.SECOND_WALK;
				dialogSprite = mSheriffAvatar;
			}
		})));
		
		// Sheriffen
		mSheriffAnimationLeft = AnimationCreator.getAnimationFromMultiTextures(Resources.getInstance().sheriff, "sheriff2", 5, .1f);
		mSheriffAnimationRight = AnimationCreator.getAnimationFromMultiTextures(Resources.getInstance().sheriff, "sheriff", 5, .1f);
		mSheriffAnimationStillRight = AnimationCreator.getAnimationFromMultiTextures(Resources.getInstance().sheriff, "sheriff", 1, .1f);
		mSheriff = new PlayerActor(5f, 5f, 2f, 2f);
		mSheriff.setAnimation(mSheriffAnimationStillRight, true, false);
		mSheriff.setSize(2f, 2f);
		
		//Skugga
		mShadow = new Sprite(Resources.getInstance().characterShadow);
		mShadow.setSize(mPlayer.getWidth() / 2f, mPlayer.getWidth() / 6f);
		mShadow.setColor(0, 0, 0, 0.4f);
		
		// Acceptera-knapp
		Skin buttonSkin = new Skin(Resources.getInstance().shopbuttons);
		ButtonStyle bs = new ButtonStyle();
		bs.up = buttonSkin.getDrawable("addbutton");
		bs.down = buttonSkin.getDrawable("addbuttonpressed");
		mAcceptButton = new Button(bs);
		mAcceptButton.setSize(1f, 1f);
		mAcceptButton.setPosition(1f, 2f);
		mAcceptButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				continueDialog();
			}
		});
		
		mDialogStage = new BaseStage(getStage().getSpriteBatch());
		
		// Dialog-bar
		mDialogBar = new Sprite(Resources.getInstance().wepinfo_full, 0, 0, 160, 20);
		mDialogBar.setSize(16f, 2f);
		mDialogBar.setPosition(0, 0);
		
		// Font etc
		mFont = Resources.getInstance().font;
		LabelStyle lbs = new LabelStyle(mFont, Color.WHITE);
		mDialog = new Label("", lbs);
		mDialog.setWrap(true);
		mDialog.setBounds(3.2f * mCamera.getScale().x, 0.4f * mCamera.getScale().y, (11f * mCamera.getScale().x) / Camera.FONT_SCALE_X, 1f * mCamera.getScale().y);
		mFont.setUseIntegerPositions(false);
		
		
		// L�gg till akt�rer i stagen
		mStage.addActor(mSheriff);
		mStage.addActor(mPlayer);
	}
	
	@Override
	public void show() {
		super.show();
		
		// Bakgrund
		Texture txt = Resources.getInstance().levelBg;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());
		setBackgroundTexture(region);
		
		mSheriffAvatar = new Sprite(Resources.getInstance().sheriff_avatar);
		mSheriffAvatar.setSize(1.5f, 1.5f);
		mSheriffAvatar.setPosition(1.3f, 0.2f);
		
		// Input
		Gdx.input.setInputProcessor(mStage);
	}
	
	private void continueDialog() {
		if(mState == State.DIALOG1) {
			mState = State.DIALOG2;
			dialogSprite = mSheriffAvatar;
			return;
		}
		if(mState == State.DIALOG2) {
			mState = State.DIALOG3;
			dialogSprite = mSheriffAvatar;
			return;
		}
		if(mState == State.DIALOG3) {
			mState = State.THIRD_WALK;
			mPlayer.setAnimation(mPlayerActorAnimationLeft, true, true);
			mSheriff.setAnimation(mSheriffAnimationLeft, true, true);
			return;
		}
	}
	
	private void populateDialogs() {
		String d1 = "Hello there, stranger! Thank you for saving our beloved town.. Tell me, what is your name, hero?";
		String d2 = "Not the talkactive type, are we?.. Very well, man with no name, we would still like to thank you.";
		String d3 = "Here's "+ GIVE_COIN_AMOUNT + "$, please follow me to the local shop and buy yourself a weapon more worthy a man like yourself!";
		
//		String d1 = "Hello there, stranger!\nThank you for saving our beloved town..\nTell me, what is your name, hero?";
//		String d2 = "Not the talkactive type, are we?\n..Very well, man with no name, \nwe would still like to thank you.";
//		String d3 = "Here's "+ GIVE_COIN_AMOUNT + "$, please follow me to the local \nshop and buy yourself \na weapon more worthy a man like yourself!";
		
		DIALOGS.put(State.DIALOG1, d1);
		DIALOGS.put(State.DIALOG2, d2);
		DIALOGS.put(State.DIALOG3, d3);
		
		AVATARS.put(State.DIALOG1, mSheriffAvatar);
		AVATARS.put(State.DIALOG2, mSheriffAvatar);
		AVATARS.put(State.DIALOG3, mSheriffAvatar);
	}
}
