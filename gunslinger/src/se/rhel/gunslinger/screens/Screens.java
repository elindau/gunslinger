package se.rhel.gunslinger.screens;

public enum Screens {
	BASE, CHOOSE, GAME, HOSTGAME, JOINGAME, LOADING, LOBBY, MAIN, MULTIPLAYER, SPLASH, CUTSCENE, SHOP, INTRO;
}
