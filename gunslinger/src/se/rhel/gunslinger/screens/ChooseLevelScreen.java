package se.rhel.gunslinger.screens;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.Levels;
import se.rhel.gunslinger.network.KryoClient;
import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.network.Network.ActorTween;
import se.rhel.gunslinger.network.Network.ScreenChange;
import se.rhel.gunslinger.tweens.ActorAccessor;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.TweenPaths;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.moribitotech.mtx.AbstractActor;
import com.moribitotech.mtx.AbstractScreen;
import com.moribitotech.mtx.AnimationCreator;

/**
 * Klass f�r hantering-vy av levlar
 * @author Emil
 *
 */
public class ChooseLevelScreen extends AbstractScreen {
	
	private static final Vector2 	WP_START = new Vector2(0.35f -1f, 5.90f);
	private static final Vector2[] 	WP_START_TO_DESERT = {new Vector2(2.70f-1f, 5.66f), new Vector2(4.66f-1f, 5.18f)};
	private static final Vector2 	WP_DESERT = new Vector2(5.87f-1f, 5.02f);
	private static final Vector2[] 	WP_DESERT_TO_ARMADILLO = {new Vector2(7.33f-1f, 5.64f), new Vector2(8.58f-1f, 6.16f), new Vector2(10.79f-1f, 6.41f)};
	private static final Vector2 	WP_ARMADILLO = new Vector2(12.41f-1f, 6.41f);
	
	private static final String 	SCREEN_NAME = "Choose level";
	
	private static final int 		SWITCH_BUTTON_WIDTH = 2;
	private static final int 		START_BUTTON_WIDTH = 4;
	private static final int 		START_BUTTON_HEIGHT = 2;
	private static final int		WIDTH = 16;
	private static final int		HEIGHT = 10;
	
	private PlayerActor 			mPlayer;
	private Animation 				mPlayerActorAnimationRight, mPlayerActorAnimationLeft, mPlayerActorAnimationStillLeft, mPlayerActorAnimationStillRight;
	private Skin					mStartSkin;
	private Button 					mRightButton, mLeftButton;
	private Button					mStartButton;
	
	private TweenManager 			mManager;
	private boolean 				mIsTweening = false;

	private Gunslinger 				mGame;
	private KryoServer				mServer;
	private KryoClient				mClient;
	
	private boolean 				mLeft;
	private boolean					mIsHost = false;
	
	private Levels					mLevelJustPlayed;
	private int						mUnlockedToLevel; 
	
	// Klient
	private ActorTween				mActorTween;
	
	// Multiplayer konstruktor
	public ChooseLevelScreen(Gunslinger game, KryoServer server, KryoClient client, Levels levelJustPlayed) {
		super(game, SCREEN_NAME);
		mClient = client;
		mServer = server;
		mGame = game;
		mIsHost = true;
		mLevelJustPlayed = levelJustPlayed;
		
		// H�mta ut hur m�nga levlar som l�sts upp och plussa med en d� n�sta level
		// ska kunna v�ljas
		if(Resources.getInstance().gameState != null) {
			mUnlockedToLevel = Resources.getInstance().gameState.levelsCleared();
			mUnlockedToLevel++;	
		} else {
			mUnlockedToLevel = 1;
		}
	}
	
	public ChooseLevelScreen(Gunslinger game, KryoClient client, Levels levelJustPlayed) {
		super(game, SCREEN_NAME);
		mClient = client;
		mGame = game;
		mLevelJustPlayed = levelJustPlayed;
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		mManager.update(delta);
		
		// Fr�ga klienten om vi f�tt n�got fr�n servern
		if(!mIsHost) {
			ActorTween tween = mClient.getTween();
			if(tween != null) {
				if(tween != mActorTween) {
					mActorTween = tween;
					mLeft = tween.left;
					tween(mActorTween.targetX, mActorTween.targetY, mActorTween.wps);
				}
			}
		}
		
		
		Levels lvl = mClient.startLevel();

		if(lvl != null) {
			System.out.println("Level to start: " + lvl);
			System.out.println("Level read?: " + mClient.getWorldModel().levelManager().isLevelRead());
						
			if (mIsHost)
				mServer.world().update(delta);
			
			mClient.getWorldModel().update(delta);
			
			// Om leveln �r inl�st s� kan vi starta spelet
			// TODO: se till att alla klienter har l�st in leveln
			if(mClient.getWorldModel().levelManager().isLevelRead()) {
				
			}
				
			//�r de level1 starta intro
//			if (lvl.getAbbreviation() == 1) {
//				if (mIsHost) {
//					mServer.world().screenChange(Screens.INTRO);
//				}
//			}
				
//			if(mIsHost) {
//				mGame.setScreen(new GameScreen(mGame, mServer, mClient));
//			} else {
//				mGame.setScreen(new GameScreen(mGame, mClient));
//			}
		}
		
		// �r det dags att byta screen
		if(mClient.getWorldModel().isItTimeToChangeScreen()) {
			if(mClient.getWorldModel().getToScreen() == Screens.GAME) {					
				if(mIsHost) {
					mClient.getWorldModel().setIsItTimeToChangeScreen(false);
					// Spara undan staten
					mGame.setScreen(new GameScreen(mGame, mServer, mClient));
				} else {
					mClient.getWorldModel().setIsItTimeToChangeScreen(false);
					mGame.setScreen(new GameScreen(mGame, mClient));
				}
			}
			
			if(mClient.getWorldModel().getToScreen() == Screens.INTRO) {					
				if(mIsHost) {
					mClient.getWorldModel().setIsItTimeToChangeScreen(false);
					// Spara undan staten
					mGame.setScreen(new IntroScreen(mGame, mServer, mClient, new GameScreen(mGame, mServer, mClient)));
				} else {
					mClient.getWorldModel().setIsItTimeToChangeScreen(false);
					mGame.setScreen(new IntroScreen(mGame, mClient, new GameScreen(mGame, mClient)));
				}
			}
		}
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		// SpelarAkt�ren
		// mPlayerActorAnimationRight = AnimationCreator.getAnimationFromSingleTexture(Resources.getInstance().cowboyAtlas, "cowboy", 5, 0.2f);
	
		mPlayerActorAnimationRight = AnimationCreator.getAnimationFromSingleTextureMultiRows(Resources.getInstance().playerActorAtlas, "player_sprite48", 5, 5, 2, 0, 0.1f); 
		mPlayerActorAnimationLeft = AnimationCreator.getAnimationFromSingleTextureMultiRows(Resources.getInstance().playerActorAtlas, "player_sprite48", 5, 5, 2, 1, 0.1f);
		mPlayerActorAnimationStillRight = AnimationCreator.getAnimationFromSingleTextureMultiRows(Resources.getInstance().playerActorAtlas, "player_sprite48", 1, 5, 2, 0, 0.1f);
		mPlayerActorAnimationStillLeft = AnimationCreator.getAnimationFromSingleTextureMultiRows(Resources.getInstance().playerActorAtlas, "player_sprite48", 1, 5, 2, 1, 0.1f);
		
		if(mLevelJustPlayed != null) {
			Waypoint wp = levelsToWaypoint(mLevelJustPlayed);
			Vector2 pos = waypointToVector2(wp);
			mPlayer = new PlayerActor(pos.x, pos.y, 34, 34);
			mPlayer.playerAt = wp;
		} else {
			mPlayer = new PlayerActor(WP_START.x, WP_START.y, 34, 34);
			mPlayer.playerAt = Waypoint.START;	
		}
		
		mPlayer.setSize(2f, 2f);
		mPlayer.setAnimation(mPlayerActorAnimationStillRight, true, true);
		
		// Knappar
		ButtonStyle bsLeft = UIcomponents.BaseButtonStyle(mStartSkin, "left", "left_pressed");
		ButtonStyle bsRight = UIcomponents.BaseButtonStyle(mStartSkin, "right", "right_pressed");
		ButtonStyle bsStart = UIcomponents.BaseButtonStyle(mStartSkin, "start", "start_pressed");
		ButtonStyle bsLeave = UIcomponents.BaseButtonStyle(mStartSkin, "leave", "leave_pressed");
		// tbs.font.setScale(0.1f);
		
		mRightButton = UIcomponents.button(bsRight, new Rectangle(
				16 - 1.3f,
				0,
				1.3f,
				10));
		mLeftButton = UIcomponents.button(bsLeft, new Rectangle(
				0,
				0,
				1.3f,
				10));
		
		mStartButton = UIcomponents.button(bsStart, new Rectangle(
				Gdx.graphics.getWidth() / 2 - START_BUTTON_WIDTH / 2,
				0,
				START_BUTTON_WIDTH,
				START_BUTTON_HEIGHT));
		
		mLeftButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(!mIsTweening)
					moveActor(true);
			}
		});
		
		mRightButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(!mIsTweening)
					moveActor(false);
			}
		});
		
		mStartButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				tryStartLevel();
			}
		});
		
		Button backButton = UIcomponents.button(bsLeave, new Rectangle(
				Gdx.graphics.getWidth() / 2 - START_BUTTON_WIDTH / 2,
				0,
				START_BUTTON_WIDTH,
				START_BUTTON_HEIGHT));
		
		backButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});
		
		getStage().addActor(mPlayer);
		
		// Table f�r knapparna
		Table table = new Table();
		table.setSize(16, 10);
		table.setFillParent(true);
		table.bottom();
		getStage().addActor(table);
		
		table.add(backButton).size(3, 1);
		
		if(mIsHost) {
			getStage().addActor(mRightButton);
			getStage().addActor(mLeftButton);
			//table.row().padTop(0.3f);
			table.add(mStartButton).size(3, 1).padLeft(0.3f);
			// getStage().addActor(mStartButton);
		}
		
		// 16:10 viewport
		getStage().setViewport(16, 10, false);
	}
	
	private void back() {
		if(mClient != null) {
			mClient.shutdown();
		}
		
		if(mIsHost) {
			if(mServer != null) {
				mServer.shutdown();
			}
		} 
		
		getGame().setScreen(new MainMenu((Gunslinger)getGame()));
	}
	
	@Override
	public void show() {
		super.show();
		
		mStartSkin = new Skin(Resources.getInstance().chooseLevelAtlas);
		
		Gdx.input.setInputProcessor(getStage());
		Gdx.input.setCatchBackKey(true);
		
		getStage().addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.BACK) {
					back();
				}
				return true;
			}
		});
		
		// Tweening
		mManager = new TweenManager();
		Tween.registerAccessor(AbstractActor.class, new ActorAccessor());
		
		// Bakgrund
		Texture txt = Resources.getInstance().levelMap;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());		
		setBackgroundTexture(region);
	}
	
	// F�rs�ker starta leveln
	private void tryStartLevel() {
		
		Levels startLevel = waypointToLevel(mPlayer.playerAt); 
		
		// F�rs�k starta nuvarande level
		if(startLevel != null) {
			if(mIsHost) {
				// H�r borde init k�ras
				mServer.world().init(startLevel);
				mServer.sendMessage(startLevel);
				if(startLevel == Levels.LEVEL1) {
					mServer.sendMessage(new ScreenChange(Screens.INTRO));	
				} else {
					mServer.sendMessage(new ScreenChange(Screens.GAME));
				}
				
				
				System.out.println("Start level server send: " + startLevel);
			}
		}
	}
	
	/**
	 * Lite hj�lpmetoder
	 * @param wp
	 * @return
	 */
	private Levels waypointToLevel(Waypoint wp) {
		switch(wp) {
		case START:
			return null;
		case DESERT:
			return Levels.LEVEL1;
		case ARMADILLO:
			return Levels.LEVEL2;
		default:
			assert false; return null;
		}
	}
	
	private Waypoint levelsToWaypoint(Levels lvl) {
		switch(lvl) {
		case LEVEL1:
			return Waypoint.DESERT;
		case LEVEL2:
			return Waypoint.ARMADILLO;
		default: assert false; return null;
		}
	}
	
	private Vector2 waypointToVector2(Waypoint wp) {
		switch(wp) {
		case START:
			return WP_START;
		case DESERT:
			return WP_DESERT;
		case ARMADILLO:
			return WP_ARMADILLO;
		default: assert false; return null;
		}
	}
	
	// Flyttar spelaren till r�tt position
	private void moveActor(boolean left) {
		
		mLeft = left;
		
		if(left) {
			mPlayer.setAnimation(mPlayerActorAnimationLeft, true, true);
			
			switch(mPlayer.playerAt) {
			case START:
				// Fr�n start till v�nster = Inget
				break;
			case DESERT:
				// Fr�n desert till v�nster = Start
				tween(WP_START.x, WP_START.y, new float[]{WP_START_TO_DESERT[1].x, WP_START_TO_DESERT[1].y, WP_START_TO_DESERT[0].x, WP_START_TO_DESERT[0].y});
				break;
			case ARMADILLO:
				// Fr�n armadillo till v�nster = Desert
				tween(WP_DESERT.x, WP_DESERT.y, new float[]{WP_DESERT_TO_ARMADILLO[2].x, WP_DESERT_TO_ARMADILLO[2].y,
						WP_DESERT_TO_ARMADILLO[1].x, WP_DESERT_TO_ARMADILLO[1].y,
						WP_DESERT_TO_ARMADILLO[0].x, WP_DESERT_TO_ARMADILLO[0].y});
				break;
			}	
			
		} else {
			// H�ger
			mPlayer.setAnimation(mPlayerActorAnimationRight, true, true);
			
			switch(mPlayer.playerAt) {
			case START:
				// Fr�n start till h�ger = Desert - Alltid uppl�st
				tween(WP_DESERT.x, WP_DESERT.y, new float[]{WP_START_TO_DESERT[0].x, WP_START_TO_DESERT[0].y, WP_START_TO_DESERT[1].x, WP_START_TO_DESERT[1].y});	
				break;
			case DESERT:
				// Fr�n desert till h�ger = Armadillo - Kan bara flyttas till om spelare l�st upp level1
				if(isLevelUnlocked(Waypoint.ARMADILLO)) {
					tween(WP_ARMADILLO.x, WP_ARMADILLO.y, new float[]{WP_DESERT_TO_ARMADILLO[0].x, WP_DESERT_TO_ARMADILLO[0].y,
							WP_DESERT_TO_ARMADILLO[1].x, WP_DESERT_TO_ARMADILLO[1].y,
							WP_DESERT_TO_ARMADILLO[2].x, WP_DESERT_TO_ARMADILLO[2].y});	
				}
				break;
			case ARMADILLO:
				// Fr�n armadillo till h�ger = Inget
				break;
			}		
		}
	}
	
	/**
	 * Koll om leveln man vill g� till �r uppl�st
	 * @return
	 */
	private boolean isLevelUnlocked(Waypoint wp) {
		return mUnlockedToLevel >= waypointToLevel(wp).getAbbreviation();
	}
	
	// Hanterar tweeningen
	private void tween(float targetX, float targetY, float... wps) {
		
		// Skicka �ver till klienterna att actorn ska flytta p� sig
		if(mIsHost) {
			mServer.sendMessage(new ActorTween(targetX, targetY, mLeft, wps));
		} else {
			// Annars borde vi f�tt ett meddelande p� servern och ska kolla mot mLeft
			if(mLeft) {
				mPlayer.setAnimation(mPlayerActorAnimationLeft, true, true);
			} else {
				mPlayer.setAnimation(mPlayerActorAnimationRight, true, true);	
			}
		}
		
		mIsTweening = true;
		Tween.setWaypointsLimit(10);
		
		Tween.to(mPlayer, ActorAccessor.POSITION_XY, 1f)
			.target(targetX, targetY)
			.path(TweenPaths.catmullRom)
			.waypoint(wps)
			.delay(0.0f)
			.setCallback(cb)
			.start(mManager);
	}
	
	// Callback f�r tweening
	private final TweenCallback cb = new TweenCallback() {
		@Override
		public void onEvent(int arg0, BaseTween<?> arg1) {
			if(mPlayer.getX() == WP_DESERT.x && mPlayer.getY() == WP_DESERT.y) {
				mPlayer.playerAt = Waypoint.DESERT;
			} else if(mPlayer.getX() == WP_START.x && mPlayer.getY() == WP_START.y) {
				mPlayer.playerAt = Waypoint.START;
			} else if(mPlayer.getX() == WP_ARMADILLO.x && mPlayer.getY() == WP_ARMADILLO.y) {
				mPlayer.playerAt = Waypoint.ARMADILLO;
			}
			
			mIsTweening = false;
			
			if(mLeft) {
				mPlayer.setAnimation(mPlayerActorAnimationStillLeft, true, true);
			} else {
				mPlayer.setAnimation(mPlayerActorAnimationStillRight, true, true);
			}
		}
	};
	
	/**
	 * Representerar spelaren som flyttar sig mellan levlar
	 * @author Emil
	 *
	 */
	public class PlayerActor extends AbstractActor {
		
		public PlayerActor(TextureRegion textureRegion, boolean isTextureRegionActive,
				float posX, float posY, float width, float height) {
			super(textureRegion, isTextureRegionActive, posX, posY, width, height);
		}
		
		public PlayerActor(float posX, float posY, float width, float height) {
			super(posX, posY, width, height);
		}
		
		public Waypoint playerAt;
				
	}
	
	public enum Waypoint {
		START, DESERT, ARMADILLO;
	}
}


