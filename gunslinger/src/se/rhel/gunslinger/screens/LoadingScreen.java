package se.rhel.gunslinger.screens;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import com.badlogic.gdx.Screen;

public class LoadingScreen implements Screen {
	
	private Gunslinger 	mGame;
	
	public LoadingScreen(Gunslinger game) {
		mGame = game;
	}
	
	@Override
	public void render(float delta) {
		
		// Is the assets done loading
		if(Resources.getInstance().assetManager.update()) {
			// Hooking up the instances
			Resources.getInstance().setGameInstances();
		}
		
		// Eventually showing the progress
		@SuppressWarnings("unused")
		float progress = Resources.getInstance().assetManager.getProgress();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// Prepare the assetmanager
		Resources.prepareManager();
		Resources.getInstance().loadGameResources();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
