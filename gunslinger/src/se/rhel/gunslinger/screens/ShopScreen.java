package se.rhel.gunslinger.screens;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.Id;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.entities.GameObject.Type;
import se.rhel.gunslinger.model.weapons.WeaponStats;
import se.rhel.gunslinger.model.weapons.WeaponCreator.Weapons;
import se.rhel.gunslinger.model.weapons.WeaponStats.WeaponStatComparer;
import se.rhel.gunslinger.network.KryoClient;
import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.network.Network.PlayerPickup;
import se.rhel.gunslinger.sceens.actors.WeaponActor;
import se.rhel.gunslinger.view.AnimatedSprite;
import se.rhel.gunslinger.view.SoundManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.moribitotech.mtx.AbstractActor;
import com.moribitotech.mtx.AbstractScreen;
import com.moribitotech.mtx.AnimationCreator;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.tablelayout.Cell;

public class ShopScreen extends AbstractScreen {
	
	private static final String		SCREEN_NAME = "Shop";
	private static final int		STAGE_WIDTH = 16;
	private static final int		STAGE_HEIGHT = 10;
	private static final int		NO_OF_WEPS = 3;
	private static final Vector2[] 	TABLE_POSITIONS = new Vector2[] { new Vector2(5.5f, 4f), new Vector2(7.5f, 4f), new Vector2(9.5f, 4f) } ;
	private boolean[] 				TABLE_POSITION_FREE = new boolean[] { true, true, true };
	private static final int		SUPERGUN_COST = 199;
	private static final int		OMEGAGUN_COST = 799;
	private static final float[]	CLERK_POSITIONS_X = new float[] {5f, 7f, 9.5f};
	
	private Gunslinger				mGame;
	private KryoServer				mServer;
	private KryoClient				mClient;
	private boolean					mIsHost = false;
	private boolean					mDrawInfo = false;

	private	TextureAtlas 			mShopAtlas;
	private Sprite					mBench;
	
	private Sprite					mWepInfo;
	private Sprite					mCash;
	private Label					mCashLabel;
	private Player					mPlayer;
	private BitmapFont				mFont;
	private BitmapFont				mUIFont;
	private Stage					mStage;
	private Skin					mButtonSkin;
	private Button					mBuyButton, mCloseButton, mLeaveButton;
	private AnimatedSprite			mGunBought, mGunSelect;
	private boolean					mIsGunBought = false;
	
	private ClerkActor				mClerkActor;
	private Array<WeaponActor> 		mWeaponActors;
	private Map<Integer, Boolean>   mDrawInfos = new HashMap<Integer, Boolean>();
	private Random 					mRand = new Random();
	
	private Vector2					mMarkedPos = new Vector2();
	
	private ClickListener			mBuyListener, mCloseListener;
	
	private float					mClerkStillTimeElapsed = 0f;
	private float					mClerkStillMaxTime = 5.5f;
	
	private Camera					mCamera;
	
	private boolean 				mIsFromBoss;

	private LabelStyle 				lbs;
	private LabelStyle 				namestyle;
	private WeaponStats 			oldWeapon;
	private WeaponStats 			newWeapon;
	private WeaponStatComparer 		wsc;
	private Table 					mWeaponSwapTable;
	private Table					mCostTable;
	
	private Label					mAttackLabel;
	private Label					mCritDmgLabel;
	private Label					mCritChanceLabel;


	public ShopScreen(Gunslinger game, KryoServer server, KryoClient client, Camera camera, boolean fromBoss) {
		super(game, SCREEN_NAME);
		
		mCamera = camera;
		mGame = game;
		mServer = server;
		mClient = client;
		mIsHost = true;
		mPlayer = mClient.getWorldModel().player();
		mIsFromBoss = fromBoss;
	}
	

	public ShopScreen(Gunslinger game, KryoClient client, Camera camera, boolean fromBoss) {
		super(game, SCREEN_NAME);
		
		mCamera = camera;
		mGame = game;
		mClient = client;
		mIsHost = false;
		mPlayer = mClient.getWorldModel().player();
		mIsFromBoss = fromBoss;
	}
	
	// Testkonstruktor

	public ShopScreen(Gunslinger game, boolean fromBoss) {
		super(game, SCREEN_NAME);
		
		mGame = game;
	}
	
	/**
	 * Hämtar ut en ledig position på bordet
	 * eller också retunerar den pos0,0
	 * @return vector2
	 */
	private Vector2 getTablePosition() {
		Vector2 pos = new Vector2(0, 0);
		for(int i = 0; i < TABLE_POSITION_FREE.length; i++) {
			if(TABLE_POSITION_FREE[i]) {
				pos.x = TABLE_POSITIONS[i].x;
				pos.y = TABLE_POSITIONS[i].y;
				TABLE_POSITION_FREE[i] = false;
				return pos;
			}
		}
		return pos;
	}
	
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		if(mIsHost) {
			mServer.update(delta);
		}
		mClient.getWorldModel().update(delta);
		
		getStage().getSpriteBatch().begin();

		mBench.draw(getStage().getSpriteBatch());
		
		mCash.setSize(1f, 1f);
		mCash.setPosition(-0.25f, 9f);
		mCash.draw(getStage().getSpriteBatch());
		mCashLabel.setText(String.valueOf(mPlayer.getInventory().getCoins()) + "$");
		mCashLabel.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
		mCashLabel.setAlignment(Align.bottom, Align.left);

		mCashLabel.setPosition(0.9f * mCamera.getScale().x - mCashLabel.getWidth() / 2, 9.05f * mCamera.getScale().y);
		mStage.addActor(mCashLabel);
		
		if(mDrawInfo) {
			mWepInfo.draw(getStage().getSpriteBatch());
			
			mGunSelect.setPosition(mMarkedPos.x, mMarkedPos.y + 1f);
			mGunSelect.update(delta);
			mGunSelect.draw(getStage().getSpriteBatch());
		}
		
		if(mIsGunBought) {
			mGunBought.setPosition(mMarkedPos.x  + 0.25f, mMarkedPos.y);
			mGunBought.update(delta);
			mGunBought.draw(getStage().getSpriteBatch());
		}
		
		if(mGunBought.isFinished()) {
			mIsGunBought = false;
			mGunBought.restart();
		}
		
		mClerkStillTimeElapsed += delta;
		if(mClerkStillTimeElapsed >= mClerkStillMaxTime) {
			mClerkStillTimeElapsed = 0f;
			moveClerk();
		}
		
		getStage().getSpriteBatch().end();
		mStage.draw();
	}
	

	private void moveClerk() {
		if(mClerkActor.getX() == CLERK_POSITIONS_X[0]) {

			mClerkActor.actionMoveTo(CLERK_POSITIONS_X[1], mClerkActor.getY(), 2f);
		} else if(mClerkActor.getX() == CLERK_POSITIONS_X[1]) {
			// Mitten
			Random rand = new Random();
			if(rand.nextInt() % 2 == 0) {
				mClerkActor.actionMoveTo(CLERK_POSITIONS_X[0], mClerkActor.getY(), 2f);
			} else {
				mClerkActor.actionMoveTo(CLERK_POSITIONS_X[2], mClerkActor.getY(), 2f);
			}
		} else {

			mClerkActor.actionMoveTo(CLERK_POSITIONS_X[1], mClerkActor.getY(), 2f);
		}
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		mStage = new Stage(width, height, false);
		
		mCash = new Sprite(Resources.getInstance().coinAtlas.createSprite("coin", 0));
		mCash.setSize(1f, 1f);
		LabelStyle style = new LabelStyle(mUIFont, Color.WHITE);
		mCashLabel = new Label(""+mPlayer.getInventory().getCoins(), style);
		

		getStage().setViewport(STAGE_WIDTH, STAGE_HEIGHT, false);
		
		// Initiera variablar
		mWeaponActors = new Array<WeaponActor>();
		
		// Clerk
		mClerkActor = new ClerkActor(7f, 3.8f, 2f, 2f);
		Animation a = AnimationCreator.getAnimationFromMultiTextures(Resources.getInstance().clerk, "clerk", 10, 0.2f);
		mClerkActor.setAnimation(a, true, true);
		getStage().addActor(mClerkActor);
		
		// Initiera shopens vapen till salu
		for(int i = 0; i < NO_OF_WEPS; i++) {
			// H�mta ut en ledig position

			Vector2 actorPos = getTablePosition();
			
			// L�gg till idt och om det ska ritas ut
			int id = Id.getInstance().get();
			mDrawInfos.put(id, false);
			
			Weapons type;
			if (mRand.nextBoolean()) {
				type = Weapons.SUPERGUN;
			} else {
				type = Weapons.OMEGAGUN;
			}
			
			final WeaponActor wp = new WeaponActor(actorPos.x, actorPos.y, 1.5f, 1f, type, id);
			Animation ani = AnimationCreator.getAnimationFromMultiTextures(Resources.getInstance().gunAtlas, "gun", 16, 0.1f);
			
			wp.setAnimation(ani, true, true);
			
			wp.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					clearShowInfoButton();
					mDrawInfo = true;
					getWeaponCheckInfo(wp.TYPE, wp.ID);
					mMarkedPos.x = wp.getX();
					mMarkedPos.y = wp.getY();
				}
			});
			
			wp.setSize(1.5f, 1f);
			
			mWeaponActors.add(wp);
			getStage().addActor(wp);
			
			// L�mnaknapp
			mLeaveButton.setPosition(13.75f, 0f);
			mLeaveButton.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					back();
				}
			});
			
			getStage().addActor(mLeaveButton);
		}
	}
	
	/**
	 * F�rs�ker k�pa vapen �t spelaren

	 */
	protected boolean tryBuyWeapon(Weapons type, int id) {

		// H�mta ut kostnaden

		int cost = getWeaponCost(type);
		
		// Har spelaren r�d

		if(mPlayer.getInventory().getCoins() >= cost) {
			// I s� fall, k�p vapnet
			// Som host g�r det bara meddela servern
			if(mIsHost) {
				if(mServer.world().playerBoughtGun(type, cost, mPlayer.getID())) {
					Resources.getInstance().gameState.setGunType(type);
					// Allt gick okej!
					// Ta bort vapnet
					removeWeaponFromTable(id);
					return true;
				}	
			} else {
				// Som enbart klient (fulfix som fungerar dugligt s� l�nge)
				PlayerPickup p = new PlayerPickup(cost, mClient.getWorldModel().player().getID(), 0, Type.WEAPON, type, true);
				mClient.sendMessageUDP(p);
				removeWeaponFromTable(id);
				return true;
			}	 
		} 

		return false;
	}
	
	private void removeWeaponFromTable(int id) {
		for(Actor a : getStage().getActors()) {
			if(a instanceof WeaponActor) {
				if(((WeaponActor) a).ID == id) {
					getStage().getActors().removeValue(a, true);
				}
			}
		}
	}
	
	/**
	 * Retunerar hur mycket ett visst vapen kostar

	 * @return  int
	 */
	private int getWeaponCost(Weapons type) {
		switch(type) {
		case SUPERGUN:
			return SUPERGUN_COST;
		case OMEGAGUN:
			return OMEGAGUN_COST;
		default:
			return 1;
		}
	}

	/**
	 * Kollar nuvarande vapen gentemot det som finns i shoppen
	 */
	private void getWeaponCheckInfo(final Weapons type, final int id) {
		mWeaponSwapTable.clear();
		mCostTable.clear();
		
	
		if (mPlayer.getInventory().gotWeapon()) {

			oldWeapon = WeaponStats.getWeaponStats(mPlayer.getInventory().weapon().getWeaponType());
			newWeapon = WeaponStats.getWeaponStats(type);
			
			wsc = WeaponStatComparer.getComapredStats(oldWeapon, newWeapon);

			Label weaponName = new Label(String.valueOf(type), namestyle);
			weaponName.setColor(Color.YELLOW);
			weaponName.setFontScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);	
			lbs.font.setScale(Camera.FONT_SCALE_X, Camera.FONT_SCALE_Y);
			
			Label dmg = new Label((wsc.mDmg > 0 ? "+" : "" ) + String.valueOf((int)(wsc.mDmg)), lbs);
			Color color = wsc.mDmg < 0 ? Color.RED : Color.GREEN;
			dmg.setColor(color);
			
			Label crit = new Label((wsc.mCrit > 0 ? "+" : "" ) + String.valueOf((int)(wsc.mCrit * 100)) + "%", lbs);
			color = wsc.mCrit < 0 ? Color.RED : Color.GREEN;
			crit.setColor(color);

			Label critmulti = new Label((wsc.mCritMultiplier > 0 ? "+" : "" ) + String.valueOf(wsc.mCritMultiplier) + "%", lbs);
			color = wsc.mCritMultiplier < 0 ? Color.RED : Color.GREEN;
			critmulti.setColor(color);
	
			mWeaponSwapTable.add(weaponName).align(Align.center).colspan(2).expandX().padBottom(0.1f * mCamera.getScale().y);
			mWeaponSwapTable.row();
			mWeaponSwapTable.add(dmg).padRight(0.2f * mCamera.getScale().x).align(Align.right);
			mWeaponSwapTable.add(mAttackLabel).align(Align.left).width(2.5f * mCamera.getScale().x);
			mWeaponSwapTable.row();
			mWeaponSwapTable.add(crit).padRight(0.2f * mCamera.getScale().x).align(Align.right);
			mWeaponSwapTable.add(mCritChanceLabel).align(Align.left);
			mWeaponSwapTable.row();
			mWeaponSwapTable.add(critmulti).padRight(0.2f * mCamera.getScale().x).align(Align.right);
			mWeaponSwapTable.add(mCritDmgLabel).align(Align.left);
			
			Label cost = new Label(String.valueOf(getWeaponCost(type)) + "$", lbs);
			cost.setFontScale(Camera.FONT_SCALE_X * 2f, Camera.FONT_SCALE_Y * 2f);
			cost.setColor(mPlayer.getInventory().getCoins() >= getWeaponCost(type) ? Color.GREEN : Color.RED);
			cost.setAlignment(Align.top, Align.center);
			
			mCostTable.add(cost).width(cost.getWidth()).height(cost.getHeight()).expand();	
			
			mStage.addActor(mWeaponSwapTable);
			mStage.addActor(mCostTable);
			
			// M�ste ha referenser till lyssnarna
			mBuyListener = new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if(tryBuyWeapon(type, id)) {
						System.out.println("Vapnet k�pt!");

						clearShowInfoButton();
						mIsGunBought = true;
						SoundManager.getInstance().playGunPickup();
					} else {
						System.out.println("Du har inte r�d");

						SoundManager.getInstance().playDenied();
					}
				};
			};
			
			mCloseListener = new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					clearShowInfoButton();
				};
			};
			// K�pknappen

			mBuyButton.addListener(mBuyListener);
			mBuyButton.setPosition(0.25f, 7f);
			
			// St�ngknappen
			mCloseButton.setPosition(1.5f, 7f);
			mCloseButton.addListener(mCloseListener);
			
			getStage().addActor(mBuyButton);
			getStage().addActor(mCloseButton);
		}
	}
	
	/**
	 * Ta bort k�pknapp och st�ngknapp

	 */
	private void clearShowInfoButton() {
		mDrawInfo = false;
		mStage.clear();
		mBuyButton.removeListener(mBuyListener);
		mCloseButton.removeListener(mCloseListener);
		getStage().getActors().removeValue(mBuyButton, true);
		getStage().getActors().removeValue(mCloseButton, true);
	}
	
	@Override
	public void show() {
		super.show();
		Gdx.input.setInputProcessor(getStage());
		
		// Font

		mFont = new BitmapFont(Gdx.files.internal("data/fonts/roto.fnt"), false);
		mUIFont = Resources.getInstance().font5x5;
		
		// Texturer
		mShopAtlas = Resources.getInstance().shop;
		
		mGunSelect = new AnimatedSprite(Resources.getInstance().gunSelect, "gunselect", 10);
		mGunSelect.setSize(1.5f, 1.5f);
		mGunSelect.play();
		mGunSelect.loop(true);
		mGunSelect.setAnimationRate(15);
		
		// Animation
		mGunBought = new AnimatedSprite(Resources.getInstance().coinPickup, "coinpickup", 6);
		mGunBought.setSize(1f, 1f);
		mGunBought.play();
		mGunBought.loop(false);
		mGunBought.setAnimationRate(10);
		
		// Knappar
		mButtonSkin = new Skin(Resources.getInstance().shopbuttons);
		ButtonStyle style = UIcomponents.BaseButtonStyle(mButtonSkin, "addbutton", "addbuttonpressed");
		mBuyButton = new Button(style);
		// mBuyButton.size(0.1f, 0.11f);
		mBuyButton.setSize(1f, 1f);
		
		// Labelstyle
		lbs = new LabelStyle(mFont, Color.WHITE);
		namestyle = new LabelStyle(mUIFont, Color.WHITE);
		
		mAttackLabel = new Label("attack damage", lbs);
		mCritChanceLabel = new Label("crit chance", lbs);
		mCritDmgLabel = new Label("crit damage", lbs);
		
		//Table
		mWeaponSwapTable = new Table();
		mWeaponSwapTable.setWidth(4.4f * mCamera.getScale().x);
		mWeaponSwapTable.setHeight(1.8f * mCamera.getScale().y);
		mWeaponSwapTable.setPosition(6.4f * mCamera.getScale().x - mWeaponSwapTable.getWidth() / 2, 0.2f * mCamera.getScale().y);
		
		mCostTable = new Table();
		mCostTable.debug();
		mCostTable.setWidth(2.4f * mCamera.getScale().x);
		mCostTable.setHeight(1.4f * mCamera.getScale().y);
		mCostTable.setPosition(9.6f * mCamera.getScale().x, 0.6f * mCamera.getScale().y);
		
		style = UIcomponents.BaseButtonStyle(mButtonSkin, "closebutton", "closebuttonpressed");
		mCloseButton = new Button(style);
		mCloseButton.setSize(1f, 1f);
		
		Skin leaveSkin = new Skin(Resources.getInstance().leave);
		style = UIcomponents.BaseButtonStyle(leaveSkin, "leave", "leavehover");
		style.over = leaveSkin.getDrawable("leavehover");
		mLeaveButton = new Button(style);
		mLeaveButton.setSize(2f, 2f);
		
		mBench = mShopAtlas.createSprite("bench");
		mWepInfo = mShopAtlas.createSprite("infobar");
		mBench.setSize(STAGE_WIDTH, STAGE_HEIGHT);
		mWepInfo.setSize(STAGE_WIDTH, STAGE_HEIGHT);
		
		
		// Bakgrund
		setBackgroundTexture(mShopAtlas.findRegion("bg"));
		
		getStage().addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.ESCAPE ||
						keycode == Keys.BACK) {
					back();
				}
				
				return true;
			}
		});
		

	}
	
	private void back() {
		// Ska eftertexterna rulla?
		if(mIsFromBoss) {
			if(mIsHost) {
				mGame.setScreen(new EndCreditScreen((Gunslinger) mGame, mServer, mClient, mCamera));
			} else {
				mGame.setScreen(new EndCreditScreen((Gunslinger) mGame, mClient, mCamera));
			}
		} else {
			if(mIsHost) {
				mGame.setScreen(new GameScreen((Gunslinger) mGame, mServer, mClient));
			} else {
				mGame.setScreen(new GameScreen((Gunslinger) mGame, mClient));
			}	
		}
	}
	
	/**
	 * Representerar shopmanagern
	 * @author Emil
	 *
	 */
	public class ClerkActor extends AbstractActor {
		public ClerkActor(TextureRegion textureRegion, boolean isTextureRegionActive,
				float posX, float posY, float width, float height) {
			super(textureRegion, isTextureRegionActive, posX, posY, width, height);
		}
		
		public ClerkActor(float posX, float posY, float width, float height) {
			super(posX, posY, width, height);
		}
	}

}
