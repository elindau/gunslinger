package se.rhel.gunslinger.screens;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.view.ForegroundRender;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.moribitotech.mtx.AbstractScreen;

/**
 * Screen f�r att Joina ett spel
 * Extendar AbstractScreen (mtx.jar)
 * @author Rickard
 *
 */
public class JoinGameScreen extends AbstractScreen {

	private static final String SCREEN_NAME = "Join Game Menu";

	private Skin 			mSkin;
	private TextField 		mIPTextField, mNameTextField;
	private TextButton 		mJoinButton;
	private BitmapFont 		mFont;
	private String 			mError = "";
	private Label 			mErrorLabel;
	private Table 			mTable;
	private float 			mErrorTime = 10f;
	private Pattern 		mNumberPattern = Pattern.compile("(^[0-9.]$)");
	private Pattern 		mNamePattern = Pattern.compile("(^[a-zA-Z0-9]$)");
	
	private ForegroundRender mForegroundRender;
	private BaseStage		mStage;
	private Camera			mCamera;
	private TweenManager	mManager;
	
	public JoinGameScreen(Gunslinger game, ForegroundRender fgr) {
		super(game, SCREEN_NAME);
		
		init(fgr);
	}
	
	public JoinGameScreen(Gunslinger game, String error, ForegroundRender fgr) {
		super(game, SCREEN_NAME);
		
		init(fgr);
		
		mError = error;
	}
	
	private void init(ForegroundRender fgr) {
		mStage = new BaseStage(getStage().getSpriteBatch());
		mCamera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		getStage().setViewport(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT, false);
		
		System.out.println("S�tter spritebatch");
		mForegroundRender = fgr;
		mForegroundRender.setSpriteBatch(getStage().getSpriteBatch());
		mManager = fgr.getManager();
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		if (!mError.isEmpty()) {
			if ((mErrorTime -= delta) <= 0f) {
				mErrorLabel.setVisible(false);
				mError = "";
			}
		}
		
		mManager.update(delta);
		
		getStage().getSpriteBatch().begin();
		
		mForegroundRender.draw(delta);	
		mStage.draw();
		
		getStage().getSpriteBatch().end();
	}
	
	@Override
	public void resize(int width, int height) {
		
		//H�mta ut styles
		TextButtonStyle tbs = UIcomponents.BaseTextButtonStyle(mSkin, mFont, Color.WHITE, "button", "buttonpressed");
		Skin textFieldSkin = new Skin(Resources.getInstance().tiledBgsAtlas);
		
		TextFieldStyle tfs = new TextFieldStyle();
		tfs.font = mFont;
		tfs.fontColor = Color.WHITE;
		tfs.background = textFieldSkin.getDrawable("black");
		
		LabelStyle lbs = UIcomponents.LabelStyle(mFont, Color.BLACK);
		
		//S�tt upp textfilters
		TextFieldFilter tff = new TextFieldFilter() {
			@Override
			public boolean acceptChar(TextField textField, char key) {
				Matcher matcher = mNumberPattern.matcher(Character.toString(key));
				return matcher.find();
			}
		};
		
		TextFieldFilter tffName = new TextFieldFilter() {
			@Override
			public boolean acceptChar(TextField textField, char key) {
				Matcher matcher = mNamePattern.matcher(Character.toString(key));
				return matcher.find();
			}
		};

		//Skapa upp textfields
		mIPTextField = UIcomponents.TextField("", tfs, 15, tff);
		mNameTextField = UIcomponents.TextField("", tfs, 10, tffName);
		
		//Skapa upp knapp
		mJoinButton = new TextButton("Join", tbs);
		mJoinButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
				if (mIPTextField.getText().trim().isEmpty())
					getGame().setScreen(new JoinGameScreen((Gunslinger)getGame(), "IP can't be empty", mForegroundRender));
				else if (mNameTextField.getText().trim().isEmpty())
					getGame().setScreen(new JoinGameScreen((Gunslinger)getGame(), "Name can't be empty", mForegroundRender));
				else
					getGame().setScreen(new LobbyScreen((Gunslinger)getGame(), mNameTextField.getText(), mIPTextField.getText(), mForegroundRender));
			}		
		});
		
		TextButton backButton = new TextButton("Back", tbs);
		backButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});
		
		if (!mError.isEmpty()) {
			mErrorLabel = new Label(mError, lbs);
			mStage.addActor(mErrorLabel);
		}
		
		
		//Table f�r layout
		mTable = new Table();
		mTable.setPosition(Gdx.graphics.getWidth() / 2, 4f * mCamera.getScale().y);

		mTable.add(new Label("Name: ", lbs)).right();
		mTable.add(mNameTextField).width(250);
		mTable.row().padTop(10);
		mTable.add(new Label("IP: ", lbs)).right();
		mTable.add(mIPTextField).width(250);
		mTable.row().padTop(10);
		mTable.add(mJoinButton).width(256).height(64).colspan(2).center();
		mTable.row().padTop(10);
		mTable.add(backButton).width(256).height(64).colspan(2).center();
		
		mStage.addActor(mTable);
	}
	
	private void back() {
		getGame().setScreen(new MultiplayerScreen((Gunslinger)getGame(), mForegroundRender));
	}

	@Override
	public void show() {
		mSkin = new Skin(Resources.getInstance().menuAtlas);
		mFont = Resources.getInstance().font;
		
		Texture txt = Resources.getInstance().logo;
		TextureRegion region = new TextureRegion(txt, txt.getWidth(), txt.getHeight());		
		setBackgroundTexture(region);
		
		Gdx.input.setInputProcessor(mStage);
		Gdx.input.setCatchBackKey(true);
		
		getStage().addListener(new InputListener() {
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.BACK) {
					back();
				}
				return true;
			}
		});
	}

	@Override
	public void hide() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		super.dispose();
		mSkin.dispose();
		mFont.dispose();
	}
}
