package se.rhel.gunslinger.screens;


import se.rhel.gunslinger.Gunslinger;
import se.rhel.gunslinger.Resources;
import se.rhel.gunslinger.controller.WorldController;
import se.rhel.gunslinger.model.Camera;
import se.rhel.gunslinger.model.CameraLookAt;
import se.rhel.gunslinger.model.ClientWorld;
import se.rhel.gunslinger.model.Levels;
import se.rhel.gunslinger.model.entities.Player;
import se.rhel.gunslinger.model.entities.Player.State;
import se.rhel.gunslinger.model.options.GameState;
import se.rhel.gunslinger.model.options.GameState.GameStatus;
import se.rhel.gunslinger.model.options.Options;
import se.rhel.gunslinger.model.weapons.Gun;
import se.rhel.gunslinger.model.weapons.SuperGun;
import se.rhel.gunslinger.model.weapons.WeaponCreator;
import se.rhel.gunslinger.network.KryoClient;
import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.observers.SoundObserver;
import se.rhel.gunslinger.observers.StageListener;
import se.rhel.gunslinger.observers.StageObserver;
import se.rhel.gunslinger.tweens.CameraAccessor;
import se.rhel.gunslinger.view.SoundManager;
import se.rhel.gunslinger.view.WorldRender;
import se.rhel.gunslinger.view.SoundManager.MusicType;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Json;

/**
 * Ut�ngsscreen
 * Extendar BaseScreen som implementerar Screen
 * samt Input som hanterar just input
 * @author Emil
 *
 */
public class GameScreen extends BaseScreen implements StageListener {
	
	@SuppressWarnings("unused") // Used for screen changing
	private Gunslinger			mGame;
	
	private ClientWorld 		mWorldModel;
	private WorldRender 		mWorldRender;
	private WorldController 	mWorldController;
	
	private Camera 				mCamera;
	
	private int 				mWidth, mHeight;
	
	private TweenManager 		mTweenManager;
	private CameraLookAt 		mCameraLookAt;
	
	private boolean 			mFirst = true;
	private boolean				mIsTweening = false;
	private boolean				mPlayerLock;
	
	//Multiplayer
	private KryoServer 			mServer;
	private KryoClient 			mClient;
	private boolean 			isHost;
	
	// Observer
	private StageObserver		mStageObserver;
	
	// Status p� spelet
	private GameStatus			GAME_STATUS;
	
	private Stage				mPauseStage;

	private boolean mBossTweenDone = false;
	
	// Test
	// ChangeScreenHelper csh = new ChangeScreenHelper();
	
	//Multiplayer konstruktorer
	public GameScreen(Gunslinger game, KryoServer server, KryoClient client) {
		mGame = game;
		mServer = server;
		mClient = client;
		isHost = true;
		mCamera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mCamera.setToOrtho(false, Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT);
	}
	
	public GameScreen(Gunslinger game, KryoClient client) {
		mGame = game;
		mClient = client;
		isHost = false;
		mCamera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mCamera.setToOrtho(false, Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT);
	}
	
	@Override
	public void draw(float delta) {
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		// Gdx.gl.glEnable(GL10.GL_BLEND);
		// Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		// mCamera.unproject(new Vector3(0,0,0));
		if (mWorldModel.getClient() != null) {
			mWorldRender.draw(delta);
		}
		
		// N�r spelet pausas
		if(GAME_STATUS == GameStatus.PAUSED) {
			mPauseStage.draw();
		}
		
		
		// csh.changeScreen();
		// csh.draw();
	}

	@Override
	public void update(float delta) {
		if(isHost) {
			mServer.update(delta);
		} else {
			if (mWorldModel.getClient() == null) {
				mGame.setScreen(new JoinGameScreen(mGame, "Disconnected", mWorldRender.getForegroundRender()));
				System.out.println("�ndrar till disconnected");
				return;
			}
		}
		
		// System.out.println(mPlayerLock);
		// Uppdatera l�set p� spelaren
		mWorldModel.player().setLock(mPlayerLock);		

		updateCamera(delta);
		
		// Update tweenmanager
		mTweenManager.update(delta);
		
		mWorldController.processCurrentInput();
		
		// Hantera bara input om spelarl�set �r oaktivt
		mWorldModel.update(delta);
		
		// �r det dags att byta screen
		if(mWorldModel.isItTimeToChangeScreen()) {
			if(mWorldModel.getToScreen() == Screens.CHOOSE) {
				// H�mta ut levlen som just spelats
				Levels levelJustPlayed = mWorldModel.levelManager().getCurrentLevel().name();
				
				mClient.reset();
				
				if(isHost) {
					mWorldModel.setIsItTimeToChangeScreen(false);
					// Spara undan staten
					saveState();
					mGame.setScreen(new ChooseLevelScreen(mGame, mServer, mClient, levelJustPlayed));
				} else {
					mWorldModel.setIsItTimeToChangeScreen(false);
					mGame.setScreen(new ChooseLevelScreen(mGame, mClient, levelJustPlayed));
				}
			} else if(mWorldModel.getToScreen() == Screens.CUTSCENE) {
				if(isHost) {
					mWorldModel.setIsItTimeToChangeScreen(false);
					mGame.setScreen(new CutScenesScreen(mGame, mServer, mClient, mCamera), true);
				} else {
					mWorldModel.setIsItTimeToChangeScreen(false);
					mGame.setScreen(new CutScenesScreen(mGame, mClient, mCamera), true);
				}
			} 
		}
	}
	
	public Camera getCamera() {
		return mCamera;
	}
	
	/**
	 * Uppdatera kameran
	 * @param delta
	 */
	private void updateCamera(float delta) {
		
		float stageX = mWorldModel.levelManager().getCurrentLevel().getCurrentStage().getPosition().x;
		float stageLength = mWorldModel.levelManager().getCurrentLevel().getCurrentStage().length();
		
		if(mFirst) {
			// Om CameraLookAt sl�par lite ska den ist�llet tweena till spelaren f�r mer mjuk r�relse
				if(!mIsTweening) {
					if(mWorldModel.player().state() == Player.State.SPAWNING) {
						mIsTweening = true;
						mBossTweenDone = false;
						Tween.to(mCameraLookAt, CameraAccessor.POSITION_X, 0.5f)
							.target(stageX + Camera.CAMERA_WIDTH / 2f)
							.setCallback(cb)
							.delay(0.0f).start(mTweenManager);
					}
					else if (mWorldModel.isBossTime() && mBossTweenDone == false) {
						mBossTweenDone = true;
						mPlayerLock = true;
						mIsTweening = true;
						Tween.to(mCameraLookAt, CameraAccessor.POSITION_X, 1f)
							.target(stageLength - Camera.CAMERA_WIDTH / 2f)
							.setCallback(cb)
							.delay(0.0f).start(mTweenManager);
					}
					else {
						// Kameran ska f�lja spelaren
						if (mWorldModel.isBossTime()) {
							mCameraLookAt.setPosition(new Vector2((stageX + stageLength) - Camera.CAMERA_WIDTH / 2f, mWorldModel.player().getPosition().y));
						} else {
							//mCameraLookAt.setPosition(new Vector2(mWorldModel.player().getPosition().x, mCameraLookAt.position().y));
							mCameraLookAt.setPosition(new Vector2(mWorldModel.player().getPosition().x, mWorldModel.player().getPosition().y));
						}
					}
				}
				
				// .. Men bara om kameran inte krockar med kanten
				if(mCameraLookAt.position().x < stageX + Camera.CAMERA_WIDTH / 2f) {
					mCameraLookAt.setPosition(new Vector2(stageX + Camera.CAMERA_WIDTH / 2f, mCameraLookAt.position().y));
				} else if(mCameraLookAt.position().x > (stageX + stageLength) - Camera.CAMERA_WIDTH / 2f) {
					mCameraLookAt.setPosition(new Vector2((stageX + stageLength) - Camera.CAMERA_WIDTH / 2f, mCameraLookAt.position().y));
				}
				
				//System.out.println(mCameraLookAt.position().y);
				if (mCameraLookAt.position().y > Camera.CAMERA_HEIGHT / 2 + 2f) {
					mCameraLookAt.setPosition(new Vector2(mCameraLookAt.position().x, Camera.CAMERA_HEIGHT / 2 + 2f));
				} else if(mCameraLookAt.position().y < Camera.CAMERA_HEIGHT / 2f) {
					mCameraLookAt.setPosition(new Vector2(mCameraLookAt.position().x, Camera.CAMERA_HEIGHT / 2));
				}
		}
		
		// Displacement
		mCamera.setDisplacement(new Vector2(mCameraLookAt.position().cpy().x - Camera.CAMERA_WIDTH / 2, mCameraLookAt.position().y - Camera.CAMERA_HEIGHT / 2));
		
		mCamera.position.set(mCameraLookAt.position().x, mCameraLookAt.position().y, 0);
		
		mCamera.update();
	}

	
	// Callback f�r tweening
	private final TweenCallback cb = new TweenCallback() {
		@Override
		public void onEvent(int arg0, BaseTween<?> arg1) {
			mFirst = true;
			mPlayerLock = false;
			mIsTweening = false;
		}
	};

	@Override
	public void resize(int width, int height) {
		
		// Alternativ 1
		// mCamera.setToOrtho(false, width / PIXELS_PER_METER, height / PIXELS_PER_METER);
		// Alternativ 2

		// What the camera is supposed to look at - embedded tweening

		mCameraLookAt.setPosition(mCamera.position.cpy());
		

		mWidth = width;
		mHeight = height;
	}

	@Override
	public void show() {	
		GAME_STATUS = GameStatus.RUNNING;
		
		mWorldModel = mClient.getWorldModel();
		
		mStageObserver = new StageObserver();
		mStageObserver.addStageListener(this);
		

		mWorldModel.setStageObservers(mStageObserver);
		
		

		mCameraLookAt = new CameraLookAt(mCamera.position.cpy());
		SoundObserver soundObserver = new SoundObserver();
		for(Player p : mWorldModel.getPlayersArray()) {
			p.setObserver(soundObserver);
		}
		mWorldRender = new WorldRender(mWorldModel, mCamera, mCameraLookAt, soundObserver);
		mWorldController = new WorldController(mWorldModel, mCamera, this);
		
		Options opt = new Options();
		SoundManager.getInstance().playMusic(true, opt.getMusicVolume(), MusicType.GAMETHEME);
		
		// Tweening
		mTweenManager = new TweenManager();
		Tween.registerAccessor(CameraLookAt.class, new CameraAccessor());

		// Make this the input processor
		Gdx.input.setInputProcessor(mWorldController);
		
		// Override 
		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		pauseGame();

		// Spara undan nuvarande state i spelet
		saveState();
		SoundManager.getInstance().pauseMusic(MusicType.GAMETHEME);
	}

	@Override
	public void resume() {
		Options opt = new Options();
		
		if (!SoundManager.getInstance().isPlaying(MusicType.GAMETHEME)) 
			SoundManager.getInstance().playMusic(true, opt.getMusicVolume(), MusicType.GAMETHEME);
		
		resumeGame();
	}

	@Override
	public void dispose() {
		// Dags att spara undan, woop woop!
		// saveState();
		SoundManager.getInstance().dispose();
		Gdx.input.setInputProcessor(null);
	}
	
	/**
	 * Anv�ndas f�r att spara nuvarande state p� spelet
	 * ner p� fil data/state/gamestate.json
	 */
	private void saveState() {
		// Men bara om man �r host
		if(isHost) {
			// Spara state
			Resources.getInstance().gameState.modifyBourbon(mWorldModel.player().getInventory().getCurrentBourbons());
			boolean gotgun = mWorldModel.player().getInventory().gotWeapon();
			Resources.getInstance().gameState.modifyCoins(mWorldModel.player().getInventory().getCoins());
			Resources.getInstance().gameState.setGun(gotgun);
			if(gotgun) {
				Resources.getInstance().gameState.setGunType(mWorldModel.player().getInventory().weapon().getWeaponType());	
			}
			// Level-state sparas n�r man klarar av banan
			// Resources.getInstance().gameState.newLevelCleared();
			
			// Skriv till fil
			FileHandle file = Gdx.files.local(GameState.PATH);
			Json json = new Json();
			json.toJson(Resources.getInstance().gameState, GameState.class, file);	
		}
	}

	@Override
	public void stageChanged() {
		float stageX = mWorldModel.levelManager().getCurrentLevel().getCurrentStage().getPosition().x;
		float stageLength = mWorldModel.levelManager().getCurrentLevel().getCurrentStage().length();
		
		// Tweening till n�sta Stage �r en viktig del och det �r relativt mycket som sker
		if(mWorldModel.player().getPosition().x + mWorldModel.player().getSize().x >= stageX + stageLength) {
			if(mFirst) {
				// H�r ska vi l�sa spelaren r�relse lite s� han inte vandrar iv�g
				mFirst = false;
				mPlayerLock = true;
				mCamera.setDisplacement(new Vector2(Camera.CAMERA_WIDTH, mCamera.position.y));
				
				// Bara byt stage
				mWorldModel.levelManager().switchStage();
				
				// Flytta spelaren till n�sta stage
				mWorldModel.player().moveTo(new Vector2(mWorldModel.player().getPosition().x + 0.1f, mWorldModel.player().getPosition().y), true);
				
				// D�refter tweenar vi
				Tween.to(mCameraLookAt, CameraAccessor.POSITION_X, 1.0f)
					.target(mCameraLookAt.position().cpy().x + Camera.CAMERA_WIDTH)
					.delay(0.0f).start(mTweenManager)
					.setCallback(cb)
					.setCallbackTriggers(TweenCallback.COMPLETE);
			}
		}
		mWorldRender.stageChanged();
	}

	@Override
	public void levelChanged() {}
	
	/**
	 * Instansierar Pause-stagen
	 */
	private void initPauseStage() {
		
		Table table = new Table();
		table.setFillParent(true);
		
		mPauseStage = new Stage(mWidth, mHeight, true);
		mPauseStage.addActor(table);
		
		Skin buttonSkin = new Skin(Resources.getInstance().menuAtlas);
		BitmapFont font = Resources.getInstance().font;
		font.setScale(1f, 1f);
		TextButtonStyle style = UIcomponents.BaseTextButtonStyle(buttonSkin, font, Color.WHITE, "button", "buttonpressed");
		
		// Quit, resume, options
		TextButton quitButton = UIcomponents.button("Main menu", style, new Rectangle(Gdx.graphics.getWidth() / 2 - 256 / 2, 0, 256, 64));
		TextButton resumeButton = UIcomponents.button("Resume game", style, new Rectangle(Gdx.graphics.getWidth() / 2 - 256 / 2, 96, 256, 64));
		TextButton optionsButton = UIcomponents.button("Options", style, new Rectangle(Gdx.graphics.getWidth() / 2 - 256 / 2, 192, 256, 64));
		
		TextButton shopButton = UIcomponents.button("Shop", style, new Rectangle(Gdx.graphics.getWidth() / 2 - 256 / 2, 192, 256, 64));
		
		shopButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(isHost) {
					mGame.setScreen(new ShopScreen(mGame, mServer, mClient, mCamera, false));
				} else {
					mGame.setScreen(new ShopScreen(mGame, mClient, mCamera, false));
				}
			}
		});
		
		quitButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(isHost) {
					mServer.shutdown();
				} 
				if(mClient != null) {
					mClient.shutdown();
				}
				SoundManager.getInstance().stopMusic(MusicType.GAMETHEME);
				Gdx.input.setInputProcessor(null);
				Gdx.input.setInputProcessor(null);
				mGame.setScreen(new MainMenu(mGame));
				// Gdx.app.exit();
				// Borde f�rmodligen restarta ChooseLevelScreen eller MainMenu
				// MainMenu borde vara enklare men chooselevelscreen g�r kanske mer sense
			}
		});
		
		// Resume, inga konstigheter
		resumeButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				resumeGame();
			}
		});
		
		// Ska starta options-screenen
		optionsButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				mPauseStage = new Stage();
				Table table = OptionsScreen.getOptionsTable();
				
				TextButton button;
				Skin buttonSkin = new Skin(Resources.getInstance().menuAtlas);
				
				TextButtonStyle bStyle = new TextButtonStyle();
				bStyle.font = Resources.getInstance().font;
				bStyle.fontColor = Color.WHITE;
				bStyle.up = buttonSkin.getDrawable("button");
				bStyle.down = buttonSkin.getDrawable("buttonpressed");
				
				button = new TextButton("Back", bStyle);
				button.setBounds(Gdx.graphics.getWidth() / 2 - OptionsScreen.CHECKBOX_WIDTH / 2, Gdx.graphics.getHeight() / 2 - 64 * 3, OptionsScreen.CHECKBOX_WIDTH, OptionsScreen.CHECKBOX_HEIGHT);
				
				button.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						pauseGame();
					}
				});
				
				table.row().padTop(10);
				table.add(button).left();
				
				mPauseStage.addActor(table); 
				Gdx.input.setInputProcessor(mPauseStage);
				Gdx.input.setCatchBackKey(true);
				// mGame.setScreen(new OptionsScreen(mGame));
			}
		});
		
		table.add(resumeButton);
		table.row().padTop(10);
		table.add(optionsButton);
		table.row().padTop(10);
		table.add(quitButton);
		table.row().padTop(10);
		table.add(shopButton);
		
		Skin bgSkin = new Skin(Resources.getInstance().tiledBgsAtlas);
		table.setBackground(bgSkin.getDrawable("black"));
	}
	
	public void resumeGame() {
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setInputProcessor(mWorldController);
		GAME_STATUS = GameStatus.RUNNING;
	}
	
	// N�r spelet ska pausas
	public void pauseGame() {
		initPauseStage();
		Gdx.input.setInputProcessor(mPauseStage);
		// Gdx.input.setCatchBackKey(false);
		GAME_STATUS = GameStatus.PAUSED;
	}
}
