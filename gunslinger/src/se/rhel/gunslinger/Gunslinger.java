package se.rhel.gunslinger;

import se.rhel.gunslinger.network.KryoServer;
import se.rhel.gunslinger.observers.ScreenChangeListener;
import se.rhel.gunslinger.observers.ScreenChangeObserver;
import se.rhel.gunslinger.screens.ChangeScreenHelper;
import se.rhel.gunslinger.screens.Screens;
import se.rhel.gunslinger.screens.SplashScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Gunslinger extends Game implements ScreenChangeListener {
	
	private KryoServer			mServer;
	private ChangeScreenHelper 	mScreenChanger;
	
	private Screen				mToScreen;
	
	@Override
	public void create() {
		ScreenChangeObserver obs = new ScreenChangeObserver();
		obs.addListener(this);
		mScreenChanger = new ChangeScreenHelper(obs);
		
		setScreen(new SplashScreen(this));
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		if(mServer != null)
			mServer.shutdown();
	}
	
	public void setServer(KryoServer server) {
		mServer = server;
	}
	
	@Override
	public void render() {
		super.render();
		
		mScreenChanger.draw();
	}
	
	public void setScreen(Screen screen, boolean smoothTransition) {
		// Byt screen
		if(smoothTransition) {
			mScreenChanger.changeScreen();
			mToScreen = screen;	
		} else {
			super.setScreen(mToScreen);
		}
	}

	@Override
	public void screenChange(Screens toScreen) {}

	@Override
	public void screenChanged() {
		// Nu �r det dags att byta screen
		if(mToScreen != null) {
			super.setScreen(mToScreen);
		}
	}
}
